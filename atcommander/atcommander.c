/*
 * atcommander.c
 *
 *  Created on: 2 апр. 2018 г.
 *      Author: Denis Shreiber
 */

#include <string.h>
#include <stdio.h>

#include "atcommander.h"

//=======================================================================

#define ENABLE_LOGOUT	0

#if ENABLE_LOGOUT
	#define LOGOUT(...)			handlers->logout(commander, __VA_ARGS__);
#else
	#define LOGOUT(...)
#endif

#define CHECK_TIMEOUT()		do {							\
								if (private->timeout) {		\
									goto exit;				\
								}							\
							} while(0);

#define CHECK_STRUCT_PTRS() 		do {																\
										if (!private->struct_ptrs_ok) {									\
											if (handlers->message_handler == NULL ||					\
													handlers->rx_it_start == NULL ||					\
													handlers->rx_it_stop == NULL ||						\
													handlers->timer_start == NULL ||					\
													handlers->timer_stop == NULL ||						\
													handlers->tx_dma == NULL ||							\
													handlers->user_rx_byte == NULL ||					\
													handlers->wait == NULL ||							\
													commander->sett.buff.rx_buff_len == 0 ||			\
													commander->sett.buff.response_buff == NULL ||		\
													commander->sett.buff.rx_buff == NULL ||				\
													commander->private.rx_byte == NULL)					\
											{															\
												return -1;												\
											}															\
											else {														\
												private->struct_ptrs_ok = true;							\
											}															\
										}																\
									} while(0);

//=======================================================================

static void reset_rx_state(ATCommander_private_t *private) {
	private->rx_cnt = 0;
	private->rx_state = ATCOMMANDER_RXSTATE_READY;
}

/**
 * Копирует @ref rx_buff в @ref response_buff и раскладывает данные на отдельные сообщения @ref ATCommander_response_t, разделенными \r\n
 * Сами \r\n выкидывает, оставляя только данные.
 * @param commander
 * @return
 */
static int parse_response(ATCommander_t *commander) {
	ATCommander_response_t *response;

	char *ptr = commander->sett.buff.response_buff;

	for (uint8_t q = 0; q < commander->request.expected_responses_num; q++) {
		response = &commander->response[q];

		ptr = strstr(ptr, "\r\n");	// Начало респонса
		if (ptr) {
			ptr += 2;
			response->data = ptr;

			ptr = strstr(ptr, "\r\n");	// Конец респонса
			if (ptr) {
				response->len = ptr - response->data;
				*ptr = '\0';
				ptr += 2;
			}
			else {
				return -1;
			}
		}
		else {
			return -1;
		}
	}

	return 0;
}

static bool is_timeout(ATCommander_t *commander, uint32_t start_time, uint32_t timeout_ms) {
	if (commander->sett.handlers.get_ms() - start_time > timeout_ms) {
		ATCommander_timeout_handler(commander);
		return true;
	}

	return false;
}

//=======================================================================

int ATCommander_send(ATCommander_t *commander, uint32_t timeout_ms) {
	ATCommander_private_t *private = &commander->private;
	struct ATCommander_handlers_s *handlers = &commander->sett.handlers;
	ATCommander_request_t *request = &commander->request;

	CHECK_STRUCT_PTRS();

	int ret = -1;

	private->timeout = false;
//	handlers->timer_start(timeout_ms);

	uint32_t start_time = commander->sett.handlers.get_ms();

	// Ждем, пока закончится предыдущий приём
	while (private->rx_state != ATCOMMANDER_RXSTATE_READY) {
		LOGOUT("\r\nrx_state %d\r\n", (int)private->rx_state);
		handlers->wait();
		if (is_timeout(commander, start_time, timeout_ms)) {
			goto exit;
		}
	}

	private->expected_responses_num_received = 0;
	private->echo_received = false;
	private->response_received = false;
	private->tx_completed = false;
	private->response_wait = true;
	commander->sett.buff.response_buff[0] = '\0';

	if (handlers->tx_dma(request->cmd, request->cmd_len) != 0) {
		goto exit;
	}

	while (!private->tx_completed) {
		handlers->wait();
		if (is_timeout(commander, start_time, timeout_ms)) {
			goto exit;
		}
	}

	while (!private->echo_received) {
		handlers->wait();
		if (is_timeout(commander, start_time, timeout_ms)) {
			goto exit;
		}
	}

	if (request->expected_responses_num) {
		while (!private->response_received) {
			handlers->wait();
			if (is_timeout(commander, start_time, timeout_ms)) {
				goto exit;
			}
		}
	}

	ret = 0;

	exit:

	handlers->timer_stop();

	// Далее ищем нужное количество респонсов в ответе
	if (ret == 0) {
		if (parse_response(commander) != 0) {
			ret = -1;
		}
	}

	reset_rx_state(private);

	return ret;
}

void ATCommander_rx_byte_handler(ATCommander_t *commander) {
	ATCommander_private_t *private = &commander->private;
	ATCommander_request_t *request = &commander->request;
	struct ATCommander_handlers_s *handlers = &commander->sett.handlers;
	struct ATCommander_buff_s *buff = &commander->sett.buff;

	if (*private->rx_byte == '\r') {
		LOGOUT("<%d>", private->rx_state);
	}

	// Переполнение буфера
	if (private->rx_cnt > buff->rx_buff_len - 1) {
		reset_rx_state(private);
		buff->rx_buff[0] = '\0';
		buff->response_buff[0] = '\0';
		LOGOUT("\r\n\n---OVERFLOW---\r\n\n");
		goto exit;
	}

	buff->rx_buff[private->rx_cnt++] = *private->rx_byte;

	ATCommander_user_rx_byte_ret_code_t ret = handlers->user_rx_byte(commander);
	if (ret == ATCOMMANDER_USER_RX_RET_CODE_QUIT) {
		goto exit;
	}

	switch (private->rx_state) {
	case ATCOMMANDER_RXSTATE_READY: {
		if (*private->rx_byte == '\r') {
			private->rx_state = ATCOMMANDER_RXSTATE_MSG_START;
		}
		// Далее проверяем на '\n' и '+' на случай, если вдруг '\r' пропустится
		else if (*private->rx_byte == '\n') {
			buff->rx_buff[0] = '\r';
			buff->rx_buff[1] = '\n';
			private->rx_cnt = 2;
			private->rx_state = ATCOMMANDER_RXSTATE_MSG_RECEIVE;
			LOGOUT("\r\n\n---WARNING 1 '%c'---\r\n\n", *private->rx_byte);
		}
		else if (*private->rx_byte == '+') {
			buff->rx_buff[0] = '\r';
			buff->rx_buff[1] = '\n';
			buff->rx_buff[2] = '+';
			private->rx_cnt = 3;
			private->rx_state = ATCOMMANDER_RXSTATE_MSG_RECEIVE;
			LOGOUT("\r\n\n---WARNING 2 '%c'---\r\n\n", *private->rx_byte);
		}
		else {
			if ((*private->rx_byte == request->cmd[0]) && private->response_wait/* && request->cmd_len*/) {
				private->rx_state = ATCOMMANDER_RXSTATE_ECHO;
			}
			else {
				/// @todo Это не эхо, а ошибка
				LOGOUT("\r\n\n---RXSTATE_READY '%c'---\r\n\n", *private->rx_byte);
				buff->rx_buff[private->rx_cnt] = '\0';
				reset_rx_state(private);
			}
		}
	}
	break;

	case ATCOMMANDER_RXSTATE_ECHO: {
		if (*private->rx_byte == request->cmd[private->rx_cnt - 1]) {
			if (private->rx_cnt == request->cmd_len) {
				// Эхо закончилось
				private->echo_received = true;
//				if (request->expected_responses_num) {
//					private->response_wait = true;
//				}

				reset_rx_state(private);
			}
		}
		else {
			/// @todo Это не эхо, а ошибка
			LOGOUT("\r\n\n---RXSTATE_ECHO (%s) '%c'---\r\n\n", request->cmd, *private->rx_byte);
			reset_rx_state(private);
		}
	}
	break;

	case ATCOMMANDER_RXSTATE_MSG_START: {
		if (*private->rx_byte == '\n') {
			private->rx_state = ATCOMMANDER_RXSTATE_MSG_RECEIVE;
		}
		else {
			LOGOUT("\r\n\n---RXSTATE_MSG_START '%c'---\r\n\n", *private->rx_byte);
			reset_rx_state(private);
		}
	}
	break;

	case ATCOMMANDER_RXSTATE_MSG_RECEIVE: {
		if (*private->rx_byte == '\r') {
			// Пришло сообщение. Ждем \n
			private->rx_state = ATCOMMANDER_RXSTATE_MSG_END;
		}
	}
	break;

	case ATCOMMANDER_RXSTATE_MSG_END: {
		if (*private->rx_byte == '\n') {
			char * msg = buff->rx_buff + 2;
			buff->rx_buff[private->rx_cnt] = '\0';

			if ( private->response_wait &&
					( (request->expected_data && memcmp(msg, request->expected_data, request->expected_data_len) == 0 ) ||
					memcmp(msg, "OK", strlen("OK")) == 0 ||
					memcmp(msg, "ERROR", strlen("ERROR")) == 0 ||
					memcmp(msg, "+CME", strlen("+CME")) == 0 ||
					memcmp(msg, "+CMS", strlen("+CMS")) == 0) ) {

				size_t max_cat_size = buff->rx_buff_len - strlen(buff->response_buff) - 1;
				if (max_cat_size < 10) {
					LOGOUT("\r\n\n---TOO LONG\r\n\n---");
				}
				strncat(buff->response_buff, buff->rx_buff, max_cat_size);

				if (++private->expected_responses_num_received == request->expected_responses_num) {
					private->expected_responses_num_received = 0;
					private->response_received = true;
					private->response_wait = false;
				}
			}
			else {
				handlers->message_handler(commander);
			}

			private->rx_cnt = 0;
		}
		else {
			LOGOUT("\r\n\n---RXSTATE_MSG_END '%c'---\r\n\n", *private->rx_byte);
			private->rx_cnt = 0;
		}

		private->rx_state = ATCOMMANDER_RXSTATE_READY;
	}
	break;
	}

	exit:
	handlers->rx_it_start(private->rx_byte);
}

void ATCommander_tx_complete_handler(ATCommander_t *commander) {
	commander->private.tx_completed = true;
}

void ATCommander_timeout_handler(ATCommander_t *commander) {
	ATCommander_private_t *private = &commander->private;
	struct ATCommander_handlers_s *handlers = &commander->sett.handlers;

	LOGOUT("\r\n\n---TIMEOUT---\r\n\n");

	private->timeout = true;

	private->echo_received = false;
	private->response_received = false;
	private->tx_completed = false;
	private->response_wait = false;
	private->rx_cnt = 0;
	private->expected_responses_num_received = 0;
	commander->sett.buff.response_buff[0] = '\0';
	private->rx_state = ATCOMMANDER_RXSTATE_READY;

	handlers->rx_it_stop();
	handlers->rx_it_start(private->rx_byte);
}

void ATCommander_reset_state(ATCommander_t *commander) {
	ATCommander_private_t *private = &commander->private;
	ATCommander_sett_t *sett = &commander->sett;

	sett->handlers.rx_it_stop();
	sett->handlers.timer_stop();

	private->tx_completed = false,
	private->echo_received = false,
	private->response_received = false,
	private->response_wait = false,
	private->timeout = false,
	private->rx_state = ATCOMMANDER_RXSTATE_READY,
	private->rx_cnt = false,
	private->expected_responses_num_received = 0,
	private->struct_ptrs_ok = false,
	private->busy = false,

	sett->buff.response_buff[0] = '\0';
	sett->buff.rx_buff[0] = '\0';
}


