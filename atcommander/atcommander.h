/*
 * atcommander.h
 *
 *  Created on: 2 апр. 2018 г.
 *      Author: Denis Shreiber
 */

#ifndef ATCOMMANDER_H_
#define ATCOMMANDER_H_

#include <stdint.h>
#include <stdbool.h>

//=======================================================================

#define ATCOMMANDER_VERSION		"1.0.0"

//=======================================================================

/// Возвращаемые значения пользовательского хэндлера
typedef enum {
	ATCOMMANDER_USER_RX_RET_CODE_NORMAL,	/// Пользовательский хэндлер не выполнял специальных операций, разрешается выполнение встроенного хэндлера
	ATCOMMANDER_USER_RX_RET_CODE_QUIT,		/// Пользовательский хэндлер выполнил операции, запрещающие выполнение встроенного хэндлера
}ATCommander_user_rx_byte_ret_code_t;

typedef int (*ATCommander_tx_dma)(const void *data, uint32_t len);
typedef ATCommander_user_rx_byte_ret_code_t (*ATCommander_user_rx_byte)(void *commander);
typedef int (*ATCommander_timer_start)(uint32_t timeout_ms);
typedef int (*ATCommander_timer_stop)();
typedef int (*ATCommander_rx_it_start)(char *rx_byte);
typedef int (*ATCommander_rx_it_stop)();
typedef void (*ATCommander_wait)();
typedef unsigned long (*ATCommander_get_ms)(void);
typedef void (*ATCommander_message_handler)(void *commander);
typedef void (*ATCommander_logout)(void *commander, const char* format, ...);

/// Состояние приема
typedef enum {
	ATCOMMANDER_RXSTATE_READY,			/// Готов к приёму
	ATCOMMANDER_RXSTATE_ECHO,			/// Приниает эхо
	ATCOMMANDER_RXSTATE_MSG_START,		/// Начало приема сообщения (принял символ '\r', ожидает '\n')
	ATCOMMANDER_RXSTATE_MSG_RECEIVE,	/// Прием сообщения
	ATCOMMANDER_RXSTATE_MSG_END,		/// Конец приема сообщения (принял символ '\r', ожидает '\n')
}ATCommander_rx_state_t;

/// Структура приватных данных модуля
typedef struct {
	volatile bool busy;							/// Флаг занятости
	volatile bool tx_completed;					/// Прием окончен
	volatile bool echo_received;				/// Эхо принято
	volatile bool response_received;			/// Все ожидаемые респонсы приняты
	volatile bool response_wait;				/// Устанавливается после приема эха, чтобы быть уверенным в том, что входящие данные - это респонс, а не кокое-то сообщение модуля
	volatile bool timeout;						/// Произошел таймаут
	volatile ATCommander_rx_state_t rx_state;	/// Состояние приема
	char * rx_byte;								/// Указатель на текущий принятый байт. Является аргументом @ref rx_it_start
	volatile uint16_t rx_cnt;					/// Количество принятых данных, включая символы \r\n
	uint8_t expected_responses_num_received;	/// Количество ожидаемых респонсов, которые уже были приняты
	bool struct_ptrs_ok;						/// Флаг для проверки корректности указателей на функции
}ATCommander_private_t;

/// Настроечная структура
typedef struct {
	/// Хэндлеры
	struct ATCommander_handlers_s {
		ATCommander_tx_dma tx_dma;						/// Отправить данные по DMA
		ATCommander_user_rx_byte user_rx_byte;			/// Пользовательский хэндлер
		ATCommander_timer_start timer_start;			/// Запустить таймер
		ATCommander_timer_stop timer_stop;				/// Остановить таймер
		ATCommander_rx_it_start rx_it_start;			/// Разрешить прием байта по UART
		ATCommander_rx_it_stop rx_it_stop;				/// Запретить прием и отправку по UART
		ATCommander_wait wait;							/// Ожидание / сон
		ATCommander_get_ms get_ms;						/// Вернуть текущие тики в миллисекундах
		ATCommander_message_handler message_handler;	/// Хэндлер по приему самостоятельного сообщения (не респонса)
		ATCommander_logout logout;							/// Функция вывода сообщения в лог
	}handlers;

	/// Буферы
	struct ATCommander_buff_s{
		char * const rx_buff;			/// Буфер приема
		char * const response_buff;		/// Буфер, хранящий респонс.
										/// После ответа модуля @ref rx_buff копируется в @ref response_buff.
										/// Таким образом разрешается прием в @ref rx_buff, пока данные обрабатываются
		uint32_t rx_buff_len;			/// Размер буферов @ref rx_buff и @ref response_buff
	}buff;
}ATCommander_sett_t;

/// Стурктура данных для запроса к модулю
typedef struct {
	const char *cmd;					/// Команда или данные для отправки модулю
	uint32_t cmd_len;					/// Длина отправляемых данных
	char const *expected_data;			/// Указатель, хранящий заголовок ожидаемых данных
	uint32_t expected_data_len;			/// Длина заголовка ожидаемых данных
	uint8_t expected_responses_num;		/// Количество ожидаемых респонсов, отделенных \r\n с двух сторон
}ATCommander_request_t;

/// Стурктура данных, хранящая ответ модуля
typedef struct {
	char *data;		/// Указатель на респонс
	uint32_t len;	/// Длина респонса
}ATCommander_response_t;

/// Основная структура данных ATCommander
typedef struct {
	ATCommander_sett_t sett;				/// Настроечная структура
	ATCommander_private_t private;			/// Структура приватных данных модуля

	ATCommander_request_t request;			/// Стурктура данных для запроса к модулю
	ATCommander_response_t response[3];		/// Массив структур, хранящих ответ модуля.
											/// Библиотека поддерживает 3 сообщения с \r\n с двух сторон в ответе
}ATCommander_t;

//=======================================================================

/**
 * Отправить данные на модуль.
 * Функция ожидает окончания отправки, прием эха и необходимого количества респонсов
 * @param commander [in] Указатель на структуру @ref ATCommander_t
 * @param timeout_ms [in] Таймаут в мс
 * @return true/false
 */
int ATCommander_send(ATCommander_t *commander, uint32_t timeout_ms);

/**
 * Хэндлер приема байта.
 * Необходимо вызывать в прерывании UART по приему
 * 		Хэндлер обеспечивает прием сообщения, отделенного \r\n с двух сторон. Исключением является только эхо.
 * 		При некорректных пришедших данных состояние приема сбрасывается к начальному.
 * 		Если необходимо обработать нестандартные данные (без \r\n), то для этого есть хандлер @ref user_rx_byte, вызываемый в самом начале текущего хендлера
 * 		Пользователь должен рализовать его сам в соответствии со своими потребностями
 * @param commander [in] Указатель на структуру @ref ATCommander_t
 */
void ATCommander_rx_byte_handler(ATCommander_t *commander);

/**
 * Хэндлер окончания оправки данных.
 * Необходимо вызывать в прерывании UART по отправке
 * @param commander [in] Указатель на структуру @ref ATCommander_t
 */
void ATCommander_tx_complete_handler(ATCommander_t *commander);

/**
 * Хэндлер таймаута
 * Необходимо вызывать в прерывании таймера
 * @param commander [in] Указатель на структуру @ref ATCommander_t
 */
void ATCommander_timeout_handler(ATCommander_t *commander);

/**
 * Сбросить состояние командера
 * @param commander [in] Указатель на структуру @ref ATCommander_t
 */
void ATCommander_reset_state(ATCommander_t *commander);

#endif /* ATCOMMANDER_H_ */
