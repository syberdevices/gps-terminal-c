/**********************************************************************************
 * �ļ���  ��spi_flash.c
 * ����    ��spi �ײ�Ӧ�ú�����         
 * ʵ��ƽ̨��STM32������
 * Ӳ������ ----------------------------
 *         | PA4-SPI1-NSS  : W25X16-CS  |
 *         | PA5-SPI1-SCK  : W25X16-CLK |
 *         | PA6-SPI1-MISO : W25X16-DO  |
 *         | PA7-SPI1-MOSI : W25X16-DIO |
 *          ----------------------------
 * ��汾  ��ST3.5.0
 * ����    ������ 

**********************************************************************************/

#include "spi_flash.h"
#include "spi_flash_setings.h"
#include "spi_phy.h"


/* Private typedef -----------------------------------------------------------*/
//#define SPI_FLASH_PageSize      4096
#define SPI_FLASH_PageSize      256
#define SPI_FLASH_PerWritePageSize      256

/* Private define ------------------------------------------------------------*/
#define W25X_WriteEnable		      0x06 
#define W25X_WriteDisable		      0x04 
#define W25X_ReadStatusReg		    0x05 
#define W25X_WriteStatusReg		    0x01 
#define W25X_ReadData			        0x03 
#define W25X_FastReadData		      0x0B 
#define W25X_FastReadDual		      0x3B 
#define W25X_PageProgram		      0x02 
#define W25X_BlockErase_64		      0xD8
#define W25X_SectorErase		      0x20 
#define W25X_ChipErase			      0xC7 
#define W25X_PowerDown			      0xB9 
#define W25X_ReleasePowerDown	    0xAB 
#define W25X_DeviceID			        0xAB 
#define W25X_ManufactDeviceID   	0x90 
#define W25X_JedecDeviceID		    0x9F 

#define WIP_Flag                  0x01  /* Write In Progress (WIP) flag */

#define Dummy_Byte                0xFF

__attribute__((__aligned__(4))) static uint8_t commandArray[4];

/*******************************************************************************
* Function Name  : SPI_FLASH_SendByte
* Description    : Sends a byte through the SPI interface and return the byte
*                  received from the SPI bus.
* Input          : byte : byte to send.
* Output         : None
* Return         : The value of the received byte.
*******************************************************************************/
static int SPI_FLASH_SendByte(uint8_t byte, uint8_t *data)
{
	return SPI_TRANSMIT_RECEIVE(&byte, data, 1);
}

///*******************************************************************************
//* Function Name  : SPI_FLASH_ReadByte
//* Description    : Reads a byte from the SPI Flash.
//*                  This function must be used only if the Start_Read_Sequence
//*                  function has been previously called.
//* Input          : None
//* Output         : None
//* Return         : Byte Read from the SPI Flash.
//*******************************************************************************/
//static int SPI_FLASH_ReadByte(uint8_t *data)
//{
//	return SPI_FLASH_SendByte(Dummy_Byte, data);
//}
//
///*******************************************************************************
//* Function Name  : SPI_FLASH_SendHalfWord
//* Description    : Sends a Half Word through the SPI interface and return the
//*                  Half Word received from the SPI bus.
//* Input          : Half Word : Half Word to send.
//* Output         : None
//* Return         : The value of the received Half Word.
//*******************************************************************************/
//static int SPI_FLASH_SendHalfWord(uint16_t HalfWord, uint16_t *data)
//{
//	return SPI_TRANSMIT_RECEIVE((uint8_t*)&HalfWord, (uint8_t*)data, 2);
//}

/*******************************************************************************
* Function Name  : SPI_FLASH_WaitForWriteEnd
* Description    : Polls the status of the Write In Progress (WIP) flag in the
*                  FLASH's status  register  and  loop  until write  opertaion
*                  has completed.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
static int SPI_FLASH_WaitForWriteEnd(void)
{
  uint8_t data = 0;

  SPI_FLASH_CS_LOW();

  int errorcode = SPI_FLASH_SendByte(W25X_ReadStatusReg, &data);
  if (errorcode == 0) {
	  /* Loop as long as the memory is busy with a write cycle */
	  uint32_t count = SPI_FLASH_GET_MS();
	  do
	  {
		/* Send a dummy byte to generate the clock needed by the FLASH
		and put the value of the status register in FLASH_Status variable */
		errorcode = SPI_FLASH_SendByte(Dummy_Byte, &data);
		if ((SPI_FLASH_GET_MS() - count) > 2000) {
			errorcode = -1;
			break;
		}
	  }
	  while ((data & WIP_Flag) && errorcode == 0); /* Write in progress */
  }

  SPI_FLASH_CS_HIGH();

  return errorcode;
}

/*******************************************************************************
* Function Name  : SPI_FLASH_PageWrite
* Description    : Writes more than one byte to the FLASH with a single WRITE
*                  cycle(Page WRITE sequence). The number of byte can't exceed
*                  the FLASH page size.
* Input          : - pBuffer : pointer to the buffer  containing the data to be
*                    written to the FLASH.
*                  - WriteAddr : FLASH's internal address to write to.
*                  - NumByteToWrite : number of bytes to write to the FLASH,
*                    must be equal or less than "SPI_FLASH_PageSize" value.
* Output         : None
* Return         : None
*******************************************************************************/
static int SPI_FLASH_PageWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint32_t NumByteToWrite)
{
	int errorcode = SPI_FLASH_WriteEnable();
	if (errorcode != 0) {
		goto fail;
	}

	errorcode = SPI_FLASH_WaitForWriteEnd();
	if (errorcode != 0) {
		goto fail;
	}

	SPI_FLASH_CS_LOW();

	commandArray[0] = W25X_PageProgram;
	commandArray[1] = (WriteAddr & 0xFF0000) >> 16;
	commandArray[2] = (WriteAddr & 0xFF00) >> 8;
	commandArray[3] = WriteAddr & 0xFF;

	errorcode = SPI_TRANSMIT(commandArray, 4);
	if (errorcode != 0) {
		goto fail;
	}

	if(NumByteToWrite > SPI_FLASH_PerWritePageSize) {
		NumByteToWrite = SPI_FLASH_PerWritePageSize;
	}

#if IS_NEED_ALLIGNED
	if ((uint32_t)pBuffer & 0x01) {
		errorcode = SPI_TRANSMIT(pBuffer, 1);
		if (errorcode != 0) {
			goto fail;
		}
		NumByteToWrite--;
		pBuffer++;
	}
#endif

	// Отправляем всё остальное
	if (NumByteToWrite > 0) {
		errorcode = SPI_TRANSMIT(pBuffer, NumByteToWrite);
		if (errorcode != 0) {
			goto fail;
		}
	}

	SPI_FLASH_CS_HIGH();

	return SPI_FLASH_WaitForWriteEnd();

	fail:

	SPI_FLASH_CS_HIGH();
	return -1;
}

// =======================================================================

/**
 *
 * @param block_addr
 * @return
 */
int SPI_FLASH_EraseBlock64(uint32_t block_addr)
{
	if (SPI_FLASH_MUTEX_WAIT(SPI_FLASH_WAIT_TIMEOUT) != 0) {
		return -1;
	}

	int errorcode = SPI_FLASH_WriteEnable();
	if (errorcode != 0) {
		goto exit;
	}

	errorcode = SPI_FLASH_WaitForWriteEnd();
	if (errorcode != 0) {
		goto exit;
	}

	SPI_FLASH_CS_LOW();

	commandArray[0] = W25X_BlockErase_64;
	commandArray[1] = (block_addr & 0xFF0000) >> 16;
	commandArray[2] = (block_addr & 0xFF00) >> 8;
	commandArray[3] = block_addr & 0xFF;

	errorcode = SPI_TRANSMIT(commandArray, 4);
	if (errorcode != 0) {
		SPI_FLASH_CS_HIGH();
		goto exit;
	}

	SPI_FLASH_CS_HIGH();

	errorcode = SPI_FLASH_WaitForWriteEnd();

	exit:

	SPI_FLASH_MUTEX_RELEASE();

	return errorcode;
}

/*******************************************************************************
* Function Name  : SPI_FLASH_SectorErase
* Description    : Erases the specified FLASH sector.
* Input          : SectorAddr: address of the sector to erase.
* Output         : None
* Return         : None
*******************************************************************************/
int SPI_FLASH_SectorErase(uint32_t SectorAddr)
{
	if (SPI_FLASH_MUTEX_WAIT(SPI_FLASH_WAIT_TIMEOUT) != 0) {
		return -1;
	}

	int errorcode = SPI_FLASH_WriteEnable();
	if (errorcode != 0) {
		goto exit;
	}

	errorcode = SPI_FLASH_WaitForWriteEnd();
	if (errorcode != 0) {
		goto exit;
	}

	SPI_FLASH_CS_LOW();

	commandArray[0] = W25X_SectorErase;
	commandArray[1] = (SectorAddr & 0xFF0000) >> 16;
	commandArray[2] = (SectorAddr & 0xFF00) >> 8;
	commandArray[3] = SectorAddr & 0xFF;

	errorcode = SPI_TRANSMIT(commandArray, 4);
	if (errorcode != 0) {
		SPI_FLASH_CS_HIGH();
		goto exit;
	}

	SPI_FLASH_CS_HIGH();

	errorcode = SPI_FLASH_WaitForWriteEnd();

	exit:

	SPI_FLASH_MUTEX_RELEASE();

	return errorcode;
}

/*******************************************************************************
* Function Name  : SPI_FLASH_BulkErase
* Description    : Erases the entire FLASH.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
int SPI_FLASH_BulkErase(void)
{
	if (SPI_FLASH_MUTEX_WAIT(SPI_FLASH_WAIT_TIMEOUT) != 0) {
		return -1;
	}

	int errorcode = SPI_FLASH_WriteEnable();
	if (errorcode != 0) {
		goto exit;
	}

	errorcode = SPI_FLASH_WaitForWriteEnd();
	if (errorcode != 0) {
		goto exit;
	}

	SPI_FLASH_CS_LOW();

	uint8_t data = 0;
	errorcode = SPI_FLASH_SendByte(W25X_ChipErase, &data);
	if (errorcode != 0) {
		SPI_FLASH_CS_HIGH();
		goto exit;
	}

	SPI_FLASH_CS_HIGH();

	errorcode = SPI_FLASH_WaitForWriteEnd();

	exit:

	SPI_FLASH_MUTEX_RELEASE();

	return errorcode;
}

/*******************************************************************************
* Function Name  : SPI_FLASH_BufferWrite
* Description    : Writes block of data to the FLASH. In this function, the
*                  number of WRITE cycles are reduced, using Page WRITE sequence.
* Input          : - pBuffer : pointer to the buffer  containing the data to be
*                    written to the FLASH.
*                  - WriteAddr : FLASH's internal address to write to.
*                  - NumByteToWrite : number of bytes to write to the FLASH.
* Output         : None
* Return         : None
*******************************************************************************/
int SPI_FLASH_BufferWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint32_t NumByteToWrite)
{
	if (SPI_FLASH_MUTEX_WAIT(SPI_FLASH_WAIT_TIMEOUT) != 0) {
		return -1;
	}

	uint32_t NumOfPage = 0, NumOfSingle = 0, Addr = 0, count = 0, temp = 0;
	int errorcode = 0;

	Addr = WriteAddr % SPI_FLASH_PageSize;
	count = SPI_FLASH_PageSize - Addr;
	NumOfPage =  NumByteToWrite / SPI_FLASH_PageSize;
	NumOfSingle = NumByteToWrite % SPI_FLASH_PageSize;

	if (Addr == 0) /* WriteAddr is SPI_FLASH_PageSize aligned  */
	{
		if (NumOfPage == 0) /* NumByteToWrite < SPI_FLASH_PageSize */
		{
			errorcode = SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumByteToWrite);
		}
		else /* NumByteToWrite > SPI_FLASH_PageSize */
		{
			while (NumOfPage--)
			{
				errorcode = SPI_FLASH_PageWrite(pBuffer, WriteAddr, SPI_FLASH_PageSize);
				if (errorcode != 0) {
					goto exit;
				}
				WriteAddr +=  SPI_FLASH_PageSize;
				pBuffer += SPI_FLASH_PageSize;
			}

			errorcode = SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumOfSingle);
			if (errorcode != 0) {
				goto exit;
			}
		}
	}
	else /* WriteAddr is not SPI_FLASH_PageSize aligned  */
	{
		if (NumOfPage == 0) /* NumByteToWrite < SPI_FLASH_PageSize */
		{
			if (NumOfSingle > count) /* (NumByteToWrite + WriteAddr) > SPI_FLASH_PageSize */
			{
				temp = NumOfSingle - count;

				errorcode = SPI_FLASH_PageWrite(pBuffer, WriteAddr, count);
				if (errorcode != 0) {
					goto exit;
				}
				WriteAddr +=  count;
				pBuffer += count;

				errorcode = SPI_FLASH_PageWrite(pBuffer, WriteAddr, temp);
				if (errorcode != 0) {
					goto exit;
				}
			}
			else
			{
				errorcode = SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumByteToWrite);
				if (errorcode != 0) {
					goto exit;
				}
			}
		}
		else /* NumByteToWrite > SPI_FLASH_PageSize */
		{
			NumByteToWrite -= count;
			NumOfPage =  NumByteToWrite / SPI_FLASH_PageSize;
			NumOfSingle = NumByteToWrite % SPI_FLASH_PageSize;

			errorcode = SPI_FLASH_PageWrite(pBuffer, WriteAddr, count);
			if (errorcode != 0) {
				goto exit;
			}
			WriteAddr +=  count;
			pBuffer += count;

			while (NumOfPage--)
			{
				errorcode = SPI_FLASH_PageWrite(pBuffer, WriteAddr, SPI_FLASH_PageSize);
				if (errorcode != 0) {
					goto exit;
				}
				WriteAddr +=  SPI_FLASH_PageSize;
				pBuffer += SPI_FLASH_PageSize;
			}

			if (NumOfSingle != 0)
			{
				errorcode = SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumOfSingle);
				if (errorcode != 0) {
					goto exit;
				}
			}
		}
	}

	exit:

	SPI_FLASH_MUTEX_RELEASE();

	return errorcode;
}

/*******************************************************************************
* Function Name  : SPI_FLASH_BufferRead
* Description    : Reads a block of data from the FLASH.
* Input          : - pBuffer : pointer to the buffer that receives the data read
*                    from the FLASH.
*                  - ReadAddr : FLASH's internal address to read from.
*                  - NumByteToRead : number of bytes to read from the FLASH.
* Output         : None
* Return         : None
*******************************************************************************/
int SPI_FLASH_BufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint32_t NumByteToRead)
{
	if (SPI_FLASH_MUTEX_WAIT(SPI_FLASH_WAIT_TIMEOUT) != 0) {
		return -1;
	}

	SPI_FLASH_CS_LOW();

	commandArray[0] = W25X_ReadData;
	commandArray[1] = (ReadAddr & 0xFF0000) >> 16;
	commandArray[2] = (ReadAddr & 0xFF00) >> 8;
	commandArray[3] = ReadAddr & 0xFF;

	int errorcode = SPI_TRANSMIT(commandArray, 4);
	if (errorcode != 0) {
		goto exit;
	}

#if IS_NEED_ALLIGNED
	if ((uint32_t)pBuffer & 0x01) {
		errorcode = SPI_RECEIVE(pBuffer, 1);
		if (errorcode != 0) {
			goto exit;
		}
		NumByteToRead--;
		pBuffer++;
	}
#endif

	// Читаем всё остальное
	if(NumByteToRead > 0) {
		errorcode = SPI_RECEIVE(pBuffer, NumByteToRead);
	}

	exit:

	SPI_FLASH_CS_HIGH();

	SPI_FLASH_MUTEX_RELEASE();

	return errorcode;
}

/*******************************************************************************
* Function Name  : SPI_FLASH_ReadID
* Description    : Reads FLASH identification.
* Input          : None
* Output         : None
* Return         : FLASH identification
*******************************************************************************/
int SPI_FLASH_ReadID(uint32_t *temp)
{
	uint32_t Temp0 = 0, Temp1 = 0, Temp2 = 0;
	uint8_t data = 0;

	if (SPI_FLASH_MUTEX_WAIT(SPI_FLASH_WAIT_TIMEOUT) != 0) {
		return -1;
	}

	SPI_FLASH_CS_LOW();

	int errorcode = SPI_FLASH_SendByte(W25X_JedecDeviceID, &data);
	if (errorcode != 0) {
		goto exit;
	}

	errorcode = SPI_FLASH_SendByte(Dummy_Byte, (uint8_t*)(&Temp0));
	if (errorcode != 0) {
		goto exit;
	}
	errorcode = SPI_FLASH_SendByte(Dummy_Byte, (uint8_t*)(&Temp1));
	if (errorcode != 0) {
		goto exit;
	}
	errorcode = SPI_FLASH_SendByte(Dummy_Byte, (uint8_t*)(&Temp2));

	exit:

	SPI_FLASH_CS_HIGH();

	SPI_FLASH_MUTEX_RELEASE();

	if (errorcode == 0) {
		*temp = (Temp0 << 16) | (Temp1 << 8) | Temp2;
	}
	else {
		*temp = -1;
	}

	return errorcode;
}

///*******************************************************************************
//* Function Name  : SPI_FLASH_ReadID
//* Description    : Reads FLASH identification.
//* Input          : None
//* Output         : None
//* Return         : FLASH identification
//*******************************************************************************/
//int SPI_FLASH_ReadDeviceID(uint32_t *temp)
//{
//  uint8_t data;
//
//  SPI_FLASH_CS_LOW();
//
//  int errorcode = SPI_FLASH_SendByte(W25X_DeviceID, &data);
//  if (errorcode != 0) {
//	  goto exit;
//  }
//  errorcode = SPI_FLASH_SendByte(Dummy_Byte, &data);
//  if (errorcode != 0) {
//	  goto exit;
//  }
//  errorcode = SPI_FLASH_SendByte(Dummy_Byte, &data);
//  if (errorcode != 0) {
//	  goto exit;
//  }
//  errorcode = SPI_FLASH_SendByte(Dummy_Byte, &data);
//  if (errorcode != 0) {
//	  goto exit;
//  }
//
//  errorcode = SPI_FLASH_SendByte(Dummy_Byte, (uint8_t*)temp);
//
//  exit:
//
//  SPI_FLASH_CS_HIGH();
//
//  return errorcode;
//}
///*******************************************************************************
//* Function Name  : SPI_FLASH_StartReadSequence
//* Description    : Initiates a read data byte (READ) sequence from the Flash.
//*                  This is done by driving the /CS line low to select the device,
//*                  then the READ instruction is transmitted followed by 3 bytes
//*                  address. This function exit and keep the /CS line low, so the
//*                  Flash still being selected. With this technique the whole
//*                  content of the Flash is read with a single READ instruction.
//* Input          : - ReadAddr : FLASH's internal address to read from.
//* Output         : None
//* Return         : None
//*******************************************************************************/
//int SPI_FLASH_StartReadSequence(uint32_t ReadAddr)
//{
//  uint8_t data = 0;
//  /* Select the FLASH: Chip Select low */
//  SPI_FLASH_CS_LOW();
//
//  /* Send "Read from Memory " instruction */
//  SPI_FLASH_SendByte(W25X_ReadData);
//
//  /* Send the 24-bit address of the address to read from -----------------------*/
//  /* Send ReadAddr high nibble address byte */
//  SPI_FLASH_SendByte((ReadAddr & 0xFF0000) >> 16);
//  /* Send ReadAddr medium nibble address byte */
//  SPI_FLASH_SendByte((ReadAddr& 0xFF00) >> 8);
//  /* Send ReadAddr low nibble address byte */
//  SPI_FLASH_SendByte(ReadAddr & 0xFF);
//}

/*******************************************************************************
* Function Name  : SPI_FLASH_WriteEnable
* Description    : Enables the write access to the FLASH.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
int SPI_FLASH_WriteEnable(void)
{
	if (SPI_FLASH_MUTEX_WAIT(SPI_FLASH_WAIT_TIMEOUT) != 0) {
		return -1;
	}

	SPI_FLASH_CS_LOW();

	uint8_t data = 0;
	int errorcode = SPI_FLASH_SendByte(W25X_WriteEnable, &data);

	SPI_FLASH_CS_HIGH();

	SPI_FLASH_MUTEX_RELEASE();

	return errorcode;
}

/**
 * Power Down
 * @return
 */
int SPI_Flash_PowerDown(void)
{ 
	if (SPI_FLASH_MUTEX_WAIT(SPI_FLASH_WAIT_TIMEOUT) != 0) {
		return -1;
	}

	SPI_FLASH_CS_LOW();

	uint8_t data = 0;
	int errorcode = SPI_FLASH_SendByte(W25X_PowerDown, &data);

	SPI_FLASH_CS_HIGH();

	SPI_FLASH_MUTEX_RELEASE();

	return errorcode;
}

/**
 * WakeUp
 * @return
 */
int SPI_Flash_WAKEUP(void)
{
	if (SPI_FLASH_MUTEX_WAIT(SPI_FLASH_WAIT_TIMEOUT) != 0) {
		return -1;
	}

	SPI_FLASH_CS_LOW();

	uint8_t data = 0;

	int errorcode = SPI_FLASH_SendByte(W25X_ReleasePowerDown, &data);
	if (errorcode != 0) {
		goto exit;
	}

	for (uint8_t q = 0; q < 4; q++) {
		errorcode = SPI_FLASH_SendByte(Dummy_Byte, &data);
		if (errorcode != 0) {
			goto exit;
		}
	}

	exit:

	SPI_FLASH_CS_HIGH();

	SPI_FLASH_MUTEX_RELEASE();

	return errorcode;
}   

/******************************END OF FILE*****************************/
