/*
 * spi_flash_setings.h
 *
 *  Created on: 19 февр. 2018 г.
 *      Author: oleynik
 */

#ifndef SPI_FLASH_SETINGS_H_
#define SPI_FLASH_SETINGS_H_

#include "main.h"
#include "rtos.h"

//==================================================================

// Для cortex-m0 stm32 адрес должен быть выравнен на 2
// Поэтому если адрес буфера нечетный, то отправляем 1 байт, а потом всё остальное, т.к. оно уже станет четное
#define IS_NEED_ALLIGNED								0

#define SPI_FLASH_WAIT_TIMEOUT							(1*60*1000)
#define SPI_FLASH_MUTEX_WAIT(ms)						osRecursiveMutexWait(spiflashRecursiveMutexHandle, ms)
#define SPI_FLASH_MUTEX_RELEASE()						osRecursiveMutexRelease(spiflashRecursiveMutexHandle)
#define SPI_FLASH_GET_MS()								HAL_GetTick()

//==================================================================

#endif /* SPI_FLASH_SETINGS_H_ */
