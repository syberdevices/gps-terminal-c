#ifndef __SPI_FLASH_H
#define __SPI_FLASH_H

#include "stdint.h"

int SPI_FLASH_EraseBlock64(uint32_t block_addr);
int SPI_FLASH_SectorErase(uint32_t SectorAddr);
int SPI_FLASH_BulkErase(void);
int SPI_FLASH_BufferWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint32_t NumByteToWrite);
int SPI_FLASH_BufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint32_t NumByteToRead);
int SPI_FLASH_ReadID(uint32_t *temp);
//int SPI_FLASH_ReadDeviceID(uint32_t *temp);
//void SPI_FLASH_StartReadSequence(uint32_t ReadAddr);
int SPI_Flash_PowerDown(void);
int SPI_Flash_WAKEUP(void);

int SPI_FLASH_WriteEnable(void);

#endif /* __SPI_FLASH_H */

