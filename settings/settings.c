// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/*
 * settings.c
 *
 *  Created on: 29 нояб. 2017 г.
 *      Author: oleynik
 */

#include <string.h>

#include "settings.h"
#include "spi_flash/spi_flash.h"
#include "crc32/crc32.h"
#include "rtos.h"
#include "common.h"
#include "main.h"

// =======================================================================

#define SETTINGS_EXT_FLASH_ADDR			EXT_FLASH_USER_REGION_ADDR

#define SETTINGS1_ADDR_OFFSET			( 0 )
#define SETTINGS1_SIZE					( sizeof(settings1_t) + sizeof(uint32_t) )

#define SETTINGS2_ADDR_OFFSET			( SETTINGS1_ADDR_OFFSET + SETTINGS1_SIZE )
#define SETTINGS2_SIZE					( sizeof(settings2_t) + sizeof(uint32_t) )

#define SETTINGS3_ADDR_OFFSET			( SETTINGS2_ADDR_OFFSET + SETTINGS2_SIZE )
#define SETTINGS3_SIZE					( sizeof(settings3_t) + sizeof(uint32_t) )

#define SETTINGS4_ADDR_OFFSET			( SETTINGS3_ADDR_OFFSET + SETTINGS3_SIZE )
#define SETTINGS4_SIZE					( sizeof(settings4_t) + sizeof(uint32_t) )

#define SETTINGS_FULL_SIZE				( SETTINGS1_SIZE + SETTINGS2_SIZE + SETTINGS3_SIZE + SETTINGS4_SIZE )

// =======================================================================

typedef struct sett_buffs_t {
	void * _ram;
	void * _ext;
	void * _int;
	uint32_t len;
	uint32_t offset;
}sett_buffs_t;

typedef struct need_write_t {
	bool _ext;
	bool _int;
}need_write_t;

// =======================================================================

settings1_t settings1_ram = {
		// Инициализируем dins как счетчики, чтобы при срабатывании прерывания по din до чтения настроек из флеш, программа прибавляла импульс.
		// Это сделано на случай, если din настроен как счетный. Если din настроен как несчетный, то при вызове gpio_state_init() в AppInit() всё равно счетчик сбросится
		.dins[0].mode = DI_MODE_PULSE_COUNT,
		.dins[1].mode = DI_MODE_PULSE_COUNT,
		.dins[2].mode = DI_MODE_PULSE_COUNT,
		.dins[3].mode = DI_MODE_PULSE_COUNT,
};
settings2_t settings2_ram;
settings3_t settings3_ram;
settings4_t settings4_ram;

uint8_t settings_buff[SETTINGS_FULL_SIZE];

// =======================================================================

static int erase_settings_ext() {
	return SPI_FLASH_EraseBlock64(SETTINGS_EXT_FLASH_ADDR);
}

static int erase_settings_int() {
	return erase_user_internal_flash();
}

static int read_settings_ext (void * data, uint32_t offset, uint32_t len) {
	uint8_t set_buff[len];
	uint32_t flash_crc;
	uint32_t addr = SETTINGS_EXT_FLASH_ADDR + offset;

	SPI_FLASH_BufferRead(set_buff, addr, len);
	SPI_FLASH_BufferRead((uint8_t*)&flash_crc, addr + len, sizeof(flash_crc));

	if (crc32_get(set_buff, len, 0) == flash_crc) {
		taskENTER_CRITICAL();
		memcpy(data, set_buff, len);
		taskEXIT_CRITICAL();
		return 0;
	}
	else {
		return -1;
	}
}

static int read_settings_int (void * data, uint32_t offset, uint32_t len) {
	uint8_t set_buff[len];
	uint32_t flash_crc;
	uint32_t addr = USER_INT_FLASH_ADDR + offset;

	memcpy(set_buff, (void*)addr, len);
	memcpy(&flash_crc, (void*)addr + len, sizeof(flash_crc));

	if (crc32_get(set_buff, len, 0) == flash_crc) {
		taskENTER_CRITICAL();
		memcpy(data, set_buff, len);
		taskEXIT_CRITICAL();
		return 0;
	}
	else {
		return -1;
	}
}

static int read_and_validate_ext_int_settings (need_write_t * need_write, sett_buffs_t * sett_buffs) {
	int res_ext = read_settings_ext(sett_buffs->_ext, sett_buffs->offset, sett_buffs->len);
	int res_int = read_settings_int(sett_buffs->_int, sett_buffs->offset, sett_buffs->len);
	bool equal = memcmp(sett_buffs->_ext, sett_buffs->_int, sett_buffs->len) == 0;

	if (res_ext == 0 && res_int == 0 && equal) {
		// Данные на внешней и внутренней флэш и crc совпадают
		// Всё ок. Не имеет значения, откуда брать данные
		taskENTER_CRITICAL();
		memcpy(sett_buffs->_ram, sett_buffs->_ext, sett_buffs->len);
		taskEXIT_CRITICAL();
		return 0;
	}
	else if (res_ext == 0 && res_int == 0 && !equal) {
		// Данные есть на внешней и внутренней флэш, но они не совпадают
		// Приоритет - внутренняя флэш, копируем на внешнюю
		taskENTER_CRITICAL();
		memcpy(sett_buffs->_ram, sett_buffs->_int, sett_buffs->len);
		taskEXIT_CRITICAL();
		need_write->_ext = true;
		return 0;
	}
	else if (res_ext == -1 && res_int == 0) {
		// Данные есть только на внутренней флэш
		// Копируем на внешнюю
		taskENTER_CRITICAL();
		memcpy(sett_buffs->_ram, sett_buffs->_int, sett_buffs->len);
		taskEXIT_CRITICAL();
		need_write->_ext = true;
		return 0;
	}
	else if (res_ext == 0 && res_int == -1) {
		// Данные есть только на внешней флэш
		// Копируем на внутреннюю
		taskENTER_CRITICAL();
		memcpy(sett_buffs->_ram, sett_buffs->_ext, sett_buffs->len);
		taskEXIT_CRITICAL();
		need_write->_int = true;
		return 0;
	}
	else {
		// Данных нет ни на внутренней, ни на внешней флэш
		need_write->_ext = true;
		need_write->_int = true;
		return -1;
	}
}

static int collect_settings_to_buff(uint8_t * buff, void * psett, uint32_t sett_len) {
	taskENTER_CRITICAL();
	uint32_t crc = crc32_get(psett, sett_len, 0);
	memcpy(buff, psett, sett_len);
	taskEXIT_CRITICAL();

	int offset = sett_len;

	memcpy(buff + offset, &crc, sizeof(crc));
	offset += sizeof(crc);

	return offset;
}

static void collect_full_settings_to_buff(uint8_t * buff) {
	// Пишем в один буфер все настройки

	uint8_t *sett = buff;

	sett += collect_settings_to_buff(sett, &settings1_ram, sizeof(settings1_ram));

	sett += collect_settings_to_buff(sett, &settings2_ram, sizeof(settings2_ram));

	sett += collect_settings_to_buff(sett, &settings3_ram, sizeof(settings3_ram));

	sett += collect_settings_to_buff(sett, &settings4_ram, sizeof(settings4_ram));
}

static int write_full_settings_ext(void * sett_buff) {
	int res = erase_settings_ext();
	if (res != 0) {
		return -1;
	}

	res = SPI_FLASH_BufferWrite((uint8_t*)sett_buff, SETTINGS_EXT_FLASH_ADDR, SETTINGS_FULL_SIZE);
	if (res != 0) {
		return -1;
	}

	return 0;
}

static int write_full_settings_int(void * sett_buff) {
	int res = erase_settings_int();
	if (res != 0) {
		return -1;
	}

	res = user_internal_flash_program(USER_INT_FLASH_ADDR, sett_buff, SETTINGS_FULL_SIZE);
	if (res != 0) {
		return -1;
	}

	return 0;
}

// =======================================================================

void set_settings1_default() {
	settings1_t *p = &settings1_ram;

	taskENTER_CRITICAL();

	p->id = DEFAULT_DEVICE_ID;

	p->protocol = PROTOCOL_EGTS;

	p->period.data_send.in_active_mode = 30;
	p->period.data_send.in_standby_mode = 60;
	p->period.data_write.in_active_mode = 30;
	p->period.data_write.in_standby_mode = 360;
	p->period.fls_read.in_active_mode = 40;
	p->period.fls_read.in_standby_mode = 20;

	p->fls[0].used = true;
	p->fls[0].address = 1;
	p->fls[1].used = true;
	p->fls[1].address = 2;
	bzero(&p->fls[2], sizeof(p->fls) - sizeof(p->fls[0]) * 2);

	bzero(p->ow_temp, sizeof(p->ow_temp));

	p->gnss.filter_on = true;
	p->gnss.freeze_speed = 3;
	p->gnss.freeze_time = 60;
	p->gnss.hdop_max = 2.0;
	p->gnss.satellites_min = 4;
	p->gnss.max_speed = 180;

	p->gnss.speed_avg = 25;

	p->min_rssi = 5;
	p->untracked_course = 7;
	p->untracked_distance = 100;

	p->acceleration_threshold = 2;

	bzero(p->apn.address, sizeof(p->apn.address));
	bzero(p->apn.user, sizeof(p->apn.user));
	bzero(p->apn.password, sizeof(p->apn.password));

#ifdef DEBUG
	strcpy(p->main_host.address, "193.193.165.165");
	p->main_host.port = 20629;//20332;
#else
	strcpy(p->main_host.address, "91.223.120.10");
	p->main_host.port = 8004;
#endif
	p->main_host.wait_response = true;

	// Настройки FTP не меняются через JSON
	strcpy(p->ftp_host.address, "91.223.120.21");	// ftp.sibinn.ru
	strcpy(p->ftp_host.port, "1221");
	strcpy(p->ftp_host.login, "nt");
	strcpy(p->ftp_host.password, "eSc1Cpr$p3p3}09?8o5W");

#ifdef DEBUG
	strcpy(p->log_host.address, "88.204.74.21");
	p->log_host.port = 12345;
#else
	strcpy(p->log_host.address, "91.223.120.117");
	p->log_host.port = 8010;
#endif

	p->dins[0].type = NORMALLY_OPEN;
	p->dins[0].mode = DI_MODE_WRITE_POINT_TRIGGER;
	p->dins[1].type = NORMALLY_OPEN;
	p->dins[1].mode = DI_MODE_WRITE_POINT_TRIGGER;
	p->dins[2].type = NORMALLY_CLOSED;
	p->dins[2].mode = DI_MODE_WRITE_POINT_TRIGGER;
	p->dins[3].type = NORMALLY_CLOSED;
	p->dins[3].mode = DI_MODE_SOS;

	p->douts[0].type = NORMALLY_OPEN;
	p->douts[1].type = NORMALLY_OPEN;

	p->ains[0].threshold = 10.0f;
	p->ains[1].threshold = 10.0f;

	p->bluetooth.used = false;
	p->bluetooth.auto_answer = false;
	bzero(p->bluetooth.mac, sizeof(p->bluetooth.mac));

	for (int q = 0; q < WHITE_PHONE_NUM_1; q++) {
		strcpy(p->white_phones_1[q].number, WHITE_PHONE_NONE);
	}

//	p->bluetooth.used = true;
//
//	p->bluetooth.mac[0] = 0xE4;
//	p->bluetooth.mac[1] = 0x22;
//	p->bluetooth.mac[2] = 0xA5;
//	p->bluetooth.mac[3] = 0x54;
//	p->bluetooth.mac[4] = 0x5D;
//	p->bluetooth.mac[5] = 0x80;
//
//	p->bluetooth.mac[0] = 0xFC;
//	p->bluetooth.mac[1] = 0x58;
//	p->bluetooth.mac[2] = 0xFA;
//	p->bluetooth.mac[3] = 0x41;
//	p->bluetooth.mac[4] = 0x18;
//	p->bluetooth.mac[5] = 0x16;
//
//	strcpy(p->white_phones[0].number,"79234154072");

	strcpy(p->password, MASTER_PASSWORD);

	// -----------------------------------

	p->rom.odometer = 0.0f;

	for (int q = 0; q < WHITE_PHONE_NUM_1; q++) {
		strcpy(p->white_phones_1[q].number, WHITE_PHONE_NONE);
	}

	taskEXIT_CRITICAL();
}

void set_settings2_default() {
	settings2_t *p = &settings2_ram;

	taskENTER_CRITICAL();

	p->auto_id = AUTO_ID_ENABLED;

	p->log_level = ZF_LOG_ERROR;

	taskEXIT_CRITICAL();
}

void set_settings3_default() {
	settings3_t *p = &settings3_ram;

	taskENTER_CRITICAL();

	p->can.used = false;
	p->can.used_param.all = 0;

	p->can.send_period.in_active_mode = 10;
	p->can.send_period.in_standby_mode = 30;
	p->can.gen_packet_on_ignition = true;

	p->can.alert.on_temp_trh.temp = 110;
	p->can.alert.on_temp_trh.duration = 30;
	p->can.alert.on_rpm_trh.max = 5000;
	p->can.alert.on_rpm_trh.duration = 30;
	p->can.alert.on_check_oil = true;

	taskEXIT_CRITICAL();
}

void set_settings4_default() {
	settings4_t *p = &settings4_ram;

	taskENTER_CRITICAL();

	p->settings_version = SETTINGS_VERSION;
	p->auto_update = true;

	for (int q = 0; q < WHITE_PHONE_NUM_2; q++) {
		strcpy(p->white_phones_2[q].number, WHITE_PHONE_NONE);
	}

	for (int q = 0; q < USER_MASTER_PHONE_NUM; q++) {
		strcpy(p->user_master_phones[q].number, WHITE_PHONE_NONE);
	}

	taskEXIT_CRITICAL();
}

void set_settings_default() {
	set_settings1_default();
	set_settings2_default();
	set_settings3_default();
	set_settings4_default();
}

int read_settings () {
//	assert_param(SETTINGS_FULL_SIZE > 1);

	int res = 0;

//	uint8_t ext_buff[SETTINGS_FULL_SIZE];
	uint8_t buff[SETTINGS_FULL_SIZE];

	sett_buffs_t sett_buffs = {
		._ext = settings_buff,
		._int = buff,
	};
	need_write_t need_write = {
		._ext = false,
		._int = false,
	};

	// Читаем settings1_ram

	sett_buffs._ram = &settings1_ram;
	sett_buffs.len = sizeof(settings1_ram);
	sett_buffs.offset = SETTINGS1_ADDR_OFFSET;

	if (read_and_validate_ext_int_settings(&need_write, &sett_buffs) != 0) {
		set_settings1_default();
	}

	// Читаем settings2_ram

	sett_buffs._ram = &settings2_ram;
	sett_buffs.len = sizeof(settings2_ram);
	sett_buffs.offset = SETTINGS2_ADDR_OFFSET;

	if (read_and_validate_ext_int_settings(&need_write, &sett_buffs) != 0) {
		set_settings2_default();
	}

	// Читаем settings3_ram

	sett_buffs._ram = &settings3_ram;
	sett_buffs.len = sizeof(settings3_ram);
	sett_buffs.offset = SETTINGS3_ADDR_OFFSET;

	if (read_and_validate_ext_int_settings(&need_write, &sett_buffs) != 0) {
		set_settings3_default();
	}

	// Читаем settings4_ram

	sett_buffs._ram = &settings4_ram;
	sett_buffs.len = sizeof(settings4_ram);
	sett_buffs.offset = SETTINGS4_ADDR_OFFSET;

	if (read_and_validate_ext_int_settings(&need_write, &sett_buffs) != 0) {
		set_settings4_default();
	}


	collect_full_settings_to_buff(settings_buff);

	if (need_write._ext) {
		if (write_full_settings_ext(settings_buff) != 0) {
			res = -1;
		}
	}

	if (need_write._int) {
		if (write_full_settings_int(settings_buff) != 0) {
			res = -1;
		}
	}

	return res;
}

int write_settings (sett_area_type_t sett_area) {
	if (!(sett_area & SETT_AREA_EXT_FLASH) && !(sett_area & SETT_AREA_INT_FLASH)) {
		return -1;
	}

	int res_ext = 0, res_int = 0;

	collect_full_settings_to_buff(settings_buff);

	if (sett_area & SETT_AREA_EXT_FLASH) {
		res_ext = write_full_settings_ext(settings_buff);
		if (res_ext != 0) {
			ZF_LOGE("write_full_settings_ext error");
		}
	}

	if (sett_area & SETT_AREA_INT_FLASH) {
		res_int = write_full_settings_int(settings_buff);
		if (res_int != 0) {
			ZF_LOGE("write_full_settings_int error");
		}
	}

	return ((res_ext == 0) && (res_int == 0)) ? 0 : -1;;
}

int check_settings_in_ram() {
	void * p_sett = (void*)USER_INT_FLASH_ADDR;

	if (memcmp(p_sett, &settings1_ram, sizeof(settings1_t)) != 0) {
		return -1;
	}

	p_sett += sizeof(settings1_t) + sizeof(uint32_t);

	if (memcmp(p_sett, &settings2_ram, sizeof(settings2_t)) != 0) {
		return -1;
	}

	p_sett += sizeof(settings2_t) + sizeof(uint32_t);

	if (memcmp(p_sett, &settings3_ram, sizeof(settings3_t)) != 0) {
		return -1;
	}

	p_sett += sizeof(settings3_t) + sizeof(uint32_t);

	if (memcmp(p_sett, &settings4_ram, sizeof(settings4_t)) != 0) {
		return -1;
	}

	return 0;
}

