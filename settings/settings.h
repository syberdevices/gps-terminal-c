/*
 * settings.h
 *
 *  Created on: 29 нояб. 2017 г.
 *      Author: oleynik
 */

#include "stdint.h"
#include "stdbool.h"

#ifndef SETTINGS_H_
#define SETTINGS_H_

// =======================================================================

#define SETTINGS_VERSION			4

#define DEFAULT_DEVICE_ID			0

#define MASTER_PASSWORD				"15975346"

#define FIXED_MASTER_PHONE_NUM		7
#define USER_MASTER_PHONE_NUM		5
#define WHITE_PHONE_NUM				18

#if (FIXED_MASTER_PHONE_NUM + USER_MASTER_PHONE_NUM + WHITE_PHONE_NUM > 30)
#error "White list settings is too long"
#endif

#define WHITE_PHONE_NUM_1			4
#define WHITE_PHONE_NUM_2			(WHITE_PHONE_NUM - WHITE_PHONE_NUM_1)
#define WHITE_PHONE_NONE			""


// =======================================================================

typedef enum sett_area_type_t {
	SETT_AREA_EXT_FLASH = 0x1,
	SETT_AREA_INT_FLASH = 0x2,
	SETT_AREA_EXT_INT_FLASH = SETT_AREA_EXT_FLASH | SETT_AREA_INT_FLASH,
}sett_area_type_t;

typedef enum dio_type_t {
	NORMALLY_CLOSED,
	NORMALLY_OPEN,
} dio_type_t;

typedef enum di_mode_t {
	DI_MODE_OFF,
	DI_MODE_PULSE_COUNT,
	DI_MODE_IGNITION,
	DI_MODE_SOS,
	DI_MODE_WRITE_POINT_TRIGGER,
} di_mode_t;

typedef struct di_t {
	dio_type_t type;
	di_mode_t mode;
} di_t;

typedef struct do_t {
	dio_type_t type;
} do_t;

typedef struct ai_t {
	float threshold;
} ai_t;

typedef struct period_t {
	uint16_t in_active_mode;
	uint16_t in_standby_mode;
} period_t;

typedef struct period_sett_t {
	period_t data_send;
	period_t data_write;
	period_t fls_read;
} period_sett_t;

typedef struct gnss_sett_t {
	bool filter_on;
	float hdop_max;
	uint8_t satellites_min;
	uint8_t freeze_speed;
	uint16_t freeze_time;
	uint8_t max_speed;
	uint8_t speed_avg;
} gnss_sett_t;

typedef struct phone_number_t {
	char number[16];
} phone_number_t;

typedef struct fls_sett_t {
	bool used;
	uint8_t address;
} fls_sett_t;

typedef struct ow_temp_sett_t {
	bool used;
	uint8_t id[8];
} ow_temp_sett_t;

typedef struct bluetooth_t {
	bool used;
	bool auto_answer;
	uint8_t mac[6];
} bluetooth_t;

typedef struct apn_sett_t {
	char address[36];
	char user[24];
	char password[24];
} apn_sett_t;

typedef struct main_host_sett_t {
	char address[36];
	uint32_t port;
	bool wait_response;
} main_host_sett_t;

typedef struct log_host_sett_t {
	char address[36];
	uint32_t port;
} log_host_sett_t;

typedef struct ftp_host_sett_t {
	char address[36];
	char port[8];
	char login[24];
	char password[24];
} ftp_host_sett_t;

typedef struct rom_t {
	float odometer;
} rom_t;

typedef enum protocol_t {
	PROTOCOL_EGTS,
	PROTOCOL_WIALON,
} protocol_t;

typedef struct settings_t {
	uint32_t			id;
	protocol_t			protocol;
	period_sett_t		period;
	fls_sett_t			fls[8];
	ow_temp_sett_t		ow_temp[8];
	gnss_sett_t			gnss;
	uint8_t				min_rssi;
	uint16_t			untracked_distance;
	uint8_t				untracked_course;
	phone_number_t		white_phones_1[WHITE_PHONE_NUM_1];	// +16 в группе white_phones_2
	uint16_t			acceleration_threshold;
	apn_sett_t			apn;
	main_host_sett_t	main_host;
	ftp_host_sett_t		ftp_host;
	log_host_sett_t		log_host;
	di_t				dins[4];
	do_t				douts[2];
	ai_t				ains[2];
	bluetooth_t			bluetooth;
	char				password[16];

	// ------------------------------------

	rom_t			rom;
} settings1_t;

// =======================================================================

typedef enum auto_id_t {
	AUTO_ID_DISABLED,
	AUTO_ID_ENABLED

} auto_id_t;

typedef struct settings2_t {
	auto_id_t	auto_id;
	uint8_t		log_level;
} settings2_t;

// =======================================================================

typedef struct can_used_param_t {
	union {
		struct {
			uint32_t rpm : 1;
			uint32_t fuel_level_percent : 1;
			uint32_t fuel_level_liter : 1;
			uint32_t odometer : 1;
			uint32_t ignition : 1;
			uint32_t engine_temp : 1;
			uint32_t oil_level : 1;
		};
		uint32_t all;
	};
} can_used_param_t;

typedef struct can_alert_threshold_temp_t {
	int16_t temp;
	uint16_t duration;	// sec
} can_alert_threshold_temp_t;

typedef struct can_alert_threshold_rpm_t {
	uint16_t max;
	uint16_t duration;	// sec
} can_alert_threshold_rpm_t;

typedef struct can_alert_threshold_t {
	can_alert_threshold_temp_t on_temp_trh;
	can_alert_threshold_rpm_t on_rpm_trh;
	bool on_check_oil;
} can_alert_t;

typedef struct can_sett_t {
	bool 					used;
	can_used_param_t		used_param;
	period_t				send_period;
	can_alert_t				alert;
	bool					gen_packet_on_ignition;
} can_sett_t;

typedef struct setting3_t {
	can_sett_t	can;
} settings3_t;

typedef struct settings4_t {
	uint32_t settings_version;
	phone_number_t white_phones_2[WHITE_PHONE_NUM_2];	// +4 в группе white_phones_1
	phone_number_t user_master_phones[USER_MASTER_PHONE_NUM];
	bool auto_update;
} settings4_t;

// =======================================================================

extern settings1_t settings1_ram;
extern settings2_t settings2_ram;
extern settings3_t settings3_ram;
extern settings4_t settings4_ram;

// =======================================================================

void set_settings1_default();
void set_settings2_default();
void set_settings3_default();
void set_settings4_default();
void set_settings_default();
int read_settings();
int write_settings(sett_area_type_t sett_area);
int check_settings_in_ram();



#endif /* SETTINGS_H_ */
