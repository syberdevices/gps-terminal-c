/*
 * crc16.h
 *
 *  Created on: 6 окт. 2018 г.
 *      Author: Denis Shreiber
 */

#ifndef CRC16_H_
#define CRC16_H_

unsigned short crc16 (const void *data, unsigned data_size, unsigned short crc_initial);

#endif /* CRC16_H_ */
