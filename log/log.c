// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/*
 * log.c
 *
 *  Created on: 31 янв. 2018 г.
 *      Author: Denis Shreiber
 */

#include "log.h"
#include "string.h"
#include "usart.h"
#include "settings/settings.h"
#include "flash_fifo/flash_fifo.h"
#include "rtos.h"

//===============================================================

static void custom_output_callback(const zf_log_message *msg, void *arg)
{
	(void)arg;
	/* p points to the log message end. By default, message is not terminated
	 * with 0, but it has some space allocated for EOL area, so there is always
	 * some place for terminating zero in the end (see ZF_LOG_EOL_SZ define in
	 * zf_log.c).
	 */

	if (strstr(msg->buf, " V ")) {
		user_printf("%s\r\n", msg->buf);
	}
	if (strstr(msg->buf, " D ")) {
		// Синий
		user_printf("\033[34m%s\033[0m\r\n", msg->buf);
	}
	if (strstr(msg->buf, " I ")) {
		// Зеленый
		user_printf("\033[32m%s\033[0m\r\n", msg->buf);
	}
	if (strstr(msg->buf, " W ")) {
		// Желтый
		user_printf("\033[33m%s\033[0m\r\n", msg->buf);
	}
	if (strstr(msg->buf, " E ")) {
		// Красный
		user_printf("\033[31m%s\033[0m\r\n", msg->buf);
	}
	if (strstr(msg->buf, " F ")) {
		// Красный подчеркнутый
		user_printf("\033[4;31m%s\033[0m\r\n", msg->buf);
	}

	/// @todo Опасно так прибавлять \r\n. Можно вылезти за границы массива, елси лог большой
	const char *eol = "\n";
	memcpy(msg->p, eol, sizeof(eol) - 1);
	*(msg->p + (sizeof(eol) - 1)) = 0;

//#ifndef DEBUG
	flash_fifo_put(&log_fifo, msg->buf, strlen(msg->buf));
//#endif
}

//===============================================================

void log_init() {
	const unsigned put_mask = ZF_LOG_PUT_STD;
	zf_log_set_output_v(put_mask, 0, custom_output_callback);
	zf_log_set_output_level(settings2_ram.log_level);
}

char* log_pop() {
	static char out_buf[512];

	uint16_t len = flash_fifo_pop(&log_fifo, out_buf);

	if (len) {
		out_buf[len] = '\0';
		return out_buf;
	}
	else {
		return NULL;
	}

	return NULL;
}

bool log_available() {
	return !flash_fifo_isEmpty(&log_fifo);
}


