/*
 * log.h
 *
 *  Created on: 31 янв. 2018 г.
 *      Author: Denis Shreiber
 */

#ifndef LOG_H_
#define LOG_H_

#include "zf_log/zf_log.h"
#include "stdbool.h"
#include <stdarg.h>

// ========================================================

extern int user_printf(const char* format, ...);
extern int user_vprintf(const char* format, va_list arg);

void log_init();
char* log_pop();
bool log_available();

#endif /* LOG_H_ */
