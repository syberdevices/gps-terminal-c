/*
 * flash_fifo_settings.h
 *
 *  Created on: 5 февр. 2018 г.
 *      Author: oleynik
 */

#ifndef FLASH_FIFO_FLASH_FIFO_SETTINGS_H_
#define FLASH_FIFO_FLASH_FIFO_SETTINGS_H_

//========================================

#include "spi_flash/spi_flash.h"
#include "rtos.h"

//========================================

#define FLASH_FIFO_DATA_WRITE(data, len, writeAddr)		SPI_FLASH_BufferWrite((uint8_t*)data, writeAddr, len)
#define FLASH_FIFO_DATA_READ(data, len, readAddr)		SPI_FLASH_BufferRead((uint8_t*)data, readAddr, len)
#define FLASH_FIFO_PAGE_ERASE(addr)						SPI_FLASH_SectorErase(addr)
#define FLASH_FIFO_BLOCK_ERASE(addr)					SPI_FLASH_EraseBlock64(addr)
#define FLASH_FIFO_MUTEX_WAIT(ms)						osRecursiveMutexWait(fifoRecursiveMutexHandle, ms)
#define FLASH_FIFO_MUTEX_RELEASE()						osRecursiveMutexRelease(fifoRecursiveMutexHandle)

#define FLASH_FIFO_SUCCESS_CODE							0

//========================================

#endif /* FLASH_FIFO_FLASH_FIFO_SETTINGS_H_ */
