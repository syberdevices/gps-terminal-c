// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/*
 * flash_fifo.c
 *
 *  Created on: 5 февр. 2018 г.
 *      Author: oleynik
 */

#include "flash_fifo_settings.h"
#include "flash_fifo.h"
#include "crc32/crc32.h"
#include "rtos.h"
#include "main.h"

//========================================

#define WAIT_TIMEOUT								(2*60*1000)
#define EMPTY_DATA									0xFFFFFFFF

#define PUT_DATA(data, len, writePoint)				do {\
														FLASH_FIFO_DATA_WRITE(&len, sizeof(len), writePoint);\
														FLASH_FIFO_DATA_WRITE(data, len, writePoint + sizeof(len));\
													} while(0)

#define CHECK_RES(goto_label) 						do {										\
														if (res != FLASH_FIFO_SUCCESS_CODE) {	\
															fifo->private.repeats++;			\
															flash_fifo_erase_full_memory(fifo);	\
															goto goto_label;					\
														}										\
													}while(0);

/*#define CHECK_RES_RETURN_WITH_ERASE(ret) 			do {										\
														if (res != FLASH_FIFO_SUCCESS_CODE) {	\
															flash_fifo_erase_full_memory(fifo);	\
															return ret;							\
														}										\
													}while(0);*/

#define CHECK_RES_RETURN(ret)						do {										\
														if (res != FLASH_FIFO_SUCCESS_CODE) {	\
															return ret;							\
														}										\
													}while(0);

//========================================

#define PAGE_SIZE						(4 * 1024)
#define USER_REGION_SIZE				(64 * 1024)
#define FLASH_SIZE						(16 * 1024 * 1024)
#define POINTER_REGION_SIZE				PAGE_SIZE

// Разбиваем так: 64 Кб под систему + 1 Мб под логи + 14,936 Мб под треки

#define LOG_FIFO_ADDR					(USER_REGION_SIZE)
#define LOG_FIFO_FULL_SIZE				(1024 * 1024)
#define LOG_FIFO_SIZE					(LOG_FIFO_FULL_SIZE - (2 * POINTER_REGION_SIZE))
#define LOG_FIFO_READ_POINTER_ADDR		(LOG_FIFO_ADDR + LOG_FIFO_SIZE)
#define LOG_FIFO_WRITE_POINTER_ADDR		(LOG_FIFO_READ_POINTER_ADDR + POINTER_REGION_SIZE)

#define TRACK_FIFO_ADDR					(LOG_FIFO_ADDR + LOG_FIFO_FULL_SIZE)
#define TRACK_FIFO_FULL_SIZE			(FLASH_SIZE - USER_REGION_SIZE - LOG_FIFO_FULL_SIZE)
#define TRACK_FIFO_SIZE					(TRACK_FIFO_FULL_SIZE - (2 * POINTER_REGION_SIZE))
#define TRACK_FIFO_READ_POINTER_ADDR	(TRACK_FIFO_ADDR + TRACK_FIFO_SIZE)
#define TRACK_FIFO_WRITE_POINTER_ADDR	(TRACK_FIFO_READ_POINTER_ADDR + POINTER_REGION_SIZE)


fifo_t log_fifo = {
		.settings.flash_size = FLASH_SIZE,
		.settings.page_size = PAGE_SIZE,
		.settings.read_pointer_pages = 1,
		.settings.write_pointer_pages = 1,
		.settings.read_pointer_size = POINTER_REGION_SIZE, 							//(FLASH_FIFO_READ_POINTER_PAGES * FLASH_FIFO_PAGE_SIZE)
		.settings.write_pointer_size = POINTER_REGION_SIZE, 						//(FLASH_FIFO_WRITE_POINTER_PAGES * FLASH_FIFO_PAGE_SIZE)
		.settings.fifo_addr = LOG_FIFO_ADDR,										//(FLASH_FIFO_USER_PAGES * FLASH_FIFO_PAGE_SIZE)
		.settings.fifo_size = LOG_FIFO_SIZE,										//(FLASH_FIFO_FLASH_SIZE - fifo->settings.fifo_addr - READ_POINTER_SIZE - WRITE_POINTER_SIZE)
		.settings.read_pointer_addr = LOG_FIFO_READ_POINTER_ADDR,					//(fifo->settings.fifo_addr + fifo->settings.fifo_size)
		.settings.write_pointer_addr = LOG_FIFO_WRITE_POINTER_ADDR,					//(READ_POINTER_ADDR + READ_POINTER_SIZE)
		.settings.max_size = 512,

		.private.readPoint = EMPTY_DATA,
		.private.writePoint = EMPTY_DATA,
		.private.point4readPoint = EMPTY_DATA,
		.private.point4writePoint = EMPTY_DATA,
		.private.repeats = 0,
		.private.isInited = false,
};

fifo_t track_fifo = {
		.settings.flash_size = FLASH_SIZE,
		.settings.page_size = PAGE_SIZE,
		.settings.read_pointer_pages = 1,
		.settings.write_pointer_pages = 1,
		.settings.read_pointer_size = POINTER_REGION_SIZE,
		.settings.write_pointer_size = POINTER_REGION_SIZE,
		.settings.fifo_addr = TRACK_FIFO_ADDR,
		.settings.fifo_size = TRACK_FIFO_SIZE,
		.settings.read_pointer_addr = TRACK_FIFO_READ_POINTER_ADDR,
		.settings.write_pointer_addr = TRACK_FIFO_WRITE_POINTER_ADDR,
		.settings.max_size = 512,

		.private.readPoint = EMPTY_DATA,
		.private.writePoint = EMPTY_DATA,
		.private.point4readPoint = EMPTY_DATA,
		.private.point4writePoint = EMPTY_DATA,
		.private.repeats = 0,
		.private.isInited = false,
};

//========================================

static uint32_t search_point(fifo_t* fifo, uint32_t startAddr) {
	uint32_t size = startAddr == fifo->settings.read_pointer_addr ? fifo->settings.read_pointer_size : fifo->settings.write_pointer_size;
	uint32_t half_size = size / 2;
	uint32_t point = startAddr + half_size;
	uint32_t data = EMPTY_DATA;
	uint32_t next_data = EMPTY_DATA;
	int res = FLASH_FIFO_SUCCESS_CODE;

	while(1) {
		res = FLASH_FIFO_DATA_READ(&data, 4, point);
		if (res != FLASH_FIFO_SUCCESS_CODE) {
			return fifo->settings.fifo_addr + fifo->settings.fifo_size + 1;
		}
		if (half_size > 4) {
			half_size = half_size / 2;
		}
		if (data == EMPTY_DATA) {
			if (point == startAddr) {
				if (startAddr == fifo->settings.read_pointer_addr) {
					fifo->private.point4readPoint = fifo->settings.read_pointer_addr;
				}
				else {
					fifo->private.point4writePoint = fifo->settings.write_pointer_addr;
				}
				return EMPTY_DATA;
			}
			point = point - half_size;
		}
		else {
			if (point >= startAddr + size - 4) {
				if (startAddr == fifo->settings.read_pointer_addr) {
					fifo->private.point4readPoint = point + 4;
				}
				else {
					fifo->private.point4writePoint = point + 4;
				}
				return data;
			}
			else {
				res = FLASH_FIFO_DATA_READ(&next_data, 4, point + 4);
				if (res != FLASH_FIFO_SUCCESS_CODE) {
					return fifo->settings.fifo_addr + fifo->settings.fifo_size + 1;
				}
				if (next_data == EMPTY_DATA) {
					if (startAddr == fifo->settings.read_pointer_addr) {
						fifo->private.point4readPoint = point + 4;
					}
					else {
						fifo->private.point4writePoint = point + 4;
					}
					return data;
				}
			}
			point = point + half_size;
		}
	}
}

static int putReadPoint(fifo_t* fifo) {
	int res = 0;
	if (fifo->private.point4readPoint < fifo->settings.read_pointer_addr + fifo->settings.read_pointer_size) {
		res = FLASH_FIFO_DATA_WRITE(&fifo->private.readPoint, 4, fifo->private.point4readPoint);
		CHECK_RES_RETURN(-1);

		fifo->private.point4readPoint += 4;
	}
	else {
		res = FLASH_FIFO_PAGE_ERASE(fifo->settings.read_pointer_addr);
		CHECK_RES_RETURN(-1);

		res = FLASH_FIFO_DATA_WRITE(&fifo->private.readPoint, 4, fifo->settings.read_pointer_addr);
		CHECK_RES_RETURN(-1);

		fifo->private.point4readPoint  = fifo->settings.read_pointer_addr + 4;

		uint32_t predPoint = 0;
		res = FLASH_FIFO_DATA_READ(&predPoint, 4, fifo->private.point4readPoint - 4);
		CHECK_RES_RETURN(-1);

		res = FLASH_FIFO_PAGE_ERASE(fifo->settings.read_pointer_addr);
		CHECK_RES_RETURN(-1);

		res = FLASH_FIFO_DATA_WRITE(&predPoint, 4, fifo->settings.read_pointer_addr);
		CHECK_RES_RETURN(-1);

		fifo->private.point4readPoint  = fifo->settings.read_pointer_addr + 4;
		res = FLASH_FIFO_DATA_WRITE(&fifo->private.readPoint, 4, fifo->private.point4readPoint);
		CHECK_RES_RETURN(-1);

		fifo->private.point4readPoint += 4;
	}
	return FLASH_FIFO_SUCCESS_CODE;
}

static int putWritePoint(fifo_t* fifo) {
	int res = FLASH_FIFO_SUCCESS_CODE;
	if (fifo->private.point4writePoint < fifo->settings.write_pointer_addr + fifo->settings.write_pointer_size) {
		res = FLASH_FIFO_DATA_WRITE(&fifo->private.writePoint, 4, fifo->private.point4writePoint);
		CHECK_RES_RETURN(-1);

		fifo->private.point4writePoint += 4;
	}
	else {
		uint32_t predPoint = 0;
		res = FLASH_FIFO_DATA_READ(&predPoint, 4, fifo->private.point4writePoint - 4);
		CHECK_RES_RETURN(-1);

		res = FLASH_FIFO_PAGE_ERASE(fifo->settings.write_pointer_addr);
		if (res != FLASH_FIFO_SUCCESS_CODE) {
			return -1;
		}
		res = FLASH_FIFO_DATA_WRITE(&predPoint, 4, fifo->settings.write_pointer_addr);
		CHECK_RES_RETURN(-1);

		fifo->private.point4writePoint  = fifo->settings.write_pointer_addr + 4;
		res = FLASH_FIFO_DATA_WRITE(&fifo->private.writePoint, 4, fifo->private.point4writePoint);
		CHECK_RES_RETURN(-1);

		fifo->private.point4writePoint += 4;
	}
	return FLASH_FIFO_SUCCESS_CODE;
}

static int writeData(fifo_t* fifo, void *data, uint16_t len) {
	int res = FLASH_FIFO_SUCCESS_CODE;
//	bool isEmty = flash_fifo_isEmty(fifo);

	uint32_t remain = fifo->settings.page_size - fifo->private.writePoint % fifo->settings.page_size;
	if (len + sizeof(len) + 4 >= remain) {
		fifo->private.writePoint += remain;
		if (fifo->private.writePoint >= fifo->settings.fifo_addr + fifo->settings.fifo_size) {
			fifo->private.writePoint = fifo->settings.fifo_addr;
		}

		res = FLASH_FIFO_PAGE_ERASE(fifo->private.writePoint);
		CHECK_RES_RETURN(-1);


		if (fifo->private.readPoint / fifo->settings.page_size == fifo->private.writePoint / fifo->settings.page_size && fifo->private.readPoint >= fifo->private.writePoint) {
			fifo->private.readPoint = fifo->private.writePoint + fifo->settings.page_size;
			if (fifo->private.readPoint >= fifo->settings.fifo_addr + fifo->settings.fifo_size) {
				fifo->private.readPoint = fifo->settings.fifo_addr;
			}
			res = putReadPoint(fifo);
			CHECK_RES_RETURN(-1);

		}
	}
//	if (isEmty) {
//		fifo->private.readPoint = fifo->private.writePoint;
//		res = putReadPoint(fifo);
//		CHECK_RES_RETURN(-1);
//
//	}

	fifo->private.writePoint += (len + sizeof(len) + 4);
	res = putWritePoint(fifo);
	CHECK_RES_RETURN(-1);

	fifo->private.writePoint -= (len + sizeof(len) + 4);
	res = FLASH_FIFO_DATA_WRITE(&len, sizeof(len), fifo->private.writePoint);
	CHECK_RES_RETURN(-1);

	res = FLASH_FIFO_DATA_WRITE(data, len, fifo->private.writePoint + sizeof(len));
	CHECK_RES_RETURN(-1);

	uint32_t crc = crc32_get(&len, sizeof(len), 0);
	crc = crc32_get(data, len, crc);

	res = FLASH_FIFO_DATA_WRITE(&crc, 4, fifo->private.writePoint + len + sizeof(len));
	CHECK_RES_RETURN(-1);

	fifo->private.writePoint += (len + sizeof(len) + 4);

	return FLASH_FIFO_SUCCESS_CODE;
}

static int checkPoint(fifo_t* fifo, uint32_t point) {
	if (point < fifo->settings.fifo_addr || point >= fifo->settings.fifo_addr + fifo->settings.fifo_size) {
		return -1;
	}
	return 0;
}


static int flash_fifo_init(fifo_t* fifo) {
	int res = FLASH_FIFO_SUCCESS_CODE;
	repeat:
	if (fifo->private.repeats == 2) {
		fifo->private.repeats = 0;
		return -1;
	}
	fifo->private.readPoint = search_point(fifo, fifo->settings.read_pointer_addr);
	fifo->private.writePoint = search_point(fifo, fifo->settings.write_pointer_addr);

	if (checkPoint(fifo, fifo->private.readPoint) != 0) {
		if (fifo->private.readPoint != EMPTY_DATA) {
			flash_fifo_erase_full_memory(fifo);
//			fifo->private.repeats++;
			goto repeat;
		}
	}

	if (checkPoint(fifo, fifo->private.writePoint) != 0) {
		if (fifo->private.writePoint != EMPTY_DATA) {
			flash_fifo_erase_full_memory(fifo);
//			fifo->private.repeats++;
			goto repeat;
		}
	}

	if (fifo->private.writePoint != EMPTY_DATA) {
		uint32_t point = EMPTY_DATA;
		res = FLASH_FIFO_DATA_READ(&point, 4, fifo->private.writePoint);
		CHECK_RES(repeat);

		if (checkPoint(fifo, fifo->private.writePoint) == -1 || point != EMPTY_DATA) {
			res = FLASH_FIFO_DATA_READ(&point, 4, fifo->private.point4writePoint - 8);
			CHECK_RES(repeat);

			uint32_t remain = fifo->settings.page_size - point % fifo->settings.page_size;
			fifo->private.writePoint = point;
			fifo->private.writePoint += remain;
			if (fifo->private.writePoint >= fifo->settings.fifo_addr + fifo->settings.fifo_size) {
				fifo->private.writePoint = fifo->settings.fifo_addr;
			}
			res = FLASH_FIFO_PAGE_ERASE(fifo->private.writePoint);
			CHECK_RES(repeat);

			if (fifo->private.readPoint / fifo->settings.page_size == fifo->private.writePoint / fifo->settings.page_size && fifo->private.readPoint >= fifo->private.writePoint) {
				fifo->private.readPoint = fifo->private.writePoint + fifo->settings.page_size;
				if (fifo->private.readPoint >= fifo->settings.fifo_addr + fifo->settings.fifo_size) {
					fifo->private.readPoint = fifo->settings.fifo_addr;
				}
				res = putReadPoint(fifo);
				CHECK_RES(repeat);
			}
			uint32_t free_slots[2];
			if (fifo->private.point4writePoint < fifo->settings.write_pointer_addr + fifo->settings.write_pointer_size - 8) {
				res = FLASH_FIFO_DATA_READ(free_slots, 8, fifo->private.point4writePoint);
				CHECK_RES(repeat);

				if (free_slots[0] == EMPTY_DATA && free_slots[1] == EMPTY_DATA) {
					res = FLASH_FIFO_DATA_WRITE(&point, 4, fifo->private.point4writePoint);
					CHECK_RES(repeat);

					fifo->private.point4writePoint += 4;
					res = putWritePoint(fifo);
					CHECK_RES(repeat);

				}
				else {
					res = FLASH_FIFO_PAGE_ERASE(fifo->settings.write_pointer_addr);
					CHECK_RES(repeat);

					fifo->private.point4writePoint = fifo->settings.write_pointer_addr;
					res = FLASH_FIFO_DATA_WRITE(&point, 4, fifo->private.point4writePoint);
					CHECK_RES(repeat);

					fifo->private.point4writePoint += 4;
					res = putWritePoint(fifo);
					CHECK_RES(repeat);
				}
			}
			else {
				res = FLASH_FIFO_PAGE_ERASE(fifo->settings.write_pointer_addr);
				CHECK_RES(repeat);

				fifo->private.point4writePoint = fifo->settings.write_pointer_addr;
				res = FLASH_FIFO_DATA_WRITE(&point, 4, fifo->private.point4writePoint);
				CHECK_RES(repeat);

				fifo->private.point4writePoint += 4;
				res = putWritePoint(fifo);
				CHECK_RES(repeat);

			}
		}
		else {
			res = FLASH_FIFO_DATA_READ(&point, 4, fifo->private.point4writePoint - 8);
			CHECK_RES(repeat);

			uint16_t len = 0;
			res = FLASH_FIFO_DATA_READ(&len, 2, point);
			CHECK_RES(repeat);

			if (len > fifo->settings.page_size) {
				uint32_t remain = fifo->settings.page_size - fifo->private.writePoint % fifo->settings.page_size;
				fifo->private.writePoint += remain;
				if (fifo->private.writePoint >= fifo->settings.fifo_addr + fifo->settings.fifo_size) {
					fifo->private.writePoint = fifo->settings.fifo_addr;
				}
				res = FLASH_FIFO_PAGE_ERASE(fifo->private.writePoint);
				CHECK_RES(repeat);

				if (fifo->private.readPoint / fifo->settings.page_size == fifo->private.writePoint / fifo->settings.page_size && fifo->private.readPoint >= fifo->private.writePoint) {
					fifo->private.readPoint = fifo->private.writePoint + fifo->settings.page_size;
					if (fifo->private.readPoint >= fifo->settings.fifo_addr + fifo->settings.fifo_size) {
						fifo->private.readPoint = fifo->settings.fifo_addr;
					}
					res = putReadPoint(fifo);
					CHECK_RES(repeat);
				}
				res = putWritePoint(fifo);
				CHECK_RES(repeat);
			}
			else {
				uint8_t lenAndData[len + sizeof(len)];
				res = FLASH_FIFO_DATA_READ(lenAndData, len + sizeof(len), point);
				CHECK_RES(repeat);

				uint32_t crc = 0;
				res = FLASH_FIFO_DATA_READ(&crc, 4, point + len + sizeof(len));
				CHECK_RES(repeat);

				if (crc32_get(lenAndData, sizeof(lenAndData), 0) != crc) {
					uint32_t remain = fifo->settings.page_size - fifo->private.writePoint % fifo->settings.page_size;
					fifo->private.writePoint += remain;
					if (fifo->private.writePoint >= fifo->settings.fifo_addr + fifo->settings.fifo_size) {
						fifo->private.writePoint = fifo->settings.fifo_addr;
					}
					res = FLASH_FIFO_PAGE_ERASE(fifo->private.writePoint);
					CHECK_RES(repeat);

					if (fifo->private.readPoint / fifo->settings.page_size == fifo->private.writePoint / fifo->settings.page_size && fifo->private.readPoint >= fifo->private.writePoint) {
						fifo->private.readPoint = fifo->private.writePoint + fifo->settings.page_size;
						if (fifo->private.readPoint >= fifo->settings.fifo_addr + fifo->settings.fifo_size) {
							fifo->private.readPoint = fifo->settings.fifo_addr;
						}
						res = putReadPoint(fifo);
						CHECK_RES(repeat);
					}
					res = putWritePoint(fifo);
					CHECK_RES(repeat);
				}
			}
		}
	}

	if (fifo->private.readPoint != EMPTY_DATA) {
		uint32_t data = 0;
		res = FLASH_FIFO_DATA_READ(&data, 4, fifo->private.readPoint);
		CHECK_RES(repeat);

		if (checkPoint(fifo, fifo->private.readPoint) == -1 || (data == EMPTY_DATA && fifo->private.readPoint > fifo->private.writePoint)) {
			res = FLASH_FIFO_DATA_READ(&data, 4, fifo->private.point4readPoint - 8);
			CHECK_RES(repeat);

			uint16_t len;
			res = FLASH_FIFO_DATA_READ(&len, sizeof(len), data);
			CHECK_RES(repeat);

			fifo->private.readPoint += (len + sizeof(len) + 4);
			res = putReadPoint(fifo);
			CHECK_RES(repeat);
		}
	}
	fifo->private.isInited = true;
	return 0;
}

//========================================

bool flash_fifo_isEmpty(fifo_t* fifo) {
	if (FLASH_FIFO_MUTEX_WAIT(WAIT_TIMEOUT) != 0) {
		return true;
	}

	if (!fifo->private.isInited) {
		flash_fifo_init(fifo);
	}

	bool res = fifo->private.writePoint == fifo->private.readPoint;

	FLASH_FIFO_MUTEX_RELEASE();

	return res;
}

int flash_fifo_used_space(fifo_t* fifo) {
	int used_space;

	if (FLASH_FIFO_MUTEX_WAIT(WAIT_TIMEOUT) != 0) {
		return -1;
	}

	if (!fifo->private.isInited) {
		flash_fifo_init(fifo);
	}

	// Если указатель чтения догоняет указател записи и последний не перескочил на начало
	if (fifo->private.writePoint - fifo->private.readPoint > 0) {
		used_space = fifo->private.writePoint - fifo->private.readPoint;
	}
	// Если указатель записи уже перескочил на начало, а указатель чтения еще остался в конце
	else if (fifo->private.writePoint - fifo->private.readPoint < 0) {
		used_space = fifo->settings.fifo_addr + fifo->settings.fifo_size - fifo->private.readPoint;
		used_space += fifo->private.writePoint - fifo->settings.fifo_addr;
	}
	else {
		used_space = 0;
	}

	FLASH_FIFO_MUTEX_RELEASE();

	return used_space;
}

int flash_fifo_put(fifo_t* fifo, void *data, uint16_t len) {
	int ret = 0;
	int res = FLASH_FIFO_SUCCESS_CODE;

	if (FLASH_FIFO_MUTEX_WAIT(WAIT_TIMEOUT) != 0) {
		return -1;
	}

	repeat:

	if (fifo->private.repeats == 2) {
		fifo->private.repeats = 0;
		ret = -1;
		goto exit;
	}
	if (!fifo->private.isInited) {
		if (flash_fifo_init(fifo) != 0) {
			ret = -1;
			goto exit;
		}
	}
	if (fifo->private.writePoint != EMPTY_DATA && fifo->private.readPoint != EMPTY_DATA) {
		res = writeData(fifo, data, len);
		CHECK_RES(repeat);
	}
	else if (fifo->private.writePoint != EMPTY_DATA && fifo->private.readPoint == EMPTY_DATA) {
		fifo->private.readPoint = fifo->private.writePoint;
		res = writeData(fifo, data, len);
		CHECK_RES(repeat);
	}
	else if (fifo->private.writePoint == EMPTY_DATA) {
		fifo->private.writePoint = fifo->settings.fifo_addr;
		fifo->private.readPoint = fifo->settings.fifo_addr;
		res = putReadPoint(fifo);
		CHECK_RES(repeat);
		res = FLASH_FIFO_PAGE_ERASE(fifo->private.writePoint);
		CHECK_RES(repeat);

		fifo->private.writePoint += (len + sizeof(len) + 4);
		res = putWritePoint(fifo);
		CHECK_RES(repeat);

		fifo->private.writePoint -= (len + sizeof(len) + 4);
		res = FLASH_FIFO_DATA_WRITE(&len, sizeof(len), fifo->private.writePoint);
		CHECK_RES(repeat);

		res = FLASH_FIFO_DATA_WRITE(data, len, fifo->private.writePoint + sizeof(len));
		CHECK_RES(repeat);

		uint32_t crc = crc32_get(&len, sizeof(len), 0);
		crc = crc32_get(data, len, crc);

		res = FLASH_FIFO_DATA_WRITE(&crc, 4, fifo->private.writePoint + len + sizeof(len));
		CHECK_RES(repeat);

		fifo->private.writePoint += (len + sizeof(len) + 4);
	}

	exit:

	FLASH_FIFO_MUTEX_RELEASE();

	return ret;
}

int flash_fifo_pop(fifo_t* fifo, void *data) {
	int ret = 0;
	int res = FLASH_FIFO_SUCCESS_CODE;

	if (FLASH_FIFO_MUTEX_WAIT(WAIT_TIMEOUT) != 0) {
		return -1;
	}

	if (!fifo->private.isInited) {
		if (flash_fifo_init(fifo) != 0) {
			goto exit;
		}
	}

	if (!flash_fifo_isEmpty(fifo)) {
		uint16_t len = 0;
		uint32_t remain  = 0;
		uint32_t crc = 0;

		repeat:
		if (flash_fifo_isEmpty(fifo)) {
			goto exit;
		}
		remain = fifo->settings.page_size - fifo->private.readPoint % fifo->settings.page_size;
		res = FLASH_FIFO_DATA_READ(&len, sizeof(len), fifo->private.readPoint);
		if (res != 0) {
			flash_fifo_erase_full_memory(fifo);
			goto exit;
		}

		if (len > fifo->settings.max_size || remain < 4) {
			fifo->private.readPoint += remain;

			if (fifo->private.readPoint >= fifo->settings.fifo_addr + fifo->settings.fifo_size) {
				fifo->private.readPoint = fifo->settings.fifo_addr;
			}

			if (remain >= 4 && len == 0xFFFF) {
//				fifo->private.readPoint = fifo->private.writePoint;
			}
			goto repeat;
		}

		res = FLASH_FIFO_DATA_READ(&crc, 4, fifo->private.readPoint + len + sizeof(len));
		if (res != 0) {
			flash_fifo_erase_full_memory(fifo);
			goto exit;
		}

		uint8_t lenAndData[len + sizeof(len)];

		res = FLASH_FIFO_DATA_READ(lenAndData, len + sizeof(len), fifo->private.readPoint);
		if (res != 0) {
			flash_fifo_erase_full_memory(fifo);
			goto exit;
		}

		if (crc32_get(lenAndData, sizeof(lenAndData), 0) != crc) {
			fifo->private.readPoint += remain;
			if (fifo->private.readPoint >= fifo->settings.fifo_addr + fifo->settings.fifo_size) {
				fifo->private.readPoint = fifo->settings.fifo_addr;
			}
			goto repeat;
		}

		res = FLASH_FIFO_DATA_READ(data, len, fifo->private.readPoint + sizeof(len));
		if (res != 0) {
			flash_fifo_erase_full_memory(fifo);
			goto exit;
		}

		fifo->private.readPoint += (len + sizeof(len) + 4);
		if (fifo->private.readPoint >= fifo->settings.fifo_addr + fifo->settings.fifo_size) {
			fifo->private.readPoint = fifo->settings.fifo_addr;
		}
		res = putReadPoint(fifo);
		if (res != 0) {
			flash_fifo_erase_full_memory(fifo);
			goto exit;
		}

		ret = len;
	}

	exit:

	FLASH_FIFO_MUTEX_RELEASE();

	return ret;
}

int flash_fifo_erase_full_memory(fifo_t* fifo) {
	int ret = 0;

	if (FLASH_FIFO_MUTEX_WAIT(WAIT_TIMEOUT) != 0) {
		return -1;
	}

	fifo->private.isInited = false;

	WDG_RESET(TASK_ALL);
	for (uint32_t addr = fifo->settings.fifo_addr; addr < (fifo->settings.write_pointer_addr + fifo->settings.write_pointer_size); addr += 64*1024) {

		ret = FLASH_FIFO_BLOCK_ERASE(addr);
		WDG_RESET(TASK_ALL);

		if (ret != 0) {
			goto exit;
		}
	}

	exit:

	FLASH_FIFO_MUTEX_RELEASE();

	return ret;
}

//========================================
