/*
 * flash_fifo.h
 *
 *  Created on: 5 февр. 2018 г.
 *      Author: oleynik
 */

#ifndef FLASH_FIFO_FLASH_FIFO_H_
#define FLASH_FIFO_FLASH_FIFO_H_

//========================================

#include "stdbool.h"
#include "stdint.h"

//========================================

typedef struct
{
	const uint32_t flash_size;
	const uint16_t page_size;
	const uint8_t  read_pointer_pages;
	const uint8_t  write_pointer_pages;
	const uint16_t read_pointer_size;
	const uint16_t write_pointer_size;
	const uint32_t fifo_addr;
	const uint32_t fifo_size;
	const uint32_t read_pointer_addr;
	const uint32_t write_pointer_addr;
	const uint32_t max_size;
}fifo_settings_t;

typedef struct
{
	uint32_t readPoint;
	uint32_t writePoint;
	uint32_t point4readPoint;
	uint32_t point4writePoint;
	uint8_t  repeats;
	bool     isInited;
}fifo_private_t;

typedef struct
{
	const fifo_settings_t settings;
	fifo_private_t private;
}fifo_t;

/**
 * @return true, если пустой буфер fifo
 */
bool flash_fifo_isEmpty(fifo_t* fifo);

/**
 * @return Количество используемой FIFO в байтах
 */
int flash_fifo_used_space(fifo_t* fifo);

/**
 * @param data указатель на данные, которые нужно положить в fifo
 * @param len длина данных
 * @return 0, если успех, -1, если ошибка
 */
int flash_fifo_put(fifo_t* fifo, void *data, uint16_t len);

/**
 * @param data указатель, куда будут извлечены данные из fifo
 * @return длина данных, -1, если ошибка
 */
int flash_fifo_pop(fifo_t* fifo, void *data);

/**
 * @brief стирание всей фифо
 * @return 0, если успех, -1, если ошибка
 */
int flash_fifo_erase_full_memory(fifo_t* fifo);

//========================================

extern fifo_t log_fifo;
extern fifo_t track_fifo;

//========================================

#endif /* FLASH_FIFO_FLASH_FIFO_H_ */
