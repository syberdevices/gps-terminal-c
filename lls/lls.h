/*
 * lls.h
 *
 *  Created on: 14 февр. 2021 г.
 *      Author: chuyec
 */

#ifndef LLS_H_
#define LLS_H_

#include <stdint.h>

// ==========================================================================

typedef struct {
	uint16_t fuel_level;
	int8_t temp;
} lls_data_t;

// ==========================================================================

/**
 * Инициализация ДУТ
 */
void lls_init();

/**
 * Настройка uart под LLS. Фактически меняется скорость на 19200 и отключаюстя прерывания
 */
void lls_init_serial();

/**
 * Запросить данные ДУТ
 *
 * Результатом будет температура и уровень топлива датчика по адресу addr
 * @param addr	Адрес ДУТ [0 - 255]
 * @param plls	Указатель на структуру данных ДУТ \ref lls_data_t
 * @return 0 - при успехе. Иначе - чтение не удалось
 */
int lls_read(uint8_t addr, lls_data_t * plls);

#endif /* LLS_H_ */
