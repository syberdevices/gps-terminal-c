/*
 * lls.c
 *
 *  Created on: 14 февр. 2021 г.
 *      Author: chuyec
 */

#include "lls.h"
#include <rs485/rs485.h>

// ===========================================================================================

uint8_t buff[12];

// ===========================================================================================

static unsigned char dallas_crc8(const unsigned char * data, const unsigned int size)
{
    unsigned char crc = 0;
    for ( unsigned int i = 0; i < size; ++i )
    {
        unsigned char inbyte = data[i];
        for ( unsigned char j = 0; j < 8; ++j )
        {
            unsigned char mix = (crc ^ inbyte) & 0x01;
            crc >>= 1;
            if ( mix ) crc ^= 0x8C;
            inbyte >>= 1;
        }
    }
    return crc;
}

// ===========================================================================================

void lls_init() {

}

void lls_init_serial() {
	rs485_rx_byte_only_disable();

	(void)rs485_set_baudrate(19200);
}

int lls_read(uint8_t addr, lls_data_t * plls) {
	buff[0] = 0x31;
	buff[1] = addr;
	buff[2] = 0x06;
	buff[3] = dallas_crc8(buff, 3);

	int ret = rs485_tx(buff, 4, 200);
	if (ret != 0) {
		return ret;
	}

	int len = rs485_rx(buff, 200);
	if (len != 9) {
		if (len < 0) {
			// len - код ошибки
			return len;
		}
		else {
			// Просто неправильная длина ответа
			return -1;
		}
	}

	if (dallas_crc8(buff, 8) != buff[8]) {
		return -1;
	}

	plls->temp = (int8_t)buff[3];
	plls->fuel_level = (((uint16_t)buff[5] & 0xFF) << 8) | ((uint16_t)buff[4] & 0xFF);

	return 0;
}

