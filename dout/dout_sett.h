/*
 * dout_sett.h
 *
 *  Created on: 15 янв. 2018 г.
 *      Author: oleynik
 */

#ifndef DOUT_SETT_H_
#define DOUT_SETT_H_

#include "stdint.h"
#include "stdbool.h"
#include "main.h"

//====================================================================

#define TIMER_PERIOD				50 ///ms, 1 тик
#define GPIO_WRITE(obj, PinState) 	HAL_GPIO_WritePin(dout_Objects[obj].port, dout_Objects[obj].pin, PinState)

//====================================================================

typedef struct {
	GPIO_TypeDef *port;
	uint16_t pin;
	uint32_t durability; /// время в тиках
	uint32_t period;	/// время в тиках
	uint32_t counter;
	uint32_t timeout; /// время в тиках
	uint32_t counter_timeout;
	bool state;
	bool inversion;
	void (*timeout_callback)();
}dout_obj_t;

typedef enum {
	LED_GSM_GREEN,
	LED_GSM_RED,
	LED_GPS_GREEN,
	LED_GPS_RED,

	LED_NUM,
}dout_t;

//====================================================================

extern dout_obj_t dout_Objects[LED_NUM];

//====================================================================

#endif /* DOUT_SETT_H_ */
