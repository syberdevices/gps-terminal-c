// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/*
 * dout_sett.c
 *
 *  Created on: 17 янв. 2018 г.
 *      Author: oleynik
 */

#include "dout_sett.h"

//====================================================================

dout_obj_t dout_Objects[LED_NUM] = {
		{
			.durability = 0,
			.period = 0,
			.timeout = 0,
			.counter_timeout = 0,
			.pin = LED_GSM_GREEN_Pin,
			.port = LED_GSM_GREEN_GPIO_Port,
			.counter = 0,
			.inversion = false,
			.state = false,
			.timeout_callback = NULL
		},
		{
			.durability = 0,
			.period = 0,
			.timeout = 0,
			.counter_timeout = 0,
			.pin = LED_GSM_RED_Pin,
			.port = LED_GSM_RED_GPIO_Port,
			.counter = 0,
			.inversion = false,
			.state = false,
			.timeout_callback = NULL
		},
		{
			.durability = 0,
			.period = 0,
			.timeout = 0,
			.counter_timeout = 0,
			.pin = LED_GPS_GREEN_Pin,
			.port = LED_GPS_GREEN_GPIO_Port,
			.counter = 0,
			.inversion = false,
			.state = false,
			.timeout_callback = NULL
		},
		{
			.durability = 0,
			.period = 0,
			.timeout = 0,
			.counter_timeout = 0,
			.pin = LED_GPS_RED_Pin,
			.port = LED_GPS_RED_GPIO_Port,
			.counter = 0,
			.inversion = false,
			.state = false,
			.timeout_callback = NULL
		},
};
