/*
 * discreteOutput.h
 *
 *  Created on: 12 янв. 2018 г.
 *      Author: oleynik
 */

#ifndef DOUT_H_
#define DOUT_H_

#include <dout/dout_sett.h>

//====================================================================

/**
 * @brief Метод установки длительности и периода сигнала объекта дискретного выхода
 * @param obj Объект дискретного выхода
 * @param durability Длительность импульса
 * @param period Период сигнала
 * @param timeout Время, в течении которого будет выдаваться периодический сигнал
 * @note При повторном вызове функции таймаут обновляется, это надо учитывать
 */
void dout_setPeriodic(dout_t obj, uint32_t durability, uint32_t period, uint32_t timeout);

/**
 * @brief Метод установки длительности одиночного импульса объекта дискретного выхода
 * @param obj Объект дискретного выхода
 * @param durability Длительность импульса
 */
void dout_setOneShot(dout_t obj, uint32_t durability);

/**
 * @brief Проверка длительности включения, надо вызывать в прерывании таймера
 */
void dout_Poll();

/**
 * @brief Метод включения объекта дискретного выхода
 * @param obj Объект дискретного выхода
 */
void dout_turnOn(dout_t obj);

/**
 * @brief Метод выключения объекта дискретного выхода
 * @param obj Объект дискретного выхода
 */
void dout_turnOff(dout_t obj);

//====================================================================

#endif /* DOUT_H_ */
