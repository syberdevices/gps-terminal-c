// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/*
 * discreteOutput.c
 *
 *  Created on: 12 янв. 2018 г.
 *      Author: oleynik
 */


#include <dout/dout.h>
#include "rtos.h"

//====================================================================

static void dout_setState(dout_t obj, bool PinState) {
	if (PinState != dout_Objects[obj].state) {
		dout_Objects[obj].state = PinState;
		if(dout_Objects[obj].inversion) PinState = !PinState;
		GPIO_WRITE(obj, PinState);
	}
}

static void dout_checkObj(dout_t obj) {
	if (dout_Objects[obj].durability == 0) {
		dout_setState(obj, 0);
	}
	else if (dout_Objects[obj].durability > 0 &&  dout_Objects[obj].period == 0) {
		if (dout_Objects[obj].counter <= dout_Objects[obj].durability) {
			dout_setState(obj, 1);
		}
		else {
			dout_setState(obj, 0);
			dout_Objects[obj].durability = 0;
		}
	}
	else if (dout_Objects[obj].durability <= dout_Objects[obj].period) {
		if (dout_Objects[obj].counter <= dout_Objects[obj].durability) {
			dout_setState(obj, 1);
		}
		if (dout_Objects[obj].counter > dout_Objects[obj].durability && dout_Objects[obj].counter <= dout_Objects[obj].period) {
			dout_setState(obj, 0);
		}
		if (dout_Objects[obj].counter == dout_Objects[obj].period) {
			dout_Objects[obj].counter = 0;
		}
		if (dout_Objects[obj].timeout != 0) {
			if (dout_Objects[obj].counter_timeout == dout_Objects[obj].timeout) {
				dout_Objects[obj].durability = 0;
				if (dout_Objects[obj].timeout_callback != NULL) {
					dout_Objects[obj].timeout_callback();
				}
			}
			else {
				dout_Objects[obj].counter_timeout++;
			}
		}
	}

	dout_Objects[obj].counter++;
}

//====================================================================

void dout_setPeriodic(dout_t obj, uint32_t durability, uint32_t period, uint32_t timeout) {
	taskENTER_CRITICAL();
//	__disable_irq();
	dout_Objects[obj].durability = durability / TIMER_PERIOD;
	dout_Objects[obj].period = period / TIMER_PERIOD;
	dout_Objects[obj].counter = 0;
	dout_Objects[obj].timeout = timeout / TIMER_PERIOD;
	dout_Objects[obj].counter_timeout = 0;
	taskEXIT_CRITICAL();
//	__enable_irq();
}

void dout_setOneShot(dout_t obj, uint32_t durability) {
	dout_setPeriodic(obj, durability, 0, 0);
}

void dout_turnOn(dout_t obj) {
	dout_setPeriodic(obj, TIMER_PERIOD, TIMER_PERIOD, 0);
}

void dout_turnOff(dout_t obj) {
	dout_setPeriodic(obj, 0, 0, 0);
}

void dout_Poll() {
	for (int i = 0; i < LED_NUM; i++) {
		dout_checkObj(i);
	}
}

//====================================================================
