/*
 * din.h
 *
 *  Created on: 6 нояб. 2018 г.
 *      Author: Denis Shreiber
 */

#ifndef DIN_H_
#define DIN_H_

#include "gpio.h"

// ========================================================================

typedef enum {
	DIN_1,
	DIN_2,
	DIN_3,
	DIN_4,

	DIN_NUM
}din_num_t;

// ========================================================================

uint8_t din_get(din_num_t din);
void apply_din_state (din_num_t din);

#endif /* DIN_H_ */
