/*
 * din.c
 *
 *  Created on: 6 нояб. 2018 г.
 *      Author: Denis Shreiber
 */

#include "din.h"
#include "settings/settings.h"
#include "common.h"

// ====================================================================

volatile uint32_t din_sos_event = 0;
volatile uint32_t din_write_event = 0;
volatile uint32_t din_ignition_event = 0;

// ====================================================================

static uint8_t hal_din_get_val(din_num_t din) {
	// Оптопары дают инверсию, поэтому инвертируем

	switch (din) {
	case DIN_1:
		return HAL_GPIO_ReadPin(DIN1_NO_GPIO_Port, DIN1_NO_Pin) == GPIO_PIN_SET ? 0 : 1;

	case DIN_2:
		return HAL_GPIO_ReadPin(DIN2_NO_GPIO_Port, DIN2_NO_Pin) == GPIO_PIN_SET ? 0 : 1;

	case DIN_3:
		return HAL_GPIO_ReadPin(DIN3_NC_GPIO_Port, DIN3_NC_Pin) == GPIO_PIN_SET ? 0 : 1;

	case DIN_4:
		return HAL_GPIO_ReadPin(DIN4_NC_GPIO_Port, DIN4_NC_Pin) == GPIO_PIN_SET ? 0 : 1;

	default:
		return 0;
	}
}

// ====================================================================

void apply_din_state (din_num_t din) {
	bool calc_crc = false;
	uint8_t din_state = din_get(din);
	uint8_t din_mask = (1 << din);
	di_mode_t din_mode = settings1_ram.dins[din].mode;

	// Сбрасываем счетчик, если изменилась настройка дискретного входа
	if (din_mode != DI_MODE_PULSE_COUNT) {
		if (board_status.io.din_counter[din] != 0) {
			board_status.io.din_counter[din] = 0;
		}

		if (cache.counters[din] != 0) {
			cache.counters[din] = 0;
			calc_crc = true;
		}
	}

	if (din_mode == DI_MODE_IGNITION) {
		cache.din &= ~din_mask;
		cache.din |= (din_state << din);
		ignition_set(din_state == 0 ? IGNITION_OFF : IGNITION_ON);
		din_ignition_event++;
	}


	if ( (cache.din & din_mask) != (din_state << din) ) {
		switch (din_mode) {
//			case DI_MODE_IGNITION:
//				ignition_set((bool)din_state);
//				din_ignition_event++;
//				break;

			case DI_MODE_PULSE_COUNT:
				// Считаем только фронты
				if (din_state) {
					board_status.io.din_counter[din]++;
					cache.counters[din] = board_status.io.din_counter[din];
				}
				break;

			case DI_MODE_SOS:
				// Только фронт
				if (din_state) {
					din_sos_event++;
				}
				break;

			case DI_MODE_WRITE_POINT_TRIGGER:
				din_write_event++;
				break;

			default:
				break;
		}

		cache.din ^= din_mask;
		calc_crc = true;
	}

	if (calc_crc) {
		cache_crc_update();
	}

	board_status.io.din_mask = (uint32_t)cache.din;
}

uint8_t din_get(din_num_t din) {
	if (settings1_ram.dins[din].type == NORMALLY_OPEN) {
		return hal_din_get_val(din);
	}
	else {
		return hal_din_get_val(din) == 0 ? 1 : 0;
	}
}

