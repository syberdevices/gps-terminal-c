// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/** @file lis3dh.c
 * @author david.siorpaes@st.com
 * LIS3DH accelerometer device driver. Uses SPI2 interface.
 * See spiConfig() documentation for port pin configuration. 
 */
 
#include <string.h>
#include "lis3dh.h"
#include "lis3dh_reg.h"
#include "spi_phy.h"

/** @defgroup LIS3DH device driver private APIs
 * @{
 */
static int spiWrite(uint8_t addr, uint8_t* buffer, int len);
static int spiRead(uint8_t addr, uint8_t* buffer, int len);

/** @defgroup LIS3DH device driver public APIs
 * @{
 */
 
 
 /** @brief Initializes LIS3DH device with following configuration:
  * 100Hz sampling rate
  * Enable all axis
	* @retval None
  */
void lis3dhConfig(void)
{
	
////	/* Configure LIS3DH device: enable X, Y, Z axis, se 10 Hz sampling rate */
//	uint8_t val = CTRL_REG1_XEN | CTRL_REG1_YEN | CTRL_REG1_ZEN | CTRL_REG1_ODR_100;
//	spiWrite(CTRL_REG1, &val, 1);
	uint8_t val = 0x27;
	spiWrite(CTRL_REG1, &val, 1);

	val = 0x01;
	spiWrite(CTRL_REG2, &val, 1);

	val = 0x40;
	spiWrite(CTRL_REG3, &val, 1);

	val = 0x80;
	spiWrite(CTRL_REG4, &val, 1);

//	val = 0x0A;
//	spiWrite(CTRL_REG5, &val, 1);

	val = 0x02;
	spiWrite(CTRL_REG6, &val, 1);

	val = 0x2A;
	spiWrite(INT1_CFG, &val, 1);

	val = 0x02;
	spiWrite(INT1_THS, &val, 1);

	val = 0x02;
	spiWrite(INT1_DURATION, &val, 1);

//	val = 0x33;
//	spiWrite(TIME_LIMIT, &val, 1);

//	val = 0xFF;
//	spiWrite(TIME_LATENCY, &val, 1);
}

int lis3dh_set_int1_threshold(uint8_t ths) {
	return spiWrite(INT1_THS, &ths, 1);
}


/** @brief Returns current acceleration value
 * @param x: pointer where to store X acceleration value
 * @param y: pointer where to store Y acceleration value
 * @param z: pointer where to store Z acceleration value
 * @retval always 0
 */
int lis3dhGetAcc(int16_t* x, int16_t* y, int16_t* z)
{
	uint8_t buffer[6];
	
	/* Read out all 6 bytes in one shot */
	spiRead(OUT_X_L, buffer, 6);

	memcpy(x, buffer, 2);
	memcpy(y, buffer + 2, 2);
	memcpy(z, buffer + 4, 2);
	
	return 0;
}


/** @brief Set accelerometer range
 * @param range: range to be set. Can be one out of
 *               RANGE_2G, RANGE_4G, RANGE_8G, RANGE_16G
 */
void lis3dhSetRange(int8_t range)
{
	uint8_t regval;
	
	assert_param(range == RANGE_2G || range == RANGE_4G || range == RANGE_8G || range == RANGE_16G);
	
	regval = spiRead(CTRL_REG4, &regval, 1);
	regval &= ~(3 << 4);
	regval |= range;
	spiWrite(CTRL_REG4, &regval, 1);
}


/**
 * @}
 */


/** @defgroup LIS3DH device driver private APIs
 * @{
 */


/** @brief Set SPI Chip select OFF
 * @retval none
 */


/** @brief Reads given amount of data from sensor
 * @param addr: Register address to read from
 * @param buffer: Destination buffer
 * @param len: Number of bytes to be read
 * @retval Zero on success, -1 otherwise
 */
static int spiRead(uint8_t addr, uint8_t* buffer, int len)
{
	int errorcode = 0;
	uint8_t data = 0xff;
	uint8_t dataRec = 0xff;
	if(len <= 0)
		return -1;
	
	/* It's a read operation */
	addr |= SPI_READ;
	
	/* It's a multiple read operation */
	if(len > 1)
		addr |= SPI_MULTI_ACCESS;
		
	/* Transmission start: pull CS low */
	ACCEL_CS_LOW();
	
	/* Send address */

	errorcode = SPI_TRANSMIT_RECEIVE(&addr, &dataRec, 1);

	if (errorcode != 0) {
		goto exit;
	}

	while(len--){
		errorcode = SPI_TRANSMIT_RECEIVE(&data, buffer++, 1);
		if (errorcode != 0) {
			goto exit;
		}
	}

	/* Transmission end: pull CS high */
	exit:

	ACCEL_CS_HIGH();

	return errorcode;
}


/** @brief Writes given amount of data to sensor
 * @param addr: Register address to write to
 * @param buffer: Source buffer
 * @param len: Number of bytes to be written
 * @retval Zero on success, -1 otherwise
 */
static int spiWrite(uint8_t addr, uint8_t* buffer, int len)
{	
	int errorcode = 0;
	uint8_t data = 0;
	if(len <= 0)
		return -1;
	
	/* It's a multiple read operation */
	if(len > 1)
		addr |= SPI_MULTI_ACCESS;
	
	/* Transmission start: pull CS low */
	ACCEL_CS_LOW();
	
	/* Send address */

	errorcode = SPI_TRANSMIT_RECEIVE(&addr, &data, 1);

	if (errorcode != 0) {
		goto exit;
	}
		
	/* Send data */
	while(len--){
		errorcode = SPI_TRANSMIT_RECEIVE(buffer++, &data, 1);
		if (errorcode != 0) {
			goto exit;
		}
	}

	/* Transmission end: pull CS high */
	exit:

	ACCEL_CS_HIGH();
	
	return 0;
}

uint8_t accelID() {
	uint8_t id = 0;
	spiRead(WHO_AM_I, &id, 1);
	return (id == WHO_AM_I_VALUE);
}

/**
 * @}
 */
