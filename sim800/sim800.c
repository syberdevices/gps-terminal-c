/*
 * sim800.c
 *
 *  Created on: 3 апр. 2018 г.
 *      Author: Denis Shreiber
 */

#include "sim800.h"

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "tim.h"
#include "gpio.h"
#include "usart.h"
#include "rtos.h"
#include "iwdg.h"
#include "usbd_cdc_if.h"
#include "usb_device.h"

#include "log/log.h"
#include "common.h"

//=======================================================================

#define SERIAL_OBJECT				huart2

#define TIM_OBJECT					htim10
#define TIM_DBG_FREEZE()			__HAL_DBGMCU_FREEZE_TIM10()
#define TIM_CLEAR_IT(it)			__HAL_TIM_CLEAR_FLAG(&TIM_OBJECT, it)

#define QUEUE_SIZE					1024

#define RX_BUFF_LEN					2048

//=======================================================================

#define PRINT_ALL_SERIAL_DATA		1
#if PRINT_ALL_SERIAL_DATA
	#define PRINTX(data, len)		printX(data, len)
#else
	#define PRINTX(data, len)
#endif

#if PRINT_ALL_SERIAL_DATA
#include "ctype.h"
static void printX(const void *data, uint32_t len) {
	const char *ptr = data;

	for (uint32_t q = 0; q < len; q++) {
		if (ptr[q] >= 0x20 && ptr[q] < 0x7F) {
			user_printf("%c", ptr[q]);
		}
		else {
			if (ptr[q] == '\r') {
				user_printf("\\r");
			}
			else if (ptr[q] == '\n') {
				user_printf("\\n\r\n");
			}
			else {
				user_printf("{0x%x}", ptr[q]);
			}
		}
	}
}
#endif

//=======================================================================

static char rx_byte;

static char rx_buff[RX_BUFF_LEN];
static char response_buff[RX_BUFF_LEN];

static uint8_t queue_array[QUEUE_SIZE];
__IO static uint16_t queue_topindex = 0;

__IO static unsigned int tcp_data_len_to_receive = 0;
__IO static bool sleeping = false;
__IO static bool have_tcp_data = false;

sim800_bluetooth_status_t bluetooth_status = {0};
//headset_status_t headset_status;

//=======================================================================

static int tx_dma(const void *data, uint32_t len) {
	PRINTX(data, len);

	HAL_StatusTypeDef res = HAL_UART_Transmit_DMA(&SERIAL_OBJECT, (uint8_t*)data, len);
	if (res != HAL_OK) {
		return -1;
	}

	return 0;
}

static int timer_start(uint32_t timeout_ms) {
	assert_param(timeout_ms < 0xFFFF);

	HAL_TIM_Base_Stop_IT(&TIM_OBJECT);

	__HAL_TIM_SET_COUNTER(&TIM_OBJECT, 0);
	__HAL_TIM_SET_AUTORELOAD(&TIM_OBJECT, timeout_ms);

	HAL_TIM_Base_Start_IT(&TIM_OBJECT);

	return 0;
}

static int timer_stop() {
	HAL_TIM_Base_Stop_IT(&TIM_OBJECT);

	return 0;
}

static int rx_it_start(char *rx_byte) {
	// Почему-то при старте uart уже выставлен флаг прерывания, поэтому оно срабатывает.
	// Очищаем, но оно всё равно срабатывает.
	// Помимо этого, если не очищать приём, то срабатывает прерывание Frame Error. Загадка
//	HAL_UART_Abort_IT(&SERIAL_OBJECT);
//	first_rx_irq = true;
	HAL_StatusTypeDef res = HAL_UART_Receive_IT(&SERIAL_OBJECT, (uint8_t*)rx_byte, 1);
	if (res != HAL_OK) {
		return -1;
	}

	return 0;
}

static int rx_it_stop() {
	HAL_UART_Abort_IT(&SERIAL_OBJECT);

	return 0;
}

static void wait() {
	osDelay(1);
}

static void message_handler(void *commander) {
	// Проверка пришедшего сообщения здесь отсортирована по частоте приема и важности скорости обработки
	// т.е. сверху самые важные/частые, снизу - неважные/редкие

	char *_rx_buff = ((ATCommander_t *)commander)->sett.buff.rx_buff;

	// Данные TCP отправлены
	if (strstr(_rx_buff, "SEND OK")) {
		sim800_status.tcp_send_ok = true;
	}
	// Событие TCP коннекта
	else if (strstr(_rx_buff, "CONNECT OK")) {
		sim800_status.tcp_connected = true;
	}
//	// TCP коннект закрыт
//	else if (strstr(_rx_buff, "CLOSE OK")) {
//		sim800_status.tcp_connected = false;
//	}
	else if (strstr(_rx_buff, "SHUT OK")) {
		sim800_status.cipshut_ok = true;
	}
	else if (strstr(_rx_buff, "RING")) {
		sim800_status.ring = true;
	}
	// Пришли данные FTP
	else if (strstr(_rx_buff, "+FTPGET: 1")) {
		int err = 0;
		user_sscanf(_rx_buff + 2, "+FTPGET: 1,%d", &err);

		if (err == 0) {
			// Нет даных / закончились
			sim800_status.ftp.byte_cnt = 0;
			sim800_status.ftp.ready_to_read = false;
			sim800_status.ftp.error = 0;
		}
		else if (err == 1) {
			// Данные готовы для чтения
			sim800_status.ftp.ready_to_read = true;
			sim800_status.ftp.error = 0;
		}
		else {
			// Ошибка
			sim800_status.ftp.byte_cnt = 0;
			sim800_status.ftp.error = err;
			sim800_status.ftp.ready_to_read = false;
		}
	}
	else if (strstr(_rx_buff, "+FTPSIZE: 1,0")) {
		user_sscanf(_rx_buff + 2, "+FTPSIZE: 1,0,%u", (unsigned int*)&sim800_status.ftp.ftp_size);
	}
	// Пришло время из сети
	else if (strstr(_rx_buff, "*PSUTTZ")) {
		sim800_status.time_received = true;
	}
	// Спонтанные дисконнект
	else if (strstr(_rx_buff, "+PDP") || strstr(_rx_buff, "CLOSED")) {
		user_printf("+PDP or CLOSED");
//		SYSTEM_RESET(NULL);
	}
	// Пришло смс
	else if (strstr(_rx_buff, "+CMTI")) {
		sim800_status.sms_received = true;
	}

	// Ошибка на команду
	else if (strstr(_rx_buff, "+CME") || strstr(_rx_buff, "+CMS")) {
//		ZF_LOGE("%s", _rx_buff);
	}

	// Парсим ответ Bluetooth-модем на команду сканирования
	else if (strstr(_rx_buff, "+BTSCAN: 0")) {
		char * pch = _rx_buff;
		pch = strchr(pch,',');
		pch++;
		pch = strchr(pch,',');
		pch++;
		pch = strchr(pch,',');
		pch++;

		user_sscanf(pch, "%x:%x:%x:%x:%x:%x", (unsigned int*)&bluetooth_status.scanned_mac[0],(unsigned int*)&bluetooth_status.scanned_mac[1],(unsigned int*)&bluetooth_status.scanned_mac[2],(unsigned int*)&bluetooth_status.scanned_mac[3],(unsigned int*)&bluetooth_status.scanned_mac[4],(unsigned int*)&bluetooth_status.scanned_mac[5]);

		user_sscanf(_rx_buff + 2, "+BTSCAN: 0,%u", (unsigned int*)&bluetooth_status.scanned_id);

		// Если MAC не соответствует настройкам, обнуляем сохраненный ранее ID устройства
		uint8_t mac_cmp = memcmp(settings1_ram.bluetooth.mac, bluetooth_status.scanned_mac, sizeof(settings1_ram.bluetooth.mac));

		if (mac_cmp != 0){
			bluetooth_status.scanned_id = 0;
		}
	}

	else if (strstr(_rx_buff, "+BTSCAN: 1") || strstr(_rx_buff, "+BTSCAN: 2") || strstr(_rx_buff, "+BTSCAN: 3")) {
		bluetooth_status.scan_status = COMPLETE;
	}

	// Парсим ответ Bluetooth-модем на команду пэйринга
	else if (strstr(_rx_buff, "+BTPAIR:")) {

		user_sscanf(_rx_buff + 2, "+BTPAIR: %u", (unsigned int*)&bluetooth_status.paired_id);
		bluetooth_status.pair_status = P_COMPLETE;
	}

	// Парсим ответ Bluetooth-модемА на команду подключения
	else if (strstr(_rx_buff, "+BTCONNECT:")) {

		user_sscanf(_rx_buff + 2, "+BTCONNECT: %u", (unsigned int*)&bluetooth_status.conn_id);

		if (bluetooth_status.conn_id != 0){
			bluetooth_status.conn_status = C_COMPLETE;
		}
		else {
			bluetooth_status.conn_status = C_FAILED;
		}
	}

	// Ловим разрыв Bluetooth-подключения
	else if (strstr(_rx_buff, "+BTDISCONN:")) {

		bluetooth_status.conn_status = DISCONNECTED;
	}

	// Ловим запрос на подключение от гарнитуры
	else if (strstr(_rx_buff, "+BTCONNECTING:")) {

		user_sscanf(_rx_buff + 2, "+BTCONNECTING: %x:%x:%x:%x:%x:%x", (unsigned int*)&bluetooth_status.request_mac[0],(unsigned int*)&bluetooth_status.request_mac[1],(unsigned int*)&bluetooth_status.request_mac[2],(unsigned int*)&bluetooth_status.request_mac[3],(unsigned int*)&bluetooth_status.request_mac[4],(unsigned int*)&bluetooth_status.request_mac[5]);
		// Если MAC соответствует настройкам, сохраняем статус
		uint8_t mac_cmp = memcmp(settings1_ram.bluetooth.mac, bluetooth_status.request_mac, sizeof(settings1_ram.bluetooth.mac));

		if (mac_cmp == 0){
			bluetooth_status.conn_status = C_REQ;
		}
	}

	// Обрабатываем непонятную ошибку, иногда возникающую в процессе подключения
	else if (strstr(_rx_buff, "+CME ERROR: BTAUD attach error")) {

		bluetooth_status.conn_status = C_FAILED;
	}

	// Ищем идентификатор спаренного устройства, используемый для подключения к гарнитуре
	else if (strstr(_rx_buff, "P: ") || strstr(_rx_buff, "+BTSTATUS")) {

		char * pch = _rx_buff;
		pch = strchr(pch,',');
		pch++;
		pch = strchr(pch,',');
		pch++;

		user_sscanf(pch, "%x:%x:%x:%x:%x:%x", (unsigned int*)&bluetooth_status.paired_mac[0],(unsigned int*)&bluetooth_status.paired_mac[1],(unsigned int*)&bluetooth_status.paired_mac[2],(unsigned int*)&bluetooth_status.paired_mac[3],(unsigned int*)&bluetooth_status.paired_mac[4],(unsigned int*)&bluetooth_status.paired_mac[5]);

		uint8_t mac_cmp = memcmp(settings1_ram.bluetooth.mac, bluetooth_status.paired_mac, sizeof(settings1_ram.bluetooth.mac));

		if (mac_cmp == 0){
			user_sscanf(_rx_buff + 2, "P: %u", (unsigned int*)&bluetooth_status.paired_id);
		}

		bluetooth_status.st_request_status = FINISH;
	}

	// Ищем идентификатор подключенного устройства устройства, используемый для подключения к гарнитуре
	else if (strstr(_rx_buff, "C: ") || strstr(_rx_buff, "+BTSTATUS") ) {

		char * pch = _rx_buff;
		pch = strchr(pch,',');
		pch++;
		pch = strchr(pch,',');
		pch++;

		user_sscanf(pch, "%x:%x:%x:%x:%x:%x", (unsigned int*)&bluetooth_status.connected_mac[0],(unsigned int*)&bluetooth_status.connected_mac[1],(unsigned int*)&bluetooth_status.connected_mac[2],(unsigned int*)&bluetooth_status.connected_mac[3],(unsigned int*)&bluetooth_status.connected_mac[4],(unsigned int*)&bluetooth_status.connected_mac[5]);

		uint8_t mac_cmp = memcmp(settings1_ram.bluetooth.mac, bluetooth_status.connected_mac, sizeof(settings1_ram.bluetooth.mac));

		if (mac_cmp == 0){
			user_sscanf(_rx_buff + 2, "C: %u", (unsigned int*)&bluetooth_status.conn_id);
		}

		bluetooth_status.st_request_status = FINISH;
	}
}

static ATCommander_user_rx_byte_ret_code_t user_rx_byte(void *commander) {
	static uint16_t tcp_data_len = 0;
	static unsigned int ftp_data_len = 0;

	ATCommander_private_t *private = &((ATCommander_t *)commander)->private;
	ATCommander_request_t *request = &((ATCommander_t *)commander)->request;
	char *_rx_buff = ((ATCommander_t *)commander)->sett.buff.rx_buff;

//	if(first_rx_irq) {
//		// Костыль. Пропускаем самый первый байт ((
//		first_rx_irq = false;
//		HAL_UART_Receive_IT(&SERIAL_OBJECT, (uint8_t*)_commander->private.rx_byte, 1);
//		return ATCOMMANDER_USER_RX_RET_CODE_QUIT;
//	}

	if (private->rx_state == ATCOMMANDER_RXSTATE_READY) {
		if (strstr(request->cmd, "AT+BTSTATUS?")) {
			if (_rx_buff[0] == 'P' || _rx_buff[0] == 'C') {
				_rx_buff[2] = _rx_buff[0];
				_rx_buff[0] = '\r';
				_rx_buff[1] = '\n';
				_rx_buff[3] = '\0';

				private->rx_cnt = 3;

				private->rx_state = ATCOMMANDER_RXSTATE_MSG_RECEIVE;

				return ATCOMMANDER_USER_RX_RET_CODE_QUIT;
			}
		}
	}
	else if (private->rx_state == ATCOMMANDER_RXSTATE_ECHO) {
		if (request->cmd[request->cmd_len - 1] == '\x1A') {
			// Обработка команды AT+CMGS
			// Текст при отправке СМС должен заканчиваться символом '\x1A',
			// Но в эхо он отсутствует. Поэтому специально убираем его в команде при приеме эхо
			char * cmd = (char *)request->cmd;
			cmd[request->cmd_len - 1] = '\0';
			request->cmd_len--;
		}
	}
	else if (private->rx_state == ATCOMMANDER_RXSTATE_MSG_RECEIVE) {
		if (_rx_buff[2] == '>' && _rx_buff[3] == ' ') {
			// На команду CIPSEND модуль возвращает приглашение "\r\n> ", т.е. без \r\n в конце
			// Дополняем искусственно, чтобы был полный респонс, окруженный \r\n с двух сторон
			_rx_buff[private->rx_cnt++] = '\r';
			_rx_buff[private->rx_cnt++] = '\n';

			_rx_buff[private->rx_cnt] = '\0';
			strcat(((ATCommander_t *)commander)->sett.buff.response_buff, _rx_buff);

			private->expected_responses_num_received = 0;
			private->response_received = true;
			private->response_wait = false;

			private->rx_state = ATCOMMANDER_RXSTATE_READY;

			return ATCOMMANDER_USER_RX_RET_CODE_QUIT;
		}
#if !SIM800_USE_MULTICONNECT
		else if (strstr(_rx_buff, "+IPD") && *private->rx_byte == ':') {
			// Событие приема данных TCP "+IPD,..." не имеет \r\n на конце. К тому же содержит поле количества данных
			// Сканируем это поле, чтобы определить, сколько байт содержится в сообщении
			if (user_sscanf(_rx_buff, "\r\n+IPD,%u", &tcp_data_len_to_receive) == 1) {
				tcp_data_len = tcp_data_len_to_receive;

				private->rx_state = ATCOMMANDER_RXSTATE_READY;

				return ATCOMMANDER_USER_RX_RET_CODE_QUIT;
			}
		}
#endif
	}
	else if (private->rx_state == ATCOMMANDER_RXSTATE_MSG_END) {
		if (*private->rx_byte == '\n') {
			bool success = false;

			_rx_buff[private->rx_cnt] = '\0';

#if SIM800_USE_MULTICONNECT
			if (strstr(_rx_buff, "+RECEIVE")) {//+RECEIVE,0,7:\r\n
				if (user_sscanf(_rx_buff, "\r\n+RECEIVE,0,%u", &tcp_data_len_to_receive) == 1) {
					tcp_data_len = tcp_data_len_to_receive;

					private->rx_state = ATCOMMANDER_RXSTATE_READY;

					success = true;
				}
			}
			else
#endif
			if (strstr(request->cmd, "+CMGR")) {
				// Чтение сообщения имеет 3 респонса: "+CMGR..." "<sms>" и "OK". При этом респонс <sms> не имеет \r\n в начале.
				// Поэтому после приема "+CMGR..." искусственно добавляем \r\n перед приемом текста сообщения
				if (private->expected_responses_num_received < 2) {
					strcat(((ATCommander_t *)commander)->sett.buff.response_buff, _rx_buff);
					private->rx_cnt = 0;

					if (private->expected_responses_num_received++ == 0) {
						_rx_buff[private->rx_cnt++] = '\r';
						_rx_buff[private->rx_cnt++] = '\n';

						private->rx_state = ATCOMMANDER_RXSTATE_MSG_RECEIVE;
					}
					else {
						private->rx_state = ATCOMMANDER_RXSTATE_READY;
					}

					success = true;
				}
			}
			else if (strstr(request->cmd, "+CIMI") || strstr(request->cmd, "+GSN")) {
				if (_rx_buff[2] >= 0x30 && _rx_buff[2] <= 0x39) {
					// Когда запрашиваем IMSI или IMEI номер, то мы не знаем какие именно цифры номера придут, поэтому в запросе указываем expected_response = NULL
					// Проверяем на запрошенную команду, затем проверяем, что первый символ в овете является цифрой
					if (private->expected_responses_num_received < 1) {
						private->expected_responses_num_received++;
						strcat(((ATCommander_t *)commander)->sett.buff.response_buff, _rx_buff);

						private->rx_cnt = 0;
						private->rx_state = ATCOMMANDER_RXSTATE_READY;
						success = true;
					}
				}
			}
			else if (strstr(request->cmd, "+CIFSR")) {
				// Когда запрашиваем IP адрес, то мы не знаем какие именно цифры придут, поэтому в запросе указываем expected_response = NULL
				// Проверяем на запрошенную команду, затем проверяем, что первый символ в овете является цифрой
				if (_rx_buff[2] >= 0x30 && _rx_buff[2] <= 0x39) {
					private->expected_responses_num_received = 0;
					private->response_received = true;
					private->response_wait = false;
					strcat(((ATCommander_t *)commander)->sett.buff.response_buff, _rx_buff);

					private->rx_cnt = 0;
					private->rx_state = ATCOMMANDER_RXSTATE_READY;
					success = true;
				}
			}
			else if (strstr(request->cmd, "+FTPGET=2")) {
				if (user_sscanf(_rx_buff, "\r\n+FTPGET: 2,%u", &ftp_data_len) == 1) {
					if (ftp_data_len > 0) {
						sim800_status.ftp.receiving = true;

						return ATCOMMANDER_USER_RX_RET_CODE_NORMAL;
					}
				}
			}

			if (success) {
				return ATCOMMANDER_USER_RX_RET_CODE_QUIT;
			}
		}
	}

	// Если есть данные TCP, принимаем их тут
	if (tcp_data_len_to_receive) {
		timer_start(200);	// величина должна зависеть от скорости уарт

		if (--tcp_data_len_to_receive == 0) {
			timer_stop();

			if (sim800_tcp_queue_put((uint8_t*)(_rx_buff + private->rx_cnt - tcp_data_len), tcp_data_len) != 0) {
//				ZF_LOGE("Queue overflow");///todo
			}

			private->rx_cnt = 0;
			private->rx_state = ATCOMMANDER_RXSTATE_READY;
		}

		return ATCOMMANDER_USER_RX_RET_CODE_QUIT;
	}

	// Данные FTP принимаем тут
//	if (sim800_status.ftp.receiving) {
//		timer_start(4000);
//
//		ftp_buffer[sim800_status.ftp.byte_cnt++] = *private->rx_byte;
//
//		if (sim800_status.ftp.byte_cnt >= ftp_data_len) {
//			timer_stop();
//
//			sim800_status.ftp.receiving = false;
//			sim800_status.ftp.packet_received = true;
//			sim800_status.ftp.byte_cnt = 0;
//		}
//
//		private->rx_cnt = 0;
//		private->rx_state = ATCOMMANDER_RXSTATE_READY;
//
//		return ATCOMMANDER_USER_RX_RET_CODE_QUIT;
//	}

	return ATCOMMANDER_USER_RX_RET_CODE_NORMAL;
}

//=======================================================================

static void logout(void *commander, const char* format, ...);

static ATCommander_t gsm_commander = {
		.sett.handlers.tx_dma = tx_dma,
		.sett.handlers.user_rx_byte = user_rx_byte,
		.sett.handlers.timer_start = timer_start,
		.sett.handlers.timer_stop = timer_stop,
		.sett.handlers.rx_it_start = rx_it_start,
		.sett.handlers.rx_it_stop = rx_it_stop,
		.sett.handlers.wait = wait,
		.sett.handlers.get_ms = HAL_GetTick,
		.sett.handlers.message_handler = message_handler,
		.sett.handlers.logout = logout,

		.sett.buff.rx_buff = rx_buff,
		.sett.buff.response_buff = response_buff,
		.sett.buff.rx_buff_len = RX_BUFF_LEN,

		.private.tx_completed = false,
		.private.echo_received = false,
		.private.response_received = false,
		.private.response_wait = false,
		.private.timeout = false,
		.private.rx_state = 0,
		.private.rx_cnt = false,
		.private.expected_responses_num_received = 0,
		.private.struct_ptrs_ok = false,
		.private.busy = false,

		.private.rx_byte = &rx_byte,
};

//=======================================================================

static void logout(void *commander, const char* format, ...) {
#if PRINT_ALL_SERIAL_DATA
	if ((ATCommander_t *)commander == &gsm_commander) {
		va_list arg;
		va_start(arg, format);
		user_vprintf(format, arg);
		va_end(arg);
	}
#endif
}

static void serial_fc_disable() {
	__HAL_UART_HWCONTROL_CTS_DISABLE(&SERIAL_OBJECT);
	__HAL_UART_HWCONTROL_RTS_DISABLE(&SERIAL_OBJECT);
}

static void serial_fc_enable() {
	__HAL_UART_HWCONTROL_CTS_ENABLE(&SERIAL_OBJECT);
	__HAL_UART_HWCONTROL_RTS_ENABLE(&SERIAL_OBJECT);
}

static void dtr_low() {
	HAL_GPIO_WritePin(GSM_DTR_GPIO_Port, GSM_DTR_Pin, GPIO_PIN_RESET);
}

static void dtr_high() {
	HAL_GPIO_WritePin(GSM_DTR_GPIO_Port, GSM_DTR_Pin, GPIO_PIN_SET);
}

static ATCommander_response_t * send_at(ATCommander_t *commander, const char * cmd, const char * expected_response, uint8_t responses_num, uint32_t timeout_ms) {
	ATCommander_request_t *request = &commander->request;

	commander->private.busy = true;

	request->cmd = cmd;
	request->cmd_len = strlen(cmd);
	request->expected_data = expected_response;
	request->expected_data_len = strlen(expected_response);
	request->expected_responses_num = responses_num;

	if (ATCommander_send(commander, timeout_ms) != 0) {
		bzero(commander->response, sizeof(commander->response));
	}

	commander->private.busy = false;

	return commander->response;
}

static int kick() {
	uint8_t ok_cont = 2;	// Только после двух успешно прошедших команд возвращаем успех
	ATCommander_response_t *rsp;

	for (uint8_t i = 0; i < 15; i++) {
		rsp = sim800_send_at("AT\r", NULL, 1, 200);
		if(strstr(rsp[0].data, "OK") != NULL) {
			if (--ok_cont == 0) {
				return 0;
			}
		}

		osDelay(10);
	}

	return -1;
}

//=======================================================================

sim800_status_t sim800_status = {0};

//=======================================================================

void sim800_rx_handler() {
	ATCommander_t *commander = &gsm_commander;

	PRINTX(commander->private.rx_byte, 1);

	ATCommander_rx_byte_handler(commander);
}

void sim800_serial_error_handler() {

}

void sim800_tx_complete_handler() {
	ATCommander_t *commander = &gsm_commander;

	ATCommander_tx_complete_handler(commander);
}

void sim800_timeout_handler() {
	ATCommander_t *commander = &gsm_commander;

	ATCommander_timeout_handler(commander);

	tcp_data_len_to_receive = 0;
	sim800_status.ftp.receiving = false;
	sim800_status.ftp.byte_cnt = 0;
}

//=======================================================================

int sim800_init() {
	// Почему-то при старте таймера уже выставлен флаг прерывания, поэтому оно срабатывает. Очищаем
	TIM_CLEAR_IT(TIM_IT_UPDATE);
	// Почему-то не работает заморозка
	TIM_DBG_FREEZE();

	ATCommander_reset_state(&gsm_commander);

	SERIAL_OBJECT.Init.BaudRate = 9600;
	HAL_UART_DeInit(&SERIAL_OBJECT);
	HAL_UART_Init(&SERIAL_OBJECT);

	dtr_low();
	serial_fc_disable();

	HAL_UART_Transmit(&SERIAL_OBJECT, (uint8_t*)"AT\r", strlen("AT\r"), 100);
	osDelay(200);

	//	uint8_t buf[20];
	//	HAL_UART_Receive(&SERIAL_OBJECT, buf, sizeof("AT\r\r\nOK\r\n") + 1, 500);
	//	HAL_UART_Transmit(&SERIAL_OBJECT, (uint8_t*)"AT\r", strlen("AT\r"), 100);
	//	HAL_UART_Receive(&SERIAL_OBJECT, buf, sizeof("AT\r\r\nOK\r\n"), 500);
	//	HAL_UART_Transmit(&SERIAL_OBJECT, (uint8_t*)"AT+IFC=2,2\r", strlen("AT+IFC=2,2\r"), 100);
	//	HAL_UART_Receive(&SERIAL_OBJECT, buf, sizeof("AT+IFC=2,2\r\r\nOK\r\n"), 500);
	//	serial_fc_enable();

	// Разрешаем прием UART
	rx_it_start(gsm_commander.private.rx_byte);

	int res = kick();
	if (res != 0) {
		return -1;
	}

	ATCommander_response_t *rsp = sim800_send_at("AT+IFC=2,2\r", NULL, 1, 1000);
	if(strstr(rsp[0].data, "OK") == NULL) {
		return -1;
	}

	serial_fc_enable();
	osDelay(10);

	res = kick();
	if (res != 0) {
		return -1;
	}

	rsp = sim800_send_at("AT+IPR=9600\r", NULL, 1, 1000);
	if(strstr(rsp[0].data, "OK") == NULL) {
		return -1;
	}

//	SERIAL_OBJECT.Init.BaudRate = 115200;
//	HAL_UART_Init(&SERIAL_OBJECT);
//	HAL_UART_Receive_IT(&SERIAL_OBJECT, (uint8_t*)gsm_commander.private.rx_byte, 1);

	res = kick();
	if (res != 0) {
		return -1;
	}

	return 0;
}

void sim800_rx_it_start() {
	rx_it_start(gsm_commander.private.rx_byte);
}

void sim800_rx_it_stop() {
	rx_it_stop();
}

ATCommander_response_t * sim800_send_at(const char * cmd, const char * expected_response, uint8_t responses_num, uint32_t timeout_ms) {
	return send_at(&gsm_commander, cmd, expected_response, responses_num, timeout_ms);
}

int sim800_dtr_sleep(uint32_t timeout_ms) {
	sleeping = true;

	dtr_high();

	osDelay(timeout_ms);

	return 0;
}

int sim800_dtr_wakeup() {
	dtr_low();
	osDelay(100);

	int res = kick();

	sleeping = false;

	return res;
}

void sim800_powerkey_hight() {
	HAL_GPIO_WritePin(GSM_POWERKEY_GPIO_Port, GSM_POWERKEY_Pin, GPIO_PIN_RESET);
}

void sim800_powerkey_low() {
	HAL_GPIO_WritePin(GSM_POWERKEY_GPIO_Port, GSM_POWERKEY_Pin, GPIO_PIN_SET);
}

bool sim800_status_pin_get() {
	return HAL_GPIO_ReadPin(GSM_STATUS_GPIO_Port, GSM_STATUS_Pin) == GPIO_PIN_SET;
}

bool sim800_status_wait(uint32_t ms, bool val) {
	bool status = !val;
	uint32_t start_time = HAL_GetTick();

	while ((status != val) && !(HAL_GetTick() - start_time > ms)) {
		osDelay(10);
		status = sim800_status_pin_get();
	}

	return status == val;
}

void sim800_gsm_power_on() {
	HAL_GPIO_WritePin(GSM_PWR_GPIO_Port, GSM_PWR_Pin, GPIO_PIN_SET);
}

void sim800_gsm_power_off() {
	HAL_GPIO_WritePin(GSM_PWR_GPIO_Port, GSM_PWR_Pin, GPIO_PIN_RESET);
}


int sim800_check_serial() {
	return kick();
}


sim800_gsm_signal_strength_t * sim800_gsm_get_signal_strength() {
	static sim800_gsm_signal_strength_t signal_strength;
	unsigned int tmp1, tmp2;

	ATCommander_response_t *rsp = sim800_send_at("AT+CSQ\r", "+CSQ", 2, 500);
	if (user_sscanf(rsp[0].data, "+CSQ: %u,%u", &tmp1, &tmp2) == 2) {
		if (tmp1 != 99) {	// 99 - not known or not detectable
			signal_strength.rssi = (uint8_t)tmp1*2 - 113;
			signal_strength.percent = (uint8_t)(tmp1*100 / 30);
			signal_strength.bit_error_rate = (uint8_t)tmp2;

			return &signal_strength;
		}
	}

	ZF_LOGE("%s -> %s", "AT+CSQ\r", rsp[0].data);

	return NULL;
}

int sim800_send_sms(char *phone_number, char *text) {
	/// \note text должен заканчиваться на \x1A

	int ret = 0;

	char cmd[24];
	sprintf(cmd, "AT+CMGS=\"%s\"\r", phone_number);

	ATCommander_response_t *rsp = sim800_send_at(cmd, "> ", 1, 500);
	if (rsp[0].data == NULL) {
		ret = -1;
		goto exit;
	}

	rsp = sim800_send_at(text, "+CMGS: ", 2, 60000);
	if (strstr(rsp[1].data, "OK") == NULL) {
		ret = -1;
		goto exit;
	}

	exit:

	if (ret == 0) {
		ZF_LOGD("Send sms \"%s\" %s", text, "SUCCESS");
	}
	else {
		ZF_LOGE("Send sms \"%s\" %s", text, "ERROR");
	}

	return ret;
}

int sim800_cipshut() {
	uint32_t timeout_ms = 65000;	// Из даташита
	sim800_status.cipshut_ok = false;

	gsm_commander.private.busy = true;

	gsm_commander.request.cmd = "AT+CIPSHUT\r";
	gsm_commander.request.cmd_len = strlen("AT+CIPSHUT\r");
	gsm_commander.request.expected_data = NULL;
	gsm_commander.request.expected_data_len = 0;
	gsm_commander.request.expected_responses_num = 0;

	if (ATCommander_send(&gsm_commander, 100) == 0) {
		do {
			osDelay(1);
			WDG_RESET(TASK_GSM);
		} while (!sim800_status.cipshut_ok && --timeout_ms > 0);
	}

	gsm_commander.private.busy = false;

	if (sim800_status.cipshut_ok) {
		return 0;
	}
	else {
		return -1;
	}
}

#if SIM800_USE_MULTICONNECT
int sim800_tcp_connect(uint8_t con_id, const char * host, uint16_t port, int32_t timeout_ms) {
#else
int sim800_tcp_connect(const char * host, uint16_t port, int32_t timeout_ms) {
#endif
	sim800_status.tcp_connected = false;

	char buff[64];

#if SIM800_USE_MULTICONNECT
	sprintf(buff, "AT+CIPSTART=%u,TCP,%s,%u\r", (unsigned int)con_id, host, (unsigned int)port);
#else
	sprintf(buff, "AT+CIPSTART=TCP,%s,%u\r", host, (unsigned int)port);
#endif

	gsm_commander.private.busy = true;

	gsm_commander.request.cmd = buff;
	gsm_commander.request.cmd_len = strlen(buff);
	gsm_commander.request.expected_data = NULL;
	gsm_commander.request.expected_data_len = 0;
	gsm_commander.request.expected_responses_num = 0;

	if (ATCommander_send(&gsm_commander, timeout_ms) == 0) {
		do {
			osDelay(1);
			WDG_RESET(TASK_GSM);
		} while (!sim800_status.tcp_connected && --timeout_ms > 0);
	}

	gsm_commander.private.busy = false;

	if (sim800_status.tcp_connected) {
		return 0;
	}
	else {
		return -1;
	}
}

#if SIM800_USE_MULTICONNECT
int sim800_tcp_send(uint8_t con_id, const void * data, uint32_t len, int32_t timeout_ms) {
#else
int sim800_tcp_send(const void * data, uint32_t len, int32_t timeout_ms) {
#endif
	sim800_status.tcp_send_ok = false;

	if (len == 0) {
		return 0;
	}

	int ret = -1;
	char buff[20];
#if SIM800_USE_MULTICONNECT
	sprintf(buff, "AT+CIPSEND=%u,%u\r", (unsigned int)con_id, (unsigned int)len);
#else
	sprintf(buff, "AT+CIPSEND=%u\r", (unsigned int)len);
#endif

	gsm_commander.private.busy = true;

	gsm_commander.request.cmd = buff;
	gsm_commander.request.cmd_len = strlen(buff);
	gsm_commander.request.expected_data = NULL;
	gsm_commander.request.expected_data_len = 0;
	gsm_commander.request.expected_responses_num = 1;

	if (ATCommander_send(&gsm_commander, timeout_ms) == 0) {
		if (strstr(gsm_commander.response[0].data, "> ")) {
			gsm_commander.request.cmd = data;
			gsm_commander.request.cmd_len = len;
			gsm_commander.request.expected_data = NULL;
			gsm_commander.request.expected_data_len = 0;
			gsm_commander.request.expected_responses_num = 0;

			if (ATCommander_send(&gsm_commander, timeout_ms) == 0) {
				do {
					osDelay(1);
					WDG_RESET(TASK_GSM);
				} while (!sim800_status.tcp_send_ok && --timeout_ms > 0);
			}
		}
	}

	if (sim800_status.tcp_send_ok) {
		ret = len;
	}
	else {
		bzero(gsm_commander.response, sizeof(gsm_commander.response));
	}

	gsm_commander.private.busy = false;

	return ret;
}

int sim800_tcp_queue_put(uint8_t * data, uint16_t size) {
	int res = -1;

	DISABLE_IRQ_USER();

	if (size <= QUEUE_SIZE) {
		memcpy(queue_array, data, size);
		queue_topindex = size;
		have_tcp_data = true;

		res = 0;
	}

	ENABLE_IRQ_USER();

	return res;
}

#if SIM800_USE_MULTICONNECT
uint16_t sim800_tcp_queue_pop(uint8_t con_id, uint8_t * data) {
	(void)(con_id);
#else
uint16_t sim800_tcp_queue_pop(uint8_t * data) {
#endif
	uint16_t len;

	DISABLE_IRQ_USER();

	len = queue_topindex;

	memcpy(data, queue_array, len);

	bzero(queue_array, sizeof(queue_array));

	queue_topindex = 0;

	have_tcp_data = false;

	ENABLE_IRQ_USER();

	return len;
}

//void sim800_tcp_queue_clear() {
//	taskENTER_CRITICAL();
//
//	bzero(queue_array, sizeof(queue_array));
//	queue_topindex = 0;
//
//	taskEXIT_CRITICAL();
//}

bool sim800_tcp_queue_is_have_data() {
	return have_tcp_data;
}

int8_t sim800_bluetooth_power_on(){

	ATCommander_response_t *rsp;

	rsp = sim800_send_at("AT+BTPOWER=1\r", NULL, 1, 4000);

	if(strstr(rsp[0].data, "OK") != NULL) {
		return 0;
	}

	osDelay(10);

	return -1;
}

int8_t bluetooth_scan(uint8_t mac[6], uint8_t timeout){

	ATCommander_response_t *rsp;
	static uint8_t scan_timeout_cnt = 0;

	if (bluetooth_status.scan_status == IDLE){


//		char buff[64];
//		sprintf(buff, "AT+BTSCAN=1,%u\r", (unsigned int)timeout);

		rsp = sim800_send_at("AT+BTSCAN=1,10\r", NULL, 1, 400);

		if(strstr(rsp[0].data, "OK") != NULL) {
			bluetooth_status.scan_status = SCAN;
			return 0;
		}
		else {
			sim800_send_at("AT+BTSTATUS?\r", NULL, 1, 200);
			return -1;
		}
	}
	else if (bluetooth_status.scan_status == SCAN){

		user_printf("---> Bluetooth scanning \r\n");
		// Останавливаем сканирование, если найдена нужная гарнитура
		if (bluetooth_status.scanned_id > 0)
		{
			rsp = sim800_send_at("AT+BTSCAN=0\r", NULL, 1, 400);
			bluetooth_status.scan_status = IDLE;
			user_printf("---> Found bluetooth headset with ID = %u \r\n", (uint8_t)bluetooth_status.scanned_id);
			return bluetooth_status.scanned_id;
		}
		else if (++scan_timeout_cnt > timeout){
			scan_timeout_cnt = 0;
			rsp = sim800_send_at("AT+BTSCAN=0\r", NULL, 1, 400);
			bluetooth_status.scan_status = IDLE;
			user_printf("---> Bluetooth scan timeout! \r\n");
			return 0;
		}
		else {
			return 0;
		}
	}
	else if (bluetooth_status.scan_status == COMPLETE){
			bluetooth_status.scan_status = IDLE;
			scan_timeout_cnt = 0;
			user_printf("---> Scan complete! \r\n");
			return -2;
	}
	else {
		return -2;
	}
}

int8_t check_bluetooth_status(uint8_t headset_id, headset_status_t *headset_status){

	if (bluetooth_status.st_request_status == START){
		sim800_send_at("AT+BTSTATUS?\r", NULL, 1, 200);
	}
	else if (bluetooth_status.st_request_status == REQUESTING){
		return 0;
	}
	else if (bluetooth_status.st_request_status == FINISH){

		if (bluetooth_status.paired_id != 0){
			headset_status->bt_id = bluetooth_status.paired_id;
			headset_status->bt_status = PAIRED;
		}
		else if (bluetooth_status.conn_id != 0){
			headset_status->bt_id = bluetooth_status.conn_id;
			headset_status->bt_status = CONNECTED;
		}

		bluetooth_status.st_request_status = START;
		return 1;
	}

	return 0;

}


uint8_t bluetooth_pair(uint8_t headset_s_id) {

	if (bluetooth_status.pair_status == P_IDLE){

		char buff[64];

		sprintf(buff, "AT+BTPAIR=0,%u\r", (unsigned int)headset_s_id);

		sim800_send_at(buff, NULL, 1, 200);

		bluetooth_status.pair_status = PAIRING;

		return 0;

	}
	else if (bluetooth_status.pair_status == PAIRING)
	{
		return false;
	}
	else if (bluetooth_status.pair_status == P_COMPLETE)
	{
		if (bluetooth_status.paired_id > 0){

			return bluetooth_status.paired_id;
		}
		else
		{
			return 0;
		}
	}

	return 0;
}

int8_t bluetooth_connect(uint8_t headset_p_id, uint8_t timeout){

	static uint8_t conn_pending_timeout;

	if (bluetooth_status.conn_status == C_IDLE){

		char buff[64];

		sprintf(buff, "AT+BTCONNECT=%u,6\r", (unsigned int)headset_p_id);

		sim800_send_at(buff, NULL, 1, 200);

		bluetooth_status.conn_status = CONNECTING;

		return 0;
	}
	else if (bluetooth_status.conn_status == CONNECTING)
	{
		osDelay(1000);

		if(++conn_pending_timeout > timeout){
			conn_pending_timeout = 0;
			bluetooth_status.conn_status = C_IDLE;
			return -1;
		}

		return 0;
	}
	else if (bluetooth_status.conn_status == C_COMPLETE)
	{
		bluetooth_status.conn_status = C_IDLE;
		conn_pending_timeout = 0;
		return 1;
	}

	else if (bluetooth_status.conn_status == C_FAILED)
	{
		bluetooth_status.conn_status = C_IDLE;
		conn_pending_timeout = 0;
		return -1;
	}
	else if (bluetooth_status.conn_status == DISCONNECTED)
	{
		bluetooth_status.conn_status = C_IDLE;
		conn_pending_timeout = 0;
		return -1;
	}
	else{
		return -1;
	}
}

bool check_bluetooth_connection() {
	if (bluetooth_status.conn_status == DISCONNECTED){
		bluetooth_status.scanned_id = 0;
		return false;
	}
	else{
		return true;
	}
}

bool incoming_conn_request(){

	ATCommander_response_t *rsp;

	// Если пришел запрос на подключение со стороны гарнитуры, принимаем его
	if (bluetooth_status.conn_status == C_REQ){

		if (bluetooth_status.scan_status != IDLE){
			rsp = sim800_send_at("AT+BTSCAN=0\r", NULL, 1, 400);
		}

		rsp = sim800_send_at("AT+BTACPT=1\r", NULL, 1, 200);

		if(strstr(rsp[0].data, "OK") != NULL) {
			bluetooth_status.conn_status = C_COMPLETE;
			return true;
		}
	}
	else {
		return false;
	}

	return false;
}

void bluetooth_reset(void) {
	ATCommander_response_t *rsp;

	rsp = sim800_send_at("AT+BTPOWER=0\r", NULL, 1, 500);

	if(strstr(rsp[0].data, "OK") != NULL) {
		rsp = sim800_send_at("AT+BTPOWER=1\r", NULL, 1, 4000);
	}
	else{
		sim800_send_at("AT+BTSTATUS?\r", NULL, 1, 200);
	}
}

bool bluetooth_unpair_all(void) {
	ATCommander_response_t *rsp;

	rsp = sim800_send_at("AT+BTUNPAIR=0\r", NULL, 1, 300);

	if(strstr(rsp[0].data, "OK") != NULL) {
		return 0;
	}
	else{
		return -1;
	}
}

// =====================================================================================

int sim800_direct_uart_tx_data(const void *data, uint32_t len) {
	HAL_StatusTypeDef res = HAL_UART_Transmit_DMA(&huart2, (uint8_t*)data, len);
	if (res != HAL_OK) {
		ZF_LOGE("Error code: %d", res);
		return -1;
	}

	PRINTX(data, len);

	return 0;
}

int sim800_direct_uart_tx_str(char *str) {
	return sim800_direct_uart_tx_data(str, strlen(str));
}

int sim800_direct_uart_rx_str(char *buf, char *str, uint32_t timeout_ms) {
	size_t len = strlen(str);

	int res = sim800_direct_uart_rx_data(buf, len, timeout_ms);
	if (res != 0) {
		return -1;
	}

	if (memcmp(buf, str, len) != 0) {
		ZF_LOGE("Error: expected %s, real %s", str, buf);
		buf[0] = 0;
		return -1;
	}

	return 0;
}

int sim800_direct_uart_rx_data(char *buf, uint32_t len, uint32_t timeout_ms) {
	buf[0] = '\0';

	HAL_StatusTypeDef res = HAL_UART_Receive(&huart2, (uint8_t *)buf, len, timeout_ms);
	if (res != HAL_OK) {
		if (timeout_ms > 0) {
			ZF_LOGE("Error code: %d", res);
		}

		return -1;
	}

	PRINTX(buf, len);

	return 0;
}

char sim800_direct_uart_rx_char(uint32_t timeout_ms) {
	char byte;

	int res = sim800_direct_uart_rx_data(&byte, 1, timeout_ms);
	if (res != 0) {
		return -1;
	}

	return byte;
}

int sim800_direct_wait_byte(char byte, uint32_t timeout_ms) {
	char rx_byte;
	uint32_t pretime = HAL_GetTick();

	do {
		WDG_RESET(TASK_ALL);
		rx_byte = sim800_direct_uart_rx_char(0);

		if (HAL_GetTick() - pretime > timeout_ms) {
			ZF_LOGE("Timeout");
			return -1;
		}
	} while (byte != rx_byte);

	return 0;
}

int sim800_direct_wait_line(sim800_direct_param_t * param) {
	char rx_byte;
	int rx_cnt = 0;
	uint32_t pretime = HAL_GetTick();

	do {
		WDG_RESET(TASK_ALL);

		if (HAL_GetTick() - pretime > param->timeout) {
			ZF_LOGE("Timeout");
			return -1;
		}

		rx_byte = sim800_direct_uart_rx_char(0);

		if (rx_byte == (char)-1) {
			// Не приняли байт
			continue;
		}

		if (rx_cnt < 2) {
			if (rx_byte == '\r' || rx_byte == '\n') {
				// "\r\n" в начале строки игнорируем
				continue;
			}
		}

		if (rx_byte == '\r') {
			// '\r' в конце строки игнорируем
			continue;
		}

		if (rx_byte == '\n') {
			// '\n' означает конец приема строки, оканчивающийся на "\r\n"
			param->rx_buff[rx_cnt++] = '\0';
			break;
		}

		if (rx_cnt < param->rx_len - 1) {
			// Если не превысили размер буфера
			param->rx_buff[rx_cnt++] = rx_byte;
		}
	} while (1);

	return 0;
}

int sim800_direct_wait_line_starting_with(sim800_direct_param_t * param, const char * starting_with) {
	uint32_t start_time = HAL_GetTick();

	do {
		if (sim800_direct_wait_line(param) != 0) {
			return -1;
		}

		if (strstr(param->rx_buff, "ERROR")) {
			ZF_LOGE("Error: expected %s, real %s", starting_with, param->rx_buff);
			// Пришло что-то типа +CME ERROR
			return -1;
		}

		if (strstr(param->rx_buff, starting_with)) {
			return 0;
		}

		if (HAL_GetTick() - start_time > param->timeout) {
			ZF_LOGE("Timeout");
			return -1;
		}
	} while (1);
}

int sim800_direct_send(sim800_direct_param_t * param, const char * format, ...) {
    va_list argptr;
    va_start(argptr, format);
    vsprintf(param->tx_buff, format, argptr);
    va_end(argptr);

    int ret = sim800_direct_uart_tx_str(param->tx_buff);
    if (ret != 0) {
    	return ret;
    }

    if (param->wait_ok) {
    	ret = sim800_direct_uart_rx_str(param->rx_buff, "\r\nOK\r\n", param->timeout);
    }

    return ret;
}
