/*
 * sim800.h
 *
 *  Created on: 3 апр. 2018 г.
 *      Author: Denis Shreiber
 */

#ifndef SIM800_H_
#define SIM800_H_

#include "atcommander/atcommander.h"

//=======================================================================

#define SIM800_USE_MULTICONNECT			1
#define SIM800_FTP_MAX_DATA_SIZE		1460

typedef enum {
	FTP_MODE_OPENNING	= 1,
	FTP_MODE_READING	= 2
}sim800_ftp_mode_t;

/// Флаги состояния модуля
typedef volatile struct {
	bool time_received;				/// Время сети определено
	bool gps_time_received;			/// Время из GPS определено
	bool sms_received;				/// Принято СМС
	bool tcp_connected;				/// TCP коннект активен
	bool cipshut_ok;				/// Принято сообщение CIPSHUT
	bool tcp_send_ok;				/// TCP данные отправлены, пришел SEND OK
	bool ring;						/// Входящий звонок
	struct {
		uint8_t error;				///
		uint16_t byte_cnt;			///
		bool ready_to_read;			/// Есть данные FTP. Надо запрашивать
		bool receiving;				///
		bool packet_received;		///
		uint32_t ftp_size;			/// Размер файла FTP в ответ на FTPSIZE
	} ftp;
}sim800_status_t;

/// Данные о силе сигнала GSM
typedef struct sim800_gsm_signal_strength_t {
	int8_t rssi;
	uint8_t percent;
	uint8_t bit_error_rate;
} sim800_gsm_signal_strength_t;

/// Статус Bluetooth-модема
typedef struct sim800_bluetooth_status_t {
	enum scan_status{IDLE, SCAN, COMPLETE} scan_status;
	enum st_request_status{START, REQUESTING, FINISH} st_request_status;
	enum pair_status{P_IDLE, PAIRING, P_COMPLETE} pair_status;
	enum conn_status{C_IDLE, CONNECTING, C_COMPLETE, C_REQ, C_FAILED, DISCONNECTED} conn_status;
	uint8_t scanned_mac[6];
	uint8_t request_mac[6];
	uint8_t paired_mac[6];
	uint8_t connected_mac[6];
	int8_t scanned_id;
	uint8_t paired_id;
	uint8_t conn_id;

} sim800_bluetooth_status_t;

typedef struct headset_status_t{
	enum bt_status {NONE, PAIRED, CONNECTED} bt_status;
	uint8_t bt_id;
} headset_status_t;

typedef struct {
	char * tx_buff;
	int tx_len;
	char * rx_buff;
	int rx_len;
	bool wait_ok;
	int timeout;
} sim800_direct_param_t;

//=======================================================================

/// Состояние модуля
extern sim800_status_t sim800_status;

//=======================================================================

void sim800_rx_handler();
void sim800_serial_error_handler();
void sim800_tx_complete_handler();
void sim800_timeout_handler();

int sim800_init();

void sim800_rx_it_start();
void sim800_rx_it_stop();

ATCommander_response_t * sim800_send_at(const char * cmd,  const char * expected_response, uint8_t responses_num, uint32_t timeout_ms);

int sim800_dtr_sleep(uint32_t timeout_ms);
int sim800_dtr_wakeup();

void sim800_powerkey_hight();
void sim800_powerkey_low();
void sim800_gsm_power_on();
void sim800_gsm_power_off();
bool sim800_status_pin_get();
bool sim800_status_wait(uint32_t ms, bool val);
void sim800_gnss_ant_power_on();
void sim800_gnss_ant_power_off();

// Bluetooth public functions
int8_t sim800_bluetooth_power_on();
int8_t bluetooth_scan(uint8_t mac[6], uint8_t);
int8_t check_bluetooth_status(uint8_t headset_id, headset_status_t *headset_status);
uint8_t bluetooth_pair(uint8_t headset_s_id);
int8_t bluetooth_connect(uint8_t headset_p_id, uint8_t timeout);
bool check_bluetooth_connection(void);
bool incoming_conn_request(void);
void bluetooth_reset(void);
bool bluetooth_unpair_all(void);

// Функции прямого доступа к уарт без использования АТкомандера
int sim800_direct_uart_tx_data(const void *data, uint32_t len);
int sim800_direct_uart_tx_str(char *str);
int sim800_direct_uart_rx_str(char *buf, char *str, uint32_t timeout_ms);
int sim800_direct_uart_rx_data(char *buf, uint32_t len, uint32_t timeout_ms);
char sim800_direct_uart_rx_char(uint32_t timeout_ms);
int sim800_direct_wait_byte(char byte, uint32_t timeout_ms);
int sim800_direct_wait_line(sim800_direct_param_t * param);
int sim800_direct_wait_line_starting_with(sim800_direct_param_t * param, const char * starting_with);
int sim800_direct_send(sim800_direct_param_t * param, const char * format, ...);

int sim800_check_serial();

sim800_gsm_signal_strength_t * sim800_gsm_get_signal_strength();

int sim800_send_sms(char *phone_number, char *text);

/**
 *
 * @return
 */
int sim800_cipshut();

/**
 *
 * @param host [in] Адрес сервера
 * @param port [in] Порт
 * @param timeout_ms [in] Таймаут
 * @return
 */
#if SIM800_USE_MULTICONNECT
int sim800_tcp_connect(uint8_t con_id, const char * host, uint16_t port, int32_t timeout_ms);
#else
int sim800_tcp_connect(const char * host, uint16_t port, int32_t timeout_ms);
#endif
/**
 * Отправить данные по TCP
 * @param data [in] Данные для отправки
 * @param len [in] Длина данных
 * @param timeout_ms [in] Таймаут
 * @return
 */
#if SIM800_USE_MULTICONNECT
int sim800_tcp_send(uint8_t con_id, const void * data, uint32_t len, int32_t timeout_ms);
#else
int sim800_tcp_send(const void * data, uint32_t len, int32_t timeout_ms);
#endif
/**
 * Положить TCP данные в очередь
 * @param data
 * @param size
 * @return
 */
int sim800_tcp_queue_put(uint8_t * data, uint16_t size);
/**
 * Извлечь TCP данные из очереди
 * @param data
 * @param size
 * @return
 */
#if SIM800_USE_MULTICONNECT
uint16_t sim800_tcp_queue_pop(uint8_t con_id, uint8_t * data);
#else
uint16_t sim800_tcp_queue_pop(uint8_t * data);
#endif
bool sim800_tcp_queue_is_have_data();
/**
 * Очистить очередь TCP
 */
//void sim800_tcp_queue_clear();


#endif /* SIM800_H_ */
