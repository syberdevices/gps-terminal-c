#include "stdint.h"
#include "stdbool.h"

#ifndef CRC32_H_
#define CRC32_H_


/**
 * Считает CRC
 * @param pData		Указатель на данные
 * @param nLen		Длина данных в байтах
 * @param crc_initial	Начальное значение CRC. Ипользуется для нескольких вызовов подряд. Если этопервый вызов, то @ref crc_initial = 0
 * @return CRC. Может быть использован в качестве @ref crc_initial для следующего вызова @ref crc32_get
 */
uint32_t crc32_get(const void* pData, uint32_t nLen, uint32_t crc_initial);


#endif // CRC32_H_
