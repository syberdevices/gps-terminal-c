// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/******************************************************************************
	���� ���������� ��������� CRC32 �� ����������� ��������� 0xEDB88320.
	�������� �����: ������������ ����� �������������
	E-mail: paveldvlip@mail.ru
	2005 ���
******************************************************************************/

#include "crc32.h"

#define CRC_POLYNOM			0xEDB88320

static uint32_t table[256];
static bool inited = false;

static void crc32_init() {
	for (uint32_t i = 0; i <= 255; i++) {
		uint32_t crc_tab = i;

		for (uint32_t j = 8; j > 0; j--) {
			if ((crc_tab & 1) == 1) {
				crc_tab = (crc_tab >> 1);
				crc_tab ^= CRC_POLYNOM;
			}
			else {
				crc_tab >>= 1;
			}
		}
		table[i] = crc_tab;
	}

	inited = true;
}

uint32_t crc32_get(const void* pData, uint32_t nLen, uint32_t crc_initial) {
	if (!inited) {
		crc32_init();
	}

	uint32_t crc = crc_initial ^ (uint32_t)-1;

	for (uint32_t i = 0; i < nLen; i++) {
		crc = (crc >> 8) ^ table[(crc & 0xFF) ^ ((uint8_t*)pData)[i]];
	}

	return crc ^ (uint32_t)-1;
}

