/*
 * rs485.c
 *
 *  Created on: 4 февр. 2021 г.
 *      Author: chuyec
 */


#include "usart.h"
#include "gpio.h"

#include <string.h>

#include "rs485/rs485.h"
#include "rtos.h"

// ===============================================================================

/// printf для дебага
#define PRINTX(...)

#define RX_BUFF_LEN				64		/// Длина буфера приема
#define RX_TIMEOUT				4		/// Таймаут приема (4 мс - это 3,5 символа при скорости 9600 Бод)

// ===============================================================================

/// Состояние вывода DE микросхемы RS485
typedef enum {
	DE_ENABLE = GPIO_PIN_SET,		/// Разрешить передачу (запретить приём)
	DE_DISABLE = GPIO_PIN_RESET,	/// Запретить передачу (разрешить приём)
} de_cfg_t;

// ===============================================================================

static UART_HandleTypeDef * const p_uart = &huart1;		/// Хэндл UART

volatile static uint32_t rx_ticks = 0;		///< Время последнего принятого байта
volatile static uint32_t rx_cnt = 0;		///< Количество данных в буфере
static uint8_t rx_buff[RX_BUFF_LEN];		///< Буфер приема

static rs485_rx_callback_t rx_byte_only_callback;

// ===============================================================================

/**
 * Установить пин Driver Enable
 * @param state Состояние пина \ref de_cfg_t
 */
static void de(de_cfg_t state) {
	HAL_GPIO_WritePin(RS485_DE_GPIO_Port, RS485_DE_Pin, state);
}

/**
 * Отправить данные
 * @param data
 * @param len
 * @param timeout
 * @return
 */
static int tx(void * data, int len, int timeout) {
	return HAL_UART_Transmit(p_uart, data, len, timeout);
}

/**
 * Стартануть прием
 *
 * DMA настраивается на прием одного байта
 * @return Результат выполнения \ref HAL_UART_Receive_DMA
 */
static int rx_start() {
	static uint8_t _byte;
	return HAL_UART_Receive_DMA(p_uart, &_byte, 1);
}

/**
 * Отменить прием
 * @return Результат выполнения \ref HAL_UART_AbortReceive
 */
static int rx_stop() {
	return HAL_UART_AbortReceive(p_uart);
}

// ===============================================================================

void rs485_init() {
	// По умолчанию настройка на приём
	de(DE_DISABLE);

	rs485_rx_byte_only_disable();
}

void rs485_rx_byte_only_enable(rs485_rx_callback_t _rx_callback) {
	rx_byte_only_callback = _rx_callback;

	de(DE_DISABLE);

	(void)rx_start();
}

void rs485_rx_byte_only_disable() {
	rx_stop();

	rx_byte_only_callback = NULL;
}

int rs485_tx(void * data, int len, int timeout) {
	de(DE_ENABLE);

	HAL_StatusTypeDef rc = tx(data, len, timeout);

	rx_cnt = 0;

	de(DE_DISABLE);

	return (int)rc;
}

int rs485_available() {
	if (rx_cnt > 0 && HAL_GetTick() - rx_ticks > RX_TIMEOUT) {
		rx_stop();
		return rx_cnt;
	}
	else {
		return 0;
	}
}

int rs485_rx(void * data, int timeout) {
	int ret = rx_start();
	if (ret != 0) {
		return ret;
	}

	int len = 0;
	uint32_t start_time = HAL_GetTick();

	do {
		len = rs485_available();
		if (len > 0) {
			break;
		}

		if (HAL_GetTick() - start_time >= timeout) {
			(void)rx_stop();
			return -1;
		}

		osDelay(1);
	} while(1);

	taskENTER_CRITICAL();

	memcpy(data, rx_buff, len);
	rx_cnt = 0;

	taskEXIT_CRITICAL();

	return len;
}

int rs485_rx_start() {
	return rx_start();
}

int rs485_rx_stop() {
	return rx_stop();
}

int rs485_set_baudrate(int baudrate) {
	p_uart->Init.BaudRate = baudrate;
	return HAL_UART_Init(p_uart);
}

void rs485_rx_callback() {
//	uint32_t isrflags   = READ_REG(p_uart->Instance->SR);
//	uint32_t cr1its     = READ_REG(p_uart->Instance->CR1);

//    if(((isrflags & USART_SR_RXNE) != RESET) && ((cr1its & USART_CR1_RXNEIE) != RESET)) {
    	uint8_t rx_byte = (uint8_t)(p_uart->Instance->DR & (uint8_t)0x00FF);

    	PRINTX(&rx_byte, 1);

    	if (rx_byte_only_callback) {
    		rx_byte_only_callback(rx_byte);
    	}
    	else {
			if (rx_cnt >= RX_BUFF_LEN) {
				return;
			}

			rx_buff[rx_cnt++] = rx_byte;

			rx_ticks = HAL_GetTick();
    	}

    	(void)rx_start();
//    }
}

void rs485_serial_error_handler() {
	(void)rx_start();
}

