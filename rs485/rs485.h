/*
 * rs485.h
 *
 *  Created on: 4 февр. 2021 г.
 *      Author: chuyec
 */

#ifndef RS485_H_
#define RS485_H_


typedef void(*rs485_rx_callback_t)(uint8_t byte);

// ====================================================================================

/**
 * Инициализация RS485
 */
void rs485_init();

/**
 * Отправить данные в RS485
 * @param data
 * @param len
 * @param timeout	Таймаут в мс
 * @return
 */
int rs485_tx(void * data, int len, int timeout);

/**
 * Разрешить работу только на прием
 *
 * Накопление массива байт и прием через @ref rs485_rx и @ref rs485_available запрещен
 * @param _rx_callback	Коллбэк на прием байта
 */
void rs485_rx_byte_only_enable(rs485_rx_callback_t _rx_callback);

/**
 *	Запретить работу только на прием
 *
 *	Накопление массива байт и прием через @ref rs485_rx и @ref rs485_available разрещен
 */
void rs485_rx_byte_only_disable();

/**
 * Возвращает количество принятых байт (после отработки таймаута @ref RX_TIMEOUT)
 * @return Количество имеющихся данных
 */
int rs485_available();

/**
 * Принять данные
 * @param data
 * @param timeout	Время ожидания данных в мс
 * @return	Количесиво принятых байт
 */
int rs485_rx(void * data, int timeout);

/**
 *
 * @return
 */
int rs485_rx_start();

/**
 *
 * @return
 */
int rs485_rx_stop();

/**
 *
 * @param baudrate
 * @return
 */
int rs485_set_baudrate(int baudrate);

/**
 * Коллбэк прерывания по приему RS485
 */
void rs485_rx_callback();

/**
 *
 */
void rs485_serial_error_handler();


#endif /* S485_H_ */
