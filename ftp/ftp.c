/*
 * ftp.c
 *
 *  Created on: 18 дек. 2021 г.
 *      Author: chuyec
 */

#include <string.h>
#include <stdlib.h>

#include "rtos.h"
#include "stm32f4xx_hal.h"
#include "usart.h"

#include "ftp.h"
#include "log/log.h"
#include "main.h"
#include "common.h"
#include "settings/settings.h"

// =====================================================================================

#define FTP_REC_BUF_SIZE			CONF_JSON_MAX_LEN

#define MIN(a, b)  (((a) < (b)) ? (a) : (b))
#define MAX(a, b)  (((a) > (b)) ? (a) : (b))

// =====================================================================================

typedef enum ftp_file_type_e {
	FTP_FILE_NONE,
	FTP_FILE_FIRMWARE,
	FTP_FILE_SETTINGS
} ftp_file_type_t;

typedef enum ftp_file_check_exist_e {
	FTP_FILE_CHECK_NOT_EXIST		= 0,
	FTP_FILE_CHECK_EXIST			= 1,

	FTP_FILE_CHECK_ERROR			= -1
} ftp_file_check_exist_t;

typedef struct ftpget_data_s {
	const void * data;
	uint32_t len;
	uint32_t written;
	bool transfer_finished;
} ftpget_data_t;

typedef int (* ftpget_data_handler_t)(ftpget_data_t * ftpget_data);

// =====================================================================================

static const char * const ftp_update_path = "/_data/gps-terminal/update/";
static const char * const ftp_conf_path = "/_data/gps-terminal/conf/";
static const char * const ftp_bin_path = "/_data/gps-terminal/bin/";
static const char * const ftp_log_file = "logs.txt";
static const char * const ftp_conf_file = "conf.json";
static const char * const ftp_new_conf_file = "confnew.json";

static uint8_t ftp_rec_buff[FTP_REC_BUF_SIZE];
static char ftp_rx_buffer[SIM800_FTP_MAX_DATA_SIZE];
static char ftp_tx_buffer[64];
static bool conf_file_must_be_updated = false;

static sim800_direct_param_t send_param = {
	.tx_buff = ftp_tx_buffer,
	.tx_len = sizeof(ftp_tx_buffer),
	.rx_buff = ftp_rx_buffer,
	.rx_len = sizeof(ftp_rx_buffer),
	.wait_ok = true,
	.timeout = 1000
};

// =====================================================================================

static uint32_t get_random_number();
static int ftp_setup_serial_direct();
static int ftp_setup_serial_at();
static int ftp_connect(const apn_t * apn, const ftp_host_sett_t * const host);
static int ftp_open_imei_path(const char * imei);
static int ftp_get_dfu_filename(const char * path, char * filename);
static int ftp_find_dfu_file(const char * path, char * filename, fw_version_t * fw_ver);
static int ftp_download_file(const char * path, const char * filename, ftpget_data_handler_t ftpget_data_handler);
static ftp_job_dfu_result_t ftp_try_dfu(ftp_job_param_t * job_param);
static int ftp_get_file_size(const char * path, const char * filename, int * filesize);
static int ftp_write_to_file(const char * path, const char * filename, const void * data, int len);
static int ftp_delete_file(const char * path, const char * filename);
static int ftp_write_log(const char * imei, const char * text);
static int ftp_arch_conf(const char * imei, const void * conf, size_t conf_data_len);
static ftp_file_check_exist_t ftp_file_check_exist(const char * path, const char * filename);
static ftp_job_conf_result_t ftp_new_conf_process(const char * imei);
static int ftp_conf_process(const char * imei);
static ftp_job_conf_result_t ftp_settings_process(ftp_job_param_t * job_param);

static int ftpget_dfu_data_handler(ftpget_data_t * ftpget_data);
static int ftpget_new_conf_data_handler(ftpget_data_t * ftpget_data);
static int ftpget_conf_data_handler(ftpget_data_t * ftpget_data);

// =====================================================================================

void ftp_job(ftp_job_param_t * job_param, ftp_job_result_t * job_result) {
	int common_result = -1;
	ftp_job_dfu_result_t dfu_result = {
			.code =FTP_JOB_DFU_RES_NO_FILE,
			.new_fw_ver.raw = {0}
	};
	ftp_job_conf_result_t conf_result = {
			.code = FTP_JOB_CONF_RES_NO_ACTION_RECUIRED
	};

	// --- Настройка и подключение к FTP ---

	common_result = ftp_setup_serial_direct();
	if (common_result != 0) {
		ZF_LOGE("FTP direct serial setup error");
		goto exit;
	}

//	while (1) {
//		osSignalSet(monitorTaskHandle, SIGNAL_TASK_GSM);
//		WDG_RESET(TASK_ALL);
//		osDelay(1000);
//	}

	common_result = ftp_connect(job_param->apn, &settings1_ram.ftp_host);
	if (common_result != 0) {
		ZF_LOGE("FTP connect error");
		goto exit;
	}

	common_result = ftp_open_imei_path(job_param->imei);
	if (common_result != 0) {
		ZF_LOGE("FTP open IMEI path error");
		goto exit;
	}

	// --- Чекаем новую прошивку ---

	dfu_result = ftp_try_dfu(job_param);
	if (dfu_result.code == FTP_JOB_DFU_RES_FILE_DOWNLOADED) {
		goto exit;
	}
	else if (dfu_result.code == FTP_JOB_DFU_RES_INTERNAL_ERROR) {
		ZF_LOGE("FTP try DFU error");
		goto exit;
	}
	else {
		ZF_LOGD("FTP DFU file not found");
	}

	// --- Чекаем настройки ---

	conf_result = ftp_settings_process(job_param);
	if (conf_result.code == FTP_JOB_CONF_RES_NEW_APPLIED) {
		ZF_LOGD("FTP new conf applied");;
	}
	else if (conf_result.code == FTP_JOB_CONF_RES_INTERNAL_ERROR) {
		ZF_LOGE("FTP conf process error");
	}
	else {
		ZF_LOGD("FTP conf no action required");
	}

	// --- Закончилши работу с FTP ---

	exit:

//	ret = ftp_disconnect();
//	if (ret != 0) {
//		goto exit;
//	}

	if (dfu_result.code == FTP_JOB_DFU_RES_FILE_DOWNLOADED) {
		char text_log[36];
		snprintf(text_log, sizeof(text_log), "Update_Success_to_v%d.%d", (int)dfu_result.new_fw_ver.major, (int)dfu_result.new_fw_ver.minor);
		ftp_write_log(job_param->imei, text_log);
	}
	else if (dfu_result.code == FTP_JOB_DFU_RES_INTERNAL_ERROR) {
		char text_log[36];
		snprintf(text_log, sizeof(text_log), "Update_Error_to_v%d.%d", (int)dfu_result.new_fw_ver.major, (int)dfu_result.new_fw_ver.minor);
		ftp_write_log(job_param->imei, text_log);
	}
	else {
		// ftp_write_log(job_param->imei, "Update_NA");
	}

	if (conf_result.code == FTP_JOB_CONF_RES_NEW_APPLIED) {
		ftp_write_log(job_param->imei, "Conf_Success");
	}
	else if (conf_result.code == FTP_JOB_CONF_RES_INTERNAL_ERROR) {
		ftp_write_log(job_param->imei, "Conf_Error");
	}
	else {
		// ftp_write_log(job_param->imei, "Conf_NA");
	}

	(void)ftp_setup_serial_at();

	job_result->dfu_res		= dfu_result;
	job_result->conf_res	= conf_result;
}

// =====================================================================================

static uint32_t get_random_number() {
	uint32_t n = HAL_GetTick();
	srand(n);
	n = rand();

	n += time(NULL);
	srand(n);
	n = rand();

	return n;
}

static ftp_file_check_exist_t ftp_file_check_exist(const char * path, const char * filename) {
	int filesize;

	if (ftp_get_file_size(path, filename, &filesize) != 0) {
		ZF_LOGE("File %s%s does't found", path, filename);
		return FTP_FILE_CHECK_ERROR;
	}
	else {
		if (filesize == -1) {
			// Это норма
			ZF_LOGD("File %s%s does't exist", path, filename);
			return FTP_FILE_CHECK_NOT_EXIST;
		}
		else {
			ZF_LOGI("File %s%s exist", path, filename);
			return FTP_FILE_CHECK_EXIST;
		}
	}
}

static ftp_job_conf_result_t ftp_new_conf_process(const char * imei) {
	ftp_job_conf_result_t result = {
			.code = FTP_JOB_CONF_RES_INTERNAL_ERROR
	};

	// "ftp_conf_path" + "imei" + "/" + '\0'
	char path[strlen(ftp_conf_path) + strlen(imei) + 1 + 1];
	sprintf(path, "%s%s/", ftp_conf_path, imei);

	const char * filename = ftp_new_conf_file;

	ZF_LOGD("New Conf check process start");

	ftp_file_check_exist_t ret = ftp_file_check_exist(path, filename);
	if (ret == FTP_FILE_CHECK_ERROR) {
		result.code = FTP_JOB_CONF_RES_INTERNAL_ERROR;
	}
	else if (ret == FTP_FILE_CHECK_EXIST) {
		int ret = ftp_download_file(path, filename, ftpget_new_conf_data_handler);
		if (ret == 0) {
			ZF_LOGI("New device configuration applied successfully");

			(void)ftp_delete_file(path, filename);

			result.code = FTP_JOB_CONF_RES_NEW_APPLIED;
		}
		else {
			ZF_LOGE("New device configuration was not applied");

			result.code = FTP_JOB_CONF_RES_INTERNAL_ERROR;
		}
	}
	else {
		// Файла не существует
		result.code = FTP_JOB_CONF_RES_NO_ACTION_RECUIRED;
	}

	return result;
}

static int ftp_conf_process(const char * imei) {
	// "ftp_conf_path" + "imei" + "/" + '\0'
	char path[strlen(ftp_conf_path) + strlen(imei) + 1 + 1];
	sprintf(path, "%s%s/", ftp_conf_path, imei);

	const char * filename = ftp_conf_file;

	ZF_LOGD("Conf check process start");

	// --- Проверяем наличие файла ---

	ftp_file_check_exist_t ret = ftp_file_check_exist(path, filename);
	if (ret == FTP_FILE_CHECK_ERROR) {
		return -1;
	}

	// --- Скачиваем файл, если он есть и проверяем, какие там настройки ---

	if (ret == FTP_FILE_CHECK_EXIST) {
		int ret = ftp_download_file(path, filename, ftpget_conf_data_handler);
		if (ret == 0) {
			if (!conf_file_must_be_updated) {
				ZF_LOGI("Conf file update is not required");
			}
		}
		else {
			return ret;
		}
	}
	else {	// FTP_FILE_CHECK_NOT_EXIST
		conf_file_must_be_updated = true;
	}

	// --- Выгружаем на FTP обновленные настройки, если это нужно ---

	if (conf_file_must_be_updated) {
		conf_file_must_be_updated = false;

		// Но сначала архивируем текущие настройки в папке /arch

		ZF_LOGI("Conf file archive...");

		// После загрузки файла в ftp_rec_buff лежит его содержимое. Костыль, но ладно
		int ret = ftp_arch_conf(imei, ftp_rec_buff, strlen((char*)ftp_rec_buff));
		if (ret != 0) {
			ZF_LOGE("Can't archive conf file");
			return ret;
		}

		// А потом уже выгружаем

		ZF_LOGI("Conf file update...");

		char * pconf = (char*)ftp_rec_buff;

		ret = pack_settings(pconf, FTP_REC_BUF_SIZE, &settings1_ram, &settings2_ram, &settings3_ram, &settings4_ram);
		if (ret != 0) {
			ZF_LOGE("Can't pack settings to json");
			return ret;
		}

		ret = ftp_write_to_file(path, filename, pconf, strlen(pconf));
		if (ret != 0) {
			ZF_LOGE("Conf file was not updated");
			return ret;
		}

		ZF_LOGI("Conf file updated successfully");
	}

	return 0;
}

static ftp_job_conf_result_t ftp_settings_process(ftp_job_param_t * job_param) {
	const char * imei = job_param->imei;

	// --- Загрузка новых настроек, если есть ---

	ftp_job_conf_result_t result = ftp_new_conf_process(imei);
	if (result.code == FTP_JOB_CONF_RES_INTERNAL_ERROR) {
		ZF_LOGE("Error while FTP new config processing");
		return result;
	}

	// --- Выгрузка текущих настроек, если требуется ---

	int ret = ftp_conf_process(imei);
	if (ret != 0) {
		ZF_LOGE("Error while FTP config processing");
	}

	/// \note Считаем, что результат ftp_conf_process не важен
	/// Не получилось выгрузить настройки сейчас - попробуем в следующий раз
	/// Поэтому наверх результат не отдаем

	return result;
}

static int ftp_setup_serial_direct() {
	send_param.rx_buff[0] = 0;

	// Отключаем прерывания UART и далее общаемся с модулем напрямую
	sim800_rx_it_stop();

	sim800_direct_uart_tx_str("AT\r");
	if (sim800_direct_uart_rx_str(send_param.rx_buff, "AT\r", 200) != 0) { return -1; }
	if (sim800_direct_uart_rx_str(send_param.rx_buff, "\r\nOK\r\n", 200) != 0) { return -1; }

	// Отключаем эхо, чтобы не мешало
	sim800_direct_uart_tx_str("ATE0\r");
	if (sim800_direct_uart_rx_str(send_param.rx_buff, "ATE0\r", 200) != 0) { return -1; }
	if (sim800_direct_uart_rx_str(send_param.rx_buff, "\r\nOK\r\n", 500) != 0) { return -1; }

	// Меняем скорость
	send_param.timeout = 1000;
	send_param.wait_ok = true;
	if (sim800_direct_send(&send_param, "AT+IPR=115200\r") != 0) { return -1; }

	huart2.Init.BaudRate = 115200;
	HAL_UART_Init(&huart2);

	// Шлем ATшки для проверки скорости
	uint8_t q = 0;
	for (; q < 20; q++) {
		send_param.timeout = 500;
		if (sim800_direct_send(&send_param, "AT\r") != 0) {
			user_printf("\r\nNo response on AT\r\n");
			HAL_Delay(100);
		}
		else {
			break;
		}
		WDG_RESET(TASK_ALL);
	}
	if (q == 20) {
		ZF_LOGE("UART direct communication error");
		return -1;;
	}

	return 0;
}

static int ftp_setup_serial_at() {
	send_param.timeout = 1000;
	send_param.wait_ok = true;

	// Включаем эхо
	if (sim800_direct_send(&send_param, "ATE1\r") != 0) { return -1; }

	sim800_rx_it_start();

	if (sim800_check_serial() != 0) {
		ZF_LOGE("UART at communication error");
		return -1;
	}

	// Возвращаем скорость
	ATCommander_response_t *rsp = sim800_send_at("AT+IPR=9600\r", NULL, 1, 1000);
	if(strstr(rsp[0].data, "OK") == NULL) {
		ZF_LOGE("UART at communication error");
		return -1;
	}

	huart2.Init.BaudRate = 9600;
	HAL_UART_Init(&huart2);
	sim800_rx_it_start();

	if (sim800_check_serial() != 0) {
		ZF_LOGE("UART at communication error");
		return -1;
	}

	return 0;
}

static int ftp_connect(const apn_t * apn, const ftp_host_sett_t * const host) {
	send_param.wait_ok = true;
	send_param.timeout = 1000;

	if (sim800_direct_send(&send_param, "AT+SAPBR=3,1,Contype,GPRS\r") != 0) { return -1; }
	if (sim800_direct_send(&send_param, "AT+SAPBR=3,1,APN,%s\r", apn->addr) != 0) { return -1; }

	if (apn->user && apn->pass) {
		if (sim800_direct_send(&send_param, "AT+SAPBR=3,1,USER,%s\r", apn->user) != 0) { return -1; }
		if (sim800_direct_send(&send_param, "AT+SAPBR=3,1,PWD,%s\r", apn->pass) != 0) { return -1; }
	}

	if (sim800_direct_send(&send_param, "AT+SAPBR=1,1\r") != 0) { return -1; }

	WDG_RESET(TASK_ALL);

	sim800_direct_uart_tx_str("AT+SAPBR=2,1\r");
	if (sim800_direct_uart_rx_str(send_param.rx_buff, "\r\n+SAPBR: 1,1", 5000) != 0) { return -1; }
	// Далее придет IP и \r\n, ждем \n
	if (sim800_direct_wait_byte('\n', 100) != 0) { return -1; }
	if (sim800_direct_uart_rx_str(send_param.rx_buff, "\r\nOK\r\n", 1000) != 0) { return -1; }

	WDG_RESET(TASK_ALL);

	if (sim800_direct_send(&send_param, "AT+FTPCID=1\r") != 0) { return -1; }

	if (sim800_direct_send(&send_param, "AT+FTPSERV=%s\r", host->address) != 0) { return -1; }
	if (sim800_direct_send(&send_param, "AT+FTPPORT=%s\r", host->port) != 0) { return -1; }

//	if (sim800_direct_send(&send_param, "AT+FTPUN=%s\r", host->login) != 0) { return -1; }
//	if (sim800_direct_send(&send_param, "AT+FTPPW=%s\r", host->password) != 0) { return -1; }

	// Этот логин с доступом на запись
	if (sim800_direct_send(&send_param, "AT+FTPUN=%s\r", "eap") != 0) { return -1; }
	if (sim800_direct_send(&send_param, "AT+FTPPW=%s\r", "KMAfg?9Y5f7~R~b$@DbG") != 0) { return -1; }

	WDG_RESET(TASK_ALL);

	return 0;
}

static int ftp_open_imei_path(const char * imei) {
	/// \warning Возможна перезагрузка по ватчдогу, если модуль будет долго дуплиться

	int p1, file_size;
	send_param.wait_ok = true;
	send_param.timeout = 90000;

	// Чекаем метафайл с логами

	// "ftp_conf_path" + "imei" + "/" + '\0'
	char path[strlen(ftp_conf_path) + strlen(imei) + 1 + 1];
	sprintf(path, "%s%s/", ftp_conf_path, imei);

	if (ftp_get_file_size(path, ftp_log_file, &file_size) != 0) { return -1; }

	bool file_exist = (file_size >= 0);

	if (!file_exist) {
		// Метафайла нету. Создаем директорию и файл

		if (sim800_direct_send(&send_param, "AT+FTPGETPATH=%s\r", path) != 0) { return -1; }
		if (sim800_direct_send(&send_param, "AT+FTPGETNAME=%s\r", ftp_log_file) != 0) { return -1; }

		if (sim800_direct_send(&send_param, "AT+FTPMKD\r") != 0) { return -1; }
		if (sim800_direct_wait_line_starting_with(&send_param, "+FTPMKD") != 0) { return -1; }
		if (sscanf(send_param.rx_buff, "+FTPMKD: 1,%d", &p1) != 1) { return -1; }

		if (p1 == 0) {
			ZF_LOGI("%s path created", path);
		}
		else if (p1 == 77) {
			ZF_LOGD("%s path already exist", path);
		}
		else {
			ZF_LOGE("%s path create (AT+FTPMKD) error: %d", path, p1);
			return -1;
		}

		WDG_RESET(TASK_ALL);

		char text[128];

		time_t current_time = time(NULL);
	    char * c_time_string = ctime(&current_time);
	    // Обрезаем '\n' в конце
	    c_time_string[strlen(c_time_string) - 1] = '\0';

		sprintf(text, "%s : %s - Directory created\r\n", c_time_string, imei);

		if (ftp_write_to_file(path, ftp_log_file, text, strlen(text)) != 0) {
			ZF_LOGE("Write to file %s%s error", path, ftp_log_file);
			return -1;
		}

		ZF_LOGI("%s%s file created", path, ftp_log_file);

		WDG_RESET(TASK_ALL);
	}
	else {
		ZF_LOGD("%s%s file already exist", path, ftp_log_file);
	}

	return 0;
}

static int ftp_get_dfu_filename(const char * path, char * filename) {
	int p1, p2;
	bool wait_list_data = false;
	uint32_t start_time = HAL_GetTick();

	send_param.wait_ok = true;
	send_param.timeout = 90000;
	filename[0] = '\0';

	if (sim800_direct_send(&send_param, "AT+FTPGETPATH=%s\r", path) != 0) { return -1; }

	if (sim800_direct_send(&send_param, "AT+FTPLIST=1\r") != 0) { return -1; }
	if (sim800_direct_wait_line_starting_with(&send_param, "+FTPLIST") != 0) { return -1; }
	if (sscanf(send_param.rx_buff, "+FTPLIST: 1,%d", &p1) != 1) { return -1; }
	if (p1 == 0) {
		// Каталог пуст
		return 0;
	}
	else if (p1 != 1) {
		ZF_LOGE("%s directory scan (AT+FTPLIST) error: %d", path, p1);
		return -1;
	}

	WDG_RESET(TASK_ALL);

	send_param.wait_ok = false;
	wait_list_data = true;

	if (sim800_direct_send(&send_param, "AT+FTPLIST=2,%d\r", send_param.rx_len) != 0) { return -1; }

	do {
		WDG_RESET(TASK_ALL);
		osSignalSet(monitorTaskHandle, SIGNAL_TASK_GSM);

		if (HAL_GetTick() - start_time > 90000) {
			ZF_LOGE("ftp_get_dfu_filename timeout");
			return -1;
		}

		if (sim800_direct_wait_line(&send_param) != 0) { return -1; }

		char * ptr = strstr(send_param.rx_buff, "+FTPLIST");
		if (ptr != NULL) {
			if (sscanf(send_param.rx_buff, "+FTPLIST: %d,%d", &p1, &p2) != 2) { return -1; }

			if (p1 == 1) {
				// Событие +FTPLIST: 1,<>
				if (p2 == 1) {
					// Есть еще данные. Запрашиваем
					if (!wait_list_data) {
						if (sim800_direct_send(&send_param, "AT+FTPLIST=2,%d\r", send_param.rx_len) != 0) { return -1; }
						wait_list_data = true;
					}
				}
				else if (p2 == 0) {
					// Конец данных
					// Если искомый файл найден, то он уже зафиксирован в filename до этого
					return 0;
				}
				else {
					// Ошибка
					ZF_LOGE("%s directory scan (AT+FTPLIST) error: %d", path, p2);
					return -1;
				}
			}
			else { // p1 == 2
				// Ответ на AT+FTPLIST=2,<>
			}

			continue;
		}

		ptr = strstr(send_param.rx_buff, "gps-terminal");
		if (ptr != NULL) {
			if (sscanf(ptr, "gps-terminal_%d.%d", &p1, &p2) == 2) {
				strncpy(filename, ptr, 36);
			}

			continue;
		}

		ptr = strstr(send_param.rx_buff, "OK");
		if (ptr != NULL) {
			wait_list_data = false;
			continue;
		}
	} while (1);
}

static int ftp_find_dfu_file(const char * path, char * filename, fw_version_t * fw_ver) {
	ZF_LOGD("Search OTA DFU file in %s", path);

	if (ftp_get_dfu_filename(path, filename) != 0) { return -1; }

	if (filename[0] != '\0') {
		int major, minor;

		if (sscanf(filename, "gps-terminal_%d.%d", &major, &minor) == 2) {
			ZF_LOGI("OTA DFU file found: %s%s", path, filename);

			fw_ver->major = major;
			fw_ver->minor = minor;
		}
		else {
			filename[0] = '\0';
			ZF_LOGW("Wrong firmware file name: %s", filename);
			// Будем считать, что файл не нашли. Это не ошибка
			// Ошибкой считаем ошибку взаимодействия с модулем
		}
	}

	return 0;
}

static int ftp_download_file(const char * path, const char * filename, ftpget_data_handler_t ftpget_data_handler) {
	int p1, p2, file_size;
	uint32_t start_time = HAL_GetTick();

	ZF_LOGI("%s%s file downloading...", path, filename);

	if (ftpget_data_handler == NULL) {
		ZF_LOGE("ftpget_data_handler = NULL");
		return -1;
	}

	ftpget_data_t ftpget_data = {
			.written = 0,
			.data = send_param.rx_buff,
			.len = 0,
			.transfer_finished = false,
	};

	send_param.wait_ok = true;
	send_param.timeout = 90000;

	if (ftp_get_file_size(path, filename, &file_size) != 0) { return -1; }

	if (file_size == 0) {
		ZF_LOGW("File %s%s is empty", path, filename);
		return -1;
	}
	else if (file_size == -1) {
		// Файла не существует
		ZF_LOGW("File %s%s does't exist", path, filename);
		return -1;
	}

	if (sim800_direct_send(&send_param, "AT+FTPGETPATH=%s\r", path) != 0) { return -1; }
	if (sim800_direct_send(&send_param, "AT+FTPGETNAME=%s\r", filename) != 0) { return -1; }

	if (sim800_direct_send(&send_param, "AT+FTPGET=1\r") != 0) { return -1; }
	if (sim800_direct_wait_line_starting_with(&send_param, "+FTPGET") != 0) { return -1; }
	if (sscanf(send_param.rx_buff, "+FTPGET: 1,%d", &p1) != 1) { return -1; }
	if (p1 != 1) {
		ZF_LOGE("File download (AT+FTPGET) error: %d", p1);
		return -1;
	}

	WDG_RESET(TASK_ALL);

	int packet_cnt = 0;
	int written_bytes = 0;

	send_param.wait_ok = false;

	if (sim800_direct_send(&send_param, "AT+FTPGET=2,%d\r", send_param.rx_len) != 0) { return -1; }

	do {
		WDG_RESET(TASK_ALL);
		osSignalSet(monitorTaskHandle, SIGNAL_TASK_GSM);

		if (HAL_GetTick() - start_time > 300000) {
			ZF_LOGE("Ffile download timeout");
			return -1;
		}

		if (sim800_direct_wait_line_starting_with(&send_param, "+FTPGET") != 0) { return -1; }
		if (sscanf(send_param.rx_buff, "+FTPGET: %d,%d", &p1, &p2) != 2) { return -1; }

		if (p1 == 1) {
			if (p2 == 1) {
				// Событие +FTPGET: 1,1
				// Есть еще данные. Запрашиваем
				if (sim800_direct_send(&send_param, "AT+FTPGET=2,%d\r", send_param.rx_len) != 0) { return -1; }
			}
			else if (p2 == 0) {
				// Событие +FTPGET: 1,0
				// Конец данных. Файл загружен

				ftpget_data.written = written_bytes;
				ftpget_data.transfer_finished = true;

				ZF_LOGD("%s%s file downloaded successfully", path, filename);

				if (ftpget_data_handler(&ftpget_data) != 0) {
					ZF_LOGE("Some error while last FTPGET data handling");
					return -1;
				}

				return 0;
			}
			else {
				// Событие +FTPGET: 1,<err>
				ZF_LOGE("File download (AT+FTPGET) error: %d", p2);
				return -1;
			}
		}
		else { // p1 == 2
			// Ответ на AT+FTPGET=2,<reqlen>
			// Событие +FTPGET: 2,<rsplen>
			// Следом пойдут полезные данные

			int len = p2;
			if (len > 0) {
				if (sim800_direct_uart_rx_data(send_param.rx_buff, len, 5000) != 0) { return -1; }

				char tmp[sizeof("\r\nOK\r\n")];
				if (sim800_direct_uart_rx_str(tmp, "\r\nOK\r\n", send_param.timeout) != 0) { return -1; }

				ftpget_data.len = len;
				ftpget_data.written = written_bytes;

				if (ftpget_data_handler(&ftpget_data) != 0) {
					ZF_LOGE("Some error while FTPGET data handling");
					return -1;
				}

				written_bytes += len;
				packet_cnt++;
				user_printf(">>> packet %d received (%d / %d bytes)\r\n", packet_cnt, written_bytes, file_size);
			}

			if (written_bytes < file_size) {
				if (sim800_direct_send(&send_param, "AT+FTPGET=2,%d\r", send_param.rx_len) != 0) { return -1; }
			}
		}
	} while (1);

	return -1;
}

static ftp_job_dfu_result_t ftp_try_dfu(ftp_job_param_t * job_param) {
	ftp_job_dfu_result_t dfu_result = {
			.code = FTP_JOB_DFU_RES_NO_FILE,
			.new_fw_ver.raw = {0}
	};
	fw_version_t new_fw_ver;
	char filename[48];
	const char * imei = job_param->imei;

	// "ftp_conf_path" + "imei" + "/" + '\0'
	size_t len1 = strlen(ftp_conf_path) + strlen(imei) + 1 + 1;
	// "ftp_update_path" + '\0'
	size_t len2 = strlen(ftp_update_path) + 1;
	// "ftp_bin_path" + '\0'
	size_t len3 = strlen(ftp_bin_path) + 1;
	// Maximum path length
	size_t len = MAX(len1, MAX(len2, len3));

	char path[len];

	// --- Принудительное обновление sms-командой Firmware ---

	if (job_param->req_src == FTP_JOB_REQ_SRC_SMS_FIRMWARE) {
		sprintf(path, "%s", ftp_bin_path);
		strcpy(filename, job_param->filename);

		int major, minor;
		if (sscanf(filename, "gps-terminal_%d.%d", &major, &minor) == 2) {
			ZF_LOGI("OTA DFU file found: %s%s", path, filename);

			new_fw_ver.major = major;
			new_fw_ver.minor = minor;

			goto download;
		}
		else {
			ZF_LOGW("Wrong firmware file name: %s", filename);
		}

		// При sms-команде Firmware ничего дальше не делаем
		// Исполняем только эту команду

		dfu_result.code = FTP_JOB_DFU_RES_INTERNAL_ERROR;
		return dfu_result;
	}


	ZF_LOGI("OTA DFU Check");

	// --- Поиск файла в папке conf/imei ---

	sprintf(path, "%s%s/", ftp_conf_path, imei);

	if (ftp_find_dfu_file(path, filename, &new_fw_ver) != 0) {
		dfu_result.code = FTP_JOB_DFU_RES_INTERNAL_ERROR;
		return dfu_result;
	}

	if (filename[0] != '\0') {
		uint32_t ver_local = (uint32_t)board_status.fw_ver[0] | (uint32_t)board_status.fw_ver[1] << 16;
		uint32_t ver_ota = (uint32_t)new_fw_ver.raw[0] | (uint32_t)new_fw_ver.raw[1] << 16;

		if (ver_ota != ver_local) {
			goto download;
		}
		else {
			ZF_LOGI("Don't want to DFU because of my version is %d.%d", board_status.fw_ver[1], board_status.fw_ver[0]);

			dfu_result.code = FTP_JOB_DFU_RES_NO_FILE;
			return dfu_result;
		}
	}

	// --- Поиск файла в папке update ---

	sprintf(path, "%s", ftp_update_path);

	if (ftp_find_dfu_file(path, filename, &new_fw_ver) != 0) {
		dfu_result.code = FTP_JOB_DFU_RES_INTERNAL_ERROR;
		return dfu_result;
	}

	if (filename[0] != '\0') {
		uint32_t ver_local = (uint32_t)board_status.fw_ver[0] | (uint32_t)board_status.fw_ver[1] << 16;
		uint32_t ver_ota = (uint32_t)new_fw_ver.raw[0] | (uint32_t)new_fw_ver.raw[1] << 16;

		if (ver_ota > ver_local) {
			goto download;
		}
		else {
			ZF_LOGI("Don't want to DFU because of my version is %d.%d", board_status.fw_ver[1], board_status.fw_ver[0]);

			dfu_result.code = FTP_JOB_DFU_RES_NO_FILE;
			return dfu_result;
		}
	}

	ZF_LOGI("OTA DFU file not found");

	dfu_result.code = FTP_JOB_DFU_RES_NO_FILE;
	return dfu_result;


	download:

	ZF_LOGI("OTA DFU file downloading...");

	char text[128];

	time_t current_time = time(NULL);
    char * c_time_string = ctime(&current_time);
    // Обрезаем '\n' в конце
    c_time_string[strlen(c_time_string) - 1] = '\0';

    fw_version_t loc_ver = {
    		.major = board_status.fw_ver[1],
			.minor = board_status.fw_ver[0],
    };

	sprintf(text, "%s : %s - Update v%d.%d to v%d.%d. ",
							c_time_string, imei,
							loc_ver.major, loc_ver.minor,
							new_fw_ver.major, new_fw_ver.minor);

	int ret = ftp_download_file(path, filename, ftpget_dfu_data_handler);
	if (ret == 0) {
		ZF_LOGI("OTA DFU file downloaded");
		strcat(text, "Firmware downloaded. Reboot\r\n");

		dfu_result.code = FTP_JOB_DFU_RES_FILE_DOWNLOADED;
	}
	else {
		ZF_LOGE("OTA DFU file download Error");
		strcat(text, "Error\r\n");

		dfu_result.code = FTP_JOB_DFU_RES_INTERNAL_ERROR;
	}

	ZF_LOGD("FTP write log %s", text);

	if (ftp_write_to_file(path, ftp_log_file, text, strlen(text)) != 0) {
		ZF_LOGE("Write to file %s%s error", path, ftp_log_file);
		// Не зависимо от того, успешно ли записали лог,
		// на общий результат это не влияет.
		// Если загрузили файл прошивки, то ОК
	}

	dfu_result.new_fw_ver = new_fw_ver;

	return dfu_result;
}

static int ftp_get_file_size(const char * path, const char * filename, int * filesize) {
	send_param.wait_ok = true;
	send_param.timeout = 90000;

	if (sim800_direct_send(&send_param, "AT+FTPGETPATH=%s\r", path) != 0) { return -1; }
	if (sim800_direct_send(&send_param, "AT+FTPGETNAME=%s\r", filename) != 0) { return -1; }

	if (sim800_direct_send(&send_param, "AT+FTPSIZE\r") != 0) { return -1; }
	if (sim800_direct_wait_line_starting_with(&send_param, "+FTPSIZE") != 0) { return -1; }

	if (sscanf(send_param.rx_buff, "+FTPSIZE: 1,0,%d", filesize) != 1) {
		// Файла не существует
		*filesize = -1;
	}

	WDG_RESET(TASK_ALL);

	return 0;
}

static int ftp_write_to_file(const char * path, const char * filename, const void * data, int len) {

	/// Реализация через FTPEXTPUT

//	int filesize;
//
//	if (ftp_get_file_size(path, filename, &filesize) != 0) { return -1; }
//
//	if (filesize == -1) {
//		// Если файла не существует создаем его записью с нулевой позиции
//		filesize = 0;
//	}
//
//	send_param.wait_ok = true;
//	send_param.timeout = 60000;
//
//	if (sim800_direct_send(&send_param, "AT+FTPPUTPATH=%s\r", path) != 0) { return -1; }
//	if (sim800_direct_send(&send_param, "AT+FTPPUTNAME=%s\r", filename) != 0) { return -1; }
//
//	if (sim800_direct_send(&send_param, "AT+FTPEXTPUT=1\r") != 0) { return -1; }
//
//	send_param.wait_ok = false;
//	if (sim800_direct_send(&send_param, "AT+FTPEXTPUT=2,%d,%d,%d\r", filesize, len, send_param.timeout) != 0) { ZF_LOGE("1"); return -1; }
//	char buff[48];
//	sprintf(buff, "\r\n+FTPEXTPUT: %d,%d", filesize, len);
//	if (sim800_direct_uart_rx_str(send_param.rx_buff, buff, 3000) != 0) { return -1; }
//
//	if (sim800_direct_uart_tx_data(data, len) != 0) { return -1; }
//	if (sim800_direct_uart_rx_str(send_param.rx_buff, "\r\nOK\r\n", send_param.timeout) != 0) { return -1; }
//
//	WDG_RESET(TASK_ALL);
//
//	send_param.wait_ok = true;
//	if (sim800_direct_send(&send_param, "AT+FTPPUT=1\r") != 0) { ZF_LOGE("7"); return -1; }
//	if (sim800_direct_wait_line_starting_with(&send_param, "+FTPPUT") != 0) { ZF_LOGE("8"); return -1; }
//	if (strstr(send_param.rx_buff, "+FTPPUT: 1,0") == NULL) { ZF_LOGE("9"); return -1; }
//
//	WDG_RESET(TASK_ALL);
//
//	if (sim800_direct_send(&send_param, "AT+FTPEXTPUT=0\r") != 0) { ZF_LOGE("10"); return -1; }

	//---------------------------------------------

	/// Реализация через классический FTPPUT

	send_param.wait_ok = true;
	send_param.timeout = 60000;

	if (sim800_direct_send(&send_param, "AT+FTPPUTPATH=%s\r", path) != 0) { return -1; }
	if (sim800_direct_send(&send_param, "AT+FTPPUTNAME=%s\r", filename) != 0) { return -1; }

	if (sim800_direct_send(&send_param, "AT+FTPPUT=1\r") != 0) { return -1; }
//	if (sim800_direct_wait_line_starting_with(&send_param, "+FTPPUT") != 0) { return -1; }
//	if (sscanf(send_param.rx_buff, "+FTPPUT: 1,1,%d", &p1) != 1) { return -1; }

	WDG_RESET(TASK_ALL);

	const uint8_t * _data = data;
	send_param.wait_ok = false;

	while (1) {
		int p1, part_len;

		if (sim800_direct_wait_line_starting_with(&send_param, "+FTPPUT") != 0) { return -1; }
		if (sscanf(send_param.rx_buff, "+FTPPUT: 1,1,%d", &part_len) != 1) { return -1; }

		if (len <= 0) {
			break;
		}

		if (len < part_len) {
			part_len = len;
		}

		if (sim800_direct_send(&send_param, "AT+FTPPUT=2,%d\r", part_len) != 0) { return -1; }
		if (sim800_direct_wait_line_starting_with(&send_param, "+FTPPUT") != 0) { return -1; }
		if (sscanf(send_param.rx_buff, "+FTPPUT: 2,%d", &p1) != 1) { return -1; }
		if (p1 != part_len) { return -1; }

		sim800_direct_uart_tx_data(_data, part_len);
		sim800_direct_uart_rx_str(send_param.rx_buff, "\r\nOK\r\n", send_param.timeout);

		WDG_RESET(TASK_ALL);

		len -= part_len;
		_data += part_len;
	}

	send_param.wait_ok = true;

	if (sim800_direct_send(&send_param, "AT+FTPPUT=2,0\r") != 0) { return -1; }
	if (sim800_direct_wait_line_starting_with(&send_param, "+FTPPUT") != 0) { return -1; }
	if (strstr(send_param.rx_buff, "+FTPPUT: 1,0") == NULL) { return -1; }

	ZF_LOGD("FTP write to file %s%s success", path, filename);

	return 0;
}

static int ftp_delete_file(const char * path, const char * filename) {
	send_param.wait_ok = true;
	send_param.timeout = 90000;

	if (sim800_direct_send(&send_param, "AT+FTPGETPATH=%s\r", path) != 0) { return -1; }
	if (sim800_direct_send(&send_param, "AT+FTPGETNAME=%s\r", filename) != 0) { return -1; }

	if (sim800_direct_send(&send_param, "AT+FTPDELE\r", path) != 0) { return -1; }
	if (sim800_direct_wait_line_starting_with(&send_param, "+FTPDELE: 1,0") != 0) { return -1; }

	ZF_LOGI("File %s%s deleted", path, filename);

	return 0;
}

static int ftp_write_log(const char * imei, const char * text) {
	int p1;
	static const char *logs_dir = "logs";
	char filename[48];
	// "ftp_conf_path" + "imei" + "/" + "logs_dir" + "/" + '\0'
	char path[strlen(ftp_conf_path) + strlen(imei) + 1 + strlen(logs_dir) + 1 + 1];

	sprintf(path, "%s%s/%s/", ftp_conf_path, imei, logs_dir);

	send_param.wait_ok = true;
	send_param.timeout = 90000;

	ZF_LOGD("FTP writing log: %s", text);

	// Создаем директорию logs_dir

	if (sim800_direct_send(&send_param, "AT+FTPGETPATH=%s\r", path) != 0) { return -1; }
//	if (sim800_direct_send(&send_param, "AT+FTPGETNAME=%s\r", ftp_log_file) != 0) { return -1; }

	if (sim800_direct_send(&send_param, "AT+FTPMKD\r") != 0) { return -1; }
	if (sim800_direct_wait_line_starting_with(&send_param, "+FTPMKD") != 0) { return -1; }
	if (sscanf(send_param.rx_buff, "+FTPMKD: 1,%d", &p1) != 1) { return -1; }

	if (p1 == 0) {
		ZF_LOGI("%s path created", path);
	}
	else if (p1 == 77) {
		ZF_LOGD("%s path already exist", path);
	}
	else {
		ZF_LOGE("%s path create (AT+FTPMKD) error: %d", path, p1);
		return -1;
	}

	WDG_RESET(TASK_ALL);

	char time_string[24];
	time_string[0] = '\0';
	time_t current_time = time(NULL);
	struct tm * timeinfo = localtime(&current_time);

	strftime(time_string, sizeof(time_string), "%Y-%m-%d_%X", timeinfo);
	int ret = snprintf(filename, sizeof(filename), "%s_%s.txt", time_string, text);

	if (ret >= sizeof(filename)) {
		ZF_LOGE("FTP log text too long");
		return -1;
	}

	ret = ftp_write_to_file(path, filename, filename, strlen(filename));
	if (ret != 0) {
		ZF_LOGE("ftp_write_to_file error");
	}

	WDG_RESET(TASK_ALL);

	return ret;
}

static int ftp_arch_conf(const char * imei, const void * conf_data, size_t conf_data_len) {
	int p1;
	static const char *arch_dir = "arch";
	char filename[32];
	// "ftp_conf_path" + "imei" + "/" + "logs_dir" + "/" + '\0'
	char path[strlen(ftp_conf_path) + strlen(imei) + 1 + strlen(arch_dir) + 1 + 1];

	sprintf(path, "%s%s/%s/", ftp_conf_path, imei, arch_dir);

	send_param.wait_ok = true;
	send_param.timeout = 90000;

	// Создаем директорию logs_dir

	if (sim800_direct_send(&send_param, "AT+FTPGETPATH=%s\r", path) != 0) { return -1; }
//	if (sim800_direct_send(&send_param, "AT+FTPGETNAME=%s\r", ftp_log_file) != 0) { return -1; }

	if (sim800_direct_send(&send_param, "AT+FTPMKD\r") != 0) { return -1; }
	if (sim800_direct_wait_line_starting_with(&send_param, "+FTPMKD") != 0) { return -1; }
	if (sscanf(send_param.rx_buff, "+FTPMKD: 1,%d", &p1) != 1) { return -1; }

	if (p1 == 0) {
		ZF_LOGI("%s path created", path);
	}
	else if (p1 == 77) {
		ZF_LOGD("%s path already exist", path);
	}
	else {
		ZF_LOGE("%s path create (AT+FTPMKD) error: %d", path, p1);
		return -1;
	}

	WDG_RESET(TASK_ALL);

	unsigned int hash1 = get_random_number();
	HAL_Delay(10);
	unsigned int hash2 = get_random_number();
	int ret = snprintf(filename, sizeof(filename), "conf_%8x%8x.json", hash1, hash2);

	if (ret >= sizeof(filename)) {
		ZF_LOGE("FTP filename too long");
		return -1;
	}

	ret = ftp_write_to_file(path, filename, conf_data, conf_data_len);
	if (ret != 0) {
		ZF_LOGE("ftp_write_to_file error");
	}

	WDG_RESET(TASK_ALL);

	return ret;
}

// ------------------------------------------------------------------------------------------

static int ftpget_dfu_data_handler(ftpget_data_t * ftpget_data) {
	int ret;
	static const uint32_t crc_size = sizeof(uint32_t);

	if (ftpget_data->transfer_finished == false) {
		if (ftpget_data->written == 0) {
			ret = erase_user_internal_flash();
			if (ret != 0) {
				ZF_LOGE("Int flash erase error");
				return -1;
			}
		}

		ret = user_internal_flash_program(USER_INT_FLASH_ADDR + crc_size + ftpget_data->written, ftpget_data->data, ftpget_data->len);
	}
	else {
		// Пишем размер скачанного файла минус 4 байта CRC
		ftpget_data->written -= crc_size;
		ret = user_internal_flash_program(USER_INT_FLASH_ADDR, &ftpget_data->written, crc_size);
	}

	if (ret != 0) {
		ZF_LOGE("Int flash program error");
	}

	return ret;
}

static int ftpget_new_conf_data_handler(ftpget_data_t * ftpget_data) {
	if (ftpget_data->transfer_finished == false) {
		static uint32_t count = 0;

		if (ftpget_data->written == 0) {
			count = 0;
		}

		if (ftpget_data->len <= FTP_REC_BUF_SIZE - count) {
			memcpy(ftp_rec_buff + ftpget_data->written, ftpget_data->data, ftpget_data->len);
			count += ftpget_data->len;
			return 0;
		}
		else {
			ZF_LOGE("%s file too large", ftp_new_conf_file);
		}
	}
	else {
		settings1_t sett1 = {0};
		settings2_t sett2 = {0};
		settings3_t sett3 = {0};
		settings4_t sett4 = {0};
		ftp_rec_buff[ftpget_data->written] = 0;

		int res = parse_settings((char*)ftp_rec_buff, &sett1, &sett2, &sett3, &sett4);
		if (res == 0) {
			taskENTER_CRITICAL();

			sett1.rom.odometer = settings1_ram.rom.odometer;
			memcpy(&settings1_ram, &sett1, sizeof(settings1_t));
			memcpy(&settings2_ram, &sett2, sizeof(settings2_t));
			memcpy(&settings3_ram, &sett3, sizeof(settings3_t));
			memcpy(&settings4_ram, &sett4, sizeof(settings4_t));

			taskEXIT_CRITICAL();

			res = write_settings(SETT_AREA_EXT_INT_FLASH);

			return res;
		}
		else {
			ZF_LOGE("%s file parse error", ftp_new_conf_file);
		}
	}

	return -1;
}

static int ftpget_conf_data_handler(ftpget_data_t * ftpget_data) {
	if (ftpget_data->transfer_finished == false) {
		static uint32_t count = 0;

		if (ftpget_data->written == 0) {
			count = 0;
		}

		if (ftpget_data->len <= FTP_REC_BUF_SIZE - count) {
			memcpy(ftp_rec_buff + ftpget_data->written, ftpget_data->data, ftpget_data->len);
			count += ftpget_data->len;
			return 0;
		}
		else {
			ZF_LOGE("%s file too large", ftp_conf_file);
		}
	}
	else {
		settings1_t sett1 = {0};
		settings2_t sett2 = {0};
		settings3_t sett3 = {0};
		settings4_t sett4 = {0};
		ftp_rec_buff[ftpget_data->written] = 0;

		int res = parse_settings((char*)ftp_rec_buff, &sett1, &sett2, &sett3, &sett4);
		if (res == 0) {
			taskENTER_CRITICAL();

			sett1.rom.odometer = settings1_ram.rom.odometer;
			int nequal = abs(memcmp(&settings1_ram, &sett1, sizeof(settings1_t)));
			nequal += abs(memcmp(&settings2_ram, &sett2, sizeof(settings2_t)));
			nequal += abs(memcmp(&settings3_ram, &sett3, sizeof(settings3_t)));
			nequal += abs(memcmp(&settings4_ram, &sett4, sizeof(settings4_t)));

			taskEXIT_CRITICAL();

			if (nequal != 0) {
				conf_file_must_be_updated = true;
			}

			return 0;
		}
		else {
			ZF_LOGE("%s file parse error", ftp_conf_file);
		}
	}

	return -1;
}

