/*
 * ftp.h
 *
 *  Created on: 18 дек. 2021 г.
 *      Author: chuyec
 */

#ifndef FTP_FTP_H_
#define FTP_FTP_H_

#include "sim800/sim800.h"
#include "common.h"
#include "settings/settings.h"

// =====================================================================================

typedef enum ftp_job_req_src_e {
	FTP_JOB_REQ_SRC_BOOT,
	FTP_JOB_REQ_SRC_TIMER,
	FTP_JOB_REQ_SRC_SMS_UPDATE,
	FTP_JOB_REQ_SRC_SMS_FIRMWARE,
} ftp_job_req_src_t;

typedef struct ftp_job_param_s {
	const char * imei;
	const apn_t * apn;
	const char * filename;
	ftp_job_req_src_t req_src;
} ftp_job_param_t;


typedef enum ftp_job_dfu_result_code_e {
	FTP_JOB_DFU_RES_FILE_DOWNLOADED		= 0,
	FTP_JOB_DFU_RES_NO_FILE				= 1,

	FTP_JOB_DFU_RES_INTERNAL_ERROR		= -1
} ftp_job_dfu_result_code_t;

typedef struct ftp_job_dfu_result_s {
	ftp_job_dfu_result_code_t code;
	fw_version_t new_fw_ver;
} ftp_job_dfu_result_t;


typedef enum ftp_job_conf_result_code_e {
	FTP_JOB_CONF_RES_NEW_APPLIED		= 0,
	FTP_JOB_CONF_RES_NO_ACTION_RECUIRED	= 1,

	FTP_JOB_CONF_RES_INTERNAL_ERROR		= -1
} ftp_job_conf_result_code_t;

typedef struct ftp_job_conf_result_s {
	ftp_job_conf_result_code_t code;
} ftp_job_conf_result_t;


typedef struct ftp_job_result_s {
	ftp_job_dfu_result_t	dfu_res;
	ftp_job_conf_result_t	conf_res;
} ftp_job_result_t;


// =====================================================================================

void ftp_job(ftp_job_param_t * job_param, ftp_job_result_t * job_result);

#endif /* FTP_FTP_H_ */
