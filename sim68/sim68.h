/*
 * sim68.h
 *
 *  Created on: 10 авг. 2018 г.
 *      Author: Denis Shreiber
 */

#ifndef SIM68_H_
#define SIM68_H_

#include <time.h>
#include <stdbool.h>

//=======================================================================

/// Флаги состояния модуля
typedef volatile struct {
	bool time_received;				/// Время сети определено
}sim68_status_t;

typedef enum {
	GNSS_VALID = 'A',
	GNSS_NOT_VALID = 'V',
} gnss_valid_status_t;

typedef enum {
	GNSS_FIX_NOT_AVAILABLE = 1,
	GNSS_FIX_2D,
	GNSS_FIX_3D,
} gnss_fix_t;

typedef struct __packed {
	uint32_t unix_time;					/// Unix время
	double latitude;					/// DD.DDDDDD
	double longitude;					/// DDD.DDDDDD
	float speed;						/// Скорость [км/ч]
	float course;						/// Курс [град]
	float hdop;							/// Точность по горизонтали (2D)
	float altitude;						/// Высота [м]
	uint8_t satellites;					/// Количество спутников
} tracked_gnss_data_t;

/// Данные GNSS (GPS and/or GLONASS)
typedef struct {
	bool run_status;						/// GPS on/off
	bool have_time;							/// Есть ли время
	gnss_valid_status_t valid_status;		/// Признаки валидности данных
	gnss_fix_t fix;							/// Нет, 2D или 3D
	tracked_gnss_data_t tracked;			/// Данные GPS, которые непосредственно отправляются
	uint32_t pkt_cnt;
} sim68_gnss_data_t;


//=======================================================================

/// Состояние модуля
extern sim68_status_t sim68_status;

//=======================================================================

void sim68_rx_handler();
void sim68_serial_error_handler();
void sim68_tx_complete_handler();
void sim68_timeout_handler();

int sim68_init();

char *sim68_send_cmd(const char *cmd, const char * expected_response, uint32_t timeout);
char *sim68_send_cmd_repeat(const char *cmd, const char * expected_response, uint32_t timeout, int repeat_cnt);

void sim68_power_on();
void sim68_power_off();
void sim68_force_hard_reset();
void sim68_release_hard_reset();
void sim68_hard_reset();

int sim68_check_serial();

void sim68_gnss_data_get(sim68_gnss_data_t * _gnss_data);
int sim68_gnss_get_snr();

float sim68_get_odometer_value(void);
int sim68_set_odometer_value(float val);
int sim68_set_static_nav_speed_threshold(float speed_kmph);
int sim68_tracker_mode_init(void);
int sim68_sleep();

#endif /* SIM68_H_ */
