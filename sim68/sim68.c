/*
 * sim68.c
 *
 *  Created on: 10 авг. 2018 г.
 *      Author: Denis Shreiber
 */

#include "sim68.h"

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "tim.h"
#include "gpio.h"
#include "usart.h"
#include "rtos.h"
#include "usbd_cdc_if.h"
#include "usb_device.h"
#include "arm_math.h"
#include "settings/settings.h"

#include "common.h"
#include "log/log.h"

//=======================================================================

#define SERIAL_OBJECT				huart6

#define TIM_OBJECT					htim10
#define TIM_DBG_FREEZE()			__HAL_DBGMCU_FREEZE_TIM10()
#define TIM_CLEAR_IT(it)			__HAL_TIM_CLEAR_FLAG(&TIM_OBJECT, it)

#define QUEUE_SIZE					256
#define RX_BUFF_LEN					256
#define RSP_BUFF_LEN				64


enum nmea_received_e {
	NMEA_GGA	= 1,
	NMEA_GSA	= 2,
	NMEA_RMC	= 4,
};


extern void rtc_write(time_t t);

//=======================================================================

#define PRINT_ALL_SERIAL_DATA		0
#if PRINT_ALL_SERIAL_DATA
	#define PRINTX(data, len)		printX(data, len)
#else
	#define PRINTX(data, len)
#endif

#if PRINT_ALL_SERIAL_DATA
#include "ctype.h"
static void printX(const void *data, uint32_t len) {
	const char *ptr = data;

	for (uint32_t q = 0; q < len; q++) {
		if (ptr[q] >= 0x20 && ptr[q] < 0x7F) {
			user_printf("%c", ptr[q]);
		}
		else {
			if (ptr[q] == '\r') {
				user_printf("\\r");
			}
			else if (ptr[q] == '\n') {
				user_printf("\\n\r\n");
			}
			else {
				user_printf("{0x%x}", ptr[q]);
			}
		}
	}
}
#endif

//=======================================================================

static uint8_t nmea_received = 0;
static sim68_gnss_data_t gnss_data = {0};
static char rx_byte;
static uint32_t rx_byte_cnt = 0;
static char rx_buff[RX_BUFF_LEN];
static char queue[RX_BUFF_LEN];
static char * expected_rsp = NULL;
static char cmd_rsp_buff[RSP_BUFF_LEN];
static float speed_avg_buff[SPEED_AVG_BUFF_LEN];
static uint32_t speed_avg_cnt = 0;
static uint8_t speed_avg_cnt_sett = 1;
volatile static bool response_received = false;

//=======================================================================

static int rx_it_start(char *rx_byte) {
	(void)rx_byte;

	__HAL_UART_ENABLE_IT(&SERIAL_OBJECT, UART_IT_RXNE);

	return 0;
}

static int rx_it_stop() {
	HAL_UART_Abort_IT(&SERIAL_OBJECT);

	return 0;
}

static void reuest_message_handler() {
	HAL_NVIC_SetPendingIRQ(RTC_Alarm_IRQn);
}

static int tx(const char *data, uint32_t len, uint32_t timeout) {
	if (HAL_UART_Transmit(&SERIAL_OBJECT, (uint8_t*)data, len, timeout) == HAL_OK) {
		return 0;
	}
	else {
		return -1;
	}
}

static uint32_t get_tick() {
	return HAL_GetTick();
}

static uint8_t nmea0183_checksum(char *nmea_data) {
	uint8_t crc = 0;

    // the first $ sign and the last two bytes of original CRC + the * sign
    for (int i = 0; i < strlen(nmea_data); i ++) {
        crc ^= nmea_data[i];
    }

    return crc;
}

static int check_msg_crc(char* msg) {
	char *ptr = strchr(msg, '*');
	if (!ptr) {
		return -1;
	}

	*ptr++ = '\0';
	unsigned int crc_msg;
	if (sscanf(ptr, "%02X", &crc_msg) != 1) {
		return -1;
	}

	if ((uint8_t)crc_msg != nmea0183_checksum(&msg[1])) {
		return -1;
	}

	return 0;
}

static void speed_avg_init() {
	if (gnss_cache.crc != gnss_cache_crc_get()) {
		gnss_cache.speed = 0;
		gnss_cache_crc_update();
	}

	float speed = gnss_cache.speed;

	if (settings1_ram.gnss.speed_avg == 0) {
		speed_avg_cnt_sett = 1;
	}
	else {
		speed_avg_cnt_sett = settings1_ram.gnss.speed_avg;
	}

	for (int q = 0; q < speed_avg_cnt_sett; q++) {
		speed_avg_buff[q] = speed;
	}

	speed_avg_cnt = 0;
}

static float speed_avg(float speed) {
	float avg;

	speed_avg_buff[speed_avg_cnt++] = speed;

	arm_mean_f32(speed_avg_buff, speed_avg_cnt_sett, &avg);

	if (speed_avg_cnt >= speed_avg_cnt_sett) {
		speed_avg_cnt = 0;
	}

	gnss_cache.speed = avg;
	gnss_cache_crc_update();

	return avg;
}

static void message_handler() {
	// Проверка пришедшего сообщения здесь отсортирована по последовательности приема
	// Сначала принимаем и обрабатываем все типы сообщений. На последнем сообщении (VTG) фиксируем данные

	static sim68_gnss_data_t gnss_data_tmp = {0};

	char *ptr = queue;
	int tmp;
	char ctmp;
	float ftmp;
	double dtmp;

	if (check_msg_crc(ptr) != 0) {
		nmea_received = 0;
		return;
	}

	if (strstr(ptr, "GGA")) {
		// Сразу ищем 14 запятых, чтобы проверить формат и не проверять его потом каждый раз при работе с указателем
		for (uint8_t q = 0; q < 14; q++) {
			ptr = strchr(ptr, ',');
			if (ptr++ == NULL) {
				/// @todo
				return;
			}
		}

		ptr = queue;

		// UTC Time
		ptr = strchr(ptr, ',') + 1;

		// Lattitude
		ptr = strchr(ptr, ',') + 1;

		// N/S Indicator
		ptr = strchr(ptr, ',') + 1;

		// Longitude
		ptr = strchr(ptr, ',') + 1;

		// E/W Indicator
		ptr = strchr(ptr, ',') + 1;

		// Position Fix Indicator
		ptr = strchr(ptr, ',') + 1;

		// Satellites Used
		ptr = strchr(ptr, ',') + 1;
		if (user_sscanf(ptr, "%d", &tmp) == 1) {
			gnss_data_tmp.tracked.satellites = tmp;
		}

		// HDOP
		ptr = strchr(ptr, ',') + 1;
		if (user_sscanf(ptr, "%f", &ftmp) == 1) {
			gnss_data_tmp.tracked.hdop = ftmp;
		}

		// MSL Altitude
		ptr = strchr(ptr, ',') + 1;
		if (user_sscanf(ptr, "%f", &ftmp) == 1) {
			gnss_data_tmp.tracked.altitude = ftmp;
		}

		// Units
		ptr = strchr(ptr, ',') + 1;

		// Geoid Separation
		ptr = strchr(ptr, ',') + 1;

		// Units
		ptr = strchr(ptr, ',') + 1;

		// Age of Diff. Corr.
		ptr = strchr(ptr, ',') + 1;

		// Diff. Ref. Station ID
		ptr = strchr(ptr, ',') + 1;

		// Checksum

		nmea_received |= NMEA_GGA;
	}
	else if (strstr(ptr, "GPGSA")) { // Есть еще GLGSA. На него забиваем
		// Сразу ищем 17 запятых, чтобы проверить формат и не проверять его потом каждый раз при работе с указателем
		for (uint8_t q = 0; q < 17; q++) {
			ptr = strchr(ptr, ',');
			if (ptr++ == NULL) {
				/// @todo
				return;
			}
		}

		ptr = queue;

		// Mode 1
		ptr = strchr(ptr, ',') + 1;

		// Mode 2
		ptr = strchr(ptr, ',') + 1;
		if (user_sscanf(ptr, "%d", &tmp) == 1) {
			gnss_data_tmp.fix = (gnss_fix_t)tmp;
		}
		else {
			gnss_data_tmp.fix = GNSS_FIX_NOT_AVAILABLE;
		}

		// Далее не парсим, т.к. слишком

		nmea_received |= NMEA_GSA;
	}
	else if (strstr(ptr, "RMC")) {
		// Сразу ищем 12 запятых, чтобы проверить формат и не проверять его потом каждый раз при работе с указателем
		for (uint8_t q = 0; q < 12; q++) {
			ptr = strchr(ptr, ',');
			if (ptr++ == NULL) {
				gnss_data_tmp.have_time = false;
				return;
			}
		}

		ptr = queue;
		struct tm ts;

		// UTC Time
		ptr = strchr(ptr, ',') + 1;
		if (user_sscanf(ptr, "%d", &tmp) == 1) {
			// Секунды
			ts.tm_sec = tmp % 100;
			// Минуты
			tmp /= 100;
			ts.tm_min = (tmp % 100);
			// Часы
			tmp /= 100;
			ts.tm_hour = tmp;
		}
		else {
			gnss_data_tmp.have_time = false;
			return;
		}

		// Status
		ptr = strchr(ptr, ',') + 1;
		if (user_sscanf(ptr, "%c", &ctmp) == 1) {
			gnss_data_tmp.valid_status = (gnss_valid_status_t)ctmp;
		}
		else {
			gnss_data_tmp.valid_status = GNSS_NOT_VALID;
		}

		// Latitude
		ptr = strchr(ptr, ',') + 1;
		if (user_sscanf(ptr, "%lf", &dtmp) == 1) {
			double degree, minute;

			dtmp = dtmp / 100;	// Отделяем градусы от минут
			minute = modf(dtmp, &degree);
			gnss_data_tmp.tracked.latitude = degree + minute*100/60;
		}

		// N/S Indicator
		ptr = strchr(ptr, ',') + 1;
		if (user_sscanf(ptr, "%c", &ctmp) == 1) {
			if (ctmp == 'S') {
				gnss_data_tmp.tracked.latitude = -gnss_data_tmp.tracked.latitude;
			}
		}

		// Longitude
		ptr = strchr(ptr, ',') + 1;
		if (user_sscanf(ptr, "%lf", &dtmp) == 1) {
			double degree, minute;

			dtmp = dtmp / 100;	// Отделяем градусы от минут
			minute = modf(dtmp, &degree);
			gnss_data_tmp.tracked.longitude = degree + minute*100/60;
		}

		// E/W Indicator
		ptr = strchr(ptr, ',') + 1;
		if (user_sscanf(ptr, "%c", &ctmp) == 1) {
			if (ctmp == 'W') {
				gnss_data_tmp.tracked.longitude = -gnss_data_tmp.tracked.longitude;
			}
		}

		// Speed Over Ground
		ptr = strchr(ptr, ',') + 1;
		if (user_sscanf(ptr, "%f", &ftmp) == 1) {
			float knots = ftmp;
			gnss_data_tmp.tracked.speed = knots * 1.852f;
		}

		// Course Over Ground
		ptr = strchr(ptr, ',') + 1;
		if (user_sscanf(ptr, "%f", &ftmp) == 1) {
			gnss_data_tmp.tracked.course = ftmp;
		}

		// Date
		ptr = strchr(ptr, ',') + 1;
		if (user_sscanf(ptr, "%d", &tmp) == 1) {
			// Год
			ts.tm_year = (tmp % 100 + 2000) - 1900;
			// Меяц
			tmp /= 100;
			ts.tm_mon = (tmp % 100 - 1);
			// Число
			tmp /= 100;
			ts.tm_mday = tmp;

			time_t unix = mktime(&ts);
			if (unix > DEFAULT_TIME) {
				gnss_data_tmp.tracked.unix_time = unix;
				gnss_data_tmp.have_time = true;
			}
			else {
				gnss_data_tmp.have_time = false;
			}
		}
		else {
			gnss_data_tmp.have_time = false;
		}

		nmea_received |= NMEA_RMC;

		if (nmea_received == (NMEA_GGA | NMEA_GSA |NMEA_RMC)) {
			if (gnss_data_tmp.fix != GNSS_FIX_NOT_AVAILABLE) {
				// Допускаем в фильтр скорости только реальную скорость
				gnss_data_tmp.tracked.speed = speed_avg(gnss_data_tmp.tracked.speed);
			}

			bool run_status = gnss_data.run_status;
			uint32_t pkt_cnt = gnss_data.pkt_cnt;
			memcpy(&gnss_data, &gnss_data_tmp, sizeof(sim68_gnss_data_t));
			gnss_data.run_status = run_status;
			gnss_data.pkt_cnt = ++pkt_cnt;

			if (gnss_data.have_time) {
				unix_time_set(gnss_data.tracked.unix_time);
				rtc_write(gnss_data.tracked.unix_time);
			}

			board_status.gnss.phy = 1;
		}

		nmea_received = 0;
	}
	else if (expected_rsp) {
		char *_start = strstr(ptr, expected_rsp);
		if (_start) {
			strncpy(cmd_rsp_buff, _start, RSP_BUFF_LEN);
			response_received = true;
		}
	}
}

//=======================================================================

void sim68_rx_handler() {
	uint32_t isrflags   = READ_REG(SERIAL_OBJECT.Instance->SR);
	uint32_t cr1its     = READ_REG(SERIAL_OBJECT.Instance->CR1);

    if(((isrflags & USART_SR_RXNE) != RESET) && ((cr1its & USART_CR1_RXNEIE) != RESET))
    {
    	rx_byte = (char)(SERIAL_OBJECT.Instance->DR & (uint8_t)0x00FF);

    	PRINTX(&rx_byte, 1);

    	rx_buff[rx_byte_cnt++] = rx_byte;

    	if (rx_byte == '\n') {
    		memcpy(queue, rx_buff, rx_byte_cnt);
    		queue[rx_byte_cnt] = '\0';
    		rx_byte_cnt = 0;

    		reuest_message_handler();
    	}

    	if (rx_byte_cnt >= RX_BUFF_LEN) {
    		rx_byte_cnt = 0;
    	}
    }
}

void sim68_serial_error_handler() {

}

void sim68_tx_complete_handler() {

}

//=======================================================================

int sim68_init() {
	gnss_data.run_status = false;

	rx_it_stop();

	speed_avg_init();

	HAL_UART_DeInit(&SERIAL_OBJECT);

	HAL_UART_Init(&SERIAL_OBJECT);

    HAL_NVIC_SetPriority(RTC_Alarm_IRQn, 2, 0);
    HAL_NVIC_EnableIRQ(RTC_Alarm_IRQn);

    bzero(&gnss_data, sizeof(sim68_gnss_data_t));

	sim68_power_on();
	sim68_hard_reset();

	// Разрешаем прием UART
	rx_it_start(&rx_byte);
	// И ждем, пока загрузится GPS
	osDelay(900);

	if (sim68_tracker_mode_init() != 0) {
		ZF_LOGE("Can't put GPS to tracker mode");
		return -1;
	}

	osDelay(900);

	/*
	 * Настраиваем вывод следующих NMEA: GGA, GSA, RMC. Каждый пакет выводится раз в секунду (раз в фикс)
	 * Команда для настройки по умолчанию: "$PMTK314,-1*04\r\n"
	 */
	char str[64];
	int len = sprintf(str, "$PMTK314,0,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0");
	sprintf(str + len, "*%02X\r\n", nmea0183_checksum(&str[1]));

	char *rsp = sim68_send_cmd_repeat(str, "$PMTK001,314,3", 1500, 10);
	if (rsp == NULL) {
		return -1;
	}

	gnss_data.run_status = true;

	return 0;
}

char *sim68_send_cmd(const char *cmd, const char * expected_response, uint32_t timeout) {
	char *rsp = NULL;
	uint32_t len = strlen(cmd);

	PRINTX(cmd, len);

	expected_rsp = (char*)expected_response;
	response_received = false;
	uint32_t start_time = get_tick();

	if (tx(cmd, len, 500) != 0) {
		goto exit;
	}

	while (!response_received) {
		if (get_tick() - start_time > timeout) {
			goto exit;
		}

		HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
	}

	rsp = cmd_rsp_buff;

	exit:

	expected_rsp = NULL;

	return rsp;
}

char *sim68_send_cmd_repeat(const char *cmd, const char * expected_response, uint32_t timeout, int repeat_cnt) {
	char *rsp;

	do {
		rsp = sim68_send_cmd(cmd, expected_response, timeout);
		if(rsp != NULL) {
			break;
		}

		WDG_RESET(TASK_ALL);

		ZF_LOGW("No GPS response. Retry");
	}
	while (--repeat_cnt > 0);

	return rsp;
}

void sim68_gnss_data_get(sim68_gnss_data_t * _gnss_data) {
	DISABLE_IRQ_USER();
	memcpy(_gnss_data, &gnss_data, sizeof(sim68_gnss_data_t));
	ENABLE_IRQ_USER();
}

void sim68_force_hard_reset() {
	gnss_data.run_status = false;
	HAL_GPIO_WritePin(GPS_RESET_GPIO_Port, GPS_RESET_Pin, GPIO_PIN_RESET);
}

void sim68_release_hard_reset() {
	HAL_GPIO_WritePin(GPS_RESET_GPIO_Port, GPS_RESET_Pin, GPIO_PIN_SET);
}

void sim68_hard_reset() {
	sim68_force_hard_reset();
	HAL_Delay(100);
	sim68_release_hard_reset();
}

void sim68_power_on() {
}

void sim68_power_off() {
//	gnss_data.run_status = false;
}

int sim68_tracker_mode_init(void) {
	char *rsp = sim68_send_cmd_repeat("$PSIMMOD,W,1*27\r\n", "$PSIMMOD,W,Ok", 1500, 10);
	if (rsp == NULL) {
		return -1;
	}

	// Сбрасываем модуль для применения настройки
	sim68_hard_reset();

	return 0;
}

float sim68_get_odometer_value(void) {
	char *rsp = sim68_send_cmd_repeat("$PSIMODO,R*3D\r\n", "$PSIMODO,R,Ok,", 1500, 10);
	if(rsp == NULL) {
		ZF_LOGE("Odometer request failed");
		return -1;
	}

	float tmp;
	char * ptr = rsp + strlen("$PSIMODO,R,Ok,");
	if (user_sscanf(ptr, "%f", &tmp) == 1) {
		return tmp;
	}
	else {
		ZF_LOGE("Odometer request failed");
		return -1;
	}
}

int sim68_set_odometer_value(float val) {
//	$PSIMODO,W,<distance>
	char str[48];
	int len = sprintf(str, "$PSIMODO,W,%u", (unsigned int)val);
	sprintf(str + len, "*%02X\r\n", nmea0183_checksum(&str[1]));

	char *rsp = sim68_send_cmd_repeat(str, "$PSIMODO,W,Ok", 1500, 10);
	if(rsp == NULL) {
		ZF_LOGE("Odometer set failed");
		return -1;
	}

	return 0;
}

int sim68_set_static_nav_speed_threshold(float speed_kmph) {
	char cmd[24];
	uint8_t speed_mph = (uint8_t)(speed_kmph/3.6);
	if (speed_mph > 2) {
		speed_mph = 2;
	}

	int len = sprintf(cmd, "$PMTK386,%d", speed_mph);
	uint8_t crc = nmea0183_checksum(&cmd[1]);
	sprintf(cmd + len, "*%02X\r\n", crc);

	char *rsp = sim68_send_cmd_repeat(cmd, "$PMTK", 1500, 10);
	if(rsp == NULL) {
		ZF_LOGE("GNSS static navigation speed thd set error");
		return -1;
	}

	return 0;
}

int sim68_sleep() {
	char str[24];
	int len = sprintf(str, "$PMTK161,0");
	sprintf(str + len, "*%02X\r\n", nmea0183_checksum(&str[1]));

	char *rsp = sim68_send_cmd_repeat(str, "$PMTK001,161,3", 1500, 10);
	if (rsp == NULL) {
		return -1;
	}

	return 0;
}

//=======================================================================

void RTC_Alarm_IRQHandler() {
	message_handler();
}

