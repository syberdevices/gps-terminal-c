/*
 * common.h
 *
 *  Created on: 7 дек. 2018 г.
 *      Author: Denis Shreiber
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <main.h>

#include "sim800/sim800.h"
#include "sim68/sim68.h"
#include "settings/settings.h"
#include "canlog/canlog.h"

// ===========================================================================

#define CLOUD_CACHE_BUFF_SIZE		6144	// 6Кб
#define CONF_JSON_MAX_LEN			4096

#define DISABLE_IRQ_USER()			{ uint32_t irq = __get_PRIMASK(); __disable_irq()
#define ENABLE_IRQ_USER()			if (!irq) { __enable_irq(); } }

#define OW_TEMP_UNKNOWN_VALUE		(-127)

#define CLOUD_TEMP_OFFSET			(273)

#define SPEED_AVG_BUFF_LEN			32

// Информация о внешней памяти продублирвана в flash_fifo.c.
// Не синхронизировал, чтобы вдруг не сломать работающее
#define EXT_FLASH_USER_REGION_ADDR	0
#define EXT_FLASH_USER_REGION_SIZE	(64 * 1024)
#define EXT_FLASH_FIFO_ADDR			(EXT_FLASH_USER_REGION_ADDR + EXT_FLASH_USER_REGION_SIZE)
#define EXT_FLASH_FIFO_SIZE			((16 * 1024 * 1024) - EXT_FLASH_USER_REGION_SIZE)

// ===========================================================================

typedef union fw_version_u {
	struct {
		uint8_t minor;
		uint8_t major;
	};
	uint8_t raw[2];
} fw_version_t;

// ---------------------------------------------------------------------------

typedef enum device_mode_t {
	DEVICE_MODE_ACTIVE,
	DEVICE_MODE_STANDBY,
	DEVICE_MODE_UNKNOWN,
} device_mode_t;

typedef enum ignition_t {
	IGNITION_OFF,
	IGNITION_ON,
	IGNITION_DISABLED
} ignition_t;

// ---------------------------------------------------------------------------

typedef struct gsm_status_t {
	uint8_t phy : 1;
	uint8_t sim : 1;
	uint8_t gprs : 1;
	uint8_t tcp_conn : 1;
	uint8_t tcp_tx : 1;
	char *imei;
	sim800_gsm_signal_strength_t signal_strength;
} gsm_status_t;

typedef struct gps_status_t {
	uint8_t phy : 1;
	gnss_fix_t fix;
	float hdop;
	float speed;
} gps_status_t;

typedef struct battery_status_t {
	float voltage;
	uint8_t charging : 1;
} battery_status_t;

typedef struct power_status_t {
	float voltage;
	volatile bool present;
	volatile uint32_t present_time_ms;
} power_status_t;

typedef struct accel_status_t {
	uint8_t phy : 1;
} accel_status_t;

typedef struct spi_flash_status_t {
	uint8_t phy : 1;
} spi_flash_status_t;

typedef struct io_status_t {
	uint32_t din_mask;
	uint32_t dout_mask;
	float ain[2];
	uint32_t din_counter[4];
} io_status_t;

typedef struct case_status_t {
	bool open;
} case_status_t;

typedef struct mcu_status_t {
	int temp;
} mcu_status_t;

typedef struct motion_status_t {
	bool active;
} motion_status_t;

typedef union alert_status_t {
	struct {
		uint32_t high_engine_temp : 1;
		uint32_t high_rpm : 1;
		uint32_t check_oil : 1;
	};
	uint32_t mask;
} alert_status_t;

typedef struct board_status_t {
	uint8_t fw_ver[3];
	uint8_t hw_ver[3];
	gsm_status_t gsm;
	gps_status_t gnss;
	battery_status_t batt;
	power_status_t power;
	accel_status_t accel;
	spi_flash_status_t spi_flash;
	io_status_t io;
	case_status_t casing;
	motion_status_t motion;
	mcu_status_t mcu;
	ignition_t ignition;
	bool bt_connection;
	alert_status_t alert;
} board_status_t;

// ---------------------------------------------------------------------------


typedef struct __packed fuel_data_t {
	uint32_t level;
	int8_t temp;
	bool ok;
} fuel_data_t;

typedef struct __packed ow_ibutton_data_t {
	uint8_t code[8];
} ow_ibutton_data_t;

typedef struct __packed ow_ibutton_t {
	uint8_t count;
	ow_ibutton_data_t dev[3];
} ow_ibutton_t;

typedef struct __packed ow_temp_data_t {
	uint8_t code[8];
	float val;
} ow_temp_data_t;

typedef struct __packed ow_temp_t {
	uint8_t count;
	ow_temp_data_t dev[8];
} ow_temp_t;

// ---------------------------------------------------------------------------


typedef struct __packed gnss_record_t {
	float hdop;
	float speed;
	float altitude;
	double lattitude;
	double longitude;
	float odometer;			// Метры
	uint16_t course;			// 0-360 deg
	uint8_t satellites_used;
	gnss_fix_t fix;
	bool valid;
	bool on;
} navigation_record_t;

typedef union __packed din_record_t {
	uint16_t _16bit;
	uint8_t _8bit[2];
} din_record_t;

typedef struct __packed io_record_t {
	float ain[2];
	uint32_t count[4];	// Кол-во счетных входов
	din_record_t din; 	// Каждый байт - маска
	uint8_t dout;		// Маска
} io_record_t;

typedef struct __packed powewr_record_t {
	float main_voltage; 	// бортовое напряжение
	float batt_voltage;		// напряжение внутреннего аккумулятора
	bool batt_used;
} power_record_t;

typedef enum __packed source_record_t {
	SRC_TIMER_WITH_IGNITION_ON	= 0,
	SRC_ODOMETER				= 1,
	SRC_COURSE					= 2,
	SRC_REQUEST					= 3,
	SRC_DIN						= 4,
	SRC_TIMER_WITH_IGNITION_OFF	= 5,
	SRC_CHANGE_IGNITION			= 6,	/// @warning Переназначено. Реальная причина ЕГТС - SRC_PERIPH_REMOVE
	SRC_GNSS_VALID				= 7,	/// @warning Переназначено. Реальная причина ЕГТС - SRC_SPEED_THRESHOLD_HIGHT
	SRC_REBOOT					= 8,
	SRC_DOUT_OVERLOAD			= 9,
	SRC_UNCOVER					= 10,
	SRC_POWER_OFF				= 11,
	SRC_VBATT_LOW				= 12,
	SRC_SOS						= 13,
	SRC_CALL_REQUEST			= 14,
	SRC_EMERGENCY_CALL			= 15,
	SRC_EXTERNAL_SERVICE_DATA	= 16,
//	SRC_RESERVED				= 17,
//	SRC_RESERVED				= 18,
	SRC_VBATT_FAULT				= 19,
	SRC_HARSH_SPEED				= 20,
	SRC_HARSH_BREAK				= 21,
	SRC_GNSS_OFF				= 22,
	SRC_ACCIDENT_SENSOR_OFF		= 23,
	SRC_GSM_ANT_OFF				= 24,
	SRC_GNSS_ANT_OFF			= 25,
//	SRC_RESERVED				= 26,
	SRC_SPEED_THRESHOLD_LOW		= 27,
	SRC_IGNITION_OFF_MOTION		= 28,
	SRC_EMERGENCY_MODE_TIMER	= 29,
	SRC_NAVIGATION_ON_OFF		= 30,
	SRC_BAD_NAVIGATION			= 31,
	SRC_IP_CONNECTION			= 32,
	SRC_BAD_GSM					= 33,
	SRC_BAD_NETWORK				= 34,
	SRC_MODE_CHANGE				= 35,

	/// @note Искусственно введенные причины для того, чтобы определить пакет с топливом на флеш
	SRC_REC_TYPE_FUEL			= 64,
	SRC_REC_TYPE_CANLOG			= 65,
} source_record_t;

typedef enum __packed mode_record_t {
	MODE_PASSIVE			= 0,
	MODE_ERA				= 1,
	MODE_ACTIVE				= 2,
	MODE_EMERGENCY_CALL		= 3,
	MODE_EMERGENCY_TRACKING	= 4,
	MODE_TEST				= 5,
	MODE_AUTOSERVICE		= 6,
	MODE_DFU				= 7,
} mode_record_t;

typedef ow_ibutton_t ow_ibutton_record_t;

typedef struct __packed ow_temp_record_t {
	bool used;
	int8_t val;
} ow_temp_record_t;

typedef struct __packed ow_record_t {
	ow_ibutton_record_t ibutton;
	ow_temp_record_t temp[8];
} ow_record_t;

typedef enum __packed record_type_t {
	RECORD_TYPE_TELEDATA,
	RECORD_TYPE_FUEL,
	RECORD_TYPE_CANLOG,
} record_type_t;

typedef struct __packed head_record_t {
	time_t time;
	uint16_t len;
	record_type_t type;
} head_record_t;

typedef struct __packed teledata_subrecord_t {
	source_record_t source;
	navigation_record_t navigation;
	io_record_t io;
	power_record_t power;
	mode_record_t mode;
	ow_record_t ow;
} teledata_subrecord_t;

typedef struct __packed teledata_record_t {
	head_record_t head;
	teledata_subrecord_t data;
} teledata_record_t;

typedef struct __packed fuel_subrecord_t {
	uint32_t level;
	int8_t temp;
	uint8_t num;
} fuel_subrecord_t;

typedef struct __packed fuel_record_t {
	head_record_t head;
	fuel_subrecord_t data[8];
} fuel_record_t;

typedef struct __packed canlog_subrecord_t {
	canlog_data_struct_t param;
	can_used_param_t used_param;	// Настроенные параметры
	can_used_param_t active_param;	// Параметры, которые прочитались
} canlog_subrecord_t;

typedef struct __packed canlog_record_t {
	head_record_t head;
	canlog_subrecord_t data;
} canlog_record_t;

// ---------------------------------------------------------------------------

/// Структура для обмена сообщениями сервера между задачами
typedef struct cloud_data_t {
	uint32_t len;
	uint8_t data[1];	// Объявляется для удобства, как массив, потому что по факту будет выделяться память data[len]
} cloud_data_t;

// ---------------------------------------------------------------------------

typedef struct cloud_cache_t {
	uint8_t txbuff[CLOUD_CACHE_BUFF_SIZE];
	uint32_t len;
	bool empty;

	uint32_t crc;
} cloud_cache_t;

typedef struct speed_cache_t {
	float speed;

	uint32_t crc;
} speed_cache_t;

typedef struct odometer_cache_t {
	float val;
	float last_track_point;
	float last_saved;
} odometer_cache_t;

typedef struct cache_t {
	ignition_t ignition;
	bool motion_status;
	device_mode_t device_mode;
	odometer_cache_t odometer;
	uint8_t din;
	uint32_t counters[4];

	uint32_t crc;
} cache_t;

typedef struct lls_cache_t {
	fuel_data_t unit[8];

	uint32_t crc;
} lls_cache_t;

typedef struct apn_t {
	char *addr;
	char *user;
	char *pass;
} apn_t;

// ===========================================================================

extern cloud_cache_t cloud_cache;
extern speed_cache_t gnss_cache;
extern lls_cache_t lls_cache;
extern cache_t cache;
extern board_status_t board_status;
extern ow_ibutton_t ow_ibutton;
extern ow_temp_t ow_temp;
extern fuel_data_t fuel_data[];
extern teledata_record_t teledata_record_global;
extern fuel_record_t fuel_record_global;
extern canlog_record_t canlog_record_global ;
extern volatile uint32_t gsm_task_send_timeout;
extern bool coordinates_freezed;
extern uint8_t hw_version[];
extern bool gsm_enable;

// ===========================================================================

void detect_hw_version();

int user_sscanf(const char * str, const char * format, ... );

void system_reset(char *msg);

// ---------------------------------------------------------------------------

int parse_settings(char *str, settings1_t *s1, settings2_t *s2, settings3_t *s3, settings4_t *s4);
int pack_settings(char * str, int max_len, settings1_t *s1, settings2_t *s2, settings3_t *s3, settings4_t *s4);

int odometer_reset();
void odometer_poll();
void odometer_last_point_updadte();
void odometer_flash_update_direct(float odo);
void odometer_flash_update(float odo);

// ---------------------------------------------------------------------------

void unix_time_set(time_t _time);
time_t unix_time_get();

// ---------------------------------------------------------------------------

bool canlog_used();

// ---------------------------------------------------------------------------

int erase_user_internal_flash();
int erase_user_external_flash();
int user_internal_flash_program(uint32_t addr, const void * data, uint32_t len);

// ---------------------------------------------------------------------------

uint32_t cloud_cache_crc_get();
void cloud_cache_crc_update();
uint32_t gnss_cache_crc_get();
void gnss_cache_crc_update();
uint32_t cache_crc_get();
void cache_crc_update();
uint32_t lls_cache_crc_get();
void lls_cache_crc_update();

void save_track_point_to_flash(source_record_t src);
void save_fuel_data_to_flash();
void save_canlog_data_to_flash();

ignition_t ignition_get();
void ignition_set(ignition_t ign_status);

bool motion_status_get();
void motion_status_set(bool motion_status);

void device_mode_set(device_mode_t mode);
device_mode_t device_mode_get();

const char * white_number_get_sett_ram_by_index(int index);
void white_number_set_sett_ram_by_index(const char *number, int index);

void check_settings_lock();
void check_settings_unlock();
bool check_settings_locked();
void check_settings_wait_unlock();

#endif /* COMMON_H_ */
