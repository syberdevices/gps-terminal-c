/*
 * rtos.h
 *
 *  Created on: 30 мар. 2018 г.
 *      Author: Denis Shreiber
 */

#ifndef RTOS_H_
#define RTOS_H_

#include "FreeRTOS.h"
#include "task.h"
#include "sim68/sim68.h"
#include "cmsis_os.h"

//==================================================

///Mutexes:
extern osMutexId recordMutexHandle;
extern osMutexId odometerRecursiveMutexHandle;
extern osMutexId fifoRecursiveMutexHandle;
extern osMutexId spiflashRecursiveMutexHandle;

///Tasks:
extern osThreadId uiTaskHandle;
extern osThreadId gsmTaskHandle;
extern osThreadId cloudTaskHandle;
extern osThreadId appTaskHandle;
extern osThreadId saveDataTaskHandle;
extern osThreadId sensorReadTaskHandle;
extern osThreadId monitorTaskHandle;

///Queues
extern osMessageQId usbinQueueHandle;
extern osMessageQId usboutQueueHandle;
extern osMessageQId cloudTxQueueHandle;
extern osMessageQId cloudRxQueueHandle;
extern osMessageQId cloudTxRspQueueHandle;
extern osMessageQId saveDataSourceQueueHandle;

///Semaphores
extern osSemaphoreId gsmWakeupSemHandle;
extern osSemaphoreId cloudSendSemHandle;
extern osSemaphoreId rtcSemHandle;
extern osSemaphoreId fuelSemHandle;
extern osSemaphoreId canlogSemHandle;

///Timers
extern osTimerId gsmWakeupTimerHandle;
extern osTimerId cloudSendTimerHandle;
extern osTimerId adcTimerHandle;
extern osTimerId fuelTimerHandle;
extern osTimerId checkMotionTimerHandle;
extern osTimerId canlogTimerHandle;
extern osTimerId ftpJobTimerHandle;

///Signals
enum Signals_e {
	SIGNAL_START			= 0x1,
	SIGNAL_TASK_UI			= 0x2,
	SIGNAL_TASK_GSM			= 0x4,
	SIGNAL_TASK_CLOUD		= 0x8,
	SIGNAL_TASK_APP			= 0x10,
	SIGNAL_TASK_SAVEDATA	= 0x20,
	SIGNAL_TASK_SENSORREAD	= 0x40,
};

/// Битовая структура статусов
typedef union {
	uint8_t u8;
	struct {
		unsigned ignition : 1;			/// bit 0. Зажигание включено
		unsigned GPS_fix_0 : 1;			/// bit 1. GPS 2D-fix status
		unsigned GPS_fix_1 : 1;			/// bit 2. GPS 3D-fix status
	}__packed bit;
}obd_status_t;

/// Битовая структура алармов
typedef union {
	uint8_t u8;
	struct {
		unsigned batt_inserted : 1;		/// bit 0. Аккумулятор установлен
		unsigned batt_removed : 1;		/// bit 1. Аккумулятор извлечен из автомобиля или устройство извлечено из OBD разъема
		unsigned harsh_breaking : 1;	/// bit 2. Резкое торможение
		unsigned harsh_speed : 1;		/// bit 3. Резкое ускорение
		unsigned over_speed : 1;		/// bit 4. Превышение скорости
		unsigned sos : 1;				/// bit 5. SOS Что это?
		unsigned fuel_discharge : 1;	/// bit 6. Слив топлива
	}__packed bit;
}obd_alarm_t;

/// Перечисление ошибок
typedef enum {
	OBD_ERROR_NONE,							/// Все в порядке
	OBD_ERROR_LOW_GSM_SIGNAL,				/// Слабый GSM сигнал
	OBD_ERROR_NETWORK_NOT_AVAILABLE,		/// Мобильная сеть недоступна
	OBD_ERROR_INTERNET_NOT_AVAILABLE,		/// Интернет недоступен
	OBD_ERROR_DATA_SEND_ERROR,				/// Ошибка отправки данных на сервер
	OBD_ERROR_SERVER_CONNECT_ERROR,			/// Ошибка коннекта
	OBD_ERROR_SERVER_NOT_RECEIVING_DATA,	/// Сервер не принял данные. Что это?
	OBD_ERROR_SERVER_DID_NOT_SEND_REPLY,	/// Сервер не ответил на запрос
}obd_error_type_t;

typedef enum {
	TRACK_DATA_GNSS_VALID	= 0x01,
	TRACK_DATA_TIME_VALID	= 0x02,
} track_data_valid_t;

/// Данные треков
typedef struct __packed {
	uint8_t data_valid_msk;				/// Наличие координат

	tracked_gnss_data_t gnss;			/// Данные GPS

	uint8_t din;						/// Маска цифровых входов
	uint8_t dout;						/// Маска цифровых выходов
	uint16_t ain[2];					/// Набор аналоговых входов [дробь * 100]
	uint8_t ibutton[8];					/// Код ключа водителя
	bool photo_sensor;					/// Статус датчика света
	char params[48];					/// Доп. параметры [строка]

} track_t;


#endif /* RTOS_H_ */
