/*
 * spi_phy.h
 *
 *  Created on: 14 мая 2018 г.
 *      Author: oleynik
 */

#ifndef SPI_PHY_H_
#define SPI_PHY_H_

#include "stm32f4xx.h"
#include "spi.h"


#define SPI_OBJ											hspi1


#define ACCEL_CS_LOW()									HAL_GPIO_WritePin(ACCEL_CS_GPIO_Port, ACCEL_CS_Pin, GPIO_PIN_RESET)
#define ACCEL_CS_HIGH()									HAL_GPIO_WritePin(ACCEL_CS_GPIO_Port, ACCEL_CS_Pin, GPIO_PIN_SET)

#define SPI_FLASH_CS_LOW()								HAL_GPIO_WritePin(FLASH_CS_GPIO_Port, FLASH_CS_Pin, GPIO_PIN_RESET)
#define SPI_FLASH_CS_HIGH()								HAL_GPIO_WritePin(FLASH_CS_GPIO_Port, FLASH_CS_Pin, GPIO_PIN_SET)


#define SPI_TRANSMIT(buf, size)							HAL_SPI_Transmit(&SPI_OBJ, buf, size, 1000)
#define SPI_RECEIVE(buf, size)							HAL_SPI_Receive(&SPI_OBJ, buf, size, 1000)
#define SPI_TRANSMIT_RECEIVE(tx_buf, rx_buf, size)		HAL_SPI_TransmitReceive(&SPI_OBJ, tx_buf, rx_buf, size, 1000)

#endif /* SPI_PHY_H_ */
