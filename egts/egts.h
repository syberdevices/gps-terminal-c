/*
Описание типов данных:
#include <stdint.h> // uint8_t, etc...
GCC/standard C (stdint.h)																				Borland Builder C++
uint8_t - 8ми битный целый беззнаковый. 	unsigned char          unsigned __int8
int8_t - 8ми битный целый знаковый.       char                   __int8
uint16_t - 16ти битный целый беззнаковый. unsigned short int     unsigned __int16
int16_t - 16ти битный целый знаковый.     short int              __int16
uint32_t - 32х битный целый беззнаковый.  unsigned int           unsigned __int32
int32_t - 32х битный целый знаковый.      int                    __int32

32-bit data types, sizes, and ranges
http://cppstudio.com/post/271/
--------------------------------------------------------------------------------
Type						Size (bits)	Range																Sample applications
--------------------------------------------------------------------------------
unsigned char				8			0 <= X <= 255													Small numbers and full PC character set
char								8			-128 <= X <= 127											Very small numbers and ASCII characters
short int						16		-32,768 <= X <= 32,767								Counting, small numbers, loop control
unsigned short int	16    0 <= X <= 65 535
unsigned int				32		0 <= X <= 4,294,967,295								Large numbers and loops
int									32		-2,147,483,648 <= X <= 2,147,483,647	Counting, small numbers, loop control
unsigned long				32		0 <= X <= 4,294,967,295								Astronomical distances
enum								32		-2,147,483,648 <= X <= 2,147,483,647	Ordered sets of values
long								32		-2,147,483,648 <= X <= 2,147,483,647	Large numbers, populations
float								32		1.18  10^-38 < |X| < 3.40  10^38			Scientific (7-digit) precision)
double							64		2.23  10^-308 < |X| < 1.79  10^308		Scientific (15-digit precision)
long double					80		3.37  10^-4932 < |X| < 1.18  10^4932	Financial (18-digit precision)
*/

/* EGTS:
D:\Work\Borland\Satellite\egts
EGTS_PT_APPDATA.xls
Упаковка: (Пример передачи данных о местоположении и состоянии транспортного средства.doc)
EGTS_PACKET {
	EGTS_PACKET_HEADER
  SFRD	Структура данных, зависящая от типа Пакета EGTS_PACKET_HEADER.PT
  SFRCS Контрольная сумма SFRD CRC-16
}
Общая длина пакета Протокола Транспортного Уровня не превышает значения 65535 байт
АТ - абонентский терминал
ТП - телематическая платформа
*/

#ifndef __EGTS__
#define __EGTS__

#include "egts/de.h"

// для проверки битов
#define B15 32768
#define B14 16384
#define B13 8192
#define B12 4096
#define B11 2048
#define B10 1024
#define B9  512
#define B8  256
#define B7 	128
#define B6 	64
#define B5 	32
#define B4 	16
#define B3 	8
#define B2 	4
#define B1 	2
#define B0 	1

// Тип пакета Транспортного Уровня
#define EGTS_PT_RESPONSE 0
#define EGTS_PT_APPDATA 1
#define EGTS_PT_SIGNED_APPDATA 2

/* пакет Протокола Транспортного Уровня
EGTS\EGTS 1.6\RUS\protocol_EGTS_transport_v.1.6_RUS.pdf
8.1.2 ФОРМАТ ПАКЕТА
*/
#pragma pack( push, 1 )
typedef struct EGTS_PACKET_HEADER {
	uint8_t		PRV;	// (Protocol Version)
	uint8_t		SKID;	// (Security Key ID)
	uint8_t		PRF;	// Flags:
	uint8_t		HL;		// Длина заголовка Транспортного Уровня в байтах с учётом байта контрольной суммы (поля HCS)
	uint8_t		HE;		// метод кодирования следующей за данным параметром части заголовка Транспортного Уровня = 0
	uint16_t	FDL;	// размер в байтах поля данных SFRD, если 0: отправка EGTS_PT_RESPONSE с RPID=PID и PR=EGTS_PC_OK
	uint16_t	PID;	// номер пакета Транспортного Уровня от 0 до 65535
	uint8_t		PT;		// Тип пакета Транспортного Уровня
	uint8_t		HCS;	// Контрольная сумма заголовка CRC-8
} EGTS_PACKET_HEADER;
#pragma pack( pop )

/* PRF:
Name	Bit Value
PRF		7-6	префикс заголовка Транспортного Уровня и для данной версии должен содержать значение 00
RTE		5		определяет необходимость дальнейшей маршрутизации = 1, то необходима
ENA		4-3	шифрование данных из поля SFRD, значение 0 0 , то данные в поле SFRD не шифруются
CMP		2		сжатие данных из поля SFRD, = 1, то данные в поле SFRD считаются сжатыми
PR		1-0	приоритет маршрутизации, 1 0 – средний
*/
/* PT
0 – EGTS_PT_RESPONSE (подтверждение на пакет Транспортного Уровня);
1 – EGTS_PT_APPDATA (пакет, содержащий данные Протокола Уровня Поддержки Услуг);
2 – EGTS_PT_SIGNED_APPDATA (пакет, содержащий данные Протокола Уровня Поддержки Услуг с цифровой подписью);
*/


/* SFRD
В зависимости от типа пакета EGTS_PACKET_HEADER.PT структура поля SFRD имеет различный
формат.
EGTS_PACKET_HEADER.PT = EGTS_PT_APPDATA
	SFRD {
		SDR 1	(Service Data Record)
		[
		SDR 2
		SDR n
		]
	}
где SDR = {
	EGTS_SDR
	[	SRD 1 (Subrecord Data) 	]
	[	SRD 2	]
	...
}
где SDR = EGTS_SR_POS_DATA_RECORD, например

EGTS_PACKET_HEADER.PT = EGTS_PT_RESPONSE
	SFRD {
  	EGTS_PT_RESPONSE_HEADER
		[
		SDR 1	(Service Data Record)
		SDR 2
		SDR n
		]
	}
где SDR = ?

EGTS_PACKET_HEADER.PT = EGTS_PT_SIGNED_APPDATA
обрабатывать не будем.
*/

/* Сервисы
EGTS\EGTS 1.6\RUS\protocol_EGTS_services_v.1.6_p1_RUS.pdf
6.1 СПИСОК СЕРВИСОВ
*/
#define EGTS_AUTH_SERVICE         (1)
#define EGTS_TELEDATA_SERVICE     (2)
#define EGTS_COMMANDS_SERVICE     (4)
#define EGTS_FIRMWARE_SERVICE     (9)
#define EGTS_ECALL_SERVICE        (10)


/* SFRD = EGTS_PT_RESPONSE
EGTS\EGTS 1.6\RUS\protocol_EGTS_transport_v.1.6_RUS.pdf
8.2.2 СТРУКТУРА ДАННЫХ ПАКЕТА EGTS_PT_RESPONSE
*/
#pragma pack( push, 1 )
typedef struct EGTS_PT_RESPONSE_HEADER {
	uint16_t	RPID;	// Идентификатор пакета Транспортного Уровня, подтверждение на который сформировано EGTS_PACKET_HEADER.PID
	uint8_t		PR;		// Код результата обработки части Пакета, относящейся к Транспортному Уровню
} EGTS_PT_RESPONSE_HEADER;
#pragma pack( pop )

/*
EGTS\EGTS 1.6\RUS\protocol_EGTS_services_v.1.6_p1_RUS.pdf
6.2.1.1 ПОДЗАПИСЬ EGTS_SR_RECORD_RESPONSE
*/
#pragma pack( push, 1 )
typedef struct EGTS_SR_RECORD_RESPONSE_RECORD {
	uint16_t	CRN;	// номер подтверждаемой записи (значение поля RN из обрабатываемой записи)
	uint8_t		RST;	// статус обработки записи
} EGTS_SR_RECORD_RESPONSE_RECORD;
#pragma pack( pop )

/*
EGTS\EGTS 1.6\RUS\protocol_EGTS_services_v.1.6_p1_RUS.pdf
6.2.1.8 ПОДЗАПИСЬ EGTS_SR_RESULT_CODE
*/
#pragma pack( push, 1 )
typedef struct EGTS_SR_RESULT_CODE_RECORD {
	uint8_t	RCD;	// код, определяющий результат выполнения операции авторизации ()
} EGTS_SR_RESULT_CODE_RECORD;
#pragma pack( pop )



/* EGTS\EGTS 1.6\RUS\protocol_EGTS_services_v.1.6_p1_RUS.pdf
5.3 ОБЩАЯ СТРУКТУРА ПОДЗАПИСЕЙ
*/
#pragma pack( push, 1 )
typedef struct EGTS_SUBRECORD_HEADER {
	uint8_t		SRT;	// тип подзаписи EGTS_SR_TERM_IDENTITY/EGTS_SR_POS_DATA/... (see RD_HEADER.SRT: below)
	uint16_t	SRL;	// длина данных в байтах подзаписи SRD (Subrecord Data)
} EGTS_SUBRECORD_HEADER;
#pragma pack( pop )

/* SFRD = EGTS_PT_APPDATA
EGTS\EGTS 1.6\RUS\protocol_EGTS_transport_v.1.6_RUS.pdf
8.2.1 СТРУКТУРА ДАННЫХ ПАКЕТА EGTS_PT_APPDATA
далее читать
EGTS\EGTS 1.6\RUS\protocol_EGTS_services_v.1.6_p1_RUS.pdf
где SDR = запись Протокола Уровня Поддержки Услуг
5.2.2 СТРУКТУРА ЗАПИСИ
*/
#pragma pack( push, 1 )
typedef struct EGTS_RECORD_HEADER {
	uint16_t	RL;		// размер данных из поля RD
	uint16_t	RN;		// номер записи от 0 до 65535
	uint8_t		RFL;	// битовые флаги
	// необязательные поля
	uint32_t	OID;	// идентификатор объекта, сгенерировавшего данную запись, или для которого данная запись предназначена
	uint32_t	EVID;	// уникальный идентификатор события
	uint32_t	TM;		// время формирования записи на стороне Отправителя (секунды с 00:00:00 01.01.2010 UTC)
	// обязательные поля
	uint8_t		SST;	// идентификатор тип Сервиса-отправителя, сгенерировавшего данную запись (=EGTS_TELEDATA_SERVICE)
	uint8_t		RST;	// идентификатор тип Сервиса-получателя данной записи (=EGTS_TELEDATA_SERVICE)
} EGTS_RECORD_HEADER;
#pragma pack( pop )

/* RFL:
Name	Bit Value
SSOD	7		1 = Сервис-отправитель расположен на стороне АТ, 0 = Сервис- отправитель расположен на ТП
RSOD	6		1 = Сервис-получатель расположен на стороне АТ, 0 = Сервис-получатель расположен на ТП
GRP		5		принадлежность передаваемых данных определённой группе идентификатор которой указан в поле OID
RPP		4-3	приоритет обработки данной записи 00 – наивысший 01 – высокий 10 – средний 11 – низкий
TMFE	2		наличие в данном пакете поля TM 1 = присутствует 0 = отсутствует
EVFE	1		наличие в данном пакете поля EVID 1 = присутствует 0 = отсутствует
OBFE	0		наличие в данном пакете поля OID 1 = присутствует 0 = отсутствует
*/


/* RST = EGTS_TELEDATA_SERVICE
subrecords:
*/
#define EGTS_SR_RECORD_RESPONSE			0
#define EGTS_SR_POS_DATA						16
#define EGTS_SR_EXT_POS_DATA				17
#define EGTS_SR_AD_SENSORS_DATA			18
#define EGTS_SR_COUNTERS_DATA				19
#define EGTS_SR_ACCEL_DATA					20
#define EGTS_SR_STATE_DATA					21	// http://forum.gurtam.com/viewtopic.php?pid=48848#p48848
#define EGTS_SR_LOOPIN_DATA 				22
#define EGTS_SR_ABS_DIG_SENS_DATA		23
#define EGTS_SR_ABS_AN_SENS_DATA		24
#define EGTS_SR_ABS_CNTR_DATA				25
#define EGTS_SR_ABS_LOOPIN_DATA			26
#define EGTS_SR_LIQUID_LEVEL_SENSOR	27
#define EGTS_SR_PASSENGERS_COUNTERS	28

/* SRD (Subrecord Data)
EGTS_SR_POS_DATA_RECORD
Используется абонентским терминалом при передаче основных данных определения местоположения
http://www.zakonprost.ru/content/base/part/1038460
*/
#pragma pack( push, 1 )
typedef struct EGTS_SR_POS_DATA_RECORD {
  uint32_t NTM; 	// 4 байта Navigation Time , seconds since 00:00:00 01.01.2010 UTC
  uint32_t LAT;		// 4 байта Latitude, degree,  (WGS - 84) / 90 * 0xFFFFFFFF
  uint32_t LONG;	// 4 байта Longitude, degree,  (WGS - 84) / 180 * 0xFFFFFFFF
  uint8_t  FLG;		// 1 байт Flagsб см. ниже
  uint16_t SPD;		// 2 байта, см. ниже
  uint8_t  DIR;     // направление движения
  uint8_t  ODM[3];  // пройденное расстояние (пробег) в км, с дискретностью 0,1 км;
  uint8_t  DIN;     // битовые флаги, определяют состояние основных дискретных входов 1 ... 8
  uint8_t  SRC;     // источник (событие), инициировавший посылку данной навигационной информации (http://www.zakonprost.ru/content/base/part/1038460 Таблица N 3)
  // опциональные поля, могут отсутствовать
  //uint8_t		ALT[3];	// высота над уровнем моря, м (опциональный параметр, наличие которого определяется битовым флагом ALTE);
  //uint16_t	SRCD;		// (см. ниже) данные, характеризующие источник (событие) из поля src. Наличие и интерпретация значения данного поля определяется полем src
} EGTS_SR_POS_DATA_RECORD;
#pragma pack( pop )

/* FLG:
Name	Bit Value
VLD   :0	признак "валидности" координатных данных: 1 - данные "валидны";
CS    :1	тип используемой системы: 0 - WGS-84; 1 - ПЗ-90.02;
FIX   :2	тип определения координат: 0 - 2D fix; 1 - 3D fix;
BB    :3	признак отправки данных из памяти ("черный ящик"): 0 - актуальные данные
MV    :4	признак движения: 1 - движение
LAHS  :5	определяет полушарие широты:  0 - северная 1 - южная
LOHS  :6	определяет полушарие долготы 0 - восточная 1 - западная
ALTE  :7	определяет наличие поля ALT в подзаписи 0 - не передается
*/
/* SPD:
Name	Bit Value
SPD 	:0-13	14 младших бит, скорость в км/ч с дискретностью 0,1 км/ч
ALTS  :14		определяет высоту относительно уровня моря и имеет смысл только при установленном флаге ALTE: 0 - точка выше уровня моря; 1 - ниже уровня моря;
DIRH  :15		(Direction the Highest bit) старший бит (8) параметра DIR;
*/


/* SRD (Subrecord Data)
EGTS_SR_EXT_POS_DATA_RECORD
Используется абонентским терминалом при передаче дополнительных данных
http://www.zakonprost.ru/content/base/part/1038460
*/
#pragma pack( push, 1 )
typedef struct EGTS_SR_EXT_POS_DATA_RECORD {
	uint8_t	FLG;	// битовые флаги
  // опциональные поля, могут отсутствовать
	//uint16_t	VDOP;	// снижение точности в вертикальной плоскости (значение, умноженное на 100)
	//uint16_t  HDOP;	// снижение точности в горизонтальной плоскости (значение, умноженное на 100) (а Galileo умножает на 10)
	//uint16_t  PDOP;	// снижение точности по местоположению (значение, умноженное на 100)
	//uint8_t		SAT;	// количество видимых спутников
	//uint16_t  NS;		// битовые флаги, характеризующие используемые навигационные спутниковые системы
} EGTS_SR_EXT_POS_DATA_RECORD;
#pragma pack( pop )

/* FLG:
Name	Bit Value
VFE 	0		- (VDOP Field Exists) определяет наличие поля VDOP: 0 - не передается
HFE 	1		- (HDOP Field Exists) определяет наличие поля HDOP: 0 - не передается
PFE 	2		- (PDOP Field Exists) определяет наличие поля PDOP: 0 - не передается
SFE 	3		- (Satellites Field Exists) определяет наличие поля SAT: 0 - не передается
NSFE 	4		- (Navigation System Field Exists) определяет наличие поля NS: 0 - не передается
остальные биты не используются
*/
/* NS:
0	- система не определена;
1 - ГЛОНАСС;
2 - GPS;
4 - Galileo;
8 - Compass;
16 - Beidou;
32 - DORIS;
64 - IRNSS;
128 - QZSS.
Остальные значения зарезервированы.
*/

/* SRD (Subrecord Data)
EGTS_SR_EXT_POS_DATA_RECORD
Используется абонентским терминалом при передаче дополнительных данных входов/выходов
http://www.zakonprost.ru/content/base/part/1038460
*/
#pragma pack( push, 1 )
typedef struct EGTS_SR_AD_SENSORS_DATA_RECORD {
	uint8_t	DIOE;	// битовые флаги
	uint8_t DOUT;	// Digital out
	uint8_t	ASFE;	// битовые флаги
	// Опциональные поля
//	uint8_t ADIO[8];	// Цифровые входы. 8 масок
//	uint8_t ANS[8][3];	// Аналоговые сенсоры. 8 штук по 3 байта
} EGTS_SR_AD_SENSORS_DATA_RECORD;
#pragma pack( pop )

/* SRD (Subrecord Data)
 EGTS_SR_COUNTERS_DATA_RECORD
Используется абонентским терминалом при передаче данных счетных входов
http://www.zakonprost.ru/content/base/part/1038460
*/
#pragma pack( push, 1 )
typedef struct EGTS_SR_COUNTERS_DATA_RECORD {
	uint8_t CFE;		// Маска счетных входов
//	uint8_t CN[8][3];	// значение счетчика
} EGTS_SR_COUNTERS_DATA_RECORD;
#pragma pack( pop )

/* SRD (Subrecord Data)
 EGTS_SR_STATE_DATA_RECORD
Используется абонентским терминалом при передаче данных счетных входов
http://www.zakonprost.ru/content/base/part/1038460
*/
#pragma pack( push, 1 )
typedef struct EGTS_SR_STATE_DATA_RECORD {
	uint8_t ST;		// State)
	uint8_t MPSV;	// Main Power Source Voltage
	uint8_t BBV;	// Back Up Battery Voltage
	uint8_t IBV;	// Internal Battery Voltage
	uint8_t FLG;
} EGTS_SR_STATE_DATA_RECORD;
#pragma pack( pop )

/* SRD (Subrecord Data)
 EGTS_SR_ABS_CNTR_DATA_RECORD
Используется абонентским терминалом при передаче данных счетных входов
http://www.zakonprost.ru/content/base/part/1038460
*/
#pragma pack( push, 1 )
typedef struct EGTS_SR_ABS_CNTR_DATA_RECORD {
	uint8_t CN;		// номер счетного входа
	uint8_t CNV[3];	// значение счетчика
} EGTS_SR_ABS_CNTR_DATA_RECORD;
#pragma pack( pop )

/* SRD (Subrecord Data)
EGTS_SR_LIQUID_LEVEL_SENSOR_RECORD
Используется абонентским терминалом при передаче данных датчиков уровня жидкости
http://www.zakonprost.ru/content/base/part/1038460
*/
#pragma pack( push, 1 )
typedef struct EGTS_SR_LIQUID_LEVEL_SENSOR_RECORD {
	uint8_t		FLG;		// битовые флаги
	uint16_t	MADDR;	// адрес модуля, данные о показаниях ДУЖ с которого поступили в абонентский терминал
	uint32_t	LLSD;		// показания ДУЖ в формате, определяемом флагом RDF
} EGTS_SR_LIQUID_LEVEL_SENSOR_RECORD;
#pragma pack( pop )

/* FLG:
Name	Bit Value
LLSN	0-2	порядковый номер датчика, 3 бита
RDF		3		флаг, определяющий формат поля LLSD данной подзаписи:
					0 - поле LLSD имеет размер 4 байта (тип данных UINT) и содержит показания ДУЖ в формате,
					определяемом полем LLSVU;
					1 - поле LLSD содержит данные ДУЖ в неизменном виде, как они поступили из внешнего
					порта абонентского терминала (размер поля LLSD при этом определяется исходя из
					общей длины данной подзаписи и размеров расположенных перед LLSD полей).
LLSVU	4-5	битовый флаг, определяющий единицы измерения показаний ДУЖ:
					00 - нетарированное показание ДУЖ.
					01 - показания ДУЖ в процентах от общего объема емкости;
					10 - показания ДУЖ в литрах с дискретностью в 0,1 литра.
LLSEF	6		битовый флаг, определяющий наличие ошибок при считывании значения датчика уровня жидкости
			7		не используется
*/



/* RST = EGTS_AUTH_SERVICE
subrecords:
*/
#define EGTS_SR_TERM_IDENTITY     (1)
#define EGTS_SR_MODULE_DATA       (2)
#define EGTS_SR_VEHICLE_DATA      (3)
#define EGTS_SR_AUTH_PARAMS       (6)
#define EGTS_SR_AUTH_INFO         (7)
#define EGTS_SR_SERVICE_INFO      (8)
#define EGTS_SR_RESULT_CODE       (9)

#define EGTS_SR_AUTH_PARAMS_OLD   (4)
#define EGTS_SR_AUTH_INFO_OLD     (5)
#define EGTS_SR_SERVICE_INFO_OLD  (6)
#define EGTS_SR_RESULT_CODE_OLD   (7)

/* SRD (Subrecord Data)
EGTS\EGTS 1.6\RUS\protocol_EGTS_services_v.1.6_p1_RUS.pdf
6.2 EGTS_AUTH_SERVICE
6.2.1.2 ПОДЗАПИСЬ EGTS_SR_TERM_IDENTITY
*/
#define EGTS_IMEI_LEN     15U
#define EGTS_IMSI_LEN     16U
#define EGTS_LNGC_LEN     3U
#define EGTS_NID_LEN      3U
#define EGTS_MSISDN_LEN   15U

#pragma pack( push, 1 )
typedef struct EGTS_SR_TERM_IDENTITY_RECORD {
	uint32_t	TID;	// уникальный идентификатор, назначаемый при программировании АТ
	uint8_t		FLG;	// Flags
/*[
	uint16_t	HDID;	// идентификатор «домашней» ТП
	char			IMEI[EGTS_IMEI_LEN];	// идентификатор мобильного устройства
	char			IMSI[EGTS_IMSI_LEN];	// идентификатор мобильного абонента
	char			LNGC[EGTS_LNGC_LEN];	// код языка, предпочтительного к использованию на стороне АТ, по ISO 639-2, например, rus – русский
	uint8_t		NID[3];	// идентификатор сети оператора, в которой зарегистрирован АТ на данный момент
	uint16_t	BS;	// максимальный размер буфера приёма АТ в байтах.
	char 			MSISDN[EGTS_MSISDN_LEN];	// телефонный номер мобильного абонента. При невозможности определения данного параметра, устройство должно заполнять данное поле значением 0 во всех 15-ти символах
	]*/
} EGTS_SR_TERM_IDENTITY_RECORD;
#pragma pack( pop )

/* FLG:
Name	Bit Value
HDIDE :0	– битовый флаг, который определяет наличие поля HDID в подзаписи (1=передаётся, 0=не передаётся)
IMEIE :1	– битовый флаг, который определяет наличие поля IMEI в подзаписи (1=передаётся, 0=не передаётся)
IMSIE :2	– битовый флаг, который определяет наличие поля IMSI в подзаписи (1=передаётся, 0=не передаётся)
LNGCE :3	– битовый флаг, который определяет наличие поля LNGC в подзаписи (1=передаётся, 0=не передаётся)
SSRA 	:4	– битовый флаг предназначен для определения алгоритма использования Сервисов (1=«простой» алгоритм, 0=«запросов» на использование сервисов)
NIDE 	:5	– битовый флаг определяет наличие поля NID в подзаписи (1=передаётся, 0=не передаётся)
BSE 	:6	– битовый флаг, определяющий наличие поля BS в подзаписи (1=передаётся, 0=не передаётся)
MNE 	:7	- битовый флаг, определяющий наличие поля MSISDN в подзаписи (1=передаётся, 0=не передаётся)
*/

#pragma pack( push, 1 )
typedef struct EGTS_SR_MODULE_DATA_RECORD {
	uint8_t MT;		// Module type
	uint32_t VID;	// Vendor identifier
	uint16_t FWV;	// Firmware version
	uint16_t SWV;	// Software version
	uint8_t MD;		// Modification
	uint8_t ST;		// State
//	char SRN[32];	// Serial Number
	uint8_t D1;		// Delimiter
//	char DSCR[32];	// Description
	uint8_t D2;		// Delimiter
} EGTS_SR_MODULE_DATA_RECORD;
#pragma pack( pop )


/*
EGTS\EGTS 1.6\RUS\protocol_EGTS_services_v.1.6_p1_RUS.pdf
6.2.1.6 ПОДЗАПИСЬ EGTS_SR_AUTH_INFO
*/
#pragma pack( push, 1 )
typedef struct EGTS_SR_AUTH_INFO_RECORD {
	char	UNM[32];	// User Name, имя пользователя
  char	D0;				// Delimiter, разделитель строковых параметров (всегда имеет значение 0)
	char UPSW[32];	// User Password, пароль пользователя
  char	D1;				// Delimiter
	char SS[255];		/* Server Sequence, специальная серверная последовательность байт, передаваемая в подзаписи
											EGTS_SR_AUTH_PARAMS (необязательное поле, наличие зависит от используемого
											алгоритма шифрования) */
  char	D2;				// Delimiter
} EGTS_SR_AUTH_INFO_RECORD;
#pragma pack( pop )


/* RST = EGTS_COMMANDS_SERVICE
http://www.zakonprost.ru/content/base/part/1038461
http://docs.cntd.ru/document/1200119664
EGTS\EGTS 1.6\RUS\protocol_EGTS_services_v.1.6_p1_RUS.pdf
6.3 EGTS_COMMANDS_SERVICE
6.3.2 ОПИСАНИЕ КОМАНД, ПАРАМЕТРОВ И ПОДТВЕРЖДЕНИЙ
subrecords:
*/
#define EGTS_SR_COMMAND_DATA			51

/* Формат команд терминала (поле CD структуры EGTS_SR_COMMAND_DATA_RECORD)
Таблица 18: Формат команд терминала
*/
#pragma pack( push, 1 )
typedef struct EGTS_SR_COMMAND_DATA_FIELD {
	uint16_t	ADR;		// (Address)(Action)
	uint8_t		SZ_ACT;	// (Size)
	uint16_t	CCD;		// (Command Code)
	/* необязательные поля
	uint8_t		DT[65200];	// (Data)
	*/
} EGTS_SR_COMMAND_DATA_FIELD;
#pragma pack( pop )

/* CT_CCT:
Name	Bit Value
SZ		:7-4	объём памяти для параметра (используется совместно с действием ACT=3
ACT		:3-0 – описание действия, используется в случае типа команды (поле CT=CT_COM подзаписи EGTS_SR_COMMAND _DATA).
			Значение поля может быть одним из следующих вариантов:
			0 = параметры команды. Используется для передачи параметров для команды, определяемой кодом из поля CCD
			1 = запрос значения. Используется для запроса информации, хранящейся в АТ. Запрашиваемый параметр определяется кодом из поля CCD
			2 = установка значения. Используется для установки нового значения определённому параметру в АТ. Устанавливаемый параметр определяется кодом из поля CCD, а его значение полем DT
			3 = добавление нового параметра в АТ. Код нового параметра указывается в поле CCD, его тип в поле SZ, а значение в поле DT
			4 = удаление имеющегося параметра из АТ. Код удаляемого параметра указывается в поле CCD
*/

// 6.3.1.1 ПОДЗАПИСЬ EGTS_SR_COMMAND_DATA
#pragma pack( push, 1 )
typedef struct EGTS_SR_COMMAND_DATA_RECORD {
	uint8_t		CT_CCT;	// CT-тип команды, CCT-тип подтверждения (имеет смысл для типов команд CT_COMCONF, CT_MSGCONF, CT_DELIV)
	uint32_t	CID;		// (Command Identifier) идентификатор команды, сообщения. Значение из данного поля должно быть использовано стороной, обрабатывающей/выполняющей команду или сообщение, для создания подтверждения. Подтверждение должно содержать в поле CID то же значение, что содержалось в самой команде или сообщении при отправке
	uint32_t	SID;		// (Source Identifier) идентификатор отправителя (уровня прикладного ПО, например, уникальный идентификатор пользователя в системе диспетчеризации) данной команды или подтверждения
	uint8_t		ACFE;		//  Flags
	/* необязательные поля
	uint8_t		CHS;		// (Charset) кодировка символов, используемая в поле CD, содержащем тело команды. При отсутствии данного поля по умолчанию должна использоваться кодировка CP-1251
	uint8_t		ACL;		// (Authorization Code Length) длина в байтах поля AC, содержащего код авторизации на стороне получателя
	uint8_t		AC[255];	// (Authorization Code) код авторизации, использующийся на принимающей стороне (АТ)
	EGTS_SR_COMMAND_DATA_FIELD	CD;	// (Command Data) тело команды, параметры, данные возвращаемые на команду-запрос, использующие кодировку из поля CHS, или значение по умолчанию Формат команды описан в Таблице 18
	*/
} EGTS_SR_COMMAND_DATA_RECORD;
#pragma pack( pop )

/* CT_CCT:
Name	Bit Value
CT	:7-4	- Command Type:
	0001 = (16) = CT_COMCONF - подтверждение о приёме, обработке или результат выполнения команды
	0010 = (32) = CT_MSGCONF - подтверждение о приёме, отображении и/или обработке информационного сообщения
	0011 = (48) = CT_MSGFROM - информационное сообщение от АТ 0100 = CT_MSGTO - информационное сообщение для вывода на устройство отображения АТ
	0101 = (80) = CT_COM - команда для выполнения на АТ
	0110 = (96) = CT_DELCOM - удаление из очереди на выполнение переданной ранее команды
	0111 = (112) = CT_SUBREQ - дополнительный подзапрос для выполнения (к переданной ранее команде)
	1000 = (128) = CT_DELIV - подтверждение о доставке команды или информационного сообщения
CCT	:3-0 - Command Confirmation Type:
	0000 = (0) = CC_OK - успешное выполнение, положительный ответ;
	0001 = (1) = CC_ERROR - обработка завершилась ошибкой
	0010 = (2) = CC_ILL - команда не может быть выполнена по причине отсутствия в списке разрешённых (определённых протоколом) команд или отсутствия разрешения на выполнение данной команды
	0011 = (3) = CC_DEL - команда успешно удалена
	0100 = (4) = CC_NFOUND - команда для удаления не найдена
	0101 = (5) = CC_NCONF - успешное выполнение, отрицательный ответ
	0110 = (6) = CC_INPROG - команда передана на обработку, но для её выполнения требуется длительное время
*/
/* ACFE:
Name	Bit Value
CHSFE	:0  – (Charset Field Exists) битовый флаг, определяющий наличие поля CHS в подзаписи 1 = поле CHS присутствует в подзаписи 0 = поле CHS отсутствует в подзаписи
ACFE	:1 – (Authorization Code Field Exists) битовый флаг, определяющий наличие полей ACL и AC в подзаписи 1 = поля ACL и AC присутствуют в подзаписи 0 = поля ACL и AC отсутствуют в подзаписи
остальные не используются
*/

/* команды EGTS_SR_COMMAND_DATA_FIELD.CCD
http://www.zakonprost.ru/content/base/part/1038461
*/
#define EGTS_FLEET_DOUT_ON 0x0009
#define EGTS_FLEET_DOUT_OFF 0x000A
#define EGTS_FLEET_GET_DOUT_DATA 0x000B
#define EGTS_FLEET_GET_POS_DATA  0x000C
#define EGTS_FLEET_GET_SENSORS_DATA 0x000D
#define EGTS_FLEET_GET_LIN_DATA 0x000E
#define EGTS_FLEET_GET_CIN_DATA 0x000F
#define EGTS_FLEET_GET_STATE 0x0010
#define EGTS_FLEET_ODOM_CLEAR 0x0011



// EGTS_PT_RESPONSE_HEADER.PR: ПРИЛОЖЕНИЕ 1 - КОДЫ РЕЗУЛЬТАТОВ ОБРАБОТКИ
#define EGTS_PC_OK							0		// успешно обработано
#define EGTS_PC_IN_PROGRESS			1		// в процессе обработки
#define EGTS_PC_UNS_PROTOCOL		128	// неподдерживаемый протокол
#define EGTS_PC_DECRYPT_ERROR		129	// ошибка декодирования
#define EGTS_PC_PROC_DENIED			130	// обработка запрещена
#define EGTS_PC_INC_HEADERFORM 	131	// неверный формат заголовка
#define EGTS_PC_INC_DATAFORM 		132	// неверный формат данных
#define EGTS_PC_UNS_TYPE 				133	// неподдерживаемый тип
#define EGTS_PC_NOTEN_PARAMS 		134	// неверное количество параметров
#define EGTS_PC_DBL_PROC 				135	// попытка повторной обработки
#define EGTS_PC_PROC_SRC_DENIED 136 // обработка данных от источника запрещена
#define EGTS_PC_HEADERCRC_ERROR 137 // ошибка контрольной суммы заголовка
#define EGTS_PC_DATACRC_ERROR 	138 // ошибка контрольной суммы данных
#define EGTS_PC_INVDATALEN 			139 // некорректная длина данных
#define EGTS_PC_ROUTE_NFOUND 		140 // маршрут не найден
#define EGTS_PC_ROUTE_CLOSED 		141 // маршрут закрыт
#define EGTS_PC_ROUTE_DENIED 		142 // маршрутизация запрещена
#define EGTS_PC_INVADDR 				143 // неверный адрес
#define EGTS_PC_TTLEXPIRED 			144 // превышено количество ретрансляции данных
#define EGTS_PC_NO_ACK 					145 // нет подтверждения
#define EGTS_PC_OBJ_NFOUND 			146 // объект не найден
#define EGTS_PC_EVNT_NFOUND 		147 // событие не найдено
#define EGTS_PC_SRVC_NFOUND 		148 // сервис не найден
#define EGTS_PC_SRVC_DENIED 		149 // сервис запрещён
#define EGTS_PC_SRVC_UNKN 			150 // неизвестный тип сервиса
#define EGTS_PC_AUTH_DENIED 		151 // авторизация запрещена
#define EGTS_PC_ALREADY_EXISTS 	152 // объект уже существует
#define EGTS_PC_ID_NFOUND 			153 // идентификатор не найден
#define EGTS_PC_INC_DATETIME 		154 // неправильная дата и время
#define EGTS_PC_IO_ERROR 				155 // ошибка ввода/вывода
#define EGTS_PC_NO_RES_AVAIL 		156 // недостаточно ресурсов
#define EGTS_PC_MODULE_FAULT 		157 // внутренний сбой модуля
#define EGTS_PC_MODULE_PWR_FLT 	158 // сбой в работе цепи питания модуля
#define EGTS_PC_MODULE_PROC_FLT 159 // сбой в работе микроконтроллера модуля
#define EGTS_PC_MODULE_SW_FLT 	160 // сбой в работе программы модуля
#define EGTS_PC_MODULE_FW_FLT 	161 // сбой в работе внутреннего ПО модуля
#define EGTS_PC_MODULE_IO_FLT 	162 // сбой в работе блока ввода/вывода модуля
#define EGTS_PC_MODULE_MEM_FLT 	163 // сбой в работе внутренней памяти модуля
#define EGTS_PC_TEST_FAILED 		164 // тест не пройден


typedef enum {
	EGTS_MSG_TYPE_UNKNOWN,
	EGTS_MSG_TYPE_RESPONSE,
	EGTS_MSG_TYPE_APPDATA,
	EGTS_MSG_TYPE_COMMAND,
} egts_msg_type_t;

typedef struct {
	egts_msg_type_t type;
	uint8_t *data;
} egts_msg_t;


// функции общие для encode/decode
int egts_packet_create(uint8_t *buffer, uint8_t pt);
int egts_packet_finalize(uint8_t *buffer, int pointer);

// функции для decode
int terminal_decode(uint8_t *data, uint32_t len, egts_msg_t *msg);
int responce_add_responce(uint8_t *buffer, int pointer, uint16_t pid, uint8_t pr);
int responce_add_record(uint8_t *buffer, int pointer, uint16_t crn, uint8_t rst);
int responce_add_result(uint8_t *buffer, int pointer, uint8_t rcd);
int responce_add_subrecord_EGTS_SR_COMMAND_DATA(char *buffer, int pointer, EGTS_SR_COMMAND_DATA_RECORD *cmdrec);
unsigned char CRC8EGTS(unsigned char *lpBlock, unsigned char len);
unsigned short CRC16EGTS(unsigned char * pcBlock, unsigned short len);
int Parse_EGTS_PACKET_HEADER(ST_ANSWER *answer, char *pc, int parcel_size);
int Parse_EGTS_RECORD_HEADER(EGTS_RECORD_HEADER *rec_head, EGTS_RECORD_HEADER *st_header, ST_ANSWER *answer);
int Parse_EGTS_SR_TERM_IDENTITY(EGTS_SR_TERM_IDENTITY_RECORD *record, ST_ANSWER *answer);
int Parse_EGTS_SR_POS_DATA(EGTS_SR_POS_DATA_RECORD *posdata, ST_RECORD *record, ST_ANSWER *answer);
int Parse_EGTS_SR_EXT_POS_DATA(EGTS_SR_EXT_POS_DATA_RECORD *posdata, ST_RECORD *record);
int Parse_EGTS_SR_LIQUID_LEVEL_SENSOR(int rlen, EGTS_SR_LIQUID_LEVEL_SENSOR_RECORD *posdata, ST_RECORD *record);
int Parse_EGTS_SR_COMMAND_DATA(ST_ANSWER *answer, EGTS_SR_COMMAND_DATA_RECORD *record);

// функции для encode
int terminal_encode(ST_RECORD *records, int reccount, uint8_t *buffer, int bufsize);
int packet_add_record_header(uint8_t *packet, int position, uint8_t sst, uint8_t rst, time_t _time);
int packet_add_subrecord_header(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, uint8_t srt);
int packet_add_subrecord_EGTS_SR_TERM_IDENTITY(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, unsigned int tid, char *imei);
int packet_add_subrecord_EGTS_SR_MODULE_DATA(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header);
int packet_add_subrecord_EGTS_SR_POS_DATA_RECORD(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, ST_RECORD *record);
int packet_add_subrecord_EGTS_SR_EXT_POS_DATA_RECORD(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, ST_RECORD *record);
int packet_add_subrecord_EGTS_SR_AD_SENSORS_DATA_RECORD(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, ST_RECORD *record, uint8_t num);
int packet_add_subrecord_EGTS_SR_COUNTERS_DATA_RECORD(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, ST_RECORD *record);
int packet_add_subrecord_EGTS_SR_STATE_DATA_RECORD(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, ST_RECORD *record);
int packet_add_subrecord_EGTS_SR_ABS_CNTR_DATA_RECORD(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, ST_RECORD *record);
int packet_add_subrecord_EGTS_SR_LIQUID_LEVEL_SENSOR_RECORD(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, uint32_t level, uint8_t seq_numb, uint8_t addr);

#endif

/* EGTS_SR_POS_DATA_RECORD.SRCD
http://zakonbase.ru/content/part/1191927
Таблица N 3. Список источников посылок координатных данных Сервиса EGTS_TELEDATA_SERVICE
Код 	Описание
0 	таймер при включенном зажигании
1 	пробег заданной дистанции
2 	превышение установленного значения угла поворота
3 	ответ на запрос
4 	изменение состояния входа X
5 	таймер при выключенном зажигании
6 	отключение периферийного оборудования
7 	превышение одного из заданных порогов скорости
8 	перезагрузка центрального процессора (рестарт)
9 	перегрузка по выходу Y
10 	сработал датчик вскрытия корпуса прибора
11 	переход на резервное питание/отключение внешнего питания
12 	снижение напряжения источника резервного питания ниже порогового значения
13 	нажата "тревожная кнопка"
14 	запрос на установление голосовой связи с оператором
15 	экстренный вызов
16 	появление данных от внешнего сервиса
17 	зарезервировано
18 	зарезервировано
19 	неисправность резервного аккумулятора
20 	резкий разгон
21 	резкое торможение
22 	отключение или неисправность навигационного модуля
23 	отключение или неисправность датчика автоматической идентификации события ДТП
24 	отключение или неисправность антенны GSM/UMTS
25 	отключение или неисправность антенны навигационной системы
26 	зарезервировано
27 	снижение скорости ниже одного из заданных порогов
28 	перемещение при выключенном зажигании
29 	таймер в режиме "экстренное слежение"
30 	начало/окончание навигации
31 	"нестабильная навигация" (превышение порога частоты прерывания режима навигации при включенном зажигании или режиме экстренного слежения)
32 	установка IP соединения
33 	нестабильная регистрация в сети подвижной радиотелефонной связи
34 	"нестабильная связь" (превышение порога частоты прерывания/восстановления IP соединения при включенном зажигании или режиме экстренного слежения)
35 	изменение режима работы
*/
