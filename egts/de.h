/*
заголовочный файл подключаемой библиотеки
декодирования/кодирования посылок трекеров
ВНИМАНИЕ: после изменения этого файла необходимо перекомпилировать все подключаемые библиотеки
*/
#ifndef __CODER__
#define __CODER__

//#include <stdio.h>  /* snprintf, FILENAME_MAX */
//#include <stdlib.h> /* malloc */
//#include <string.h> /* memset */
//#include <errno.h>  /* errno */
//#include <time.h>		/* localtime */
//#include <math.h>		/* fabs */

#include "common.h"

#ifndef MILE
	#define MILE 1.852	// коэфф мили/километры
#endif
#ifndef SIZE_TRACKER_FIELD
	#define SIZE_TRACKER_FIELD 16
#endif
#ifndef BAD_OBJ
	#define BAD_OBJ (-1)
#endif
#ifndef SOCKET_BUF_SIZE
	#define SOCKET_BUF_SIZE (4096)
#endif
#ifndef MAX_RECORDS
	#define MAX_RECORDS (1)
#endif


// декодированные данные
typedef teledata_record_t ST_RECORD;


/* структура, возвращаемая функцией terminal_decode
если в процессе декодирования произошла ошибка, поле error должно быть > 0
если декодированных данных нет,
	поле count должно быть равно 0
если ответ устройству не требуется,
	поле size должно быть равно 0
*/
typedef struct {
	int error;	// > 0 if decode/encode error occur
	int size;	// size of field answer
	int count;	// number of decoded records in array
	uint8_t answer[SOCKET_BUF_SIZE];	// answer to gps/glonass terminal
	ST_RECORD records[MAX_RECORDS];	// array of the decoded records
	ST_RECORD lastpoint;	// last navigation data
} ST_ANSWER;
// sizeof(ST_ANSWER)=11056

#endif
