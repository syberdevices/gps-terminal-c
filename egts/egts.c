// decode/encode EGTS protocol
// make -B egts
// http://www.cy-pr.com/tools/time/

#include <stdlib.h> /* malloc */
#include <string.h> /* memset */
#include <errno.h>  /* errno */
#include <stdint.h> /* uint8_t, etc... */
#include <math.h>
#include "de.h"     // ST_ANSWER
#include "egts.h"

// for encode
#define MAX_TERMINALS 1
#define UTS2010	(1262304000)	// unix timestamp 00:00:00 01.01.2010


const unsigned char CRC8Table[256] = {
	0x00, 0x31, 0x62, 0x53, 0xC4, 0xF5, 0xA6, 0x97,
	0xB9, 0x88, 0xDB, 0xEA, 0x7D, 0x4C, 0x1F, 0x2E,
	0x43, 0x72, 0x21, 0x10, 0x87, 0xB6, 0xE5, 0xD4,
	0xFA, 0xCB, 0x98, 0xA9, 0x3E, 0x0F, 0x5C, 0x6D,
	0x86, 0xB7, 0xE4, 0xD5, 0x42, 0x73, 0x20, 0x11,
	0x3F, 0x0E, 0x5D, 0x6C, 0xFB, 0xCA, 0x99, 0xA8,
	0xC5, 0xF4, 0xA7, 0x96, 0x01, 0x30, 0x63, 0x52,
	0x7C, 0x4D, 0x1E, 0x2F, 0xB8, 0x89, 0xDA, 0xEB,
	0x3D, 0x0C, 0x5F, 0x6E, 0xF9, 0xC8, 0x9B, 0xAA,
	0x84, 0xB5, 0xE6, 0xD7, 0x40, 0x71, 0x22, 0x13,
	0x7E, 0x4F, 0x1C, 0x2D, 0xBA, 0x8B, 0xD8, 0xE9,
	0xC7, 0xF6, 0xA5, 0x94, 0x03, 0x32, 0x61, 0x50,
	0xBB, 0x8A, 0xD9, 0xE8, 0x7F, 0x4E, 0x1D, 0x2C,
	0x02, 0x33, 0x60, 0x51, 0xC6, 0xF7, 0xA4, 0x95,
	0xF8, 0xC9, 0x9A, 0xAB, 0x3C, 0x0D, 0x5E, 0x6F,
	0x41, 0x70, 0x23, 0x12, 0x85, 0xB4, 0xE7, 0xD6,
	0x7A, 0x4B, 0x18, 0x29, 0xBE, 0x8F, 0xDC, 0xED,
	0xC3, 0xF2, 0xA1, 0x90, 0x07, 0x36, 0x65, 0x54,
	0x39, 0x08, 0x5B, 0x6A, 0xFD, 0xCC, 0x9F, 0xAE,
	0x80, 0xB1, 0xE2, 0xD3, 0x44, 0x75, 0x26, 0x17,
	0xFC, 0xCD, 0x9E, 0xAF, 0x38, 0x09, 0x5A, 0x6B,
	0x45, 0x74, 0x27, 0x16, 0x81, 0xB0, 0xE3, 0xD2,
	0xBF, 0x8E, 0xDD, 0xEC, 0x7B, 0x4A, 0x19, 0x28,
	0x06, 0x37, 0x64, 0x55, 0xC2, 0xF3, 0xA0, 0x91,
	0x47, 0x76, 0x25, 0x14, 0x83, 0xB2, 0xE1, 0xD0,
	0xFE, 0xCF, 0x9C, 0xAD, 0x3A, 0x0B, 0x58, 0x69,
	0x04, 0x35, 0x66, 0x57, 0xC0, 0xF1, 0xA2, 0x93,
	0xBD, 0x8C, 0xDF, 0xEE, 0x79, 0x48, 0x1B, 0x2A,
	0xC1, 0xF0, 0xA3, 0x92, 0x05, 0x34, 0x67, 0x56,
	0x78, 0x49, 0x1A, 0x2B, 0xBC, 0x8D, 0xDE, 0xEF,
	0x82, 0xB3, 0xE0, 0xD1, 0x46, 0x77, 0x24, 0x15,
	0x3B, 0x0A, 0x59, 0x68, 0xFF, 0xCE, 0x9D, 0xAC
};

const unsigned short Crc16Table[256] = {
	0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50A5, 0x60C6, 0x70E7,
	0x8108, 0x9129, 0xA14A, 0xB16B, 0xC18C, 0xD1AD, 0xE1CE, 0xF1EF,
	0x1231, 0x0210, 0x3273, 0x2252, 0x52B5, 0x4294, 0x72F7, 0x62D6,
	0x9339, 0x8318, 0xB37B, 0xA35A, 0xD3BD, 0xC39C, 0xF3FF, 0xE3DE,
	0x2462, 0x3443, 0x0420, 0x1401, 0x64E6, 0x74C7, 0x44A4, 0x5485,
	0xA56A, 0xB54B, 0x8528, 0x9509, 0xE5EE, 0xF5CF, 0xC5AC, 0xD58D,
	0x3653, 0x2672, 0x1611, 0x0630, 0x76D7, 0x66F6, 0x5695, 0x46B4,
	0xB75B, 0xA77A, 0x9719, 0x8738, 0xF7DF, 0xE7FE, 0xD79D, 0xC7BC,
	0x48C4, 0x58E5, 0x6886, 0x78A7, 0x0840, 0x1861, 0x2802, 0x3823,
	0xC9CC, 0xD9ED, 0xE98E, 0xF9AF, 0x8948, 0x9969, 0xA90A, 0xB92B,
	0x5AF5, 0x4AD4, 0x7AB7, 0x6A96, 0x1A71, 0x0A50, 0x3A33, 0x2A12,
	0xDBFD, 0xCBDC, 0xFBBF, 0xEB9E, 0x9B79, 0x8B58, 0xBB3B, 0xAB1A,
	0x6CA6, 0x7C87, 0x4CE4, 0x5CC5, 0x2C22, 0x3C03, 0x0C60, 0x1C41,
	0xEDAE, 0xFD8F, 0xCDEC, 0xDDCD, 0xAD2A, 0xBD0B, 0x8D68, 0x9D49,
	0x7E97, 0x6EB6, 0x5ED5, 0x4EF4, 0x3E13, 0x2E32, 0x1E51, 0x0E70,
	0xFF9F, 0xEFBE, 0xDFDD, 0xCFFC, 0xBF1B, 0xAF3A, 0x9F59, 0x8F78,
	0x9188, 0x81A9, 0xB1CA, 0xA1EB, 0xD10C, 0xC12D, 0xF14E, 0xE16F,
	0x1080, 0x00A1, 0x30C2, 0x20E3, 0x5004, 0x4025, 0x7046, 0x6067,
	0x83B9, 0x9398, 0xA3FB, 0xB3DA, 0xC33D, 0xD31C, 0xE37F, 0xF35E,
	0x02B1, 0x1290, 0x22F3, 0x32D2, 0x4235, 0x5214, 0x6277, 0x7256,
	0xB5EA, 0xA5CB, 0x95A8, 0x8589, 0xF56E, 0xE54F, 0xD52C, 0xC50D,
	0x34E2, 0x24C3, 0x14A0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
	0xA7DB, 0xB7FA, 0x8799, 0x97B8, 0xE75F, 0xF77E, 0xC71D, 0xD73C,
	0x26D3, 0x36F2, 0x0691, 0x16B0, 0x6657, 0x7676, 0x4615, 0x5634,
	0xD94C, 0xC96D, 0xF90E, 0xE92F, 0x99C8, 0x89E9, 0xB98A, 0xA9AB,
	0x5844, 0x4865, 0x7806, 0x6827, 0x18C0, 0x08E1, 0x3882, 0x28A3,
	0xCB7D, 0xDB5C, 0xEB3F, 0xFB1E, 0x8BF9, 0x9BD8, 0xABBB, 0xBB9A,
	0x4A75, 0x5A54, 0x6A37, 0x7A16, 0x0AF1, 0x1AD0, 0x2AB3, 0x3A92,
	0xFD2E, 0xED0F, 0xDD6C, 0xCD4D, 0xBDAA, 0xAD8B, 0x9DE8, 0x8DC9,
	0x7C26, 0x6C07, 0x5C64, 0x4C45, 0x3CA2, 0x2C83, 0x1CE0, 0x0CC1,
	0xEF1F, 0xFF3E, 0xCF5D, 0xDF7C, 0xAF9B, 0xBFBA, 0x8FD9, 0x9FF8,
	0x6E17, 0x7E36, 0x4E55, 0x5E74, 0x2E93, 0x3EB2, 0x0ED1, 0x1EF0
};

const long GMT_diff = (7*60*60);

void logging(char * template, ...)
{

}

// rounding real numbers, steals from the Internet
double Round(double Value, int SignNumber)
{
	int i;
	int    Sign;
	double Fraction;
	double Integer;
	double Ratio;
	double Correction;

	// get sign
	if (Value < 0)
		Sign = -1;
	else
		Sign = 1;

	// get module
	Value *= Sign;

	Ratio = 1;
	Correction = 1;

	// for precision
	for (i = 0; i < SignNumber; i++) {
		Ratio *= 10;
		Correction /= 10;
	}

	// rounding correction
	Correction /= 1000;
	Value *= Ratio;
	Value += Correction;

	// get fractions
	Fraction = modf(Value, &Integer);

	// if fraction > 0.5 add 1
	Value = Integer;
	if(Fraction >= 0.5)
		Value += 1;
	Value /= Ratio;

	Value *= Sign;
	return( Value );
}

int terminal_decode(uint8_t *data, uint32_t len, egts_msg_t *msg) {
	msg->type = EGTS_MSG_TYPE_UNKNOWN;
	msg->data = NULL;

	if(data == NULL || len == 0) {
		return -1;
	}

	EGTS_PACKET_HEADER *header = (EGTS_PACKET_HEADER *)data;

	if( header->PRV != 1 || (header->PRF & 192) ) {
		user_printf("EGTS_PC_UNS_PROTOCOL\r\n");
		return EGTS_PC_UNS_PROTOCOL;
	}

	if( header->HL != 11 && header->HL != 16 ) {
		user_printf("EGTS_PC_INC_HEADERFORM\r\n");
		return EGTS_PC_INC_HEADERFORM;
	}

	if( CRC8EGTS((unsigned char *)header, header->HL-1) != header->HCS ) {
		user_printf("EGTS_PC_HEADERCRC_ERROR\r\n");
		return EGTS_PC_HEADERCRC_ERROR;
	}

	if( B5 & header->PRF ) {
		user_printf("EGTS_PC_TTLEXPIRED\r\n");
		return EGTS_PC_TTLEXPIRED;
	}

	if( !header->FDL ) {
		user_printf("EGTS_PC_OK");
		return EGTS_PC_OK;
	}

	// проверяем CRC16
	unsigned short *SFRCS = (unsigned short *)&data[header->HL + header->FDL];
	if( *SFRCS != CRC16EGTS( (unsigned char *)&data[header->HL], header->FDL) ) {
		user_printf("EGTS_PC_DATACRC_ERROR\r\n");
		return EGTS_PC_DATACRC_ERROR;
	}

	// проверяем шифрование данных
	if( header->PRF & 24 ) {
		user_printf("EGTS_PC_DECRYPT_ERROR\r\n");
		return EGTS_PC_DECRYPT_ERROR;
	}

	// проверяем сжатие данных
	if( header->PRF & B2 ) {
		user_printf("EGTS_PC_INC_DATAFORM\r\n");
		return EGTS_PC_INC_DATAFORM;
	}

//	user_printf("head->PRV=%d\r\n", header->PRV);
//	user_printf("head->SKID=%d\r\n", header->SKID);
//	user_printf("head->PRF=%d\r\n", header->PRF);
//	user_printf("head->HL=%d\r\n", header->HL);
//	user_printf("head->HE=%d\r\n", header->HE);
//	user_printf("head->FDL=%d\r\n", header->FDL);
//	user_printf("head->PID=%d\r\n", header->PID);
//	user_printf("head->PT=%d\r\n", header->PT);
//	user_printf("head->HCS=%d\r\n", header->HCS);

	switch (header->PT) {
	case EGTS_PT_RESPONSE: {
		msg->type = EGTS_MSG_TYPE_RESPONSE;

		EGTS_PT_RESPONSE_HEADER *response_hdr =			(EGTS_PT_RESPONSE_HEADER *)&data[header->HL];
		EGTS_RECORD_HEADER *record_hdr =				(EGTS_RECORD_HEADER*)((uint8_t*)response_hdr + sizeof(EGTS_PT_RESPONSE_HEADER));
		EGTS_SUBRECORD_HEADER *subrecord_hdr =			(EGTS_SUBRECORD_HEADER*)((uint8_t*)record_hdr + (sizeof(EGTS_RECORD_HEADER) - 3*4)); // Отнимаем необязательные поля
		EGTS_SR_RECORD_RESPONSE_RECORD *response_rec =	(EGTS_SR_RECORD_RESPONSE_RECORD*)((uint8_t*)subrecord_hdr + sizeof(EGTS_SUBRECORD_HEADER));

		ZF_LOGW("EGTS Response code: %d%s", (int)response_rec->RST, response_rec->RST == 0 ? "" : ". Ignore");
		/// @todo Нельзя так просто игнорировать ошибку
		response_rec->RST = 0;

		if (response_hdr->PR == 0 && response_rec->RST == 0) {
			return EGTS_PC_OK;
		}
		else {
			user_printf("Response Error code\r\n");
			return -1;
		}
	}
	break;

	case EGTS_PT_SIGNED_APPDATA: {
		msg->type = EGTS_MSG_TYPE_UNKNOWN;

		// данные с цифровой подписью
		user_printf("Not implemented\r\n");
		return -1;
	}
	break;

	case EGTS_PT_APPDATA: {
		msg->type = EGTS_MSG_TYPE_APPDATA;

		// получаем указатель на SDR (Service Data Record)
		EGTS_RECORD_HEADER * rec_head = (EGTS_RECORD_HEADER *)&data[header->HL];

		// Далее делаем втупую, сиходя из того, что по факту приходит, потмоу что я не понял полностью входящие пакеты
		{
			uint8_t * rec = (uint8_t*)rec_head;
			if (rec[0] == 4 && (rec[7] == EGTS_SR_RESULT_CODE || rec[7] == EGTS_SR_RESULT_CODE_OLD) && rec[10] == EGTS_PC_OK) {
				// Успешная авторизация
				return EGTS_PC_OK;
			}
			else if (rec[0] == 4 && (rec[7] == EGTS_SR_RESULT_CODE || rec[7] == EGTS_SR_RESULT_CODE_OLD) && rec[10] >= EGTS_PC_UNS_PROTOCOL) {
				// Ошибка авторзации
				return rec[10];
			}
			else if (rec[7] == 51) { // Сервис команд
				msg->type = EGTS_MSG_TYPE_COMMAND;
				msg->data = &rec[10];
				return EGTS_PC_OK;
//				uint8_t cmd = rec[26]; // 'o' сброс одометра
			}
		}

		return -1;
	}
	break;

	}	// switch( pak_head->PT )

	return -1;
}

/* функция декодирования посылки от терминала
parcel - данные из сокета
parcel_size - длинна данных
answer - структура с декодированными данными
*/
//void _terminal_decode(char *parcel, int parcel_size, ST_ANSWER *answer){
//  int parcel_pointer, sdr_readed;
//	EGTS_PACKET_HEADER *pak_head;
//	EGTS_RECORD_HEADER *rec_head, st_rec_header;
//	EGTS_SUBRECORD_HEADER *srd_head;
//	ST_RECORD *record = NULL;
//	/*
//	EGTS_PT_RESPONSE_HEADER *response_hdr = NULL;
//	EGTS_SR_RECORD_RESPONSE_RECORD *response_rec = NULL;
//	EGTS_SR_RESULT_CODE_RECORD *result_rec = NULL;
//	*/
//
//	if( !parcel || parcel_size <= 0 || !answer )
//		return;
//
//  // создаем ответ на пакет
//	answer->size = egts_packet_create(answer->answer, EGTS_PT_RESPONSE);
//
//	// разбираем заголовок пакета
//	pak_head = (EGTS_PACKET_HEADER *)parcel;
//	if( !Parse_EGTS_PACKET_HEADER(answer, parcel, parcel_size) ){
//		answer->size += egts_packet_finalize(answer->answer, answer->size);
//		return;
//	}
//	parcel_pointer = pak_head->HL;
//
//  // проверяем тип пакета
//	switch( pak_head->PT ){
//	case EGTS_PT_RESPONSE:	// ответ на что-то
//		//log2file("/var/www/locman.org/tmp/gcc/r_EGTS_PT_RESPONSE", parcel, parcel_size);
//
//		answer->size = 0;
//
//    /*
//		response_hdr = (EGTS_PT_RESPONSE_HEADER *)&parcel[pak_head->HL];
//		response_rec = (EGTS_SR_RECORD_RESPONSE_RECORD *)&parcel[pak_head->HL + sizeof(EGTS_PT_RESPONSE_HEADER)];
//		result_rec = (EGTS_SR_RESULT_CODE_RECORD *)&parcel[pak_head->HL + sizeof(EGTS_PT_RESPONSE_HEADER) + sizeof(EGTS_SR_RECORD_RESPONSE_RECORD)];
//
//    logging("terminal_decode[egts]: EGTS_PT_RESPONSE packet[%u]=%u, rec[%u]=%u, res=%u\n",
//																								response_hdr->RPID,
//																								response_hdr->PR,
//																								response_rec->CRN,
//																								response_rec->RST,
//																								result_rec->RCD);
//		*/
//
//    return; // отвечать на ответ моветон
//	case EGTS_PT_SIGNED_APPDATA:	// данные с цифровой подписью
//		//log2file("/var/www/locman.org/tmp/gcc/r_EGTS_PT_SIGNED_APPDATA", parcel, parcel_size);
//
//		// пропускаем цифровую подпись: 2 байта SIGL(Signature Length) + Signature Length
//		parcel_pointer += (*(uint16_t *)&parcel[parcel_pointer] + sizeof(uint16_t));
//
//		break;
//	case EGTS_PT_APPDATA:	// просто данные
//		break;
//	}	// switch( pak_head->PT )
//
//  // вставляем в ответ на пакет OK для транспортного уровня пакета, получили, типа
//	answer->size += responce_add_responce(answer->answer, answer->size, pak_head->PID, EGTS_PC_OK);
//
//
//	// Чтение данных SFRD
//	while( parcel_pointer < pak_head->HL + pak_head->FDL ){
//
//		// получаем указатель на SDR (Service Data Record)
//	  rec_head = (EGTS_RECORD_HEADER *)&parcel[parcel_pointer];
//		// проверяем длинну присланных данных
//		if( !rec_head->RL ){	// EGTS_PC_INVDATALEN
//			logging("terminal_decode[egts]: SDR:EGTS_PC_INVDATALEN error\n");
//			answer->size += responce_add_record(answer->answer, answer->size, rec_head->RN, EGTS_PC_INVDATALEN);
//			answer->size += egts_packet_finalize(answer->answer, answer->size);
//			return;
//		}
//
//		// разбираем SDR
//		parcel_pointer += Parse_EGTS_RECORD_HEADER(rec_head, &st_rec_header, answer);
//
//		sdr_readed = 0;	// прочитано данных, байт
//    while(sdr_readed < rec_head->RL){
//
//			// получаем указатель на SRD (Subrecord Data) заголовок
//			srd_head = (EGTS_SUBRECORD_HEADER *)&parcel[parcel_pointer];
//			// проверяем длинну присланных данных
//			if( !srd_head->SRL ){
//				logging("terminal_decode[egts]: SRD:EGTS_PC_INVDATALEN error\n");
//				answer->size += responce_add_record(answer->answer, answer->size, rec_head->RN, EGTS_PC_INVDATALEN);
//				answer->size += egts_packet_finalize(answer->answer, answer->size);
//				return;
//			}
//
//      sdr_readed += sizeof(EGTS_SUBRECORD_HEADER);
//			parcel_pointer += sizeof(EGTS_SUBRECORD_HEADER);
//
//			// разбираем SRD (Subrecord Data) в зависимости от типа записи
//			switch( srd_head->SRT ){
//			case EGTS_SR_TERM_IDENTITY:	// авторизация
//
//				answer->size += responce_add_record(answer->answer, answer->size, rec_head->RN, EGTS_PC_OK);
//
//				if( !Parse_EGTS_SR_TERM_IDENTITY( (EGTS_SR_TERM_IDENTITY_RECORD *)&parcel[parcel_pointer], answer ) && !st_rec_header.OID ){ // нет ID терминала
//					// формируем ответ пройдите на хуй, пожалуйста
//					answer->size += responce_add_result(answer->answer, answer->size, EGTS_PC_AUTH_DENIED);
//				}
//				else {
//					// формируем ответ добро пожаловать
//					answer->size += responce_add_result(answer->answer, answer->size, EGTS_PC_OK);
//				}
//
//				break;
//	    case EGTS_SR_POS_DATA:	// навигационные данные
//
//	      // создаем запись для сохранения данных
//		  	if( answer->count < MAX_RECORDS - 1 )
//					answer->count++;
//				record = &answer->records[answer->count - 1];
//
//				if( Parse_EGTS_SR_POS_DATA( (EGTS_SR_POS_DATA_RECORD *)&parcel[parcel_pointer], record, answer ) ){
//					answer->size += responce_add_record(answer->answer, answer->size, rec_head->RN, EGTS_PC_OK);
//					memcpy(&answer->lastpoint, record, sizeof(ST_RECORD));
//				}
//				else {
//					answer->size += responce_add_record(answer->answer, answer->size, rec_head->RN, EGTS_PC_OK);
//				}
//
//				break;
//			case EGTS_SR_EXT_POS_DATA:
//
//        if( record ){
//					Parse_EGTS_SR_EXT_POS_DATA((EGTS_SR_EXT_POS_DATA_RECORD *)&parcel[parcel_pointer], record);
//				}
//
//				break;
//			case EGTS_SR_LIQUID_LEVEL_SENSOR:	// датчики уровня жидкости
//
//        if( record ){
//        	Parse_EGTS_SR_LIQUID_LEVEL_SENSOR(srd_head->SRL, (EGTS_SR_LIQUID_LEVEL_SENSOR_RECORD *)&parcel[parcel_pointer], record);
//				}
//
//				break;
//			case EGTS_SR_COMMAND_DATA:	// команда
//
//       	// сформировать подтверждение в виде подзаписи EGTS_SR_COMMAND_DATA сервиса EGTS_COMMAND_SERVICE
//				answer->size += responce_add_record(answer->answer, answer->size, rec_head->RN, EGTS_PC_OK);
//
//				if( Parse_EGTS_SR_COMMAND_DATA(answer, (EGTS_SR_COMMAND_DATA_RECORD *)&parcel[parcel_pointer]) ){
//					// завершаем пакет
//					answer->size += egts_packet_finalize(answer->answer, answer->size);
//
//					// сформировать подзапись EGTS_SR_POS_DATA сервиса EGRS_TELEDATA_SERVICE
//					// создать новый пакет и вернуть его
//					return;
//				}	// if( Parse_EGTS_SR_COMMAND_DATA(
//
//				break;
//			default:	// мы такое не обрабатываем
//
//				//log2file("/var/www/locman.org/tmp/gcc/r_default", parcel, parcel_size);
//				answer->size += responce_add_record(answer->answer, answer->size, rec_head->RN, EGTS_PC_OK);
//
//			}	// switch( srd_head->SRT )
//
//      // переходим к следующей подзаписи
//			sdr_readed += srd_head->SRL;
//			parcel_pointer += srd_head->SRL;
//		}	// while(sdr_readed < rec_head->RL)
//
//  }	// while( parcel_pointer < pak_head->HL + pak_head->FDL )
//
//	/* debug
//	if(record){
//	logging("terminal_decode[egts]: record->imei=%s\n", record->imei);
//	logging("terminal_decode[egts]: record->lat=%lf\n", record->lat);
//	logging("terminal_decode[egts]: record->lon=%lf\n", record->lon);
//	logging("terminal_decode[egts]: record->valid=%d\n", record->valid);
//	logging("terminal_decode[egts]: record->satellites=%d\n", record->satellites);
//	logging("terminal_decode[egts]: record->hdop=%d\n", record->hdop);
//	logging("terminal_decode[egts]: record->height=%d\n", record->height);
//	logging("terminal_decode[egts]: record->curs=%d\n", record->curs);
//	logging("terminal_decode[egts]: record->speed=%lf\n", record->speed);
//	logging("terminal_decode[egts]: record->probeg=%lf\n", record->probeg);
//	}
//	*/
//
//	if(answer->size){
//		//log2file("/var/www/locman.org/tmp/gcc/w_answer", answer->answer, answer->size);
//		answer->size += egts_packet_finalize(answer->answer, answer->size);
//	}
//
//}
//------------------------------------------------------------------------------

/* создание заголовка транспортного уровня (EGTS_PACKET_HEADER) ответа на сообщение
buffer - укакзатель на буфер, в котором формируется пакет
pt - Тип пакета Транспортного Уровня
*/
int egts_packet_create(uint8_t *buffer, uint8_t pt){
	static unsigned short int PaketNumber = 0;	// packet number, joint for encode & decode
	EGTS_PACKET_HEADER *pak_head = (EGTS_PACKET_HEADER *)buffer;
	pak_head->PRV = 1;
	pak_head->SKID = 0;
	pak_head->PRF = 0;
	pak_head->HL = sizeof(EGTS_PACKET_HEADER);	// 11
	pak_head->HE = 0;
	pak_head->FDL = 0;
	pak_head->PID = PaketNumber++;
	pak_head->PT = pt;	// Тип пакета Транспортного Уровня
	//pak_head->HCS = CRC8((unsigned char *)pak_head, pak_head->HL-1); // see packet_finalize

	return pak_head->HL;
}
//------------------------------------------------------------------------------

// добавление в ответ результата обработки транспортного уровня
int responce_add_responce(uint8_t *buffer, int pointer, uint16_t pid, uint8_t pr){
	EGTS_PT_RESPONSE_HEADER *res_head = (EGTS_PT_RESPONSE_HEADER *)&buffer[pointer];
	res_head->RPID = pid;
  res_head->PR = pr;

	EGTS_PACKET_HEADER *pak_head = (EGTS_PACKET_HEADER *)buffer;
	pak_head->FDL = sizeof(EGTS_PT_RESPONSE_HEADER);

	return sizeof(EGTS_PT_RESPONSE_HEADER);
}
//------------------------------------------------------------------------------

// добавление в ответ результата обработки записей
int responce_add_record(uint8_t *buffer, int pointer, uint16_t crn, uint8_t rst){
	EGTS_SR_RECORD_RESPONSE_RECORD *record = (EGTS_SR_RECORD_RESPONSE_RECORD *)&buffer[pointer];
	record->CRN = crn;
  record->RST = rst;

	EGTS_PACKET_HEADER *pak_head = (EGTS_PACKET_HEADER *)buffer;
	pak_head->FDL += sizeof(EGTS_SR_RECORD_RESPONSE_RECORD);

	return sizeof(EGTS_SR_RECORD_RESPONSE_RECORD);
}
//------------------------------------------------------------------------------

// добавление в ответ результата обработки чего-нибудь
int responce_add_result(uint8_t *buffer, int pointer, uint8_t rcd){
	EGTS_SR_RESULT_CODE_RECORD *record = (EGTS_SR_RESULT_CODE_RECORD *)&buffer[pointer];
	record->RCD = rcd;

	EGTS_PACKET_HEADER *pak_head = (EGTS_PACKET_HEADER *)buffer;
	pak_head->FDL += sizeof(EGTS_SR_RESULT_CODE_RECORD);

	return sizeof(EGTS_SR_RESULT_CODE_RECORD);
}
//------------------------------------------------------------------------------

// расчет CRC16 & CRC8 для данных егтс пакета
int egts_packet_finalize(uint8_t *buffer, int pointer){
	EGTS_PACKET_HEADER *pak_head = (EGTS_PACKET_HEADER *)buffer;

	if( pointer - pak_head->HL > pak_head->FDL ){
		logging("terminal_decode[egts]: pak_head->FDL correct from %u to %u\n", pak_head->FDL, pointer - pak_head->HL);
		pak_head->FDL = pointer - pak_head->HL;
	}

	// добавляем CRC16 в конец пакета
	unsigned short *SFRCS = (unsigned short *)&buffer[pointer];
	// рассчитываем CRC16
	*SFRCS = CRC16EGTS( (unsigned char *)&buffer[pak_head->HL], pak_head->FDL );

  // рассчитываем CRC8
	pak_head->HCS = CRC8EGTS((unsigned char *)pak_head, pak_head->HL - 1);	// последний байт это CRC

	return sizeof(unsigned short);
}
//------------------------------------------------------------------------------

/*
Name : CRC-8
Poly : 0x31 x^8 + x^5 + x^4 + 1
Init : 0xFF
Revert: false
XorOut: 0x00
Check : 0xF7 ("123456789")
*/
unsigned char CRC8EGTS(unsigned char *lpBlock, unsigned char len)
{
	unsigned char crc = 0xFF;
	while (len--)
	crc = CRC8Table[crc ^ *lpBlock++];
	return crc;
}
//------------------------------------------------------------------------------

/*
Name : CRC-16 CCITT
Poly : 0x1021 x^16 + x^12 + x^5 + 1
Init : 0xFFFF
Revert: false
XorOut: 0x0000
Check : 0x29B1 ("123456789")
*/
unsigned short CRC16EGTS(unsigned char * pcBlock, unsigned short len)
{
	unsigned short crc = 0xFFFF;
	while (len--)
	crc = (crc << 8) ^ Crc16Table[(crc >> 8) ^ *pcBlock++];
	return crc;
}
//------------------------------------------------------------------------------

//int Parse_EGTS_PACKET_HEADER(ST_ANSWER *answer, char *pc, int parcel_size){
//	EGTS_PACKET_HEADER *ph = (EGTS_PACKET_HEADER *)pc;
//
//  if( ph->PRV != 1 || (ph->PRF & 192) ){
//		logging("terminal_decode[egts]: EGTS_PC_UNS_PROTOCOL error\n");
//		answer->size += responce_add_responce(answer->answer, answer->size, ph->PID, EGTS_PC_UNS_PROTOCOL);
//		return 0;
//  }
//
//	if( ph->HL != 11 && ph->HL != 16 ){
//		logging("terminal_decode[egts]: EGTS_PC_INC_HEADERFORM error\n");
//		answer->size += responce_add_responce(answer->answer, answer->size, ph->PID, EGTS_PC_INC_HEADERFORM);
//		return 0;
//	}
//
//	if( CRC8EGTS((unsigned char *)ph, ph->HL-1) != ph->HCS ){
//		logging("terminal_decode[egts]: EGTS_PC_HEADERCRC_ERROR error %u/%u\n", CRC8EGTS((unsigned char *)ph, ph->HL-1), ph->HCS);
//		answer->size += responce_add_responce(answer->answer, answer->size, ph->PID, EGTS_PC_HEADERCRC_ERROR);
//		return 0;
//	}
//
//	if( B5 & ph->PRF ){
//		logging("terminal_decode[egts]: EGTS_PC_TTLEXPIRED error\n");
//		answer->size += responce_add_responce(answer->answer, answer->size, ph->PID, EGTS_PC_TTLEXPIRED);
//		return 0;
//	}
//
//	if( !ph->FDL ){
//		logging("terminal_decode[egts]: EGTS_PC_OK\n");
//		answer->size += responce_add_responce(answer->answer, answer->size, ph->PID, EGTS_PC_OK);
//		return 0;
//	}
//
//	// проверяем CRC16
//	unsigned short *SFRCS = (unsigned short *)&pc[ph->HL + ph->FDL];
//	if( *SFRCS != CRC16EGTS( (unsigned char *)&pc[ph->HL], ph->FDL) ){
//		logging("terminal_decode[egts]: EGTS_PC_DATACRC_ERROR error\n");
//		answer->size += responce_add_responce(answer->answer, answer->size, ph->PID, EGTS_PC_DATACRC_ERROR);
//		return 0;
//	}
//
//  // проверяем шифрование данных
//	if( ph->PRF & 24 ){
//		logging("terminal_decode[egts]: EGTS_PC_DECRYPT_ERROR error\n");
//		answer->size += responce_add_responce(answer->answer, answer->size, ph->PID, EGTS_PC_DECRYPT_ERROR);
//		return 0;
//	}
//
//  // проверяем сжатие данных
//	if( ph->PRF & B2 ){
//		logging("terminal_decode[egts]: EGTS_PC_INC_DATAFORM error\n");
//		answer->size += responce_add_responce(answer->answer, answer->size, ph->PID, EGTS_PC_INC_DATAFORM);
//	}
//
//	/*
//	logging("terminal_decode[egts]: pak_head->PRV=%d\n", ph->PRV);
//	logging("terminal_decode[egts]: pak_head->SKID=%d\n", ph->SKID);
//	logging("terminal_decode[egts]: pak_head->PRF=%d\n", ph->PRF);
//	logging("terminal_decode[egts]: pak_head->HL=%d\n", ph->HL);
//	logging("terminal_decode[egts]: pak_head->HE=%d\n", ph->HE);
//	logging("terminal_decode[egts]: pak_head->FDL=%d\n", ph->FDL);
//	logging("terminal_decode[egts]: pak_head->PID=%d\n", ph->PID);
//	logging("terminal_decode[egts]: pak_head->PT=%d\n", ph->PT);
//	logging("terminal_decode[egts]: pak_head->HCS=%d\n", ph->HCS);
//	*/
//
//	return 1;
//}

//------------------------------------------------------------------------------


//int Parse_EGTS_RECORD_HEADER(EGTS_RECORD_HEADER *rec_head, EGTS_RECORD_HEADER *st_header, ST_ANSWER *answer){
//
//	int rec_head_size = 2 * sizeof(uint16_t) + sizeof(uint8_t);
//	char *pc = (char *)rec_head;
//
//	memset(st_header, 0, sizeof(EGTS_RECORD_HEADER));
//
//	st_header->RL = rec_head->RL;
//	st_header->RN = rec_head->RN;
//	st_header->RFL = rec_head->RFL;
//
//	// OBFE	0		наличие в данном пакете поля OID 1 = присутствует 0 = отсутствует
//	if( st_header->RFL & B0 ){
//		st_header->OID = *(uint32_t *)&pc[rec_head_size];
//		if( st_header->OID && !strlen(answer->lastpoint.imei) ){	// Рекомендации по реализации протокола передачи данных в РНИЦ.doc 5.1.1	Идентификация АС посредством поля OID
//		  memset(answer->lastpoint.imei, 0, SIZE_TRACKER_FIELD);
//			snprintf(answer->lastpoint.imei, SIZE_TRACKER_FIELD, "%d", st_header->OID);
//		}
//		rec_head_size += sizeof(uint32_t);
//	}
//
//  // EVFE	1		наличие в данном пакете поля EVID 1 = присутствует 0 = отсутствует
//	if( st_header->RFL & B1 ){
//		st_header->EVID = *(uint32_t *)&pc[rec_head_size];
//		rec_head_size += sizeof(uint32_t);
//	}
//
//	// TMFE	2		наличие в данном пакете поля TM 1 = присутствует 0 = отсутствует
//	if( st_header->RFL & B2 ){
//		st_header->TM = *(uint32_t *)&pc[rec_head_size];
//		rec_head_size += sizeof(uint32_t);
//	}
//
//	st_header->SST = *(uint8_t *)&pc[rec_head_size];
//	rec_head_size += sizeof(uint8_t);
//	st_header->RST = *(uint8_t *)&pc[rec_head_size];
//	rec_head_size += sizeof(uint8_t);
//
//	/*
//	logging("terminal_decode[egts]: st_header->RL=%d\n", st_header->RL);
//	logging("terminal_decode[egts]: st_header->RN=%d\n", st_header->RN);
//	logging("terminal_decode[egts]: st_header->RFL=%d\n", st_header->RFL);
//	logging("terminal_decode[egts]: st_header->OID=%d\n", st_header->OID);
//	logging("terminal_decode[egts]: st_header->EVID=%d\n", st_header->EVID);
//	logging("terminal_decode[egts]: st_header->TM=%d\n", st_header->TM);
//	logging("terminal_decode[egts]: st_header->SST=%d\n", st_header->SST);
//	logging("terminal_decode[egts]: st_header->RST=%d\n", st_header->RST);
//	logging("terminal_decode[egts]: rec_head_size=%d\n", rec_head_size);
//	*/
//
//	return rec_head_size;
//}
//------------------------------------------------------------------------------

//// разбор записи EGTS_SR_TERM_IDENTITY (Подзапись используется АТ при запросе авторизации на ТП и содержит учётные данные АТ)
//// Рекомендации по реализации протокола передачи данных в РНИЦ.doc
//// 5.1.2	Идентификация АС посредством сервиса EGTS_AUTH_SERVICE
//int Parse_EGTS_SR_TERM_IDENTITY(EGTS_SR_TERM_IDENTITY_RECORD *record, ST_ANSWER *answer){
////  int record_size = sizeof(uint32_t) + sizeof(uint8_t);
////	char *pc = (char *)record;
////
////	memset(answer->lastpoint.imei, 0, SIZE_TRACKER_FIELD);
////
////	if( record->FLG & B1 ){ // наличие поля IMEI в подзаписи
////		if( record->FLG & B0 ){	// наличие поля HDID в подзаписи
////			record_size += sizeof(uint16_t);	// пропускаем поле HDID, если есть
////		}
////		memcpy(answer->lastpoint.imei, &pc[record_size], 15);
////		record_size += EGTS_IMEI_LEN;
////	}	// if( record->FLG & B1 )
////	else if( record->TID ){
////		snprintf(answer->lastpoint.imei, SIZE_TRACKER_FIELD, "%d", record->TID);
////	}
////	// если не прислан IMEI и record->TID = 0, то answer->lastpoint.imei окажется пустым
////
////  /*
////	if( record->FLG & B2 ){ // наличие поля IMSI в подзаписи
////		record_size += EGTS_IMSI_LEN;
////	}
////
////	if( record->FLG & B3 ){ // наличие поля LNGC в подзаписи
////		record_size += EGTS_LNGC_LEN;
////	}
////
////	if( record->FLG & B5 ){ // наличие поля NID в подзаписи
////		record_size += (3 * sizeof(uint8_t));
////	}
////
////	if( record->FLG & B6 ){ // наличие поля BS в подзаписи
////		record_size += sizeof(uint16_t);
////	}
////
////	if( record->FLG & B7 ){ // наличие поля MSISDN в подзаписи
////		record_size += EGTS_MSISDN_LEN;
////	}
////
////	return record_size;
////	*/
////
////	return strlen(answer->lastpoint.imei);	// всех пускать
//	return 0;
//}
////------------------------------------------------------------------------------
//
//// разбираем запись с навигационными данными
//int Parse_EGTS_SR_POS_DATA(EGTS_SR_POS_DATA_RECORD *posdata, ST_RECORD *record, ST_ANSWER *answer){
////	char *pc = (char *)posdata;
////	void *tpp;
////	struct tm tm_data;
////	time_t ulliTmp;
////
////	if( !record )
////		return 0;
////
////  ulliTmp = posdata->NTM + GMT_diff;	// UTC ->local
////	gmtime_r(&ulliTmp, &tm_data);           // local simple->local struct
////	// получаем время как число секунд от начала суток
////	record->time = 3600 * tm_data.tm_hour + 60 * tm_data.tm_min + tm_data.tm_sec;
////	// в tm_data обнуляем время
////  tm_data.tm_hour = tm_data.tm_min = tm_data.tm_sec = 0;
////	// получаем дату
////	tm_data.tm_year = (tm_data.tm_year + 2010 - 1970);
////	record->data = mktime(&tm_data) - GMT_diff;	// local struct->local simple & mktime epoch
////
////	// координаты
////	record->lat = 90.0 * posdata->LAT / 0xFFFFFFFF;
////	record->lon = 180.0 * posdata->LONG / 0xFFFFFFFF;
////	/* пиздят, как сивый мерин, присылаются в WGS84
////  if( posdata->FLG & B1 ){ // прислано в ПЗ-90.02, надо перевести в WGS-84 ибо Если координаты не трансформировать, то возникнет погрешность до 10 м
////		//Geo2Geo(PZ90, WGS84, &record->lon, &record->lat);
////  }	// if( posdata->FLG & B1 )
////	*/
////
////	record->valid = (posdata->FLG & B0);
////
////	if(posdata->FLG & B5)
////		record->clat = 'S';
////	else
////		record->clat = 'N';
////
////	if(posdata->FLG & B6)
////		record->clon = 'W';
////	else
////		record->clon = 'E';
////
////	// 14 младших бит, скорость в км/ч с дискретностью 0,1 км/ч
////	record->speed = (posdata->SPD & 16383) / 10;
////	// направление движения
////	// DIRH  :15		(Direction the Highest bit) старший бит (8) параметра DIR
////	record->curs = posdata->DIR;
////	if( posdata->SPD & 32768 )
////		record->curs = posdata->DIR + 256;
////
////	// пробег в км, с дискретностью 0,1 км
////	tpp = &posdata->ODM;
////	record->probeg = *(uint16_t *)tpp;
////	if(posdata->ODM[2])
////		record->probeg += (((unsigned int)posdata->ODM[2]) << 16);
////	record->probeg *= 100;	// 0,1 км -> м.
////
////	// состояние основных дискретных входов 1 ... 8
////	record->ainputs[0] = (posdata->DIN & B0);
////	record->ainputs[1] = (posdata->DIN & B1);
////	record->ainputs[2] = (posdata->DIN & B2);
////	record->ainputs[3] = (posdata->DIN & B3);
////	record->ainputs[4] = (posdata->DIN & B4);
////	record->ainputs[5] = (posdata->DIN & B5);
////	record->ainputs[6] = (posdata->DIN & B6);
////	record->ainputs[7] = (posdata->DIN & B7);
////
////	// высота над уровнем моря
////	if( posdata->FLG & B7 ){	// есть поле ALT
////		record->height = *(uint16_t *)&pc[sizeof(EGTS_SR_POS_DATA_RECORD)];
////		record->height += (((unsigned int)pc[sizeof(EGTS_SR_POS_DATA_RECORD) + 2]) << 16);
////
////		if( posdata->SPD & B14 )
////			record->height *= -1;
////	}
////
////	snprintf(record->imei, SIZE_TRACKER_FIELD, "%s", answer->lastpoint.imei);
////	snprintf(record->tracker, SIZE_TRACKER_FIELD, "EGTS");
////	//snprintf(record->hard, SIZE_TRACKER_FIELD, "%d", iHard);
////	snprintf(record->soft, SIZE_TRACKER_FIELD, "%f", 1.6);
////
////	return 1;
//	return 0;
//}
////------------------------------------------------------------------------------
//
//// Используется абонентским терминалом при передаче дополнительных данных
//int Parse_EGTS_SR_EXT_POS_DATA(EGTS_SR_EXT_POS_DATA_RECORD *posdata, ST_RECORD *record){
//	char *pc = (char *)posdata;
//	int data_size = sizeof(EGTS_SR_EXT_POS_DATA_RECORD);
//
//	if( !record )
//		return 0;
//
//	if( posdata->FLG & B0 ){	// VDOP Field Exists
//		data_size += sizeof(uint16_t);
//	}
//
//	if( posdata->FLG & B1 ){	// HDOP Field Exists
//		record->hdop = Round(0.1 * (*(uint16_t *)&pc[data_size]), 0);
//		data_size += sizeof(uint16_t);
//	}
//
//	if( posdata->FLG & B2 ){	// PDOP Field Exists
//		data_size += sizeof(uint16_t);
//	}
//
//	if( posdata->FLG & B3 ){	// Satellites Field Exists
//		record->satellites = *(uint8_t *)&pc[data_size];
//		data_size += sizeof(uint8_t);
//	}
//
//	if( posdata->FLG & B4 ){	// Navigation System Field Exists
//		data_size += sizeof(uint16_t);
//	}
//
//	return data_size;
//}
////------------------------------------------------------------------------------
//
//// Используется абонентским терминалом при передаче данных датчиков уровня жидкости
//// rlen - длинна данной записи в байтах
//int Parse_EGTS_SR_LIQUID_LEVEL_SENSOR(int rlen, EGTS_SR_LIQUID_LEVEL_SENSOR_RECORD *posdata, ST_RECORD *record){
//	int data_size;
//
//	if( !record )
//		return 0;
//
//	if( posdata->FLG & B3 ){	// размер поля LLSD определяется исходя из общей длины данной подзаписи и размеров расположенных перед LLSD полей
//   	data_size = rlen;	// здесь нас интересует общая длинна записи
//		// ибо как хранить такие данные хз
//	}
//	else {	// поле LLSD имеет размер 4 байта
//   	data_size = sizeof(EGTS_SR_LIQUID_LEVEL_SENSOR_RECORD);
//
//		if( !(posdata->FLG & B6) ){	// ошибок не обнаружено
//	  	if( record->fuel[0] ){	// показания первого датчика уже записаны
//				record->fuel[1] = posdata->LLSD;
//				if( posdata->FLG & 32 )	// показания ДУЖ в литрах с дискретностью в 0,1 литра
//					record->fuel[1] = 0.1 * posdata->LLSD;
//			}
//			else {
//				record->fuel[0] = posdata->LLSD;
//				if( posdata->FLG & 32 )	// показания ДУЖ в литрах с дискретностью в 0,1 литра
//					record->fuel[0] = 0.1 * posdata->LLSD;
//			}
//		}	// if( !(posdata->FLG & B6) )
//	}
//
//	return data_size;
//}
////------------------------------------------------------------------------------
//
//
///* обработка команды и ответ на команду
//EGTS\EGTS 1.6\RUS\protocol_EGTS_services_v.1.6_p1_RUS.pdf
//6.3 EGTS_COMMANDS_SERVICE
//*/
//int Parse_EGTS_SR_COMMAND_DATA(ST_ANSWER *answer, EGTS_SR_COMMAND_DATA_RECORD *record){
//	EGTS_SR_COMMAND_DATA_FIELD *CD;
//	uint8_t	*ACL;
//
//	if( record->CT_CCT & 80 ){	// CT_COM - команда для выполнения на АТ
//
//    // calculate address off field CD
//		CD = (EGTS_SR_COMMAND_DATA_FIELD *)(&record->ACFE + sizeof(uint8_t));
//
//		if( record->ACFE & 1 )	// поле CHS присутствует в подзаписи
//			CD += sizeof(uint8_t);
//		if( record->ACFE & 2 ){	// поля ACL и AC присутствуют в подзаписи
//			ACL = (uint8_t *)CD;
//			CD += (sizeof(uint8_t) + (*ACL));
//		}
//
//		// check field CD
//		if( CD->SZ_ACT & 1 ){	// запрос значения
//			// http://www.zakonprost.ru/content/base/part/1038461
//			// http://docs.cntd.ru/document/1200119664
//
//     	// сформировать подтверждение в виде подзаписи EGTS_SR_COMMAND_DATA сервиса EGTS_COMMAND_SERVICE
//			answer->size += responce_add_subrecord_EGTS_SR_COMMAND_DATA(answer->answer, answer->size, record);
//
//			switch( CD->CCD ){	// Запрашиваемый параметр определяется кодом из поля CCD
//			case EGTS_FLEET_GET_DOUT_DATA:	// Команда запроса состояния дискретных выходов
//      case EGTS_FLEET_GET_POS_DATA:	// Команда запроса текущих данных местоположения
//			case EGTS_FLEET_GET_SENSORS_DATA:	// Команда запроса состояния дискретных и аналоговых входов
//			case EGTS_FLEET_GET_LIN_DATA:	// Команда запроса состояния шлейфовых входов
//			case EGTS_FLEET_GET_CIN_DATA:	// Команда запроса состояния счетных входов
//			case EGTS_FLEET_GET_STATE:	// Команда запроса состояния абонентского терминала
//				/* При получении данной команды помимо подтверждения в виде
//				подзаписи EGTS_SR_COMMAND_DATA сервиса EGTS_COMMAND_SERVICE
//					абонентский терминал отправляет телематическое сообщение, содержащее
//				подзапись EGTS_SR_POS_DATA сервиса EGRS_TELEDATA_SERVICE
//				*/
//				return 1;
//			}	// switch( CD->CCD )
//
//		}	// if( CD->SZ_ACT & 1 )
//
//	}	// if( record->CT_CCT & 80 )
//
//	return 0;
//}
////------------------------------------------------------------------------------
//
//
///* добавление в ответ позаписи EGTS_SR_COMMAND_DATA_RECORD с ответом на команду
//pointer - размер уже сформированного ответа (конец массива байт)
//cmdrec - указатель на пришедшую запись EGTS_SR_COMMAND_DATA_RECORD, на которую отвечаем
//*/
//int responce_add_subrecord_EGTS_SR_COMMAND_DATA(char *buffer, int pointer, EGTS_SR_COMMAND_DATA_RECORD *cmdrec){
//	EGTS_SR_COMMAND_DATA_RECORD *cmdresponse;	// формируемая запись
//	EGTS_SR_COMMAND_DATA_FIELD *CD, *cmdcd;
//	uint8_t	*ACL;
//
//	cmdcd = (EGTS_SR_COMMAND_DATA_FIELD *)(&cmdrec->ACFE + sizeof(uint8_t));
//	if( cmdrec->ACFE & 1 )	// поле CHS присутствует в подзаписи
//		cmdcd += sizeof(uint8_t);
//	if( cmdrec->ACFE & 2 ){	// поля ACL и AC присутствуют в подзаписи
//		ACL = (uint8_t *)cmdcd;
//		cmdcd += (sizeof(uint8_t) + (*ACL));
//	}
//
//	cmdresponse = (EGTS_SR_COMMAND_DATA_RECORD *)&buffer[pointer];
//	cmdresponse->CT_CCT = 16;	// CT_COMCONF + CC_OK
//	cmdresponse->CID = cmdrec->CID;
//	cmdresponse->SID = cmdrec->SID;
//	cmdresponse->ACFE = 0;	// поля ACL и AC отсутствуют в подзаписи + поле CHS отсутствует в подзаписи
//	CD = (EGTS_SR_COMMAND_DATA_FIELD *)(&cmdresponse->ACFE + sizeof(uint8_t));
//	CD->ADR = cmdcd->ADR;
//	CD->SZ_ACT = 0;
//	CD->CCD = cmdcd->CCD;
//
//	return sizeof(EGTS_SR_COMMAND_DATA_RECORD);
//}
////------------------------------------------------------------------------------



/* добавление в егтс пакет записи EGTS_RECORD_HEADER (SDR)
packet - указатель на буфер формирования пакета
position - позиция в буфере, куда вставляется запись, фактически это длинна заполненной части буфера
sst - идентификатор тип Сервиса-отправителя
rst - идентификатор тип Сервиса-получателя
возвращает новый размер данных в буфере
*/
int packet_add_record_header(uint8_t *packet, int position, uint8_t sst, uint8_t rst, time_t _time){
	EGTS_PACKET_HEADER *packet_header = (EGTS_PACKET_HEADER *)packet;
	EGTS_RECORD_HEADER *record_header = (EGTS_RECORD_HEADER *)&packet[position];
	static unsigned short int RecordNumber = 0;	// record number for EGTS_RECORD_HEADER
	int new_size, rh_size;
	uint8_t *psst, *prst, rfl;
	uint32_t *poid, *pevid, *ptm;

	rh_size = sizeof(uint16_t) + sizeof(uint16_t) + sizeof(uint8_t);	// первые обязательные поля
	new_size = position + rh_size;
 	new_size += sizeof(uint8_t);	// SST
 	new_size += sizeof(uint8_t);	// RST

	rfl = B7 + B4 + B3 + B2;	// битовые флаги (запись сформирована в абонентском терминале + приоритет низкий + есть поле TM)

	if( rfl & B0 )	// наличие в данном пакете поля OID
		new_size += sizeof(uint32_t);

	if( rfl & B1 )	// наличие в данном пакете поля EVID
		new_size += sizeof(uint32_t);

	if( rfl & B2 )	// наличие в данном пакете поля TM
		new_size += sizeof(uint32_t);

	record_header->RL = 0;		// размер данных из поля RD
	record_header->RN = RecordNumber++;		// номер записи от 0 до 65535
	record_header->RFL = rfl;

	// необязательные поля
	if( rfl & B0 ){	// наличие в данном пакете поля OID
		poid = (uint32_t *)&packet[position + rh_size];
		rh_size += sizeof(uint32_t);
	}
	else
		poid = NULL;

	if( rfl & B1 ){	// наличие в данном пакете поля EVID
		pevid = (uint32_t *)&packet[position + rh_size];
		rh_size += sizeof(uint32_t);
	}
	else
		pevid = NULL;

	if( rfl & B2 ){	// наличие в данном пакете поля TM
		ptm = (uint32_t *)&packet[position + rh_size];
		rh_size += sizeof(uint32_t);
	}
	else
		ptm = NULL;

	// идентификатор объекта, сгенерировавшего данную запись
	if( poid )
		*poid = 50;	// чисто для прикола
	// уникальный идентификатор события
	if( pevid )
		*pevid = 0;
	// время формирования записи на стороне Отправителя
	if( ptm ){
		*ptm = (uint32_t)(_time - UTS2010);
	}

	// снова обязательные поля
	psst = (uint8_t *)&packet[position + rh_size];
	*psst = sst;	// идентификатор тип Сервиса-отправителя, сгенерировавшего данную запись (=EGTS_TELEDATA_SERVICE)
	rh_size += sizeof(uint8_t);

	prst = (uint8_t *)&packet[position + rh_size];
	*prst = rst;	// идентификатор тип Сервиса-получателя данной записи (=EGTS_TELEDATA_SERVICE)
	rh_size += sizeof(uint8_t);

	packet_header->FDL += rh_size;

	return new_size;
}
//------------------------------------------------------------------------------

/* добавление в егтс пакет записи EGTS_SUBRECORD_HEADER (SRD)
*/
int packet_add_subrecord_header(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, uint8_t srt){
	EGTS_PACKET_HEADER *packet_header = (EGTS_PACKET_HEADER *)packet;
	EGTS_SUBRECORD_HEADER *subrecord_header = (EGTS_SUBRECORD_HEADER *)&packet[position];
	int new_size = position + sizeof(EGTS_SUBRECORD_HEADER);

	packet_header->FDL += sizeof(EGTS_SUBRECORD_HEADER);
	record_header->RL += sizeof(EGTS_SUBRECORD_HEADER);
	subrecord_header->SRT = srt;

	return new_size;
}
//------------------------------------------------------------------------------

/* добавление в пакет поздаписи EGTS_SR_TERM_IDENTITY
с данными идентификации терминала
*/
int packet_add_subrecord_EGTS_SR_TERM_IDENTITY(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, unsigned int tid, char *imei){
	EGTS_PACKET_HEADER *packet_header = (EGTS_PACKET_HEADER *)packet;
	//                       TID & FLG(B1 + B6)                IMEI              BS
	int rec_size = sizeof(EGTS_SR_TERM_IDENTITY_RECORD) + EGTS_IMEI_LEN + sizeof(uint16_t);
	int new_size = position + rec_size;
	char *pimei;
	EGTS_SR_TERM_IDENTITY_RECORD *record;
	uint16_t *bs;

	packet_header->FDL += rec_size;
	record_header->RL += rec_size;
	subrecord_header->SRL = rec_size;

	record = (EGTS_SR_TERM_IDENTITY_RECORD *)&packet[position];

	// уникальный идентификатор, назначаемый при программировании АТ
	record->TID = tid;
	// Flags
	record->FLG = B1 + B6;	// наличие поля IMEI + наличие поля BS в подзаписи

	// imei
	pimei = (char *)&packet[position + sizeof(EGTS_SR_TERM_IDENTITY_RECORD)];
	memset(pimei, 0, EGTS_IMEI_LEN);
	snprintf(pimei, EGTS_IMEI_LEN+1, "%s", imei);	// EGTS_IMEI_LEN=15, SIZE_TRACKER_FIELD=16

	// BS
	bs = (uint16_t *)&packet[position + sizeof(EGTS_SR_TERM_IDENTITY_RECORD) + EGTS_IMEI_LEN];
	*bs = 2048;

	return new_size;
}
//------------------------------------------------------------------------------

/* добавление в пакет поздаписи EGTS_SR_MODULE_DATA
с данными идентификации терминала
*/
int packet_add_subrecord_EGTS_SR_MODULE_DATA(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header){
	EGTS_PACKET_HEADER *packet_header = (EGTS_PACKET_HEADER *)packet;
	int rec_size = sizeof(EGTS_SR_MODULE_DATA_RECORD);
	int new_size = position + rec_size;
	EGTS_SR_MODULE_DATA_RECORD *data;

	packet_header->FDL += rec_size;
	record_header->RL += rec_size;
	subrecord_header->SRL = rec_size;

	data = (EGTS_SR_MODULE_DATA_RECORD *)&packet[position];

	extern const uint8_t fw_version[];
	extern uint8_t hw_version[];

	data->MT = 1;			// Основной модуль
	data->VID = 321654987;	// Код производителя из головы
	data->FWV = ((uint16_t)hw_version[1] << 8) | (uint16_t)hw_version[0];	// Версия hw
	data->SWV = ((uint16_t)fw_version[1] << 8) | (uint16_t)fw_version[0];	// Версия fw
	data->MD = 1;	// Модификаци sw
	data->ST = 1;	// Включен
	data->D1 = 0;	// Разделитель
	data->D2 = 0;	// Разделитель

	return new_size;
}
//------------------------------------------------------------------------------

/* добавление в пакет поздаписи EGTS_SR_POS_DATA
с данными навигации
*/
int packet_add_subrecord_EGTS_SR_POS_DATA_RECORD(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, ST_RECORD *record){
	EGTS_PACKET_HEADER *packet_header = (EGTS_PACKET_HEADER *)packet;
	int rec_size = sizeof(EGTS_SR_POS_DATA_RECORD) + sizeof(uint8_t) * 3;	// высота над уровнем моря
	int new_size = position + rec_size;
	EGTS_SR_POS_DATA_RECORD *pos_data_rec;
	navigation_record_t *nav = &record->data.navigation;

	packet_header->FDL += rec_size;
	record_header->RL += rec_size;
	subrecord_header->SRL = rec_size;

	pos_data_rec = (EGTS_SR_POS_DATA_RECORD *)&packet[position];

	// time since 2010
	pos_data_rec->NTM = (uint32_t)(/*record->data + */record->head.time - UTS2010);

	pos_data_rec->LAT = (uint32_t)(fabs(nav->lattitude) / 90 * 0xFFFFFFFF);
	pos_data_rec->LONG = (uint32_t)(fabs(nav->longitude) / 180 * 0xFFFFFFFF);

	pos_data_rec->FLG = nav->valid ? B0 : 0;
	if (nav->fix == GNSS_FIX_3D) {
		pos_data_rec->FLG += B1;
	}
//	pos_data_rec->FLG += B2;	// WGS-84
	pos_data_rec->FLG += B3;	// Черный ящик
	if (nav->speed > 0) {
		pos_data_rec->FLG += B4;
	}
	if (nav->lattitude < 0) {
		pos_data_rec->FLG += B5;	// Южная широта
	}
	if (nav->longitude < 0) {
		pos_data_rec->FLG += B6;	// Западная долгота
	}
	pos_data_rec->FLG += B7;	// Высота (ALT) передается

	// 14 младших бит - скорость в км/ч с дискретностью 0,1 км/ч
	pos_data_rec->SPD = (uint16_t)(10 * nav->speed) & 0x3FFF;

	// Выше или ниже уровня моря
	if( nav->altitude < 0 )
		pos_data_rec->SPD |= 0x4000;

	// направление движения
	pos_data_rec->DIR = (uint8_t)(nav->course & 0xFF);
	// Старший бит направления (DIR(8))
	if( nav->course > 0xFF )
		pos_data_rec->SPD |= 0x8000;

	// пройденное расстояние (пробег) в км, с дискретностью 0,1 км
	uint32_t odm = (uint32_t)(nav->odometer / 100);
	pos_data_rec->ODM[0] = (uint8_t)(odm & 0xFF);
	pos_data_rec->ODM[1] = (uint8_t)((odm >> 8) & 0xFF);
	pos_data_rec->ODM[2] = (uint8_t)((odm >> 16) & 0xFF);

	// битовые флаги, определяют состояние основных дискретных входов 1 ... 8
	pos_data_rec->DIN = record->data.io.din._8bit[0];

	// источник (событие), инициировавший посылку данной навигационной информации
	pos_data_rec->SRC = (uint8_t)record->data.source;

	// Опциональные параметры

	// высота над уровнем моря, м
	uint32_t alt = (uint32_t)nav->altitude;
	uint8_t *puint8 = (uint8_t *)(&pos_data_rec->SRC + sizeof(uint8_t));
	puint8[0] = (uint8_t)(alt & 0xFF);
	puint8[1] = (uint8_t)((alt >> 8) & 0xFF);
	puint8[2] = (uint8_t)((alt >> 16) & 0xFF);

	return new_size;
}
//------------------------------------------------------------------------------

/* добавление в пакет поздаписи EGTS_SR_EXT_POS_DATA
с дополнительными данными навигации
*/
int packet_add_subrecord_EGTS_SR_EXT_POS_DATA_RECORD(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, ST_RECORD *record){
	EGTS_PACKET_HEADER *packet_header = (EGTS_PACKET_HEADER *)packet;
	int rec_size = sizeof(EGTS_SR_EXT_POS_DATA_RECORD) + sizeof(uint8_t) + sizeof(uint16_t);	// количество видимых спутников
	int new_size = position + rec_size;
	EGTS_SR_EXT_POS_DATA_RECORD *pos_data_rec;
	navigation_record_t *nav = &record->data.navigation;
	uint8_t *puint8_t;
	uint16_t *puint16_t;

	packet_header->FDL += rec_size;
	record_header->RL += rec_size;
	subrecord_header->SRL = rec_size;

	pos_data_rec = (EGTS_SR_EXT_POS_DATA_RECORD *)&packet[position];
	pos_data_rec->FLG = B1;		// определяет наличие поля HDOP
	pos_data_rec->FLG += B3;	// определяет наличие поля SAT

	// HDOP
	puint16_t = (uint16_t *)(&pos_data_rec->FLG + sizeof(uint8_t));
	*puint16_t = (uint16_t)(nav->hdop * 100);

	// Количество спутников
	puint8_t = (uint8_t *)(&pos_data_rec->FLG + sizeof(uint8_t) + sizeof(uint16_t));
	*puint8_t = nav->satellites_used;

	return new_size;
}
//------------------------------------------------------------------------------

/* добавление в пакет поздаписи EGTS_SR_AD_SENSORS_DATA
с дополнительными данными с входов и выходов
*/
int packet_add_subrecord_EGTS_SR_AD_SENSORS_DATA_RECORD(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, ST_RECORD *record, uint8_t num){
	EGTS_PACKET_HEADER *packet_header = (EGTS_PACKET_HEADER *)packet;
	EGTS_SR_AD_SENSORS_DATA_RECORD *pos_data_rec;
	int rec_size = sizeof(EGTS_SR_AD_SENSORS_DATA_RECORD);
	io_record_t *io = &record->data.io;

	pos_data_rec = (EGTS_SR_AD_SENSORS_DATA_RECORD *)&packet[position];

	if (num == 1) {
		rec_size += sizeof(uint8_t)*3*2 + sizeof(uint8_t);	// Два аналоговых входа по 3 байта + одинбайт din

		pos_data_rec->DIOE = B1;			// определяет наличие полей ADIO
		pos_data_rec->DOUT = io->dout;	// цифровые выходы
		pos_data_rec->ASFE = B0 + B1;	// определяет наличие полей ANS

		uint8_t *puint8 = (uint8_t *)(&pos_data_rec->ASFE + sizeof(uint8_t));

		// Дополнительные дискрентные входы
		*puint8++ = (uint8_t)(io->din._8bit[1] & 0xFF);

		// Аналоговые входы
		uint32_t ain = (uint32_t)(io->ain[0] * 100);
		*puint8++ = (uint8_t)(ain & 0xFF);
		*puint8++ = (uint8_t)((ain >> 8) & 0xFF);
		*puint8++ = (uint8_t)((ain >> 16) & 0xFF);

		ain = (uint32_t)(io->ain[1] * 100);
		*puint8++ = (uint8_t)(ain & 0xFF);
		*puint8++ = (uint8_t)((ain >> 8) & 0xFF);
		*puint8++ = (uint8_t)((ain >> 16) & 0xFF);
	}
	else {
		uint8_t *puint8 = (uint8_t *)(&pos_data_rec->ASFE + sizeof(uint8_t));

		pos_data_rec->DIOE = 0;
		pos_data_rec->DOUT = 0;
		pos_data_rec->ASFE = 0;

		for (int q = 0; q < 8; q++) {
			ow_temp_record_t * temp_rec = &record->data.ow.temp[q];

			if (temp_rec->used) {
				rec_size += sizeof(uint8_t)*3;
				pos_data_rec->ASFE |= (1 << q);

				int temp_val = (temp_rec->val != OW_TEMP_UNKNOWN_VALUE) ? ((int)temp_rec->val + CLOUD_TEMP_OFFSET) : 0;

				uint32_t ain = temp_val * 100;
				*puint8++ = (uint8_t)(ain & 0xFF);
				*puint8++ = (uint8_t)((ain >> 8) & 0xFF);
				*puint8++ = (uint8_t)((ain >> 16) & 0xFF);
			}
		}
	}


	int new_size = position + rec_size;

	packet_header->FDL += rec_size;
	record_header->RL += rec_size;
	subrecord_header->SRL = rec_size;

	return new_size;
}
//------------------------------------------------------------------------------

/* добавление в пакет поздаписи EGTS_SR_COUNTERS_DATA
с дополнительными данными с входов и выходов
*/
int packet_add_subrecord_EGTS_SR_COUNTERS_DATA_RECORD(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, ST_RECORD *record){
	EGTS_PACKET_HEADER *packet_header = (EGTS_PACKET_HEADER *)packet;
	int rec_size = sizeof(EGTS_SR_COUNTERS_DATA_RECORD) + sizeof(uint8_t)*3*4;
	int new_size = position + rec_size;
	EGTS_SR_COUNTERS_DATA_RECORD *pos_data_rec;
	io_record_t *io = &record->data.io;

	packet_header->FDL += rec_size;
	record_header->RL += rec_size;
	subrecord_header->SRL = rec_size;

	pos_data_rec = (EGTS_SR_COUNTERS_DATA_RECORD *)&packet[position];
	pos_data_rec->CFE = B0 + B1 + B2 + B3;		// определяет наличие счетных входов

	// Счетные входы
	uint8_t *puint8 = (uint8_t *)(&pos_data_rec->CFE + sizeof(uint8_t));

	uint32_t count = (uint32_t)io->count[0];
	*puint8++ = (uint8_t)(count & 0xFF);
	*puint8++ = (uint8_t)((count >> 8) & 0xFF);
	*puint8++ = (uint8_t)((count >> 16) & 0xFF);

	count = (uint32_t)io->count[1];
	*puint8++ = (uint8_t)(count & 0xFF);
	*puint8++ = (uint8_t)((count >> 8) & 0xFF);
	*puint8++ = (uint8_t)((count >> 16) & 0xFF);

	count = (uint32_t)io->count[2];
	*puint8++ = (uint8_t)(count & 0xFF);
	*puint8++ = (uint8_t)((count >> 8) & 0xFF);
	*puint8++ = (uint8_t)((count >> 16) & 0xFF);

	count = (uint32_t)io->count[3];
	*puint8++ = (uint8_t)(count & 0xFF);
	*puint8++ = (uint8_t)((count >> 8) & 0xFF);
	*puint8++ = (uint8_t)((count >> 16) & 0xFF);

	return new_size;
}
//------------------------------------------------------------------------------

/* добавление в пакет поздаписи EGTS_SR_ABS_CNTR_DATA
с дополнительными данными с входов и выходов
*/
int packet_add_subrecord_EGTS_SR_STATE_DATA_RECORD(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, ST_RECORD *record){
	EGTS_PACKET_HEADER *packet_header = (EGTS_PACKET_HEADER *)packet;
	int rec_size = sizeof(EGTS_SR_STATE_DATA_RECORD);
	int new_size = position + rec_size;
	EGTS_SR_STATE_DATA_RECORD *pos_data_rec;
	teledata_subrecord_t *data = &record->data;

	packet_header->FDL += rec_size;
	record_header->RL += rec_size;
	subrecord_header->SRL = rec_size;

	pos_data_rec = (EGTS_SR_STATE_DATA_RECORD *)&packet[position];
	pos_data_rec->ST = (uint8_t)data->mode;
	pos_data_rec->MPSV = (uint8_t)(data->power.main_voltage * 10);	// Бортовое напряжение
	pos_data_rec->BBV = 0;	// Нет резервного источника - напряжение 0
	pos_data_rec->IBV = (uint8_t)(data->power.batt_voltage * 10); // Напряжение внутренней батареи

	pos_data_rec->FLG = (data->power.batt_used & B0) + (((uint8_t)data->navigation.on << 2) & B2);

	return new_size;
}
//------------------------------------------------------------------------------

/* добавление в пакет поздаписи EGTS_SR_ABS_CNTR_DATA
с дополнительными данными с входов и выходов
*/
int packet_add_subrecord_EGTS_SR_ABS_CNTR_DATA_RECORD(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, ST_RECORD *record){
	EGTS_PACKET_HEADER *packet_header = (EGTS_PACKET_HEADER *)packet;
	int rec_size = sizeof(EGTS_SR_ABS_CNTR_DATA_RECORD) + sizeof(uint8_t)*4;
	int new_size = position + rec_size;
	EGTS_SR_ABS_CNTR_DATA_RECORD *pos_data_rec;

	packet_header->FDL += rec_size;
	record_header->RL += rec_size;
	subrecord_header->SRL = rec_size;

	pos_data_rec = (EGTS_SR_ABS_CNTR_DATA_RECORD *)&packet[position];
	pos_data_rec->CN = 0;	// номер счетного входа. Фикс 0

	uint32_t count = record->data.io.count[0];
	pos_data_rec->CNV[0] = (uint8_t)(count & 0xFF);
	pos_data_rec->CNV[1] = (uint8_t)((count >> 8) & 0xFF);
	pos_data_rec->CNV[2] = (uint8_t)((count >> 16) & 0xFF);

	return new_size;
}
//------------------------------------------------------------------------------


/* добавление в пакет поздаписи EGTS_SR_LIQUID_LEVEL_SENSOR
с дополнительными данными навигации
*/
int packet_add_subrecord_EGTS_SR_LIQUID_LEVEL_SENSOR_RECORD(uint8_t *packet, int position, EGTS_RECORD_HEADER *record_header, EGTS_SUBRECORD_HEADER *subrecord_header, uint32_t level, uint8_t seq_numb, uint8_t addr){
	EGTS_PACKET_HEADER *packet_header = (EGTS_PACKET_HEADER *)packet;
	int rec_size = sizeof(EGTS_SR_LIQUID_LEVEL_SENSOR_RECORD);
	int new_size = position + rec_size;
	EGTS_SR_LIQUID_LEVEL_SENSOR_RECORD *pos_data_rec;

	packet_header->FDL += rec_size;
	record_header->RL += rec_size;
	subrecord_header->SRL = rec_size;

	pos_data_rec = (EGTS_SR_LIQUID_LEVEL_SENSOR_RECORD *)&packet[position];

	/* поле LLSD имеет размер 4 байта (тип данных UINT) и содержит показания ДУЖ в формате,
	определяемом полем LLSVU: нетарированное показание ДУЖ
	*/
	pos_data_rec->FLG = seq_numb & 0x07;
	pos_data_rec->MADDR = addr;							// адрес модуля, данные о показаниях ДУЖ с которого поступили
	pos_data_rec->LLSD = level;		// показания ДУЖ

	return new_size;
}
//------------------------------------------------------------------------------
