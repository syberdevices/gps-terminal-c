/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stm32f4xx.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
osTimerId ftpJobTimerHandle;
/* USER CODE END Variables */
osThreadId uiTaskHandle;
osThreadId gsmTaskHandle;
osThreadId appTaskHandle;
osThreadId cloudTaskHandle;
osThreadId saveDataTaskHandle;
osThreadId sensorReadTaskHandle;
osThreadId monitorTaskHandle;
osMessageQId usbinQueueHandle;
osMessageQId usboutQueueHandle;
osMessageQId cloudTxQueueHandle;
osMessageQId cloudRxQueueHandle;
osMessageQId cloudTxRspQueueHandle;
osMessageQId saveDataSourceQueueHandle;
osTimerId gsmWakeupTimerHandle;
osTimerId cloudSendTimerHandle;
osTimerId adcTimerHandle;
osTimerId fuelTimerHandle;
osTimerId checkMotionTimerHandle;
osTimerId canlogTimerHandle;
osMutexId recordMutexHandle;
osMutexId fifoRecursiveMutexHandle;
osMutexId odometerRecursiveMutexHandle;
osMutexId spiflashRecursiveMutexHandle;
osSemaphoreId gsmWakeupSemHandle;
osSemaphoreId cloudExchangeSemHandle;
osSemaphoreId cloudSendSemHandle;
osSemaphoreId rtcSemHandle;
osSemaphoreId fuelSemHandle;
osSemaphoreId canlogSemHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
void ftpJobTimerCallback(void const * argument);
/* USER CODE END FunctionPrototypes */

void ui_task(void const * argument);
void gsm_task(void const * argument);
void app_task(void const * argument);
void cloud_task(void const * argument);
void save_data_task(void const * argument);
void sensor_read_task(void const * argument);
void monitor_task(void const * argument);
void gsmWakeupTimerCallback(void const * argument);
void cloudSendTimerCallback(void const * argument);
void adcTimerCallback(void const * argument);
void fuelTimerCallback(void const * argument);
void checkMotionTimerCallback(void const * argument);
void canlogTimerCallback(void const * argument);

extern void MX_USB_DEVICE_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);
void vApplicationIdleHook(void);
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{

}

__weak unsigned long getRunTimeCounterValue(void)
{
return 0;
}
/* USER CODE END 1 */

/* USER CODE BEGIN 2 */
__weak void vApplicationIdleHook( void )
{
   /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
   to 1 in FreeRTOSConfig.h. It will be called on each iteration of the idle
   task. It is essential that code added to this hook function never attempts
   to block in any way (for example, call xQueueReceive() with a block time
   specified, or call vTaskDelay()). If the application makes use of the
   vTaskDelete() API function (as this demo application does) then it is also
   important that vApplicationIdleHook() is permitted to return to its calling
   function, because it is the responsibility of the idle task to clean up
   memory allocated by the kernel to any task that has since been deleted. */

	HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
}
/* USER CODE END 2 */

/* USER CODE BEGIN 4 */
__weak void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
   /* Run time stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
   called if a stack overflow is detected. */

	Error_Handler();
}
/* USER CODE END 4 */

/* USER CODE BEGIN 5 */
__weak void vApplicationMallocFailedHook(void)
{
   /* vApplicationMallocFailedHook() will only be called if
   configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
   function that will get called if a call to pvPortMalloc() fails.
   pvPortMalloc() is called internally by the kernel whenever a task, queue,
   timer or semaphore is created. It is also called by various parts of the
   demo application. If heap_1.c or heap_2.c are used, then the size of the
   heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
   FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
   to query the size of free heap space that remains (although it does not
   provide information on how the remaining heap might be fragmented). */

	Error_Handler();
}
/* USER CODE END 5 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
       
  /* USER CODE END Init */

  /* Create the mutex(es) */
  /* definition and creation of recordMutex */
  osMutexDef(recordMutex);
  recordMutexHandle = osMutexCreate(osMutex(recordMutex));

  /* Create the recursive mutex(es) */
  /* definition and creation of fifoRecursiveMutex */
  osMutexDef(fifoRecursiveMutex);
  fifoRecursiveMutexHandle = osRecursiveMutexCreate(osMutex(fifoRecursiveMutex));

  /* definition and creation of odometerRecursiveMutex */
  osMutexDef(odometerRecursiveMutex);
  odometerRecursiveMutexHandle = osRecursiveMutexCreate(osMutex(odometerRecursiveMutex));

  /* definition and creation of spiflashRecursiveMutex */
  osMutexDef(spiflashRecursiveMutex);
  spiflashRecursiveMutexHandle = osRecursiveMutexCreate(osMutex(spiflashRecursiveMutex));

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of gsmWakeupSem */
  osSemaphoreDef(gsmWakeupSem);
  gsmWakeupSemHandle = osSemaphoreCreate(osSemaphore(gsmWakeupSem), 1);

  /* definition and creation of cloudExchangeSem */
  osSemaphoreDef(cloudExchangeSem);
  cloudExchangeSemHandle = osSemaphoreCreate(osSemaphore(cloudExchangeSem), 1);

  /* definition and creation of cloudSendSem */
  osSemaphoreDef(cloudSendSem);
  cloudSendSemHandle = osSemaphoreCreate(osSemaphore(cloudSendSem), 1);

  /* definition and creation of rtcSem */
  osSemaphoreDef(rtcSem);
  rtcSemHandle = osSemaphoreCreate(osSemaphore(rtcSem), 1);

  /* definition and creation of fuelSem */
  osSemaphoreDef(fuelSem);
  fuelSemHandle = osSemaphoreCreate(osSemaphore(fuelSem), 1);

  /* definition and creation of canlogSem */
  osSemaphoreDef(canlogSem);
  canlogSemHandle = osSemaphoreCreate(osSemaphore(canlogSem), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* definition and creation of gsmWakeupTimer */
  osTimerDef(gsmWakeupTimer, gsmWakeupTimerCallback);
  gsmWakeupTimerHandle = osTimerCreate(osTimer(gsmWakeupTimer), osTimerPeriodic, NULL);

  /* definition and creation of cloudSendTimer */
  osTimerDef(cloudSendTimer, cloudSendTimerCallback);
  cloudSendTimerHandle = osTimerCreate(osTimer(cloudSendTimer), osTimerPeriodic, NULL);

  /* definition and creation of adcTimer */
  osTimerDef(adcTimer, adcTimerCallback);
  adcTimerHandle = osTimerCreate(osTimer(adcTimer), osTimerPeriodic, NULL);

  /* definition and creation of fuelTimer */
  osTimerDef(fuelTimer, fuelTimerCallback);
  fuelTimerHandle = osTimerCreate(osTimer(fuelTimer), osTimerPeriodic, NULL);

  /* definition and creation of checkMotionTimer */
  osTimerDef(checkMotionTimer, checkMotionTimerCallback);
  checkMotionTimerHandle = osTimerCreate(osTimer(checkMotionTimer), osTimerPeriodic, NULL);

  /* definition and creation of canlogTimer */
  osTimerDef(canlogTimer, canlogTimerCallback);
  canlogTimerHandle = osTimerCreate(osTimer(canlogTimer), osTimerPeriodic, NULL);

  /* USER CODE BEGIN RTOS_TIMERS */
  osTimerDef(ftpJobTimer, ftpJobTimerCallback);
  ftpJobTimerHandle = osTimerCreate(osTimer(ftpJobTimer), osTimerPeriodic, NULL);
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of uiTask */
  osThreadDef(uiTask, ui_task, osPriorityNormal, 0, 1024);
  uiTaskHandle = osThreadCreate(osThread(uiTask), NULL);

  /* definition and creation of gsmTask */
  osThreadDef(gsmTask, gsm_task, osPriorityNormal, 0, 1024);
  gsmTaskHandle = osThreadCreate(osThread(gsmTask), NULL);

  /* definition and creation of appTask */
  osThreadDef(appTask, app_task, osPriorityNormal, 0, 1024);
  appTaskHandle = osThreadCreate(osThread(appTask), NULL);

  /* definition and creation of cloudTask */
  osThreadDef(cloudTask, cloud_task, osPriorityNormal, 0, 512);
  cloudTaskHandle = osThreadCreate(osThread(cloudTask), NULL);

  /* definition and creation of saveDataTask */
  osThreadDef(saveDataTask, save_data_task, osPriorityNormal, 0, 512);
  saveDataTaskHandle = osThreadCreate(osThread(saveDataTask), NULL);

  /* definition and creation of sensorReadTask */
  osThreadDef(sensorReadTask, sensor_read_task, osPriorityNormal, 0, 1024);
  sensorReadTaskHandle = osThreadCreate(osThread(sensorReadTask), NULL);

  /* definition and creation of monitorTask */
  osThreadDef(monitorTask, monitor_task, osPriorityAboveNormal, 0, 512);
  monitorTaskHandle = osThreadCreate(osThread(monitorTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Create the queue(s) */
  /* definition and creation of usbinQueue */
  osMessageQDef(usbinQueue, 2048, uint8_t);
  usbinQueueHandle = osMessageCreate(osMessageQ(usbinQueue), NULL);

  /* definition and creation of usboutQueue */
  osMessageQDef(usboutQueue, 2048, uint8_t);
  usboutQueueHandle = osMessageCreate(osMessageQ(usboutQueue), NULL);

  /* definition and creation of cloudTxQueue */
  osMessageQDef(cloudTxQueue, 1, uint32_t);
  cloudTxQueueHandle = osMessageCreate(osMessageQ(cloudTxQueue), NULL);

  /* definition and creation of cloudRxQueue */
  osMessageQDef(cloudRxQueue, 64, uint32_t);
  cloudRxQueueHandle = osMessageCreate(osMessageQ(cloudRxQueue), NULL);

  /* definition and creation of cloudTxRspQueue */
  osMessageQDef(cloudTxRspQueue, 1, uint8_t);
  cloudTxRspQueueHandle = osMessageCreate(osMessageQ(cloudTxRspQueue), NULL);

  /* definition and creation of saveDataSourceQueue */
  osMessageQDef(saveDataSourceQueue, 36, uint8_t);
  saveDataSourceQueueHandle = osMessageCreate(osMessageQ(saveDataSourceQueue), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* USER CODE BEGIN Header_ui_task */
/**
  * @brief  Function implementing the uiTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_ui_task */
__weak void ui_task(void const * argument)
{
  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();

  /* USER CODE BEGIN ui_task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END ui_task */
}

/* USER CODE BEGIN Header_gsm_task */
/**
* @brief Function implementing the gsmTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_gsm_task */
__weak void gsm_task(void const * argument)
{
  /* USER CODE BEGIN gsm_task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END gsm_task */
}

/* USER CODE BEGIN Header_app_task */
/**
* @brief Function implementing the appTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_app_task */
__weak void app_task(void const * argument)
{
  /* USER CODE BEGIN app_task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END app_task */
}

/* USER CODE BEGIN Header_cloud_task */
/**
* @brief Function implementing the cloudTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_cloud_task */
__weak void cloud_task(void const * argument)
{
  /* USER CODE BEGIN cloud_task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END cloud_task */
}

/* USER CODE BEGIN Header_save_data_task */
/**
* @brief Function implementing the saveDataTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_save_data_task */
__weak void save_data_task(void const * argument)
{
  /* USER CODE BEGIN save_data_task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END save_data_task */
}

/* USER CODE BEGIN Header_sensor_read_task */
/**
* @brief Function implementing the sensorReadTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_sensor_read_task */
__weak void sensor_read_task(void const * argument)
{
  /* USER CODE BEGIN sensor_read_task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END sensor_read_task */
}

/* USER CODE BEGIN Header_monitor_task */
/**
* @brief Function implementing the monitorTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_monitor_task */
__weak void monitor_task(void const * argument)
{
  /* USER CODE BEGIN monitor_task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END monitor_task */
}

/* gsmWakeupTimerCallback function */
__weak void gsmWakeupTimerCallback(void const * argument)
{
  /* USER CODE BEGIN gsmWakeupTimerCallback */
  
  /* USER CODE END gsmWakeupTimerCallback */
}

/* cloudSendTimerCallback function */
__weak void cloudSendTimerCallback(void const * argument)
{
  /* USER CODE BEGIN cloudSendTimerCallback */
  
  /* USER CODE END cloudSendTimerCallback */
}

/* adcTimerCallback function */
__weak void adcTimerCallback(void const * argument)
{
  /* USER CODE BEGIN adcTimerCallback */
  
  /* USER CODE END adcTimerCallback */
}

/* fuelTimerCallback function */
__weak void fuelTimerCallback(void const * argument)
{
  /* USER CODE BEGIN fuelTimerCallback */
  
  /* USER CODE END fuelTimerCallback */
}

/* checkMotionTimerCallback function */
__weak void checkMotionTimerCallback(void const * argument)
{
  /* USER CODE BEGIN checkMotionTimerCallback */
  
  /* USER CODE END checkMotionTimerCallback */
}

/* canlogTimerCallback function */
__weak void canlogTimerCallback(void const * argument)
{
  /* USER CODE BEGIN canlogTimerCallback */

  /* USER CODE END canlogTimerCallback */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
