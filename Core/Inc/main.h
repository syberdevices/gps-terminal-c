/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

//#ifdef DEBUG
//#define ZF_LOG_LEVEL		ZF_LOG_VERBOSE
//#else
//#define ZF_LOG_LEVEL		ZF_LOG_WARN
//#endif

#include "log/log.h"

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define GSM_PWR_Pin GPIO_PIN_13
#define GSM_PWR_GPIO_Port GPIOC
#define GSM_POWERKEY_Pin GPIO_PIN_0
#define GSM_POWERKEY_GPIO_Port GPIOC
#define GSM_DTR_Pin GPIO_PIN_1
#define GSM_DTR_GPIO_Port GPIOC
#define GSM_STATUS_Pin GPIO_PIN_2
#define GSM_STATUS_GPIO_Port GPIOC
#define GSM_RING_Pin GPIO_PIN_3
#define GSM_RING_GPIO_Port GPIOC
#define GSM_CTS_Pin GPIO_PIN_0
#define GSM_CTS_GPIO_Port GPIOA
#define GSM_RTS_Pin GPIO_PIN_1
#define GSM_RTS_GPIO_Port GPIOA
#define GSM_TX_Pin GPIO_PIN_2
#define GSM_TX_GPIO_Port GPIOA
#define GSM_RX_Pin GPIO_PIN_3
#define GSM_RX_GPIO_Port GPIOA
#define AIN1_Pin GPIO_PIN_4
#define AIN1_GPIO_Port GPIOA
#define AIN2_Pin GPIO_PIN_5
#define AIN2_GPIO_Port GPIOA
#define DIN_PHOTO_Pin GPIO_PIN_6
#define DIN_PHOTO_GPIO_Port GPIOA
#define DIN_PHOTO_EXTI_IRQn EXTI9_5_IRQn
#define AIN_UPWR_Pin GPIO_PIN_7
#define AIN_UPWR_GPIO_Port GPIOA
#define AIN_UBAT_Pin GPIO_PIN_4
#define AIN_UBAT_GPIO_Port GPIOC
#define DIN1_NO_Pin GPIO_PIN_5
#define DIN1_NO_GPIO_Port GPIOC
#define DIN1_NO_EXTI_IRQn EXTI9_5_IRQn
#define DIN2_NO_Pin GPIO_PIN_0
#define DIN2_NO_GPIO_Port GPIOB
#define DIN2_NO_EXTI_IRQn EXTI0_IRQn
#define DIN3_NC_Pin GPIO_PIN_1
#define DIN3_NC_GPIO_Port GPIOB
#define DIN3_NC_EXTI_IRQn EXTI1_IRQn
#define DIN4_NC_Pin GPIO_PIN_2
#define DIN4_NC_GPIO_Port GPIOB
#define DIN4_NC_EXTI_IRQn EXTI2_IRQn
#define ONE_WIRE_Pin GPIO_PIN_10
#define ONE_WIRE_GPIO_Port GPIOB
#define DOUT2_Pin GPIO_PIN_12
#define DOUT2_GPIO_Port GPIOB
#define DOUT1_Pin GPIO_PIN_13
#define DOUT1_GPIO_Port GPIOB
#define LED_GPS_GREEN_Pin GPIO_PIN_14
#define LED_GPS_GREEN_GPIO_Port GPIOB
#define LED_GPS_RED_Pin GPIO_PIN_15
#define LED_GPS_RED_GPIO_Port GPIOB
#define GPS_TX_Pin GPIO_PIN_6
#define GPS_TX_GPIO_Port GPIOC
#define GPS_RX_Pin GPIO_PIN_7
#define GPS_RX_GPIO_Port GPIOC
#define LED_GSM_GREEN_Pin GPIO_PIN_8
#define LED_GSM_GREEN_GPIO_Port GPIOC
#define LED_GSM_RED_Pin GPIO_PIN_9
#define LED_GSM_RED_GPIO_Port GPIOC
#define CHARGE_STATUS_Pin GPIO_PIN_8
#define CHARGE_STATUS_GPIO_Port GPIOA
#define CHARGE_STATUS_EXTI_IRQn EXTI9_5_IRQn
#define ACCEL_INT1_Pin GPIO_PIN_10
#define ACCEL_INT1_GPIO_Port GPIOA
#define ACCEL_INT1_EXTI_IRQn EXTI15_10_IRQn
#define GPS_RESET_Pin GPIO_PIN_15
#define GPS_RESET_GPIO_Port GPIOA
#define GPS_SHORT_Pin GPIO_PIN_10
#define GPS_SHORT_GPIO_Port GPIOC
#define HW_VERSION_Pin GPIO_PIN_11
#define HW_VERSION_GPIO_Port GPIOC
#define ACCEL_CS_Pin GPIO_PIN_12
#define ACCEL_CS_GPIO_Port GPIOC
#define FLASH_CS_Pin GPIO_PIN_2
#define FLASH_CS_GPIO_Port GPIOD
#define RS485_TX_Pin GPIO_PIN_6
#define RS485_TX_GPIO_Port GPIOB
#define RS485_RX_Pin GPIO_PIN_7
#define RS485_RX_GPIO_Port GPIOB
#define RS485_DE_Pin GPIO_PIN_8
#define RS485_DE_GPIO_Port GPIOB
#define CHARGE_DISABLE_Pin GPIO_PIN_9
#define CHARGE_DISABLE_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

#include "stdint.h"

// ============================================================================

typedef enum {
	TASK_APP	= 0x1,
	TASK_GSM	= 0x2,
//	TASK_UI		= 0x4,

	TASKS_NUM	= 2,

	TASK_ALL	= -1,
}Tasks_t;

// Макросы для управления отдельными битами
#define B(bit_no)         (1 << (bit_no))
#define CB(reg, bit_no)   (reg) &= ~B(bit_no)
#define SB(reg, bit_no)   (reg) |= B(bit_no)
#define VB(reg, bit_no)   ( (reg) & B(bit_no) )
#define TB(reg, bit_no)   (reg) ^= B(bit_no)


// ============================================================================

extern void Watchdog_Reset(uint32_t task_mask);

// =====================================================================

#ifdef DEBUG
#define BUILD_CONFIG	'd'
#else
#define BUILD_CONFIG	'r'
#endif

#define FW_VERSION		{3, 2, BUILD_CONFIG}	// {minor, major, build config}
#define HW_VERSION		{0, 2, 'v'}

// =====================================================================

#define USE_IWDG	1
#if USE_IWDG
#define WDG_FREEZE()					__HAL_DBGMCU_FREEZE_IWDG()
#define WDG_RESET(task)					Watchdog_Reset(task)
#else
#define MX_IWDG_Init()
#define WDG_FREEZE()
#define WDG_RESET(task)
#endif

// =====================================================================

#define SIGNAL_STRENGTH_LOW_PERCENT						15

#define DEFAULT_TIME							1539251588 // 11/10/2018

#define USER_INT_FLASH_ADDR						0x8040000
#define INT_FLASH_SIZE							0x80000
#define INT_FLASH_ADDR							0x80000000

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
