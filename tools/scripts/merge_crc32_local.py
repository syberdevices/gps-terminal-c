# Hello World program in Python
import binascii

fw_file = open('..\..\Release\gps-terminal-c.binary','rb')
bindata = fw_file.read()
fw_file.close()

crc = (binascii.crc32(bindata) & 0xFFFFFFFF)

new_file = open('..\..\!!!Releases\gps-terminal.bin','wb')
new_file.write(bindata)

hexcrc = binascii.a2b_hex("%08X" % crc)
new_file.write(hexcrc[3] + hexcrc[2] + hexcrc[1] + hexcrc[0])

new_file.close()

print ("CRC32: 0x%08X" % crc)