# Hello World program in Python
import binascii
    
print ("Merge CRC32 to binary file...")

fw_file = open('gps-terminal-c.binary','rb')
bindata = fw_file.read()
fw_file.close()

crc = (binascii.crc32(bindata) & 0xFFFFFFFF)

new_file = open('..\!!!Releases\gps-terminal.bin','wb')
new_file.write(bindata)

hexcrc = binascii.a2b_hex("%08X" % crc)
hexcrc_swap = bytearray(hexcrc)
hexcrc_swap.reverse()
hexcrc_bytes = bytes(hexcrc_swap)
hexcrc_bytes
new_file.write(hexcrc_bytes)

new_file.close()

print ("CRC32: 0x%08X" % crc)