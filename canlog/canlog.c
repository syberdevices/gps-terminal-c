/*
 * canlog.c
 *
 *  Created on: 3 февр. 2021 г.
 *      Author: chuyec
 */

#include "canlog.h"
#include "common.h"
#include <rs485/rs485.h>
#include <settings/settings.h>

#include <stm32f4xx_hal.h>

#include <stdbool.h>
#include <string.h>
#include <time.h>

// =================================================================================

#define BUFF_SIZE									128

#define CANLOG_DATA_TIMEOUT							10			// sec
#define CANLOG_CMD_TIMEOUT							200			// ms

#define CANLOG_MSG_HEADER							(0xC7)

#define CANLOG_CMD_RD_RPM							(0x0039)
#define CANLOG_CMD_RD_FUEL_PERCENT					(0x0037)
#define CANLOG_CMD_RD_FUEL_LITER					(0x0038)
#define CANLOG_CMD_RD_ODOMETER						(0x0033)
#define CANLOG_CMD_RD_TEMP							(0x003A)
#define CANLOG_CMD_RD_SECURITY_FLAGS				(0x1020)
#define CANLOG_CMD_RD_INDICATION_FLAGS				(0x1040)

// =================================================================================

typedef struct {
	time_t rpm;
	time_t fuel_percent;
	time_t fuel_liter;
	time_t odometer;
	time_t security_flags;
	time_t indication_flags;
	time_t temp;
} canlog_data_rx_time_t;

// =================================================================================

static uint8_t buff[BUFF_SIZE];
volatile static uint32_t rx_cnt = 0;		///< Количество данных в буфере
static bool wait_header = true;
static uint8_t expected_data_len = 0;

static canlog_data_union_t canlog_data_union;
static canlog_data_rx_time_t canlog_data_rx_time = {0};
#if CANLOG_USE_TX
static uint16_t expected_ans_cmd = 0;
volatile static bool answer_received = false;
#endif

const static unsigned char crctab[256] = {
	0x00, 0x5e, 0xbc, 0xe2, 0x61, 0x3f, 0xdd, 0x83, 0xc2, 0x9c, 0x7e, 0x20, 0xa3, 0xfd, 0x1f, 0x41,
	0x9d, 0xc3, 0x21, 0x7f, 0xfc, 0xa2, 0x40, 0x1e, 0x5f, 0x01, 0xe3, 0xbd, 0x3e, 0x60, 0x82, 0xdc,
	0x23, 0x7d, 0x9f, 0xc1, 0x42, 0x1c, 0xfe, 0xa0, 0xe1, 0xbf, 0x5d, 0x03, 0x80, 0xde, 0x3c, 0x62,
	0xbe, 0xe0, 0x02, 0x5c, 0xdf, 0x81, 0x63, 0x3d, 0x7c, 0x22, 0xc0, 0x9e, 0x1d, 0x43, 0xa1, 0xff,
	0x46, 0x18, 0xfa, 0xa4, 0x27, 0x79, 0x9b, 0xc5, 0x84, 0xda, 0x38, 0x66, 0xe5, 0xbb, 0x59, 0x07,
	0xdb, 0x85, 0x67, 0x39, 0xba, 0xe4, 0x06, 0x58, 0x19, 0x47, 0xa5, 0xfb, 0x78, 0x26, 0xc4, 0x9a,
	0x65, 0x3b, 0xd9, 0x87, 0x04, 0x5a, 0xb8, 0xe6, 0xa7, 0xf9, 0x1b, 0x45, 0xc6, 0x98, 0x7a, 0x24,
	0xf8, 0xa6, 0x44, 0x1a, 0x99, 0xc7, 0x25, 0x7b, 0x3a, 0x64, 0x86, 0xd8, 0x5b, 0x05, 0xe7, 0xb9,
	0x8c, 0xd2, 0x30, 0x6e, 0xed, 0xb3, 0x51, 0x0f, 0x4e, 0x10, 0xf2, 0xac, 0x2f, 0x71, 0x93, 0xcd,
	0x11, 0x4f, 0xad, 0xf3, 0x70, 0x2e, 0xcc, 0x92, 0xd3, 0x8d, 0x6f, 0x31, 0xb2, 0xec, 0x0e, 0x50,
	0xaf, 0xf1, 0x13, 0x4d, 0xce, 0x90, 0x72, 0x2c, 0x6d, 0x33, 0xd1, 0x8f, 0x0c, 0x52, 0xb0, 0xee,
	0x32, 0x6c, 0x8e, 0xd0, 0x53, 0x0d, 0xef, 0xb1, 0xf0, 0xae, 0x4c, 0x12, 0x91, 0xcf, 0x2d, 0x73,
	0xca, 0x94, 0x76, 0x28, 0xab, 0xf5, 0x17, 0x49, 0x08, 0x56, 0xb4, 0xea, 0x69, 0x37, 0xd5, 0x8b,
	0x57, 0x09, 0xeb, 0xb5, 0x36, 0x68, 0x8a, 0xd4, 0x95, 0xcb, 0x29, 0x77, 0xf4, 0xaa, 0x48, 0x16,
	0xe9, 0xb7, 0x55, 0x0b, 0x88, 0xd6, 0x34, 0x6a, 0x2b, 0x75, 0x97, 0xc9, 0x4a, 0x14, 0xf6, 0xa8,
	0x74, 0x2a, 0xc8, 0x96, 0x15, 0x4b, 0xa9, 0xf7, 0xb6, 0xe8, 0x0a, 0x54, 0xd7, 0x89, 0x6b, 0x35,
};

// =================================================================================

canlog_data_struct_t canlog_data = {0};

// =================================================================================

static unsigned char docrc(unsigned char crc, unsigned char data) {
	return crctab[crc ^ data]; //XORED Data with temporary CRC value
}

static uint8_t crc_calc(uint8_t * data, uint32_t len) {
	uint8_t crc = 0;

	for (int q = 0; q < len; q++) {
		crc = docrc(crc, data[q]);
	}

	return crc;
}

static float unpack_odometer(uint32_t val) {
	return 0.005f * val;
}

static float unpack_fuel_level_percent(uint16_t val) {
	return 0.1f * val;
}

static float unpack_fuel_level_liter(uint16_t val) {
	return 0.1f * val;
}

static float unpack_rpm(uint16_t val) {
	return 0.25f * val;
}

static int8_t unpack_engine_temp(uint8_t val) {
	return (int8_t)val - 60;
}

static void message_handler(canlog_msg_t * msg) {
	if (msg->len > sizeof(canlog_data_union.data)) {
		// Сдлишком маленький буфер для копирования сообщения длиной msg->len
		// в canlog_data.data
		return;
	}

	memcpy(canlog_data_union.data, msg->data, msg->len);

	time_t ts = time(NULL);

	// Критическая секция?

	switch (msg->cmd) {
		case CANLOG_CMD_RD_RPM:
			canlog_data.rpm = unpack_rpm(canlog_data_union.rpm);
			canlog_data_rx_time.rpm = ts;
			break;
		case CANLOG_CMD_RD_FUEL_PERCENT:
			canlog_data.fuel_percent = unpack_fuel_level_percent(canlog_data_union.fuel_percent);
			canlog_data_rx_time.fuel_percent = ts;
			break;
		case CANLOG_CMD_RD_FUEL_LITER:
			canlog_data.fuel_liter = unpack_fuel_level_liter(canlog_data_union.fuel_liter);
			canlog_data_rx_time.fuel_liter = ts;
			break;
		case CANLOG_CMD_RD_ODOMETER:
			canlog_data.odometer = unpack_odometer(canlog_data_union.odometer);
			canlog_data_rx_time.odometer = ts;
			break;
		case CANLOG_CMD_RD_TEMP:
			canlog_data.temp = unpack_engine_temp(canlog_data_union.temp);
			canlog_data_rx_time.temp = ts;
			break;
		case CANLOG_CMD_RD_SECURITY_FLAGS:
			memcpy(&canlog_data.security_flags, &canlog_data_union.security_flags, sizeof(canlog_security_flags_t));
			canlog_data_rx_time.security_flags = ts;
			break;
		case CANLOG_CMD_RD_INDICATION_FLAGS:
			memcpy(&canlog_data.indication_flags, &canlog_data_union.indication_flags, sizeof(canlog_indication_flags_t));
			canlog_data_rx_time.indication_flags = ts;
			break;
		default:
			break;
	}

#if CANLOG_USE_TX
	if (msg->cmd == expected_ans_cmd) {
		answer_received = true;
	}
#endif
}

// =================================================================================

void canlog_reset_state() {
	expected_data_len = 0;
	wait_header = true;
	rx_cnt = 0;

//	bzero(&canlog_data_rx_time, sizeof(canlog_data_rx_time));
}

void canlog_init() {
	canlog_reset_state();
}

void canlog_init_serial() {
	(void)rs485_rx_stop();

	canlog_reset_state();

	(void)rs485_set_baudrate(9600);
	rs485_rx_byte_only_enable(canlog_rx_byte_handler);
}

void canlog_rx_byte_handler(uint8_t byte) {
	if (wait_header) {
		if (byte == CANLOG_MSG_HEADER) {
			wait_header = false;
			rx_cnt = 0;
			buff[rx_cnt++] = byte;
		}

		return;
	}

	if (rx_cnt >= BUFF_SIZE) {
		// Поступаем жестко. Сбрасываем состояние
		canlog_reset_state();
		return;
	}

	buff[rx_cnt++] = byte;

	if (rx_cnt <= 3) {

	}
	else if (rx_cnt == 4) {
		expected_data_len = byte;
	}
	else if (expected_data_len) {
		expected_data_len--;
	}
	else {
		uint8_t crc = byte;
		if (crc == crc_calc(buff, rx_cnt - 1)) {
			canlog_msg_t msg = {
				.data = &buff[4],
				.len = buff[3],
				.cmd = (uint16_t)buff[1] | ((uint16_t)buff[2] << 8),
				.token = 0
			};

			message_handler(&msg);
		}

		canlog_reset_state();
	}
}

void canlog_data_reset() {
	bzero(&canlog_data, sizeof(canlog_data));
}

void canlog_data_get(canlog_data_struct_t * _canlog_data, can_used_param_t * p_ap) {
	time_t ts_minimum = time(NULL) - CANLOG_DATA_TIMEOUT;

	DISABLE_IRQ_USER();

	p_ap->all = 0;

	if (canlog_data_rx_time.rpm > ts_minimum) {
		p_ap->rpm = 1;
	}
	else {
		canlog_data.rpm = 0;
	}

	if (canlog_data_rx_time.fuel_percent > ts_minimum) {
		p_ap->fuel_level_percent = 1;
	}
	else {
		canlog_data.fuel_percent = 0;
	}

	if (canlog_data_rx_time.fuel_liter > ts_minimum) {
		p_ap->fuel_level_liter = 1;
	}
	else {
		canlog_data.fuel_liter = 0;
	}

	if (canlog_data_rx_time.odometer > ts_minimum) {
		p_ap->odometer = 1;
	}
	else {
		canlog_data.odometer = 0;
	}

	if (canlog_data_rx_time.security_flags > ts_minimum) {
		p_ap->ignition = 1;
	}
	else {
		canlog_data.security_flags.ignition = 0;
	}

	if (canlog_data_rx_time.indication_flags > ts_minimum) {
		p_ap->oil_level = 1;
	}
	else {
		canlog_data.indication_flags.oil_level = 0;
	}

	if (canlog_data_rx_time.temp > ts_minimum) {
		p_ap->engine_temp = 1;
	}
	else {
		canlog_data.temp = 0;
	}

	memcpy(_canlog_data, &canlog_data, sizeof(canlog_data_struct_t));

	ENABLE_IRQ_USER();
}

#if CANLOG_USE_TX

int canlog_cmd(canlog_msg_t *msg) {
	if (!msg) {
		return -1;
	}

	if (msg->len != 0 || msg->token != 0) {
		// Пока нет реализации
		return -1;
	}

	uint8_t _buff[8];
	int cnt = 0;

	_buff[cnt++] = CANLOG_MSG_HEADER;
	_buff[cnt++] = (uint8_t)(msg->cmd & 0x00FF);
	_buff[cnt++] = (uint8_t)((msg->cmd >> 8) & 0x00FF);
	_buff[cnt++] = msg->len;
	_buff[cnt++] = crc_calc(_buff, 4);

	answer_received = false;
	expected_ans_cmd = msg->cmd;

	int rc = rs485_tx(_buff, cnt, 100);
	if ( rc != 0) {
		return rc;
	}

	uint32_t start_time = HAL_GetTick();

	while ( !answer_received && ((HAL_GetTick() - start_time) < CANLOG_CMD_TIMEOUT) ) {

	}

	if (answer_received) {
		msg->val = &canlog_data_union;
		return 0;
	}
	else {
		return -1;
	}
}

int canlog_get_odometer_0033(float * odo) {
	canlog_msg_t msg = {
		.data = NULL,
		.val = NULL,
		.len = 0,
		.cmd = CANLOG_CMD_RD_ODOMETER,
		.token = 0
	};

	int rc = canlog_cmd(&msg);
	if (rc != 0) {
		return rc;
	}

	*odo = 0.005f * msg.val->odometer;

	return 0;
}

int canlog_get_fuel_level_percent_0037(float * fuel_level) {
	canlog_msg_t msg = {
		.data = NULL,
		.val = NULL,
		.len = 0,
		.cmd = CANLOG_CMD_RD_FUEL_PERCENT,
		.token = 0
	};

	int rc = canlog_cmd(&msg);
	if (rc != 0) {
		return rc;
	}

	*fuel_level = 0.1f * msg.val->fuel_percent;

	return 0;
}

int canlog_get_fuel_level_liter_0038(float * fuel_level) {
	canlog_msg_t msg = {
		.data = NULL,
		.val = NULL,
		.len = 0,
		.cmd = CANLOG_CMD_RD_FUEL_LITER,
		.token = 0
	};

	int rc = canlog_cmd(&msg);
	if (rc != 0) {
		return rc;
	}

	*fuel_level = 0.1f * msg.val->fuel_liter;

	return 0;
}

int canlog_get_rpm_0039(float * rpm) {
	canlog_msg_t msg = {
		.data = NULL,
		.val = NULL,
		.len = 0,
		.cmd = CANLOG_CMD_RD_RPM,
		.token = 0
	};

	int rc = canlog_cmd(&msg);
	if (rc != 0) {
		return rc;
	}

	*rpm = (float)msg.val->rpm / 4;

	return 0;
}

int canlog_get_engine_temp_003A(int8_t * temp) {
	canlog_msg_t msg = {
		.data = NULL,
		.val = NULL,
		.len = 0,
		.cmd = CANLOG_CMD_RD_TEMP,
		.token = 0
	};

	int rc = canlog_cmd(&msg);
	if (rc != 0) {
		return rc;
	}

	*temp = (int8_t)msg.val->temp - 60;

	return 0;
}

int canlog_get_security_flags_1020(canlog_security_flags_t * security_flags) {
	canlog_msg_t msg = {
		.data = NULL,
		.val = NULL,
		.len = 0,
		.cmd = CANLOG_CMD_RD_SECURITY_FLAGS,
		.token = 0
	};

	int rc = canlog_cmd(&msg);
	if (rc != 0) {
		return rc;
	}

	memcpy(security_flags, msg.val->security_flags, sizeof(canlog_security_flags_t));

	return 0;
}

int canlog_get_indication_flags_1040(canlog_indication_flags_t * indication_flags) {
	canlog_msg_t msg = {
		.data = NULL,
		.val = NULL,
		.len = 0,
		.cmd = CANLOG_CMD_RD_INDICATION_FLAGS,
		.token = 0
	};

	int rc = canlog_cmd(&msg);
	if (rc != 0) {
		return rc;
	}

	memcpy(indication_flags, msg.val->indication_flags, sizeof(canlog_indication_flags_t));

	return 0;
}

#endif // CANLOG_USE_TX
