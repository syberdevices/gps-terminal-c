/**
 * canlog.h
 *
 *  Created on: 3 февр. 2021 г.
 *      Author: chuyec
 *
 * @note Протестирована работа canlog с версией ПО SOFT: 2021-05-24 (v.1)
 * 			Другие версии софта могут работать некорректно
 */

#ifndef CANLOG_H_
#define CANLOG_H_

#include <settings/settings.h>

#include <stdint.h>

// ===============================================================================================

#define CANLOG_USE_TX			0

// ===============================================================================================

typedef int(*canlog_tx_t)(void * data, uint32_t len, uint32_t timeout);
typedef int(*canlog_rx_t)(void * data, uint32_t timeout);

typedef enum __attribute__((packed)) {
	CANLOG_SEC_FLAG_IGN_OFF,
	CANLOG_SEC_FLAG_IGN_ON,
	CANLOG_SEC_FLAG_IGN_WAIT,
	CANLOG_SEC_FLAG_IGN_UNKNOUN,
} canlog_security_flag_ignition_t;

typedef union {
	uint8_t data[13];

	struct {
		canlog_security_flag_ignition_t	ignition : 2;
		uint8_t							reserved : 2;
		/// \note Остальные биты пока не поддерживаем и не описываем
	} ;
} canlog_security_flags_t;

typedef enum __attribute__((packed)) {
	CANLOG_IND_FLAG_OIL_LEVEL_OFF,
	CANLOG_IND_FLAG_OIL_LEVEL_ON,
	CANLOG_IND_FLAG_OIL_LEVEL_WAIT,
	CANLOG_IND_FLAG_OIL_LEVEL_UNKNOUN,
} canlog_indication_flag_oil_level_t;

typedef union {
	uint8_t data[9];

	struct {
		uint8_t								reserved1 : 8;	// не описано

		uint8_t								reserved : 2;	// не описано
		canlog_indication_flag_oil_level_t	oil_level : 2;
		/// \note Остальные биты пока не поддерживаем и не описываем
	} ;
} canlog_indication_flags_t;

typedef struct {
	float rpm;
	float fuel_percent;
	float fuel_liter;
	float odometer;
	canlog_security_flags_t security_flags;
	canlog_indication_flags_t indication_flags;
	int8_t temp;
} canlog_data_struct_t;

typedef union {
	uint16_t rpm;									// 2
	uint16_t fuel_percent;							// 2
	uint16_t fuel_liter;							// 2
	uint32_t odometer;								// 4
	canlog_security_flags_t security_flags;			// 13        <--|
	canlog_indication_flags_t indication_flags;		// 9		    |
	uint8_t temp;									// 1
													//			    |
	uint8_t data[24];								// 13 минимум --|
} canlog_data_union_t;

typedef struct {
	uint8_t * data;
	canlog_data_union_t * val;
	uint16_t cmd;
	uint16_t token;
	uint8_t len;
} canlog_msg_t;

// ===============================================================================================

extern canlog_data_struct_t canlog_data;

// ===============================================================================================

void canlog_reset_state();
void canlog_init();
void canlog_init_serial();
void canlog_rx_byte_handler(uint8_t byte);
void canlog_data_reset();
void canlog_data_get(canlog_data_struct_t * _canlog_data, can_used_param_t * p_ap);

#if CANLOG_USE_TX

int canlog_cmd(canlog_msg_t *msg);

int canlog_get_odometer_0033(float * odo);
int canlog_get_fuel_level_percent_0037(float * fuel_level);
int canlog_get_fuel_level_liter_0038(float * fuel_level);
int canlog_get_rpm_0039(float * rpm);
int canlog_get_engine_temp_003A(int8_t * temp);
int canlog_get_security_flags_1020(canlog_security_flags_t * security_flags);
int canlog_get_indication_flags_1040(canlog_indication_flags_t * indication_flags);

#endif // CANLOG_USE_TX


#endif /* CANLOG_H_ */
