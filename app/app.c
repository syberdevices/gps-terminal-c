/*
 * data_collector.c
 *
 *  Created on: 25 сент. 2018 г.
 *      Author: Denis Shreiber
 */

// C includes
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>
// HAL includes
#include "rtos.h"
#include "gpio.h"
#include "usart.h"
#include "rtc.h"
#include "tim.h"
#include "iwdg.h"
#include "tim.h"
#include "usbd_cdc_if.h"
#include "usb_device.h"
#include "adc.h"
#include "main.h"
// User includes
#include "lis3dh/lis3dh.h"
#include "spi_flash/spi_flash.h"
#include "dout/dout.h"
#include "settings/settings.h"
#include "sim800/sim800.h"
#include "sim68/sim68.h"
#include "log/flash_fifo/flash_fifo.h"
#include "log/log.h"
#include "arm_math.h"
#include "din/din.h"
#include "common.h"
#include "canlog/canlog.h"
#include "rs485/rs485.h"
#include "canlog/canlog.h"
//#include "defines.h"
#include "tm_stm32_delay.h"
#include "tm_stm32f4_onewire.h"
#include "tm_stm32f4_ds18b20.h"

//=========================================================================

#define CAR_BATTERY_VOLTAGE_TYPE_THRESHOLD_MV				16000

// Гистерезис на пульсации напряжения для определения зажигания [mV]
#define IGNITION_VOLTAGE_RIPPLE_12V_THRESHOLD_LOW			200
#define IGNITION_VOLTAGE_RIPPLE_12V_THRESHOLD_HIGH			350
#define IGNITION_VOLTAGE_RIPPLE_24V_THRESHOLD_LOW			200
#define IGNITION_VOLTAGE_RIPPLE_24V_THRESHOLD_HIGH			350

#define IGNITION_BAT_VOLTAGE_12V_HYSTERESIS_LOW				12900
#define IGNITION_BAT_VOLTAGE_12V_HYSTERESIS_HIGH			13300
#define IGNITION_BAT_VOLTAGE_24V_HYSTERESIS_LOW				26900
#define IGNITION_BAT_VOLTAGE_24V_HYSTERESIS_HIGH			27100

#define SHUTDOWN_TIMEOUT								15

typedef enum {
	CAR_VOLTAGE_12V,
	CAR_VOLTAGE_24V,
} CarVoltageType_t;

//=========================================================================

extern volatile uint32_t din_sos_event;
extern volatile uint32_t din_write_event;
extern volatile uint32_t din_ignition_event;

extern void record_io_update();

//=========================================================================

const uint8_t fw_version[3] = FW_VERSION;
uint8_t hw_version[3] = HW_VERSION;
bool gsm_enable = true;
bool enableShutdown = true;
bool enableSleepMode;
track_t track_global;
bool coordinates_freezed = true;

//=========================================================================

static const char * rstReason;
static sim68_gnss_data_t gnss_data;
static float current_freezing_speed = 0;

float last_point_course = 0;
float current_course = 0;
uint32_t save_track_data_timer_sec_cnt = 0;

//=========================================================================

#if USE_IWDG
static uint32_t wdg_mask = (uint32_t)-1 << TASKS_NUM;

void Watchdog_Reset(uint32_t task_mask) {
	wdg_mask |= task_mask;

	if (wdg_mask == (uint32_t)-1) {
		wdg_mask <<= TASKS_NUM;
		HAL_IWDG_Refresh(&hiwdg);
	}
}
#endif

//=========================================================================

void vApplicationIdleHook( void )
{
	HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
}

void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
   /* Run time stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
   called if a stack overflow is detected. */

	CDC_Transmit_FS((uint8_t*)"Stack overflow", strlen("Stack overflow"));

#ifdef DEBUG
	if (CoreDebug->DHCSR & 1)  {
		__BKPT (0);
	}
#endif
	system_reset("StackOverflow");
}

void vApplicationMallocFailedHook(void)
{
   /* vApplicationMallocFailedHook() will only be called if
   configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
   function that will get called if a call to pvPortMalloc() fails.
   pvPortMalloc() is called internally by the kernel whenever a task, queue,
   timer or semaphore is created. It is also called by various parts of the
   demo application. If heap_1.c or heap_2.c are used, then the size of the
   heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
   FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
   to query the size of free heap space that remains (although it does not
   provide information on how the remaining heap might be fragmented). */

	CDC_Transmit_FS((uint8_t*)"Malloc Failed", strlen("Malloc Failed"));

#ifdef DEBUG
	if (CoreDebug->DHCSR & 1)  {
		__BKPT (0);
	}
#endif
	system_reset("MallocFailed");
}

#if configGENERATE_RUN_TIME_STATS && 0
volatile static unsigned long run_time_counter = 0;

void increment_run_time_counter() {
	run_time_counter++;
}

/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
void configureTimerForRunTimeStats(void) {
	HAL_TIM_Base_Start_IT(&htim4);

    HAL_NVIC_SetPriority(TIM4_IRQn, 4, 0);
    HAL_NVIC_EnableIRQ(TIM4_IRQn);
}

unsigned long getRunTimeCounterValue(void) {
	return run_time_counter;
}

void TIM4_IRQHandler() {
	HAL_TIM_IRQHandler(&htim4);
}
#else
void increment_run_time_counter() {
}
#endif

//=========================================================================

void rtc_write(time_t t) //-V795
{
//	osSemaphoreWait(rtcSemHandle, osWaitForever);

	RTC_DateTypeDef dateStruct;
	RTC_TimeTypeDef timeStruct;

	// Convert the time into a tm
	struct tm *timeinfo = localtime(&t);

	dateStruct.WeekDay        = timeinfo->tm_wday + 1;
	dateStruct.Month          = timeinfo->tm_mon + 1;
	dateStruct.Date           = timeinfo->tm_mday;
	dateStruct.Year           = timeinfo->tm_year - 100;
	timeStruct.Hours          = timeinfo->tm_hour;
	timeStruct.Minutes        = timeinfo->tm_min;
	timeStruct.Seconds        = timeinfo->tm_sec;
	timeStruct.TimeFormat     = RTC_HOURFORMAT12_PM;
	timeStruct.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	timeStruct.StoreOperation = RTC_STOREOPERATION_RESET;

	HAL_RTC_SetDate(&hrtc, &dateStruct, RTC_FORMAT_BIN);
	HAL_RTC_SetTime(&hrtc, &timeStruct, RTC_FORMAT_BIN);

//	osSemaphoreRelease(rtcSemHandle);
}

time_t rtc_read(void) //-V795
{
//	osSemaphoreWait(rtcSemHandle, osWaitForever);

	RTC_DateTypeDef dateStruct;
	RTC_TimeTypeDef timeStruct;
	struct tm timeinfo;

	// Read actual date and time
	// Warning: the time must be read first!
	HAL_RTC_GetTime(&hrtc, &timeStruct, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &dateStruct, RTC_FORMAT_BIN);

	timeinfo.tm_wday = dateStruct.WeekDay;
	timeinfo.tm_mon  = dateStruct.Month - 1;
	timeinfo.tm_mday = dateStruct.Date;
	timeinfo.tm_year = dateStruct.Year + 100;
	timeinfo.tm_hour = timeStruct.Hours;
	timeinfo.tm_min  = timeStruct.Minutes;
	timeinfo.tm_sec  = timeStruct.Seconds;

	time_t t = mktime(&timeinfo); //-V795

//	osSemaphoreRelease(rtcSemHandle);

	return t;
}

// ==========================================================

static const char* get_reset_reason() {
	char * p_reason = NULL;

	if (__HAL_RCC_GET_FLAG(RCC_FLAG_PORRST)) {
		p_reason = "PORRST";
		rtc_write(DEFAULT_TIME);
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_BORRST)) {
		p_reason = "BORRST";
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST)) {
		p_reason = "SFTRST";
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_LPWRRST)) {
		p_reason = "LPWRRST";
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST)) {
		p_reason = "WWDGRST";
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST)) {
		p_reason = "IWDGRST";
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_PINRST)) {
		p_reason = "PINRST";
	}
	else {
		p_reason = "UNKNOWNRST";
	}

	__HAL_RCC_CLEAR_RESET_FLAGS();

	return p_reason;
}

static void accelerometer_init(uint8_t sensitivity) {
	if (accelID()){
		board_status.accel.phy = 1;
		lis3dhConfig();
		lis3dh_set_int1_threshold(sensitivity);
	}
}

static bool spi_flash_init() {
	SPI_Flash_WAKEUP();

	uint32_t FlashID = 0;
	if (SPI_FLASH_ReadID(&FlashID) == HAL_OK && FlashID != 0 && FlashID != 0xFFFFFF) {
		board_status.spi_flash.phy = 1;
	}

	return board_status.spi_flash.phy == 1;
}

static void cache_init() {
	// Пытаемся по максимуму восстановить работу
	bool ignition_disabled = true;

	if (settings1_ram.dins[0].mode == DI_MODE_IGNITION ||
			settings1_ram.dins[1].mode == DI_MODE_IGNITION ||
			settings1_ram.dins[2].mode == DI_MODE_IGNITION ||
			settings1_ram.dins[3].mode == DI_MODE_IGNITION) {
		ignition_disabled = false;
	}

	if (cache.crc != cache_crc_get()) {
		if (ignition_disabled) {
			cache.ignition = IGNITION_DISABLED;
		}
		else {
			cache.ignition = IGNITION_OFF;
		}
		cache.motion_status = false;
		cache.device_mode = DEVICE_MODE_STANDBY;
		cache.odometer.val = settings1_ram.rom.odometer;
		cache.odometer.last_track_point = settings1_ram.rom.odometer;
		cache.odometer.last_saved = settings1_ram.rom.odometer;
		cache.din = 0;
		cache.counters[0] = 0;
		cache.counters[1] = 0;
		cache.counters[2] = 0;
		cache.counters[3] = 0;
	}
	else {
		if (ignition_disabled) {
			cache.ignition = IGNITION_DISABLED;
		}
		else if (cache.ignition == IGNITION_DISABLED) {
			cache.ignition = IGNITION_OFF;
		}
		odometer_flash_update_direct(cache.odometer.val);

		/// \note Даже если CRC совпала, делаем виксированное значение по режиму, движениюи зажиганию
		///  По умолчанию всё выключено
		cache.motion_status = false;
		cache.device_mode = DEVICE_MODE_STANDBY;
		cache.ignition = ignition_disabled ? IGNITION_DISABLED : IGNITION_OFF;
	}

	cache_crc_update();

	board_status.io.din_counter[0] = cache.counters[0];
	board_status.io.din_counter[1] = cache.counters[1];
	board_status.io.din_counter[2] = cache.counters[2];
	board_status.io.din_counter[3] = cache.counters[3];

	//--------

	if (cloud_cache.crc != cloud_cache_crc_get()) {
		cloud_cache.empty = true;
		cloud_cache.len = 0;
	}

	//--------

	if (lls_cache.crc != lls_cache_crc_get()) {
		bzero(&lls_cache, sizeof(lls_cache) - sizeof(lls_cache.crc));
	}

	memcpy(&lls_cache, fuel_data, sizeof(fuel_data_t) * 8);
}

static void gpio_state_init() {
	// Актуализируем состояние дискертных входов
	for(din_num_t din = DIN_1; din < DIN_NUM; din++){
		apply_din_state (din);
	}

	board_status.casing.open = HAL_GPIO_ReadPin(DIN_PHOTO_GPIO_Port, DIN_PHOTO_Pin) == GPIO_PIN_RESET;
	board_status.batt.charging = HAL_GPIO_ReadPin(CHARGE_STATUS_GPIO_Port, CHARGE_STATUS_Pin) == GPIO_PIN_RESET;
}

static void battery_charge_enable() {
	HAL_GPIO_WritePin(CHARGE_DISABLE_GPIO_Port, CHARGE_DISABLE_Pin, GPIO_PIN_RESET);

	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = CHARGE_DISABLE_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(CHARGE_DISABLE_GPIO_Port, &GPIO_InitStruct);
}

static void battery_charge_disable() {
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = CHARGE_DISABLE_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(CHARGE_DISABLE_GPIO_Port, &GPIO_InitStruct);
}

static void battery_charge_poll() {
	static bool charge_enabled = true;

	if (board_status.mcu.temp >= 5 && board_status.mcu.temp <= 60) {
		if (!charge_enabled) {
			battery_charge_enable();
			charge_enabled = true;
		}
	}
	else if (board_status.mcu.temp <= 0 || board_status.mcu.temp >= 65) {
		if (charge_enabled) {
			battery_charge_disable();
			charge_enabled = false;
		}
	}
}

static int AppInit() {
	// IWDG
	WDG_FREEZE();
	WDG_RESET(TASK_ALL);

	detect_hw_version();
	memcpy(board_status.hw_ver, hw_version, sizeof(board_status.hw_ver));
	memcpy(board_status.fw_ver, fw_version, sizeof(board_status.fw_ver));

	rstReason = get_reset_reason();

	osSemaphoreRelease(rtcSemHandle);
	// Актуализируем время
	unix_time_set(rtc_read());

	//стартуем таймер для dout
	HAL_TIM_Base_Start_IT(&htim9);

	int cnt = 3*60*10;
	while (board_status.spi_flash.phy == 0 && cnt-- > 0) {
		osDelay(100);
		osSignalSet(monitorTaskHandle, SIGNAL_TASK_APP);
		spi_flash_init();
	}

	if (board_status.spi_flash.phy == 0) {
		// На флеш писать не можем, поэтому просто выводим в usb
		user_printf("Can't init spi flash");
		set_settings_default();
	}
	else if (read_settings() != 0) {
//		set_settings_default();
//		set_settings2_default();
	}

	/// @warning Далее акселерометр не используется.
	/// Если понадобится использовтаь, то нужно сделать мьютекс на SPI, чтобы не было конфликта с spi_flash
	accelerometer_init(settings1_ram.acceleration_threshold);

	log_init();

	cache_init();

	gpio_state_init();

	device_mode_set(cache.device_mode);

	ignition_set(cache.ignition);

	rs485_init(canlog_rx_byte_handler);

	return sim68_init();
}

static void course_trigger_poll(float _current_course) {
	current_course = _current_course;

	device_mode_t mode = device_mode_get();

	if (mode == DEVICE_MODE_ACTIVE) {
		// Критическая секция, чтобы не было конфликта с last_point_course в коллбэке таймера
		taskENTER_CRITICAL();
		{
			if (fabsf(_current_course - last_point_course) >= (float)settings1_ram.untracked_course) {
				save_track_point_to_flash(SRC_COURSE);
			}
		}
		taskEXIT_CRITICAL();
	}
	else {
		last_point_course = _current_course;
	}
}

static void fix_valid_trigger_poll(bool current_fix_valid) {
	static bool pre_fix_valid = false;

	if (!pre_fix_valid && current_fix_valid) {
		save_track_point_to_flash(SRC_GNSS_VALID);		// Т.к. специального источника для причины "появился фикс" нет, переназначаем
	}

	pre_fix_valid = current_fix_valid;
}

static void device_mode_poll() {
	uint32_t cloud_send_period = xTimerGetPeriod(cloudSendTimerHandle);
	uint32_t fuel_period = xTimerGetPeriod(fuelTimerHandle);
	uint32_t canlog_period = xTimerGetPeriod(canlogTimerHandle);

	bool timers_in_active_state =	( cloud_send_period == (settings1_ram.period.data_send.in_active_mode * 1000) ) &&
									( fuel_period == (settings1_ram.period.fls_read.in_active_mode * 1000) ) &&
									( canlog_period == (settings3_ram.can.send_period.in_active_mode * 1000) );
	bool timers_in_standby_state =	( cloud_send_period == (settings1_ram.period.data_send.in_standby_mode * 1000) ) &&
									( fuel_period == (settings1_ram.period.fls_read.in_standby_mode * 1000) ) &&
									( canlog_period == (settings3_ram.can.send_period.in_standby_mode * 1000) );

	ignition_t ignition = ignition_get();
	bool motion_status = motion_status_get();
	device_mode_t device_mode; // = device_mode_get();;

	// Определяем режим работы девайса не по кэшу, а по текущим настройкам таймеров
	if (timers_in_active_state){
		device_mode = DEVICE_MODE_ACTIVE;
	}
	else if (timers_in_standby_state) {
		device_mode = DEVICE_MODE_STANDBY;
	}
	else {
		device_mode = DEVICE_MODE_UNKNOWN;
	}

//	bool speed_flag = (gnss_data.fix == GNSS_FIX_NOT_AVAILABLE) || (current_freezing_speed != 0);	// Если нет фикса, то не можем учитывать скорость, поэтому считаем, что она есть
//
//	extern void fuelTimerCallback(void const * argument);
//
//	if (device_mode_get() == DEVICE_MODE_STANDBY) {
//		if (/*ignition && */(motion_status/* || speed_flag*/)) {
//			device_mode_set(DEVICE_MODE_ACTIVE);
//			// Т.к. специального источника для причины "смена режима" нет, переназначаем
//			save_track_point_to_flash(SRC_MODE_CHANGE);	// Пишем точку при переходе в активный режим
//			fuelTimerCallback(NULL);
//		}
//	}
//	else {
//		if (!motion_status && !speed_flag) {
//			save_track_point_to_flash(SRC_MODE_CHANGE);	// Пишем точку при переходе в режим ожидания
//			fuelTimerCallback(NULL);
//			device_mode_set(DEVICE_MODE_STANDBY);
//		}
//	}

	if (ignition == IGNITION_DISABLED) {
		switch (device_mode) {
			case DEVICE_MODE_STANDBY:
				if (motion_status) {
					device_mode_set(DEVICE_MODE_ACTIVE);
				}
			break;

			case DEVICE_MODE_ACTIVE:
				if (!motion_status) {
					device_mode_set(DEVICE_MODE_STANDBY);
				}
			break;

			default:
				if (motion_status) {
					device_mode_set(DEVICE_MODE_ACTIVE);
				}
				else {
					device_mode_set(DEVICE_MODE_STANDBY);
				}
			break;
		}
	}
	else {
		switch (device_mode) {
			case DEVICE_MODE_STANDBY:
				if (motion_status || (ignition == IGNITION_ON)) {
					device_mode_set(DEVICE_MODE_ACTIVE);
				}
			break;

			case DEVICE_MODE_ACTIVE:
				if (!motion_status && (ignition == IGNITION_OFF)) {
					device_mode_set(DEVICE_MODE_STANDBY);
				}
			break;

			default:
				if (motion_status || (ignition == IGNITION_ON)) {
					device_mode_set(DEVICE_MODE_ACTIVE);
				}
				else {
					device_mode_set(DEVICE_MODE_STANDBY);
				}
			break;
		}
	}
}

static void canlog_data_poll() {
	static int8_t engine_temp_pre = 0;
	static time_t engine_temp_evt_time = 0;
	static float rpm_pre = 0;
	static time_t rpm_evt_time = 0;
	static bool check_oil_pre = false;
	static bool ignition_pre = false;

	if (canlog_used()) {
		int16_t engine_temp = 0;
		uint16_t rpm = 0;
		bool check_oil = false;
		bool ignition = false;
		can_alert_t * ptrh = &settings3_ram.can.alert;
		can_used_param_t active_param;

		DISABLE_IRQ_USER();

		engine_temp = (int16_t)canlog_data.temp;
		rpm = (uint16_t)roundf(canlog_data.rpm);
		check_oil = (bool)canlog_data.indication_flags.oil_level == CANLOG_IND_FLAG_OIL_LEVEL_ON;
		ignition = (bool)canlog_data.security_flags.ignition == CANLOG_SEC_FLAG_IGN_ON;

		active_param.all = canlog_record_global.data.active_param.all;

		ENABLE_IRQ_USER();

		// --- Обработка температуры ---

		if ( settings3_ram.can.used_param.engine_temp && active_param.engine_temp && (engine_temp >= ptrh->on_temp_trh.temp) ) {
			if (engine_temp_pre < ptrh->on_temp_trh.temp) {
				// Событие превышения порога
				engine_temp_evt_time = time(NULL);
			}
			else {
				if (time(NULL) - engine_temp_evt_time > ptrh->on_temp_trh.duration) {
					if (board_status.alert.high_engine_temp == 0) {
						// Сорбытие алерта
						board_status.alert.high_engine_temp = 1;
						// Спецсобытия нет, поэтому пишем в SRC_TIMER_WITH_IGNITION_ON
						save_track_point_to_flash(SRC_TIMER_WITH_IGNITION_ON);

						// Передача управления в задачу sensor_task для копирования данных CAN
						// и записи пакета на флэш через save_canlog_data_to_flash()
						osSemaphoreRelease(canlogSemHandle);
					}
				}
			}
		}
		else {
			board_status.alert.high_engine_temp = 0;
		}

		engine_temp_pre = engine_temp;

		// --- Обработка оборотов ---

		if ( settings3_ram.can.used_param.rpm && active_param.rpm && (rpm >= ptrh->on_rpm_trh.max) ) {
			if (rpm_pre < ptrh->on_rpm_trh.max) {
				// Событие превышения порога
				rpm_evt_time = time(NULL);
			}
			else {
				if (time(NULL) - rpm_evt_time > ptrh->on_rpm_trh.duration) {
					if (board_status.alert.high_rpm == 0) {
						// Событие алерта
						board_status.alert.high_rpm = 1;
						// Спецсобытия нет, поэтому пишем в SRC_TIMER_WITH_IGNITION_ON
						save_track_point_to_flash(SRC_TIMER_WITH_IGNITION_ON);

						// Передача управления в задачу sensor_task для копирования данных CAN
						// и записи пакета на флэш через save_canlog_data_to_flash()
						osSemaphoreRelease(canlogSemHandle);
					}
				}
			}
		}
		else {
			board_status.alert.high_rpm = 0;
		}

		rpm_pre = rpm;

		// --- Обработка уровня масла ---

		if (settings3_ram.can.used_param.oil_level && active_param.oil_level && settings3_ram.can.alert.on_check_oil) {
			if (check_oil != check_oil_pre) {
				if (check_oil == true) {
					// Сорбытие алерта
					board_status.alert.check_oil = 1;
					// Спецсобытия нет, поэтому пишем в SRC_TIMER_WITH_IGNITION_ON
					save_track_point_to_flash(SRC_TIMER_WITH_IGNITION_ON);

					// Передача управления в задачу sensor_task для копирования данных CAN
					// и записи пакета на флэш через save_canlog_data_to_flash()
					osSemaphoreRelease(canlogSemHandle);
				}
				else {
					board_status.alert.check_oil = 0;
				}

				check_oil_pre = check_oil;
			}
		}
		else {
			check_oil_pre = false;
			board_status.alert.check_oil = 0;
		}

		// --- Обработка зажигания ---

		if (settings3_ram.can.used_param.ignition && settings3_ram.can.gen_packet_on_ignition) {
			if (ignition != ignition_pre) {
				// Передача управления в задачу sensor_task для копирования данных CAN
				// и записи пакета на флэш через save_canlog_data_to_flash()
				osSemaphoreRelease(canlogSemHandle);

				ignition_pre = ignition;
			}
		}
		else {
			ignition_pre = false;
		}
	}
	else {
		engine_temp_pre = 0;
		rpm_pre = 0;
		check_oil_pre = false;
		ignition_pre = false;
		board_status.alert.mask = 0;
	}
}

/**
 * Получить текущую скорость с учетом настроек по заморозке скорости freeze_speed и freeze_time
 * @param current_speed Текущая актуальная скорость из GPS
 * @return
 */
static float freeze_speed_poll(float current_speed) {
	static uint32_t freeze_time = 0;

	if (current_speed < settings1_ram.gnss.freeze_speed) {
		if (HAL_GetTick() - freeze_time > settings1_ram.gnss.freeze_time * 1000) {
			current_freezing_speed = 0;
			freeze_time = HAL_GetTick();
		}
	}
	else {
		current_freezing_speed = current_speed;
		freeze_time = HAL_GetTick();
	}

	return current_freezing_speed;
}

static void sim68_check(sim68_gnss_data_t *gnss_data) {
	static uint32_t gnss_valid_tick = 0;
	static uint32_t gnss_uart_ok_tick = 0;
	static uint32_t gnss_uart_pkt_cnt = 0;
	uint32_t curr_tick = HAL_GetTick();

	if (gnss_data->pkt_cnt - gnss_uart_pkt_cnt > 0) {
		gnss_uart_pkt_cnt = gnss_data->pkt_cnt;
		gnss_uart_ok_tick = curr_tick;
	}
	else {
		if (curr_tick - gnss_uart_ok_tick > 5000) {
			ZF_LOGW("GPS UART silent. Deinit");

			if (sim68_init() != 0) {
				system_reset("Can't deinit GPS. Reboot");
			}

			gnss_uart_ok_tick = curr_tick;
		}
	}

	if (gnss_data->valid_status == GNSS_VALID) {
		gnss_valid_tick = curr_tick;
	}
	else {
		if (curr_tick - gnss_valid_tick > 120000) {
			ZF_LOGW("GPS data not valid. Deinit");

			if (sim68_init() != 0) {
				system_reset("Can't deinit GPS. Reboot");
			}

			gnss_valid_tick = curr_tick;
		}
	}
}

static void record_init() {
	record_io_update();

	memset(&teledata_record_global.data.ow.temp, OW_TEMP_UNKNOWN_VALUE, sizeof(teledata_record_global.data.ow.temp));
}

static void gnss_poll() {
	sim68_gnss_data_get(&gnss_data);

	//--------------------------
	//Эмуляция скорости и курса
//	typedef enum dir_t { UP, DOWN } dir_t;
//	static dir_t speed_dir = UP;
//	static float speed = 0, course = 0;
//
//	if (device_mode_get() == DEVICE_MODE_ACTIVE) {
//		if (speed_dir == UP) {
//			if (++speed > 60) {
//				speed_dir = DOWN;
//			}
//		}
//		else {
//			if (--speed <= 0) {
//				speed_dir = UP;
//			}
//		}
//
//		if ((course += 2) >= 360) {
//			course = 0;
//		}
//	}
//	else {
//		speed_dir = UP;
//		speed = 0;
//	}
//
//	gnss_data.tracked.speed = speed;
//	gnss_data.tracked.course = course;
	//--------------------------

	board_status.gnss.fix = gnss_data.fix;
	board_status.gnss.hdop = gnss_data.fix > GNSS_FIX_NOT_AVAILABLE ? (float)gnss_data.tracked.hdop : 0;
	board_status.gnss.speed = gnss_data.tracked.speed;

	// Основан на нефильтрованной скорости
	float freeze_speed = freeze_speed_poll(gnss_data.fix != GNSS_FIX_NOT_AVAILABLE ? gnss_data.tracked.speed : 0);

	if (osMutexWait(recordMutexHandle, 1000) == osOK) {
		navigation_record_t *nav = &teledata_record_global.data.navigation;

		nav->on = gnss_data.run_status;
		nav->fix = nav->on ? gnss_data.fix : GNSS_FIX_NOT_AVAILABLE;

		if (nav->on && nav->fix != GNSS_FIX_NOT_AVAILABLE) {
			// Мы можем делать прополку курса и одометра только если есть fix
			course_trigger_poll(gnss_data.tracked.course);

			if (nav->lattitude == 0 && nav->longitude == 0) {
				nav->lattitude = gnss_data.tracked.latitude;
				nav->longitude = gnss_data.tracked.longitude;

				coordinates_freezed = true;
			}

			if (device_mode_get() == DEVICE_MODE_ACTIVE) {
				nav->lattitude = gnss_data.tracked.latitude;
				nav->longitude = gnss_data.tracked.longitude;

				nav->speed = freeze_speed;

				nav->course = (uint16_t)gnss_data.tracked.course;

				nav->altitude = gnss_data.tracked.altitude;

				coordinates_freezed = false;
			}
			else {
				nav->speed = 0;

				coordinates_freezed = true;
			}

			nav->hdop = gnss_data.tracked.hdop;

			nav->odometer = cache.odometer.val;

			nav->satellites_used = gnss_data.tracked.satellites;
		}

		if ( nav->on && (nav->fix != GNSS_FIX_NOT_AVAILABLE) && (gnss_data.valid_status == GNSS_VALID) &&
				(!settings1_ram.gnss.filter_on ||
					(nav->hdop <= settings1_ram.gnss.hdop_max &&
					nav->satellites_used >= settings1_ram.gnss.satellites_min &&
					nav->speed <= (float)settings1_ram.gnss.max_speed)) ) {
			nav->valid = true;
		}
		else {
			nav->valid = false;
		}

		fix_valid_trigger_poll(nav->valid);

		osMutexRelease(recordMutexHandle);
	}

	// ---- Check GPS ---------------------

	sim68_check(&gnss_data);
}

static void power_off_callback() {
	odometer_flash_update(cache.odometer.val);
	save_track_point_to_flash(SRC_POWER_OFF);
}

static void power_on_callback() {

}

static void power_poll() {
	static bool pre_power_present = 0;
	bool power_present = board_status.power.present;

	if (!power_present && pre_power_present) {
		power_off_callback();
	}
	else if (power_present && !pre_power_present) {
		power_on_callback();
	}

	pre_power_present = power_present;


	if (power_present || board_status.batt.voltage > 3.9) {
		gsm_enable = true;
	}
	else {
		if (board_status.batt.voltage <= 3.8) {
			if (gsm_enable) {
				gsm_enable = false;
				ZF_LOGE("Low battery voltage. GSM Disable");
			}
		}
		if (board_status.batt.voltage <= 3.6) {
			ZF_LOGF("Critical battery voltage. Power Down");

			int cnt = 5;

			while (cnt > 0) {
				if (sim68_sleep() == -1) {
					if (--cnt <= 0) {
						sim68_force_hard_reset();
						break;
					}
				}
				else {
					break;
				}

				WDG_RESET(TASK_ALL);
			}

			// Блокируем мьютекс fifo на тот случай, чтобы в момент отключения flash не было активной работы с ней
			osRecursiveMutexWait(fifoRecursiveMutexHandle, 2 * 60 * 1000);

			WDG_RESET(TASK_ALL);

//			vTaskSuspend(appTaskHandle);
			vTaskSuspend(uiTaskHandle);
			vTaskSuspend(sensorReadTaskHandle);
			vTaskSuspend(saveDataTaskHandle);
			vTaskSuspend(monitorTaskHandle);
			vTaskSuspend(gsmTaskHandle);

			sim800_gsm_power_off();

			SPI_Flash_PowerDown();

			dout_turnOff(LED_GSM_RED);
			dout_turnOff(LED_GSM_GREEN);
			dout_turnOff(LED_GPS_RED);
			dout_turnOff(LED_GPS_GREEN);

			while (!board_status.power.present) {
				osDelay(1000);
				WDG_RESET(TASK_ALL);
			}

			system_reset(NULL);
		}
	}
}

int wait_good_power(uint32_t timeout) {
	uint32_t start_time = HAL_GetTick();

	while (HAL_GetTick() - start_time < timeout) {
		if (board_status.power.present) {
			if (board_status.power.present_time_ms > 3000) {
				return 0;
			}

			start_time = HAL_GetTick();
		}
		else if (board_status.batt.voltage > 3.6) {
			return 0;
		}

		osDelay(10);
	}

	return -1;
}

void validate_settings_zero() {
	if (check_settings_locked()) {
		return;
	}

	// Эти настройки не могут быть нулевыми
	if (settings1_ram.ftp_host.address[0] == 0) {
		set_settings1_default();
	}
	if (settings2_ram.log_level == 0) {
		set_settings2_default();
	}
	if (settings3_ram.can.send_period.in_active_mode == 0) {
		set_settings3_default();
	}
	// settings4 нулевыми могут быть, поэтому не проверяем
//	if (settings4_ram.auto_update == 0) {
//		set_settings4_default();
//	}
}

static void din_event_poll() {
	if (din_sos_event) {
		din_sos_event--;
		save_track_point_to_flash(SRC_SOS);
	}

	if (din_write_event) {
		din_write_event--;
		save_track_point_to_flash(SRC_DIN);
	}

	if (din_ignition_event) {
		din_ignition_event--;
		save_track_point_to_flash(SRC_CHANGE_IGNITION);		// Т.к. специального источника для причины "появился фикс" нет, переназначаем
	}
}

static void uncover_event_poll() {
	static bool pre_case_open = false;
	bool case_open = board_status.casing.open;

	if (!pre_case_open && case_open) {
		save_track_point_to_flash(SRC_UNCOVER);
	}

	pre_case_open = case_open;
}

static void save_track_data_timer_poll() {
	device_mode_t device_mode = device_mode_get();

	if (device_mode == DEVICE_MODE_ACTIVE) {
		if (save_track_data_timer_sec_cnt >= settings1_ram.period.data_write.in_active_mode) {
			// save_track_data_timer_sec_cnt уже сбрасывается в synchronize_odo_cource_and_timer_points
			save_track_point_to_flash(SRC_TIMER_WITH_IGNITION_ON);
		}
	}
	else {
		if (save_track_data_timer_sec_cnt >= settings1_ram.period.data_write.in_standby_mode) {
			// save_track_data_timer_sec_cnt уже сбрасывается в synchronize_odo_cource_and_timer_points
			save_track_point_to_flash(SRC_TIMER_WITH_IGNITION_OFF);
		}
	}

	save_track_data_timer_sec_cnt++;
}

static void one_sec_poll() {
	static uint32_t pre_app_timer = 0;

	if ((HAL_GetTick() - pre_app_timer) >= 1000) {
//		user_printf(">>> Min Free Heap Size: %d\r\n", xPortGetMinimumEverFreeHeapSize());

		pre_app_timer = HAL_GetTick();

		save_track_data_timer_poll();

		gnss_poll();

		device_mode_poll();

		power_poll();

		din_event_poll();

		uncover_event_poll();

		battery_charge_poll();

		time_t rtc_time;
		DISABLE_IRQ_USER();
		rtc_time = rtc_read();
		ENABLE_IRQ_USER();
		if (rtc_time - unix_time_get() > 2) {
			// unixtime обновляется в GPS. И только, если там нет обновления в течение 2 секунд, обновляем из RTC
			ZF_LOGW("Update unixtime from RTC");
			unix_time_set(rtc_time);
		}

		odometer_poll();

		canlog_data_poll();

		if (!check_settings_locked()) {
			int ret = check_settings_in_ram();
			if (ret != 0) {
				system_reset(NULL);
			}
		}

		if (0) {
			// Логируем состояние ignition и motion для дебага проблемы standby режима
			/// \note Предположительная проблема дребезг и пропуск прерываний от din
			/// Нужно убрать din из прерывания и опрашивать его периодически вручную
			din_num_t din;

			if (settings1_ram.dins[DIN_1].mode == DI_MODE_IGNITION) {
				din = DIN_1;
			}
			else if (settings1_ram.dins[DIN_2].mode == DI_MODE_IGNITION) {
				din = DIN_2;
			}
			else if (settings1_ram.dins[DIN_3].mode == DI_MODE_IGNITION) {
				din = DIN_3;
			}
			else if (settings1_ram.dins[DIN_4].mode == DI_MODE_IGNITION) {
				din = DIN_4;
			}
			else {
				din = DIN_NUM;	// Что-то кладем. Главное - понять, что не нашли настройки
			}

			int din_state = (int)din_get(din);
			ZF_LOGW("Ignition: din-%d, board.ign-%d, cache.ign-%d", din_state, (int)board_status.ignition, (int)cache.ignition);
			ZF_LOGW("Ignition: board.din-%d, cache.din-%d", (int)((board_status.io.din_mask & (1 << din)) != 0), (int)((cache.din & (1 << din)) != 0));

			ZF_LOGW("Motion: accint-%d, board-%d, cache-%d", (int)(HAL_GPIO_ReadPin(ACCEL_INT1_GPIO_Port, ACCEL_INT1_Pin) == GPIO_PIN_RESET), (int)board_status.motion.active, (int)cache.motion_status);

			ZF_LOGW("Device_mode: %s", cache.device_mode == DEVICE_MODE_STANDBY ? "standby" : "active");

		}

//		uint32_t timeout = device_mode_get() == DEVICE_MODE_STANDBY ? settings_ram.period.data_send.in_standby_mode : settings_ram.period.data_send.in_active_mode;
//		if ( (++cloud_task_send_timeout > timeout*3) && (cloud_task_send_timeout > 180) ) {
//			ZF_LOGF("Cloud Timeout Reboot");
//			osDelay(100);
//			system_reset(NULL);
//		}
	}
}

static void led_poll() {
	static uint32_t predTick_led = 0;

	if ((HAL_GetTick() - predTick_led) >= 1000) {
		predTick_led = HAL_GetTick();

		// ------------------

		if (gnss_data.valid_status == GNSS_VALID){
			dout_turnOff(LED_GPS_RED);
			dout_turnOn(LED_GPS_GREEN);
		}
		else {
			dout_turnOff(LED_GPS_GREEN);
			dout_turnOn(LED_GPS_RED);
		}

		// ------------------

		if (!board_status.gsm.phy) {
			dout_turnOff(LED_GSM_RED);
			dout_turnOff(LED_GSM_GREEN);
		}
		else if (!board_status.gsm.sim) {
			dout_setPeriodic(LED_GSM_RED, 250, 500, 1000);
			dout_turnOff(LED_GSM_GREEN);
		}
		else if (!board_status.gsm.gprs) {
			dout_turnOn(LED_GSM_RED);
			dout_turnOff(LED_GSM_GREEN);
		}
		else if (!board_status.gsm.tcp_tx) {
			dout_turnOff(LED_GSM_RED);
			dout_setPeriodic(LED_GSM_GREEN, 250, 500, 1000);
		}
		else {
			dout_turnOff(LED_GSM_RED);
			dout_turnOn(LED_GSM_GREEN);
		}
	}
}

//=========================================================================

void app_task(void const * argument)
{
	UNUSED(argument);

	// Задержка для того, чтобы перед логом поднялся USB
	osDelay(200);
	user_printf("\r\n>>> Start program\r\n");
	user_printf(">>> Free Heap Size: %d\r\n", xPortGetFreeHeapSize());

	wait_good_power(10000);

	if (AppInit() != 0) {
		system_reset("Can't init device");
	}

	record_init();

	ZF_LOGF("Boot. Reason: %s. fw: %c.%d.%d. hw: %c.%d.%d", rstReason, fw_version[2], fw_version[1], fw_version[0], hw_version[2], hw_version[1], hw_version[0]);

	save_track_point_to_flash(SRC_REBOOT);

	osSignalSet(gsmTaskHandle, SIGNAL_START);
	osSignalSet(cloudTaskHandle, SIGNAL_START);
	osSignalSet(saveDataTaskHandle, SIGNAL_START);
	osSignalSet(sensorReadTaskHandle, SIGNAL_START);

	for(;;)
	{
		led_poll();

		one_sec_poll();

		validate_settings_zero();

		osSignalSet(monitorTaskHandle, SIGNAL_TASK_APP);

		osDelay(50);
	}
}
