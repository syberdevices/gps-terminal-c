/*
 * cloud.c
 *
 *  Created on: 7 дек. 2018 г.
 *      Author: Denis Shreiber
 */

// C includes
#include <string.h>
#include <math.h>

// HAL includes
#include "rtos.h"

// User includes
#include "common.h"
#include "log/flash_fifo/flash_fifo.h"
#include "egts/egts.h"
#include "settings/settings.h"
#include "crc16/crc16.h"

// ===========================================================================

#define HOST_COMMAND_ODOMETER_RESET		"odometer reset"

#define TRANSMIT_POINTS_NUM				10

// ===========================================================================

static uint8_t rx_buff[1024];
static uint8_t rec_buff[1024];

// ===========================================================================

#if 0
#include "ctype.h"
static void printX(const void *data, uint32_t len) {
	const char *ptr = data;

	for (uint32_t q = 0; q < len; q++) {
		if (ptr[q] >= 0x20 && ptr[q] < 0x7F) {
			user_printf("%c", ptr[q]);
		}
		else {
			if (ptr[q] == '\r') {
				user_printf("\\r");
			}
			else if (ptr[q] == '\n') {
				user_printf("\\n\r\n");
			}
			else {
				user_printf("{0x%x}", ptr[q]);
			}
		}
	}
}
#endif

// ---------------------------------------------------------------------------

bool packet_validate(uint8_t *packet, uint32_t len) {
	head_record_t *hr = (head_record_t *)packet;

	if (len == hr->len) {
		uint32_t rec_len = len - sizeof(head_record_t);

		if (hr->type == RECORD_TYPE_TELEDATA) {
			if (rec_len == sizeof(teledata_subrecord_t)) {
				return true;
			}
		}
		else if (hr->type == RECORD_TYPE_FUEL) {
			if (rec_len != 0) {
				if (rec_len / sizeof(fuel_subrecord_t) <= 8) {
					if (rec_len % sizeof(fuel_subrecord_t) == 0) {
						return true;
					}
				}
			}
		}
		else if (hr->type == RECORD_TYPE_CANLOG) {
			if (rec_len == sizeof(canlog_subrecord_t)) {
				return true;
			}
		}
	}

	return false;
}

static int cloud_send(void *data, uint32_t len) {
	if (len == 0) {	// Нечего отправлять
		return 0;
	}

	if (osMessageWaiting(cloudTxQueueHandle) != 0) {	// Очередь не пуста
		return -1;
	}

	cloud_data_t *cloud_data = pvPortMalloc(sizeof(len) + len);
	if (cloud_data == NULL) {
		return -1;
	}

	cloud_data->len = len;
	memcpy(cloud_data->data, data, len);
	if (osMessagePut(cloudTxQueueHandle, (uint32_t)cloud_data, 0) != osOK) {
		vPortFree(cloud_data);
		return -1;
	}

	while (1) {
		osEvent evt = osMessageGet(cloudTxRspQueueHandle, 1000);
		if (evt.status == osEventMessage) {
			return evt.value.v;
		}

		osSignalSet(monitorTaskHandle, SIGNAL_TASK_CLOUD);
	}

	return -1;
}

static uint32_t cloud_receive(void *data, uint32_t timeout) {
	uint32_t len = 0;

	if (osMessagePeek(cloudRxQueueHandle, timeout).status == osEventMessage) {	// Ждем, пока появятся данные в очереди
		osEvent evt = osMessageGet(cloudRxQueueHandle, 1);
		if (evt.status == osEventMessage) {
			cloud_data_t *cloud_data = evt.value.p;
			if (cloud_data) {
				len = cloud_data->len;
				memcpy(data, cloud_data->data, len);

				vPortFree(cloud_data);
			}
		}
	}

	return len;
}

// ---------------------------------------------------------------------------
// EGTS ----------------------------------------------------------------------

static int egts_pack_fuel(uint8_t *buff, int position, EGTS_RECORD_HEADER *record_header, fuel_record_t *f_record, uint8_t f_size) {
	fuel_subrecord_t * rec = f_record->data;

	for (int q = 0; q < f_size && q < sizeof(settings1_ram.fls)/sizeof(settings1_ram.fls[0]); q++) {
		// add subrecord (SRD) EGTS_SR_LIQUID_LEVEL_SENSOR
		EGTS_SUBRECORD_HEADER *subrecord_header = (EGTS_SUBRECORD_HEADER *)&buff[position];
		position = packet_add_subrecord_header(buff, position, record_header, EGTS_SR_LIQUID_LEVEL_SENSOR);
		position = packet_add_subrecord_EGTS_SR_LIQUID_LEVEL_SENSOR_RECORD(buff, position, record_header, subrecord_header, rec[q].level, rec[q].num, settings1_ram.fls[rec[q].num].address);
	}

	return position;
}

static int egts_pack_teledata(uint8_t *buff, int position, EGTS_RECORD_HEADER *record_header, teledata_record_t *record) {
	// navigation data
	// add subrecord (SRD) EGTS_SR_POS_DATA
	EGTS_SUBRECORD_HEADER *subrecord_header = (EGTS_SUBRECORD_HEADER *)&buff[position];
	position = packet_add_subrecord_header(buff, position, record_header, EGTS_SR_POS_DATA);
	position = packet_add_subrecord_EGTS_SR_POS_DATA_RECORD(buff, position, record_header, subrecord_header, record);

	// extended navigation data
	// add subrecord (SRD) EGTS_SR_EXT_POS_DATA
	subrecord_header = (EGTS_SUBRECORD_HEADER *)&buff[position];
	position = packet_add_subrecord_header(buff, position, record_header, EGTS_SR_EXT_POS_DATA);
	position = packet_add_subrecord_EGTS_SR_EXT_POS_DATA_RECORD(buff, position, record_header, subrecord_header, record);

	// additional analog and digital sensors
	// add subrecord (SRD) EGTS_SR_AD_SENSORS_DATA
	subrecord_header = (EGTS_SUBRECORD_HEADER *)&buff[position];
	position = packet_add_subrecord_header(buff, position, record_header, EGTS_SR_AD_SENSORS_DATA);
	position = packet_add_subrecord_EGTS_SR_AD_SENSORS_DATA_RECORD(buff, position, record_header, subrecord_header, record, 1);

	// add subrecord (SRD) EGTS_SR_AD_SENSORS_DATA (1-wire temperature)
	subrecord_header = (EGTS_SUBRECORD_HEADER *)&buff[position];
	position = packet_add_subrecord_header(buff, position, record_header, EGTS_SR_AD_SENSORS_DATA);
	position = packet_add_subrecord_EGTS_SR_AD_SENSORS_DATA_RECORD(buff, position, record_header, subrecord_header, record, 2);

	// counters
	// add subrecord (SRD) EGTS_SR_COUNTERS_DATA
	subrecord_header = (EGTS_SUBRECORD_HEADER *)&buff[position];
	position = packet_add_subrecord_header(buff, position, record_header, EGTS_SR_COUNTERS_DATA);
	position = packet_add_subrecord_EGTS_SR_COUNTERS_DATA_RECORD(buff, position, record_header, subrecord_header, record);

	// device status end power voltages
	// add subrecord (SRD) EGTS_SR_STATE_DATA
	subrecord_header = (EGTS_SUBRECORD_HEADER *)&buff[position];
	position = packet_add_subrecord_header(buff, position, record_header, EGTS_SR_STATE_DATA);
	position = packet_add_subrecord_EGTS_SR_STATE_DATA_RECORD(buff, position, record_header, subrecord_header, record);

	return position;
}

static int egts_add_record_to_packet(uint8_t *buff, int position, uint8_t *data) {
	// add record (SDR) EGTS_RECORD_HEADER
	EGTS_RECORD_HEADER *record_header = (EGTS_RECORD_HEADER *)&buff[position];
	position = packet_add_record_header(buff, position, EGTS_TELEDATA_SERVICE, EGTS_TELEDATA_SERVICE, ((head_record_t *)data)->time);

	head_record_t *hr = (head_record_t *)data;

	if (hr->type == RECORD_TYPE_TELEDATA) {
		position = egts_pack_teledata(buff, position, record_header, (teledata_record_t*)data);
	}
	else if (hr->type == RECORD_TYPE_FUEL) {
		uint8_t fsize = (hr->len - sizeof(head_record_t)) / sizeof(fuel_subrecord_t);
		position = egts_pack_fuel(buff, position, record_header, (fuel_record_t*)data, fsize);
	}
	else {
		return 0;
	}

	return position;
}

static int authorise_pack_egts(const char * imei, uint8_t * buff) {
	unsigned int tid = settings1_ram.id;

	if (tid == 0) {
		// Если нет tid, берем последние 6 байт IMEI
		user_sscanf(imei + 9, "%u", &tid);
	}

	// create egts packet header
	int top = egts_packet_create(buff, EGTS_PT_APPDATA);

	// add record (SDR) EGTS_RECORD_HEADER
	EGTS_RECORD_HEADER *record_header = (EGTS_RECORD_HEADER *)&buff[top];
	top = packet_add_record_header(buff, top, EGTS_AUTH_SERVICE, EGTS_AUTH_SERVICE, time(NULL));

	// add subrecord EGTS_SR_TERM_IDENTITY
	EGTS_SUBRECORD_HEADER *subrecord_header = (EGTS_SUBRECORD_HEADER *)&buff[top];
	top = packet_add_subrecord_header(buff, top, record_header, EGTS_SR_TERM_IDENTITY);
	top = packet_add_subrecord_EGTS_SR_TERM_IDENTITY(buff, top, record_header, subrecord_header, tid, (char*)imei);

	// add subrecord EGTS_SR_MODULE_DATA
	subrecord_header = (EGTS_SUBRECORD_HEADER *)&buff[top];
	top = packet_add_subrecord_header(buff, top, record_header, EGTS_SR_MODULE_DATA);
	top = packet_add_subrecord_EGTS_SR_MODULE_DATA(buff, top, record_header, subrecord_header);

	// add CRC
	top += egts_packet_finalize(buff, top);

	return top;
}

static int authorise_response_decode_egts(uint8_t * buff, uint32_t len) {
	egts_msg_t msg;

	int res = terminal_decode(buff, len, &msg);
	if ( !(res == EGTS_PC_OK && (msg.type == EGTS_MSG_TYPE_RESPONSE || msg.type == EGTS_MSG_TYPE_APPDATA)) ) {
		ZF_LOGE("%s authorization error", "EGTS");
		return -1;
	}

	return 0;
}

static uint32_t get_and_encode_data_egts(uint8_t *buff) {
	int record_num = 0;
	int len = egts_packet_create(buff, EGTS_PT_APPDATA);

	for (; record_num < TRANSMIT_POINTS_NUM; record_num++) {
		int rec_len = flash_fifo_pop(&track_fifo, rec_buff);
		if (rec_len == 0) {
			break;
		}

		if (packet_validate(rec_buff, rec_len)) {
			len = egts_add_record_to_packet(buff, len, rec_buff);
		}
		else {
			// Если запись на флэш некорректна
			flash_fifo_erase_full_memory(&track_fifo);
			ZF_LOGW("Track data memory erased!");
			break;
		}
	}

	if (record_num) {
		user_printf(">>> Pop %d records\r\n", record_num);

		len += egts_packet_finalize(buff, len);
	}
	else {
		len = 0;
	}

	return len;
}

static int decode_received_data_egts(uint8_t * buff, uint32_t len) {
	egts_msg_t msg;

	int res = terminal_decode(buff, len, &msg);
	if (res == EGTS_PC_OK) {
		if (msg.type == EGTS_MSG_TYPE_RESPONSE) {
			return 0;
		}
		else if (msg.type == EGTS_MSG_TYPE_COMMAND) {
			ZF_LOGW("EGTS Command");

			int res = -1;
			uint16_t comand_code = *((uint16_t*)(&msg.data[14]));

			if (comand_code == EGTS_FLEET_ODOM_CLEAR || strcmp((char*)&msg.data[16], HOST_COMMAND_ODOMETER_RESET)) {
				res = odometer_reset();
			}

			if (res == 0) {
				/// @todo Send response Success
			}
			else {
				/// @todo Send response Failure
			}

			return 0;
		}
		else {
			ZF_LOGI("EGTS parse error");
		}
	}
	else {
		ZF_LOGI("EGTS parse error");
	}

	return -1;
}

// ---------------------------------------------------------------------------
// Wialon --------------------------------------------------------------------

static int wialon_pack_fuel(char *buff, fuel_record_t *f_record, uint8_t f_size) {
	char *ptr = buff;

	if (1) {	// Если еcть время
		struct tm *ts = localtime((time_t*)&f_record->head.time);

		ptr += sprintf(ptr, "%02d%02d%02d;%02d%02d%02d",
				ts->tm_mday, ts->tm_mon + 1, ts->tm_year - 100, ts->tm_hour, ts->tm_min, ts->tm_sec);
	}
	else {
		ptr += sprintf(ptr, "NA;NA");
	}

	ptr += sprintf(ptr, ";NA;NA;NA;NA;NA;NA;NA;NA;NA");
	ptr += sprintf(ptr, ";NA;NA;;NA;");

	for (int q = 0; q < f_size && q < sizeof(settings1_ram.fls)/sizeof(settings1_ram.fls[0]); q++) {
		if (*(ptr - 1) != ';') {
			ptr += sprintf(ptr, ",");
		}

		fuel_subrecord_t *rec = &f_record->data[q];

		ptr += sprintf(ptr, "Fuel_L_%d:1:%d", (int)rec->num + 1, (int)rec->level);	// Первый параметр - номер датчика
		ptr += sprintf(ptr, ",temp%d:1:%d", (int)rec->num + 1, (int)rec->temp);	// Первый параметр - номер датчика
	}

	ptr += sprintf(ptr, "|");

	return (int)(ptr - buff);
}

static int wialon_pack_canlog(char *buff, canlog_record_t *f_record) {
	char *ptr = buff;

	if (1) {	// Если еcть время
		struct tm *ts = localtime((time_t*)&f_record->head.time);

		ptr += sprintf(ptr, "%02d%02d%02d;%02d%02d%02d",
				ts->tm_mday, ts->tm_mon + 1, ts->tm_year - 100, ts->tm_hour, ts->tm_min, ts->tm_sec);
	}
	else {
		ptr += sprintf(ptr, "NA;NA");
	}

	ptr += sprintf(ptr, ";NA;NA;NA;NA;NA;NA;NA;NA;NA");
	ptr += sprintf(ptr, ";NA;NA;;NA;");

	canlog_subrecord_t *rec = &f_record->data;

	if (rec->used_param.rpm) {
		if (rec->active_param.rpm) {
			ptr += sprintf(ptr, "can_rpm:1:%d", (int)roundf(rec->param.rpm));
			ptr += sprintf(ptr, ",");
		}
	}

	if (rec->used_param.engine_temp) {
		if (rec->active_param.engine_temp) {
			ptr += sprintf(ptr, "can_temp:1:%d", (int)rec->param.temp);
			ptr += sprintf(ptr, ",");
		}
	}

	if (rec->used_param.fuel_level_liter) {
		if (rec->active_param.fuel_level_liter) {
			ptr += sprintf(ptr, "can_fuel_L:1:%d", (int)roundf(rec->param.fuel_liter));
			ptr += sprintf(ptr, ",");
		}
	}

	if (rec->used_param.fuel_level_percent) {
		if (rec->active_param.fuel_level_percent) {
			ptr += sprintf(ptr, "can_fuel_P:1:%d", (int)roundf(rec->param.fuel_percent));
			ptr += sprintf(ptr, ",");
		}
	}

	if (rec->used_param.ignition) {
		if (rec->active_param.ignition) {
			ptr += sprintf(ptr, "can_ign:1:%d", (int)rec->param.security_flags.ignition);
			ptr += sprintf(ptr, ",");
		}
	}

	if (rec->used_param.odometer) {
		if (rec->active_param.odometer) {
			ptr += sprintf(ptr, "can_odo:1:%d", (int)roundf(rec->param.odometer));
			ptr += sprintf(ptr, ",");
		}
	}

	if (rec->used_param.oil_level) {
		if (rec->active_param.oil_level) {
			ptr += sprintf(ptr, "can_oil:1:%d", (int)rec->param.indication_flags.oil_level);
			ptr += sprintf(ptr, ",");
		}
	}

	// Последний символ не должен быть ',' или ';'
	char * last_char = ptr - 1;
	if (*last_char == ',' || *last_char == ';') {
		ptr = last_char;
	}

	ptr += sprintf(ptr, "|");

	return (int)(ptr - buff);
}

static int wialon_pack_teledata(char *buff, teledata_record_t *record) {
	char *ptr = buff;

	if (1) {	// Если еcть время
		struct tm *ts = localtime((time_t*)&record->head.time);

		ptr += sprintf(ptr, "%02d%02d%02d;%02d%02d%02d",
				ts->tm_mday, ts->tm_mon + 1, ts->tm_year - 100, ts->tm_hour, ts->tm_min, ts->tm_sec);
	}
	else {
		ptr += sprintf(ptr, "NA;NA");
	}

	navigation_record_t *nav = &record->data.navigation;

	if (nav->fix != GNSS_FIX_NOT_AVAILABLE) {
		double dval = fabs(nav->lattitude);
		double degree;
		double minute = modf(dval, &degree);
		dval = (degree + minute*60/100)*100;
		minute = modf(dval, &degree);

		ptr += sprintf(ptr, ";%04d.%04d;%c", (int)degree, (int)(minute*10000), nav->lattitude >= 0 ? 'N' : 'S');

		dval = fabs(nav->longitude);
		minute = modf(dval, &degree);
		dval = (degree + minute*60/100)*100;
		minute = modf(dval, &degree);

		ptr += sprintf(ptr, ";%05d.%04d;%c", (int)degree, (int)(minute*10000), nav->longitude >= 0 ? 'E' : 'W');

		minute = modf((double)nav->hdop, &degree);
		ptr += sprintf(ptr, ";%d;%d;%d;%d;%d.%02d", (int)nav->speed, (int)nav->course, (int)nav->altitude, nav->satellites_used, (int)nav->hdop, (int)(minute*100));
	}
	else {
		ptr += sprintf(ptr, ";NA;NA;NA;NA;NA;NA;NA;NA;NA");
	}

	ptr += sprintf(ptr, ";%d;%d", (int)record->data.io.din._16bit | ((nav->valid == true) ? 0x0100 : 0x0000), record->data.io.dout);

	float integ;
	float fract = modff(record->data.io.ain[0], &integ);
	ptr += sprintf(ptr, ";%d.%02d", (int)integ, (int)(fract*100));

	fract = modff(record->data.io.ain[1], &integ);
	ptr += sprintf(ptr, ",%d.%02d", (int)integ, (int)(fract*100));

	ptr += sprintf(ptr, ",,,,,,");

	for (int q = 0; q < 8; q++) {
		ow_temp_record_t * temp_rec = &record->data.ow.temp[q];

		if (temp_rec->used) {
			int temp_val = (temp_rec->val != OW_TEMP_UNKNOWN_VALUE) ? ((int)temp_rec->val + CLOUD_TEMP_OFFSET) : 0;
			ptr += sprintf(ptr, ",%d.00", temp_val);
		}
		else {
			ptr += sprintf(ptr, ",");
		}
	}

	ptr += sprintf(ptr, ";NA;");

	// params

	for (int q = 0; q < sizeof(record->data.io.count)/sizeof(record->data.io.count[0]); q++) {
		if (*(ptr - 1) != ';') {
			ptr += sprintf(ptr, ",");
		}
		ptr += sprintf(ptr, "count%d:1:%u", q + 1, (unsigned int)record->data.io.count[q]);	// Первый параметр - номер датчика
	}

	fract = fabsf(modff(record->data.power.main_voltage, &integ));
	ptr += sprintf(ptr, ",pwr_ext:2:%d.%02d", (int)integ, (int)(fract*100));

	fract = fabsf(modff(record->data.power.batt_voltage, &integ));
	ptr += sprintf(ptr, ",pwr_akb:2:%d.%02d", (int)integ, (int)(fract*100));

	ptr += sprintf(ptr, ",Odom:1:%d", (int)record->data.navigation.odometer);

	for (int q = 0; q < record->data.ow.ibutton.count; q++) {
		uint8_t *code = record->data.ow.ibutton.dev[q].code;

		ptr += sprintf(ptr, ",avl_driver%d:3:%02X%02X%02X%02X%02X%02X%02X%02X", q + 1,
				code[7], code[6], code[5], code[4],
				code[3], code[2], code[1], code[0]);
	}

	if (record->data.source == SRC_SOS) {
		ptr += sprintf(ptr, ",SOS:1:1");
	}

	if (record->data.source == SRC_UNCOVER) {
		ptr += sprintf(ptr, ",ind:1:1");
	}

	if (record->data.source == SRC_POWER_OFF) {
		ptr += sprintf(ptr, ",pex:1:0");
	}

	ptr += sprintf(ptr, "|");

	return (int)(ptr - buff);
}

static int wialon_add_record_to_packet(char *buff, uint8_t *data) {
	head_record_t *hr = (head_record_t *)data;

	if (hr->type == RECORD_TYPE_TELEDATA) {
		return wialon_pack_teledata(buff, (teledata_record_t*)data);
	}
	else if (hr->type == RECORD_TYPE_FUEL) {
		uint8_t fsize = (hr->len - sizeof(head_record_t)) / sizeof(fuel_subrecord_t);
		return wialon_pack_fuel(buff, (fuel_record_t*)data, fsize);
	}
	else if (hr->type == RECORD_TYPE_CANLOG) {
		return wialon_pack_canlog(buff, (canlog_record_t*)data);
	}

	return 0;
}

static int authorise_pack_wialon(const char * imei, char *buff) {
	size_t len = sprintf(buff, "#L#2.0;%s;NA;", imei);
	uint16_t crc = crc16(buff + strlen("#L#"), len - strlen("#L#"), 0);
	sprintf(buff + len, "%X\r\n", crc);

	return strlen(buff);
}

static int authorise_response_decode_wialon(char * buff, uint32_t len) {
	char *ptr = strchr(buff, '\r');
	if (ptr) {
		*ptr = '\0';
	}

	if (strstr(buff, "#AL#1") == NULL) {
		ZF_LOGE("%s authorization error: \"%s\"", "Wialon", buff);
		return -1;
	}

	return 0;
}

static uint32_t get_and_encode_data_wialon(char *buff) {
	strcpy(buff, "#B#");
	int len = strlen(buff);
	int record_num = 0;

	for (; record_num < TRANSMIT_POINTS_NUM; record_num++) {
		int rec_len = flash_fifo_pop(&track_fifo, rec_buff);
		if (rec_len == 0) {
			break;
		}

		if (packet_validate(rec_buff, rec_len)) {
			len += wialon_add_record_to_packet(buff + len, rec_buff);
		}
		else {
			// Если запись на флэш не соответствует размеру record_t
			flash_fifo_erase_full_memory(&track_fifo);
			ZF_LOGW("Track data memory erased!");
			break;
		}
	}

	if (record_num) {
		user_printf(">>> Pop %d records\r\n", record_num);

		uint16_t crc = crc16(buff + strlen("#B#"), len - strlen("#B#"), 0);
		len += sprintf(buff + len, "%X\r\n", crc);
	}
	else {
		len = 0;
	}

	return len;
}

static int decode_received_data_wialon(char * buff, uint32_t len) {
	unsigned int ack;
	char *ptr = strchr(buff, '\r');
	if (ptr) {
		*ptr = '\0';
	}

	ZF_LOGI("Wialon received: %s", buff);

	// Ack
	if ( (user_sscanf(buff, "#AB#%d", &ack) == 1) && (ack != 0)) {
		return 0;
	}
	// Command
	else if (strstr(buff, HOST_COMMAND_ODOMETER_RESET) == buff) {
		odometer_reset();
		return 0;
	}
	else {
		ZF_LOGE("Wialon receive error: \"%s\"", buff);
		return -1;
	}
}

// ---------------------------------------------------------------------------

static uint32_t get_and_encode_track_data_from_flash(uint8_t *buff) {
	if (settings1_ram.protocol == PROTOCOL_EGTS) {
		return get_and_encode_data_egts(buff);
	}
	else {
		return get_and_encode_data_wialon((char*)buff);
	}
}

static int decode_received_data(uint8_t * buff, uint32_t len) {
	if (settings1_ram.protocol == PROTOCOL_EGTS) {
		return decode_received_data_egts(buff, len);
	}
	else {
		return decode_received_data_wialon((char*)buff, len);
	}
}

// ===========================================================================

void cloudSendTimerCallback(void const * argument) {
	UNUSED(argument);

	osSemaphoreRelease(cloudSendSemHandle);
}

// ---------------------------------------------------------------------------

int authorise_pack(const char * imei, uint8_t * buff) {
	if (settings1_ram.protocol == PROTOCOL_EGTS) {
		return authorise_pack_egts(imei, buff);
	}
	else {
		return authorise_pack_wialon(imei, (char*)buff);
	}
}

int authorise_response_decode(uint8_t * buff, uint32_t len) {
	if (settings1_ram.protocol == PROTOCOL_EGTS) {
		return authorise_response_decode_egts(buff, len);
	}
	else {
		return authorise_response_decode_wialon((char*)buff, len);
	}
}

// ===========================================================================

void cloud_task(void const * argument) {
	UNUSED(argument);

	osEvent evt;
	do {
		evt = osSignalWait(SIGNAL_START, osWaitForever);
	} while( !(evt.status == osEventSignal && evt.value.signals & SIGNAL_START) );

//	osTimerStart(cloudSendTimerHandle, settings_ram.period.data_send.in_standby_mode * 1000);

	while(1) {
		int len = cloud_receive(rx_buff, 1);
		if (len) {
			if (decode_received_data(rx_buff, len) != 0) {
				ZF_LOGE("Can't decode received data");
			}
		}

		osStatus status = osSemaphoreWait(cloudSendSemHandle, 1000);
		if (status != osOK && flash_fifo_isEmpty(&track_fifo)) {
			osSignalSet(monitorTaskHandle, SIGNAL_TASK_CLOUD);
			continue;
		}

		if (!cloud_cache.empty && cloud_cache.len != 0) {
			len = cloud_cache.len;
		}
		else {
			len = get_and_encode_track_data_from_flash(cloud_cache.txbuff);
		}

		if (len) {
			cloud_cache.empty = false;
			cloud_cache.len = len;
			cloud_cache_crc_update();

			if (cloud_send(cloud_cache.txbuff, len) == 0) {
				if (settings1_ram.main_host.wait_response) {
					len = cloud_receive(rx_buff, 10000);
					if (len) {
						if (decode_received_data(rx_buff, len) != 0) {
							ZF_LOGE("Can't decode received data");
						}
					}
					else {
						ZF_LOGE("No response from server");
					}
				}

				// Сбрасываем кеш не зависимо от ответа сервера, потому что он может не прийти
				// Так же в OBD
				cloud_cache.empty = true;
				cloud_cache.len = 0;
				cloud_cache_crc_update();
			}
		}

		osSignalSet(monitorTaskHandle, SIGNAL_TASK_CLOUD);
	}
}

