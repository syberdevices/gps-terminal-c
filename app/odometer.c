/*
 * odometer.c
 *
 *  Created on: 29 янв. 2019 г.
 *      Author: Denis Shreiber
 */

// C includes
#include <math.h>
// HAL includes
#include "rtos.h"
// User includes
#include "common.h"
#include "sim68/sim68.h"
#include "settings/settings.h"

// ==========================================================================================

static float pre_odometer = 0;

// ==========================================================================================

static void odometer_local_add(float odo) {
	if (coordinates_freezed) {
		// В покое не нужно менять одометр, поэтому перед обработкой устанавливаем предыдущее значение текущим
		pre_odometer = odo;
	}

	float diff = odo - pre_odometer;
	if (diff < 0) {
		diff = 0;
	}

	if (diff != 0) {
		cache.odometer.val += diff;
		cache_crc_update();
	}
	pre_odometer = odo;
}

static void odometer_trigger_poll() {
	if (fabsf(cache.odometer.val - cache.odometer.last_track_point) >= (float)settings1_ram.untracked_distance) {
		save_track_point_to_flash(SRC_ODOMETER);
	}
}

static int odometer_set(float val) {
	return sim68_set_odometer_value(val);
}

static int odometer_get(float *val) {
	//--------------------------
	//Эмуляция одометра
//	static float odo = 0;
//	odo = odo + 50.0f;
//	*val = odo;
//	return 0;
	//--------------------------

	*val = sim68_get_odometer_value();
	return val >= 0 ? 0 : -1;
}

// ==========================================================================================

int odometer_reset() {
	int res = -1;

	if (osRecursiveMutexWait(odometerRecursiveMutexHandle, 500) != osOK) {
		return res;
	}

	if (odometer_set(0) == 0) {

		float odo;
		if (odometer_get(&odo) == 0 && odo == 0) {
			DISABLE_IRQ_USER();
			cache.odometer.val = 0;
			cache.odometer.last_track_point = 0;
			ENABLE_IRQ_USER();
			cache_crc_update();
			pre_odometer = 0;

			odometer_flash_update(0);

			res = 0;
		}
	}

	osRecursiveMutexRelease(odometerRecursiveMutexHandle);

	return res;
}

static void odometer_flash_update_poll() {
	static uint32_t ticks = 0;
	uint32_t curr_ticks = HAL_GetTick();
	bool must_save = false;

	// Сохраняем каждые 35 км (10430 записей в год при пробеге 1000 км/день, хватит на 9,5 лет)
	if (cache.odometer.val - cache.odometer.last_saved > 35000) {
		must_save = true;
	}

	// Сохраняем каждые 3 часа (2920 записей в год, хватит на 34 года)
	if (curr_ticks - ticks > 3*60*60*1000) {
		if (cache.odometer.val - cache.odometer.last_saved > 20) {
			// Только если есть изменения одометра в 20м, иначе нет смысла сохранять
			must_save = true;
		}
	}

	if (must_save) {
		ticks = curr_ticks;
		cache.odometer.last_saved = cache.odometer.val;
		cache_crc_update();
		odometer_flash_update(cache.odometer.val);
	}
}

// ======================================================================

void odometer_poll() {
	static uint32_t odometer_poll_cnt = 0;

	if (++odometer_poll_cnt >= 2) {	// Раз в 2 сек запрашиваем одометр
		odometer_poll_cnt = 0;

		float odo;
		if (odometer_get(&odo) == 0 && odo > pre_odometer && board_status.gnss.fix != GNSS_FIX_NOT_AVAILABLE/* && gnss_data.tracked.speed >= settings_ram.gnss.freeze_speed*/) {
			odometer_local_add(odo);
			odometer_trigger_poll();
		}
	}

	odometer_flash_update_poll();
}

void odometer_last_point_updadte() {
	cache.odometer.last_track_point = cache.odometer.val;
	cache_crc_update();
}

void odometer_flash_update_direct(float odo) {
	if (check_settings_locked()) {
		return;
	}

	check_settings_lock();

		if (settings1_ram.rom.odometer != odo) {
			settings1_ram.rom.odometer = odo;
			write_settings(SETT_AREA_EXT_INT_FLASH);
		}
		
	check_settings_unlock();
}

void odometer_flash_update(float odo) {
	if (osRecursiveMutexWait(odometerRecursiveMutexHandle, 500) != osOK) {
		return;
	}

	odometer_flash_update_direct(odo);

	osRecursiveMutexRelease(odometerRecursiveMutexHandle);
}

