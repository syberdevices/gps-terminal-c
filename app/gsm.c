/*
 * gsm.c
 *
 *  Created on: 25 сент. 2018 г.
 *      Author: Denis Shreiber
 */

// C includes
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
// HAL includes
#include "rtos.h"
#include "usart.h"
// User includes
#include "sim800/sim800.h"
#include "ftp/ftp.h"
#include "log/log.h"
#include "log/flash_fifo/flash_fifo.h"
#include "settings/settings.h"
#include "egts/egts.h"

//========================================================

#define TCP_SEND_DIRECT				0
#define TCP_RECEIVE_DIRECT			0

#define MAIN_HOST_CON_HANDLE		0
#define LOG_HOST_CON_HANDLE			1

//========================================================

extern int authorise_pack(const char * imei, uint8_t * buff);
extern int authorise_response_decode(uint8_t * buff, uint32_t len);

//========================================================

typedef enum GSM_State_t {
	MODULE_STATE_HW_RESET,
	MODULE_STATE_INIT,
	MODULE_STATE_SIM_DETECT,
	MODULE_STATE_SMS_CONFIG,
	MODULE_STATE_NETWORK_REG,
	MODULE_STATE_GPRS_CONN,
	MODULE_STATE_WAIT_IP,
	MODULE_STATE_SERVER_CONN,
	MODULE_STATE_SERVER_DISCONN,
//	MODULE_STATE_COLLECTION_DATA,
	MODULE_STATE_LOOP,
//	MODULE_STATE_PING,
	MODULE_STATE_FTP,

	MODULE_STATE_NUM,
} GSM_State_t;

static const char * gsm_state_str[MODULE_STATE_NUM] = {
	"HW_RESET",
	"INIT",
	"SIM_DETECT",
	"SMS_CONFIG",
	"NETWORK_REG",
	"GPRS_CONN",
	"WAIT_IP",
	"SERVER_CONN",
	"SERVER_DISCONN",
//	"COLLECTION_DATA",
	"LOOP",
//	"PING",
	"FTP",
};

static const char * gsm_state_to_str(GSM_State_t state) {
	if (state >= MODULE_STATE_NUM) {
		ZF_LOGE("Invalid parameter");
		return NULL;
	}
	else {
		return gsm_state_str[state];
	}
}

typedef enum bluetooth_state_t {
	BLUETOOTH_STATE_POWER_OFF,
	BLUETOOTH_STATE_RESET,
	BLUETOOTH_STATE_SCANNING,
	BLUETOOTH_STATE_STATUS_REQUEST,
	BLUETOOTH_STATE_PAIRING,
	BLUETOOTH_STATE_PAIRED,
	BLUETOOTH_STATE_CONNECTED,
	BLUETOOTH_STATE_UNPAIR,
} bluetooth_state_t;

typedef enum ftp_file_type_t {
	FTP_FILE_NONE,
	FTP_FILE_FIRMWARE,
	FTP_FILE_SETTINGS
} ftp_file_type_t;

typedef enum operator_t {
	OPERATOR_TELE2,
	OPERATOR_MTS,
	OPERATOR_MEGAFON,
	OPERATOR_BEELINE,
	OPERATOR_EMT_GOODLINE,
	OPERATOR_UNKNOWN,

	OPERATOR_NUM
} operator_t;

//========================================================

static GSM_State_t module_state = MODULE_STATE_HW_RESET;
static bluetooth_state_t bluetooth_state = BLUETOOTH_STATE_RESET;

// При загрузке первым делом идем на FTP
static volatile bool ftp_job_request = true;
static int ftp_job_req_src = FTP_JOB_REQ_SRC_BOOT;

static uint32_t cnt_loops = 0;
static uint8_t buff[CLOUD_CACHE_BUFF_SIZE] = {0};
static char buff_log[1024] = {0};
static char rec_sms_phone_number[16];
static char ftp_file_name[24];
static ftp_file_type_t ftp_file_type = FTP_FILE_NONE;
static char imsi[16] = {0};
static operator_t operator = OPERATOR_UNKNOWN;
static apn_t known_apns[OPERATOR_NUM] = {
		{ "internet.tele2.ru", NULL, NULL },				// TELE2
		{ "internet.mts.ru", "mts", "mts" },				// MTS
		{ "internet", NULL, NULL },							// MegaFon
		{ "internet.beeline.ru", "beeline", "beeline" },	// BeeLine
		{ "internet.emt.ee", NULL, NULL },					// EMT Гудлайн
		{ "CMNET", NULL, NULL }								// Unknown. SIMCom default
};
static apn_t apn = {NULL, NULL, NULL};

const static char * const fixed_master_phone_number[FIXED_MASTER_PHONE_NUM] = {
		"79609730044",
		"79095435007",
		"79095434413",
		"79138506007",
		"",
		"",
		""
};

//--------------------------------------------------------

char imei[16] = {0};

//========================================================


#if TCP_SEND_DIRECT || TCP_RECEIVE_DIRECT
static int to_direct() {
	uint8_t buff[16];

	// Отключаем эхо, чтобы не мешало
	sim800_send_at("ATE0\r", NULL, 1, 500);
	uart_rx_str(buff, "ATE0\r", 100);
	uart_rx_str(buff, "\r\nOK\r\n", 100);

	// Отключаем прерывания UART и далее общаемся с модулем напрямую
	HAL_UART_Abort_IT(&huart2);

	uart_tx_str("AT\r");
	uart_rx_str(buff, "\r\nOK\r\n", 100);

	return 0;
}

static int to_atcommander() {
	uint8_t buff[16];

	// Возвращаем эхо
	uart_tx_str("ATE1\r");
	uart_rx_str(buff, "\r\nOK\r\n", 100);

	sim800_start_rx_it();

	return 0;
}
#endif

#if TCP_SEND_DIRECT
static int tcp_send_direct(const void *data, uint32_t len, uint32_t timeout) {
	to_direct();

	bool send_ok_received = false;
	char * pbuff = (char*)ftp_buffer;
	int line = 0;
	char cmd[48];
	pbuff[0] = 0;

	// ---------------------------

	// Делаем CIPSEND
	sprintf(cmd, "AT+CIPSEND=%u\r", (unsigned int)len);
	uart_tx_str(cmd);

	bool ready = false;
	int _timeout = 5000;

	while(!ready && _timeout > 0) {
		uint32_t time = HAL_GetTick();

		if (uart_rx_data((uint8_t*)&pbuff[0], 1, 1000) != 0) { line = __LINE__; goto exit; }
		if (pbuff[0] == '>') {
			if (uart_rx_str((uint8_t*)pbuff, " ", 200) != 0) { line = __LINE__; goto exit; }
			ready = true;;
		}

		time = HAL_GetTick() - time;
		_timeout -= time;

		WDG_RESET(TASK_GSM);
	}

//	if (uart_rx_str((uint8_t*)pbuff, "\r\n> ", 1000) != 0) { line = __LINE__; goto error; }
	if (uart_tx_data(data, len) != 0) { line = __LINE__; goto exit; }

	// ---------------------------

	// Ожидаем "SEND OK"
	_timeout = timeout;

	while(!send_ok_received && _timeout > 0) {
		uint32_t time = HAL_GetTick();

		// Ожидаем '\r\n' (начало сообщения)
		if (uart_rx_str((uint8_t*)pbuff, "\r\n", _timeout) != 0) { line = __LINE__; goto exit; }

		// Принимаем, пока не придет '\n' (конец сообщения)
		uint8_t cnt = 0;
		bzero(pbuff, 16);	// Зануляем просто первые несколько байт

		do {
			if (uart_rx_data((uint8_t*)&pbuff[cnt++], 1, 200) != 0) { line = __LINE__; goto exit; }
			// Условимся на максимальном размере 128, хотя сообщение GPS имеет длину 94 байта без учета \r\n
			if (cnt > 128) { line = __LINE__; goto exit; }
		} while (pbuff[cnt-1] != '\n');

		pbuff[cnt-2] = '\0';

		if (strstr(pbuff, "SEND OK")){
			send_ok_received = true;
		}
		else if (strstr(pbuff, "+PDP") || strstr(pbuff, "CLOSED") || strstr(pbuff, "ERROR")){
			ZF_LOGE("+PDP or CLOSED");
			break;
		}

		time = HAL_GetTick() - time;
		_timeout -= time;

		WDG_RESET(TASK_GSM);
	}

	exit:

	to_atcommander();

	if (send_ok_received) {
		ZF_LOGI("TCP send Success");
		return 0;
	}
	else {
		ZF_LOGE("Error during TCP transaction. Line %d", line);
		return -1;
	}
}
#else
#if SIM800_USE_MULTICONNECT
static int tcp_send_at(uint8_t con_id, const void *data, int len, uint32_t timeout) {
#else
static int tcp_send_at(const void *data, int len, uint32_t timeout) {
#endif
	unsigned int cipsend_max_len = 1024;

	if (con_id == 0) {
		ATCommander_response_t *rsp = sim800_send_at("AT+CIPSEND?\r", "+CIPSEND", 2, 500);
		if ( !(user_sscanf(rsp[0].data, "+CIPSEND: 0,%u", &cipsend_max_len) == 1) && (cipsend_max_len != 0) ) {
			ZF_LOGE("%s -> %s", "AT+CIPSEND?", rsp[0].data);
			return -1;
		}
		osDelay(100);
	}

	uint8_t *ptr = (uint8_t*)data;

	while (len > 0) {
		uint32_t _len = len > cipsend_max_len ? cipsend_max_len : len;

		if (sim800_tcp_send(con_id, ptr, _len, timeout) == -1) {
			ZF_LOGE("TCP send Error");
			return -1;
		}

		len -= _len;
		ptr += _len;
	}

	ZF_LOGI("TCP send Success");

	return 0;
}
#endif

#if TCP_RECEIVE_DIRECT
static int tcp_receive_direct(void *pdata, uint32_t timeout) {
	to_direct();

	int line = 0;
	unsigned int len = 0;
	bool data_received = false;
	char* data = pdata;

	while(!data_received && timeout > 0) {
		uint32_t time = HAL_GetTick();

		// Ожидаем '\r\n' (начало сообщения)
		if (uart_rx_str((uint8_t*)data, "\r\n", timeout) != 0) { line = __LINE__; goto error; }

		// Принимаем, пока не придет '\n' (конец сообщения)
		uint32_t cnt = 0;
		uint32_t true_cnt = 0;
		bzero(data, 16);	// Зануляем просто первые несколько байт

		do {
			if (uart_rx_data((uint8_t*)&data[cnt++], 1, 200) != 0) { line = __LINE__; goto error; }
			if (cnt > CLOUD_TX_QUEUE_SIZE) { line = __LINE__; goto error; }

			if (!data_received) {
				if (data[cnt-1] == ':' && strstr(data, "+IPD")) {
					if (user_sscanf(data, "+IPD,%u:", &len) == 1) {
						data_received = true;
					}
					else {
						ZF_LOGE("Broken IPD data");
						break;
					}
				}
			}
			else {
				if (++true_cnt == len) {
					// Закончили принимать полезные данные
					break;
				}
			}
		} while (data[cnt-1] != '\n');

		if (true_cnt != len) {
			len = 0;
		}

		if (data_received){

		}
		else if (strstr(data, "+PDP") || strstr(data, "CLOSED") || strstr(data, "ERROR")){
			ZF_LOGE("Connection Error");
			break;
		}

		time = HAL_GetTick() - time;
		timeout -= time;

		WDG_RESET(TASK_GSM);
	}

	if (!data_received) {
		if (timeout <= 0) {
			ZF_LOGE("TCP receive timeout");
		}

		to_atcommander();
		return 0;
	}

	// ---------------------------

	char * ptr = strchr(data, ':');
	if (!ptr) {
		ZF_LOGE("Broken IPD data");
		to_atcommander();
		return 0;
	}

	memcpy(data, ++ptr, len);
	data[len] = '\0';

	user_printf("%s\r\n", data);

	to_atcommander();
	return len;

	error:
	ZF_LOGE("Error during TCP transaction. Line %d", line);
	to_atcommander();

	return 0;
}
#else

#if SIM800_USE_MULTICONNECT

int tcp_receive_at(uint8_t con_id, void *pdata, uint32_t timeout) {
#else
int tcp_receive_at(void *pdata, uint32_t timeout) {
#endif
	int len = 0;

	while (!len && timeout-- > 0) {
		osDelay(1);
#if SIM800_USE_MULTICONNECT
		len = sim800_tcp_queue_pop(con_id, pdata);
#else
		len = sim800_tcp_queue_pop(pdata);
#endif
	}

	return len;
}

#endif

#if SIM800_USE_MULTICONNECT

int tcp_send(uint8_t con_id, const void *data, uint32_t len, int timeout) {
#if TCP_SEND_DIRECT
	return tcp_send_direct(con_id, data, len, timeout);
#else
	return tcp_send_at(con_id, data, len, timeout);
#endif
}

int tcp_receive(uint8_t con_id, void *pdata, uint32_t timeout) {
#if TCP_RECEIVE_DIRECT
	return tcp_receive_direct(con_id, pdata, timeout);
#else
	return tcp_receive_at(con_id, pdata, timeout);
#endif
}

#else

int tcp_send(const void *data, uint32_t len, int timeout) {
#if TCP_SEND_DIRECT
	return tcp_send_direct(data, len, timeout);
#else
	return tcp_send_at(data, len, timeout);
#endif
}

int tcp_receive(void *pdata, uint32_t timeout) {
#if TCP_RECEIVE_DIRECT
	return tcp_receive_direct(pdata, timeout);
#else
	return tcp_receive_at(pdata, timeout);
#endif
}

#endif


static uint32_t cloud_tx_queue_pop(uint8_t *buff) {
	uint32_t len = 0;

	if (osMessagePeek(cloudTxQueueHandle, 1).status == osEventMessage) {	// Ждем, пока появятся данные в очереди
		osEvent evt = osMessageGet(cloudTxQueueHandle, 1);
		if (evt.status == osEventMessage) {
			cloud_data_t *cloud_data = evt.value.p;
			if (cloud_data) {
				len = cloud_data->len;
				memcpy(buff, cloud_data->data, len);

				vPortFree(cloud_data);
			}
		}
	}

	return len;
}

static int cloud_rx_queue_put(uint8_t *data, uint32_t len) {
	if (len == 0) {	// Нечего отправлять
		return 0;
	}

	if (osMessageWaiting(cloudRxQueueHandle) != 0) {	// Очередь не пуста
		return -1;
	}

	cloud_data_t *cloud_data = pvPortMalloc(sizeof(len) + len);
	if (cloud_data == NULL) {
		return -1;
	}

	cloud_data->len = len;
	memcpy(cloud_data->data, data, len);
	if (osMessagePut(cloudRxQueueHandle, (uint32_t)cloud_data, 0) != osOK) {
		vPortFree(cloud_data);
		return -1;
	}

	return 0;
}

//--------------------------------------------------------

static void rssi_update() {
	if (board_status.gsm.phy) {
		sim800_gsm_signal_strength_t *signal_strength = sim800_gsm_get_signal_strength();
		if (signal_strength) {
			board_status.gsm.signal_strength.rssi = signal_strength->rssi;
			board_status.gsm.signal_strength.percent = signal_strength->percent;
		}
	}
	else {
		board_status.gsm.signal_strength.rssi = 0;
		board_status.gsm.signal_strength.percent = 0;
	}
}

static int white_number_set(const char *new_number, uint8_t index) {
	char cmd[48];
	sprintf(cmd, "AT+CWHITELIST=3,%d,%s\r", (int)index, new_number);

	ATCommander_response_t *rsp = sim800_send_at(cmd, NULL, 1, 500);
	if(!strstr(rsp[0].data, "OK")) {
		ZF_LOGE("White number set error (%s)", rsp[0].data);
		return -1;
	}

	return 0;
}

static int white_number_compare_and_set(const char *current_number, const char *new_number, uint8_t index) {
	const char * _new_number = (new_number == NULL || strlen(new_number) == 0) ? "000" : new_number;

	if (strcmp(current_number, _new_number) != 0) {
		return white_number_set(_new_number, index);
	}

	return 0;
}

static int white_list_config() {
	// Включаем white list
	ATCommander_response_t *rsp = sim800_send_at("AT+CWHITELIST=3\r", NULL, 1, 500);
	if(!strstr(rsp[0].data, "OK")) {
		ZF_LOGE("%s -> %s", "AT+CWHITELIST=3", rsp[0].data);
		return -1;
	}

	// Запрашиваем white list
	rsp = sim800_send_at("AT+CWHITELIST?\r", "+CWHITELIST: ", 2, 500);
	if(!strstr(rsp[1].data, "OK")) {
		ZF_LOGE("%s -> %s", "AT+CWHITELIST?", rsp[0].data);
		return -1;
	}

	char str[512];
	char *ptr = str;
	strcpy(str, rsp[0].data);

	int whitelist_cnt = 0;

	// Проверяем жесткие мастер номера (1-7)

	for (int q = 0; q < FIXED_MASTER_PHONE_NUM; q++) {
		whitelist_cnt++;

		ptr = strstr(ptr, ",\"");
		if (ptr) {
			ptr += 2;

			char current_number[21] = {0};
			user_sscanf(ptr, "%[^\"]", current_number);

			if (white_number_compare_and_set(current_number, fixed_master_phone_number[q], whitelist_cnt) != 0) {
				ZF_LOGE("Can't set master number");
				return -1;
			}
		}
		else {
			ZF_LOGE("White list parse error");
			return -1;
		}
	}

	// Проверяем пользовательские мастер номера (8-12)

	for (int q = 0; q < USER_MASTER_PHONE_NUM; q++) {
		whitelist_cnt++;

		ptr = strstr(ptr, ",\"");
		if (ptr) {
			ptr += 2;

			char current_number[21] = {0};
			user_sscanf(ptr, "%[^\"]", current_number);

			if (white_number_compare_and_set(current_number, settings4_ram.user_master_phones[q].number, whitelist_cnt) != 0) {
				ZF_LOGE("Can't set master number");
				return -1;
			}
		}
		else {
			ZF_LOGE("White list parse error");
			return -1;
		}
	}

	// Проверяем простые белые номера (13-30)

	for (int q = 0; q < WHITE_PHONE_NUM; q++) {
		whitelist_cnt++;

		ptr = strstr(ptr, ",\"");
		if (ptr) {
			ptr += 2;

			char current_number[21] = {0};
			user_sscanf(ptr, "%[^\"]", current_number);

			const char * new_number = white_number_get_sett_ram_by_index(q);

			if (white_number_compare_and_set(current_number, new_number, whitelist_cnt) != 0) {
				ZF_LOGE("Can't set master number");
				return -1;
			}
		}
		else {
			ZF_LOGE("White list parse error");
			return -1;
		}
	}

	return 0;
}

//static int send_sms_direct(char *phone_number, char *text) {
//	char cmd[24];
//	sprintf(cmd, "AT+CMGS=\"%s\"\r", phone_number);
//	sim800_direct_uart_tx_str(cmd);
//	HAL_Delay(500);
//	sim800_direct_uart_tx_str(text);
//	HAL_Delay(2000);
//
//	return 0;
//}

/**
 * Скопировать строку до символа chr, не более длины num
 * @return Длина строки, 0 - при ошибке
 */
int strnchrcpy(char *to_str, char *from_str, char chr, int num) {
	if (!to_str || !from_str) {
		return 0;
	}

	char *ptr = strchr(from_str, chr);
	if (!ptr) {
		return 0;
	}

	int len = ptr - from_str;
	if (len > num) {
		return 0;
	}

	memcpy(to_str, from_str, len);
	to_str[len] = '\0';

	return len;
}

/**
 * Проверяет, что строка состоит только из цифр
 */
static bool str_isdigit(const char * str) {
	int len = strlen(str);

	for (int q = 0; q < len; q++) {
		if (!isdigit(str[q])) {
			return false;
		}
	}

	return true;
}

static bool is_master_number(const char *number) {
	for (int q = 0; q < FIXED_MASTER_PHONE_NUM; q++) {
		if (strlen(fixed_master_phone_number[q]) != 0) {
			if (strstr(number, fixed_master_phone_number[q])) {
				return true;
			}
		}
	}

	for (int q = 0; q < USER_MASTER_PHONE_NUM; q++) {
		if (strlen(settings4_ram.user_master_phones[q].number) != 0) {
			if (strstr(number, settings4_ram.user_master_phones[q].number)) {
				return true;
			}
		}
	}

	return false;
}

static int loglevel_zf_to_sms_format(int zf_loglevel, int *sms_loglevel) {
	int ret = 0;

	switch (zf_loglevel) {
		case ZF_LOG_VERBOSE:
			*sms_loglevel = 6;
			break;
		case ZF_LOG_DEBUG:
			*sms_loglevel = 5;
			break;
		case ZF_LOG_INFO:
			*sms_loglevel = 4;
			break;
		case ZF_LOG_WARN:
			*sms_loglevel = 3;
			break;
		case ZF_LOG_ERROR:
			*sms_loglevel = 2;
			break;
		case ZF_LOG_FATAL:
			*sms_loglevel = 1;
			break;
		case ZF_LOG_NONE:
			*sms_loglevel = 0;
			break;
		default:
			ret = -1;
			break;
	}

	return ret;
}

static int loglevel_sms_to_zf_format(int sms_loglevel, int *zf_loglevel) {
	int ret = 0;

	switch (sms_loglevel) {
		case 6:
			*zf_loglevel = ZF_LOG_VERBOSE;
			break;
		case 5:
			*zf_loglevel = ZF_LOG_DEBUG;
			break;
		case 4:
			*zf_loglevel = ZF_LOG_INFO;
			break;
		case 3:
			*zf_loglevel = ZF_LOG_WARN;
			break;
		case 2:
			*zf_loglevel = ZF_LOG_ERROR;
			break;
		case 1:
			*zf_loglevel = ZF_LOG_FATAL;
			break;
		case 0:
			*zf_loglevel = ZF_LOG_NONE;
			break;
		default:
			ret = -1;
			break;
	}

	return ret;
}

static void sms_poll() {
	const static int SMS_MAX_LEN = 160;

	if (module_state > MODULE_STATE_NETWORK_REG) {
		// Читаем смс
		ATCommander_response_t *rsp = sim800_send_at("AT+CMGR=1\r", "+CMGR", 3, 1000);
		WDG_RESET(TASK_GSM);

		if (strstr(rsp[2].data, "OK")) {
			// Запоминаем номер, с которого пришло смс
			char *ptr_start = strstr(rsp[0].data, ",\"") + 2;
			char *ptr_end = strstr(ptr_start, "\",");
			bzero(rec_sms_phone_number, sizeof(rec_sms_phone_number));
			memcpy(rec_sms_phone_number, ptr_start, ptr_end - ptr_start);

			// Кэшируем текст смс и удаляем смс
			char sms_text[SMS_MAX_LEN];		
			strncpy(sms_text, rsp[1].data, sizeof(sms_text));

			ZF_LOGW("SMS received: %s", sms_text);

			rsp = sim800_send_at("AT+CMGDA=\"DEL ALL\"\r", NULL, 1, 5000);
			WDG_RESET(TASK_GSM);

			if(strstr(rsp[0].data, "OK") == NULL) {
				module_state = MODULE_STATE_HW_RESET;
				ZF_LOGE("%s -> %s", "AT+CMGDA=\"DEL ALL\"", rsp[0].data);
				ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
			}

			// Проверяем, разрешен ли этот номер
			// для доступа к настройкам (мастер-номер)

			if (!is_master_number(rec_sms_phone_number)) {
				ZF_LOGW("SMS config from number %s denied", rec_sms_phone_number);
				sim800_send_sms(rec_sms_phone_number, "Access denied\x1A");
				return;
			}

			// Далее парсим текст смс

			// Принудительное обновление
			if (strstr(sms_text, "Update") == sms_text) {
				ftp_job_request = true;
				ftp_job_req_src = FTP_JOB_REQ_SRC_SMS_UPDATE;
				osSemaphoreRelease(gsmWakeupSemHandle);
			}

			// Файл прошивки
			else if (strstr(sms_text, "Firmware:") == sms_text) {
				strncpy(ftp_file_name, sms_text + strlen("Firmware:"), sizeof(ftp_file_name));
				ftp_file_type = FTP_FILE_FIRMWARE;
				ftp_job_request = true;
				ftp_job_req_src = FTP_JOB_REQ_SRC_SMS_FIRMWARE;
				osSemaphoreRelease(gsmWakeupSemHandle);
			}
			
			// Файл настроек
			else if (strstr(sms_text, "Settings:") == sms_text) {
				strncpy(ftp_file_name, sms_text + strlen("Settings:"), sizeof(ftp_file_name));
				ftp_file_type = FTP_FILE_SETTINGS;
				osSemaphoreRelease(gsmWakeupSemHandle);
			}

			// FTP Хост
			else if (strstr(sms_text, "Ftp host=") == sms_text) {
				ftp_host_sett_t ftp_host;
				char *pstart = sms_text + strlen("Ftp host=");
				char *pend = strchr(pstart, ',');
				size_t len = pend - pstart;

				if (pend && len < sizeof(settings1_ram.ftp_host.address) - 1) {
					memcpy(ftp_host.address, pstart, len);
					ftp_host.address[len] = '\0';

					pstart = pend + 1;
					pend = strchr(pstart, ',');
					len = pend - pstart;

					if (pend && len < sizeof(settings1_ram.ftp_host.port) - 1) {
						memcpy(ftp_host.port, pstart, len);
						ftp_host.port[len] = '\0';

						pstart = pend + 1;
						pend = strchr(pstart, ',');
						len = pend - pstart;

						if (pend && len < sizeof(settings1_ram.ftp_host.login) - 1) {
							memcpy(ftp_host.login, pstart, len);
							ftp_host.login[len] = '\0';

							pstart = pend + 1;
							pend = sms_text + strlen(sms_text);
							len = pend - pstart;
							if (len < sizeof(settings1_ram.ftp_host.password) - 1) {
								memcpy(ftp_host.password, pstart, len);
								ftp_host.password[len] = '\0';

								memcpy(&settings1_ram.ftp_host, &ftp_host, sizeof(ftp_host_sett_t));

								if (write_settings(SETT_AREA_EXT_INT_FLASH) == 0) {
									WDG_RESET(TASK_GSM);
									sim800_send_sms(rec_sms_phone_number, "FTP host changed. Reboot\x1A");
									WDG_RESET(TASK_GSM);

									system_reset("SMS host changed. Reboot");
								}
							}
						}
					}
				}

				ZF_LOGE("SMS parse error");

				WDG_RESET(TASK_GSM);
				sim800_send_sms(rec_sms_phone_number, "Error\x1A");
				WDG_RESET(TASK_GSM);

				rec_sms_phone_number[0] = '\0';
			}

			// Сервер мониторинга
			else if (strstr(sms_text, "Server?") == sms_text) {
				// “<protocol>,<address>,<port>,<wait respond>”

				sprintf(sms_text, "%s,%s,%d,%s\x1A",
									settings1_ram.protocol == PROTOCOL_EGTS ? "EGTS" : "IPS",
									settings1_ram.main_host.address,
									(int)settings1_ram.main_host.port,
									settings1_ram.main_host.wait_response ? "on" : "off");
				
				sim800_send_sms(rec_sms_phone_number, sms_text);

				rec_sms_phone_number[0] = '\0';
			}
			else if (strstr(sms_text, "Server=") == sms_text) {
				// “Server=<protocol>,<address>,<port>,<wait respond>”

				main_host_sett_t main_host;
				char *pstart = sms_text + strlen("Server=");
				char *pend = strchr(pstart, ',');
				size_t len = pend - pstart;

				if (pend) {
					bool found = false;
					protocol_t protocol;

					if (strstr(pstart, "EGTS") == pstart) {
						protocol = PROTOCOL_EGTS;
						found = true;
					}
					else if (strstr(pstart, "IPS") == pstart) {
						protocol = PROTOCOL_WIALON;
						found = true;
					}

					if (found) {
						pstart = pend + 1;
						pend = strchr(pstart, ',');
						len = pend - pstart;

						if (pend && len < sizeof(main_host.address) - 1) {
							memcpy(main_host.address, pstart, len);
							main_host.address[len] = '\0';

							pstart = pend + 1;
							pend = strchr(pstart, ',');
							len = pend - pstart;

							int port;

							if (pend && user_sscanf(pstart, "%d", &port) == 1) {
								main_host.port = port;

								pstart = pend + 1;

								found = false;

								if (strstr(pstart, "on") == pstart) {
									main_host.wait_response = true;
									found = true;
								}
								else if (strstr(pstart, "off") == pstart) {
									main_host.wait_response = false;
									found = true;
								}

								if (found) {
									settings1_ram.main_host = main_host;
									settings1_ram.protocol = protocol;

									if (write_settings(SETT_AREA_EXT_INT_FLASH) == 0) {
										WDG_RESET(TASK_GSM);
										sim800_send_sms(rec_sms_phone_number, "Server host changed. Reboot\x1A");
										WDG_RESET(TASK_GSM);

										system_reset("SMS host changed. Reboot");
									}
								}
							}
						}
					}
				}

				ZF_LOGE("SMS parse error");

				WDG_RESET(TASK_GSM);
				sim800_send_sms(rec_sms_phone_number, "Error\x1A");
				WDG_RESET(TASK_GSM);

				rec_sms_phone_number[0] = '\0';
			}

			// Перезагрузка
			else if (strstr(sms_text, "Reboot") == sms_text) {
				WDG_RESET(TASK_GSM);
				sim800_send_sms(rec_sms_phone_number, "OK\x1A");

				osDelay(2000);
				system_reset("SMS Reboot");
			}

			// APN
			else if (strstr(sms_text, "Apn?") == sms_text) {
				sprintf(sms_text, "%s;%s,%s,%s\x1A",
						settings1_ram.apn.address[0] == '\0' ? "auto" : "manual", settings1_ram.apn.address, settings1_ram.apn.user, settings1_ram.apn.password);
				sim800_send_sms(rec_sms_phone_number, sms_text);
			}
			else if (strstr(sms_text, "Apn=") == sms_text) {
				bool address_only = false;
				bool save = false;
				apn_sett_t apn;

				char *pstart = sms_text + strlen("Apn=");
				char *pend = strchr(pstart, ',');
				if (pend == NULL) {
					// нет данных о юзере и пароле
					address_only = true;
					pend = pstart + strlen(pstart);
				}

				size_t len = pend - pstart;

				if (pend && len < sizeof(settings1_ram.apn.address)) {
					memcpy(apn.address, pstart, len);
					apn.address[len] = '\0';

					if (address_only) {
						bzero(apn.user, sizeof(apn.user));
						bzero(apn.password, sizeof(apn.password));

						save = true;
					}
					else {
						pstart = pend + 1;
						pend = strchr(pstart, ',');
						len = pend - pstart;

						if (pend && len < sizeof(settings1_ram.apn.user)) {
							memcpy(apn.user, pstart, len);
							apn.user[len] = '\0';

							pstart = pend + 1;
							pend = sms_text + strlen(sms_text);
							len = pend - pstart;
							if (len < sizeof(settings1_ram.apn.password)) {
								memcpy(apn.password, pstart, len);
								apn.password[len] = '\0';

								save = true;
							}
						}
					}

					if (save) {
						memcpy(&settings1_ram.apn, &apn, sizeof(apn_sett_t));

						if (write_settings(SETT_AREA_EXT_INT_FLASH) == 0) {
							WDG_RESET(TASK_GSM);
							sim800_send_sms(rec_sms_phone_number, "APN changed. Reboot\x1A");
							WDG_RESET(TASK_GSM);

							system_reset("APN changed. Reboot");
						}
					}
				}
			}

			// Filter
			else if (strstr(sms_text, "Filter?") == sms_text) {
				sprintf(sms_text, "%s;fs-%d;ft-%d;hdop-%d.%d;sm-%d;ms-%d;savg-%d\x1A",
						settings1_ram.gnss.filter_on ? "on" : "off", settings1_ram.gnss.freeze_speed, settings1_ram.gnss.freeze_time,
						(int)settings1_ram.gnss.hdop_max, (int)(settings1_ram.gnss.hdop_max * 10) % 10,
						settings1_ram.gnss.satellites_min, settings1_ram.gnss.max_speed, settings1_ram.gnss.speed_avg);
				sim800_send_sms(rec_sms_phone_number, sms_text);
			}
			else if (strstr(sms_text, "Filter=") == sms_text) {
				char str[4];
				bool ok = false;
				int fs, ft, sm, ms, savg;
				float hdop;
				bool filter_on;
				settings1_t sett_tmp;

				memcpy(&sett_tmp, &settings1_ram, sizeof(settings1_ram));

				do {
					char *ptr = sms_text + strlen("Filter=");

					int len = strnchrcpy(str, ptr, ';', 3);
					if (len > 0) {
						if (strstr(str, "on")) {
							filter_on = true;
						}
						else if (strstr(str, "off")) {
							filter_on = false;
						}
						else {
							break;
						}

						ptr += (len + 1);

						int assignments = user_sscanf(ptr, "fs-%d;ft-%d;hdop-%f;sm-%d;ms-%d;savg-%d",
								&fs, &ft, &hdop, &sm, &ms, &savg);

						if (assignments == 6) {
							settings1_ram.gnss.filter_on = filter_on;
							settings1_ram.gnss.freeze_speed = fs;
							settings1_ram.gnss.freeze_time = ft;
							settings1_ram.gnss.hdop_max = hdop;
							settings1_ram.gnss.satellites_min = sm;
							settings1_ram.gnss.max_speed = ms;
							settings1_ram.gnss.speed_avg = savg;

							ok = true;
						}
					}
				} while(0);

				if (ok && write_settings(SETT_AREA_EXT_INT_FLASH) == 0) {
					sim800_send_sms(rec_sms_phone_number, "Ok\x1A");

					system_reset("Filter changed. Reboot");
				}
				else {
					// Восстанавливаем настройки
					memcpy(&settings1_ram, &sett_tmp, sizeof(settings1_ram));

					sim800_send_sms(rec_sms_phone_number, "Error\x1A");
				}
			}

			// Track
			else if (strstr(sms_text, "Track?") == sms_text) {
				sprintf(sms_text, "mrssi-%d;crs-%d;dst-%d;accth-%d\x1A",
						settings1_ram.min_rssi, settings1_ram.untracked_course, settings1_ram.untracked_distance, settings1_ram.acceleration_threshold);
				sim800_send_sms(rec_sms_phone_number, sms_text);
			}
			else if (strstr(sms_text, "Track=") == sms_text) {
				int mrssi, crs, dst, accth;
				settings1_t sett_tmp;

				memcpy(&sett_tmp, &settings1_ram, sizeof(settings1_ram));

				char *ptr = sms_text + strlen("Track=");

				int assignments = user_sscanf(ptr, "mrssi-%d;crs-%d;dst-%d;accth-%d",
						&mrssi, &crs, &dst, &accth);

				if (assignments == 4) {
					settings1_ram.min_rssi = mrssi;
					settings1_ram.untracked_course = crs;
					settings1_ram.untracked_distance = dst;
					settings1_ram.acceleration_threshold = accth;
				}

				if (assignments == 4 && write_settings(SETT_AREA_EXT_INT_FLASH) == 0) {
					sim800_send_sms(rec_sms_phone_number, "Ok\x1A");

					system_reset("Track changed. Reboot");
				}
				else {
					// Восстанавливаем настройки
					memcpy(&settings1_ram, &sett_tmp, sizeof(settings1_ram));

					sim800_send_sms(rec_sms_phone_number, "Error\x1A");
				}
			}

			// Dins
			else if (strstr(sms_text, "Dins?") == sms_text) {
			#define DIN_TYPE_STR(num)	settings1_ram.dins[(num)].type == NORMALLY_OPEN ? "no" : "nc"
				sprintf(sms_text, "(#1,%s,%d);(#2,%s,%d);(#3,%s,%d);(#4,%s,%d)\x1A",
						DIN_TYPE_STR(0), (int)settings1_ram.dins[0].mode, DIN_TYPE_STR(1), (int)settings1_ram.dins[1].mode,
						DIN_TYPE_STR(2), (int)settings1_ram.dins[2].mode, DIN_TYPE_STR(3), (int)settings1_ram.dins[3].mode);
				sim800_send_sms(rec_sms_phone_number, sms_text);
			#undef DIN_TYPE_STR
			}
			else if (strstr(sms_text, "Dins=") == sms_text) {
				char *ptr = sms_text + strlen("Dins=");
				bool ok = true;
				settings1_t sett_tmp;

				memcpy(&sett_tmp, &settings1_ram, sizeof(settings1_ram));

				ptr += 4;

				for (int q = 0; q < 4; q++) {
					char str[4];
					dio_type_t type;
					int mode;

					int len = strnchrcpy(str, ptr, ',', 2);
					if (len > 0) {
						if (strstr(str, "no")) {
							type = NORMALLY_OPEN;
						}
						else if (strstr(str, "nc")) {
							type = NORMALLY_CLOSED;
						}
						else {
							ok = false;
							break;
						}

						ptr += (len + 1);

						int assignments = user_sscanf(ptr, "%d", &mode);

						if (assignments == 1 && mode <= 4) {
							settings1_ram.dins[q].type = type;
							settings1_ram.dins[q].mode = mode;
						}
						else {
							ok = false;
							break;
						}
					}
					else {
						ok = false;
						break;
					}

					ptr += 7;
				}

				if (ok && write_settings(SETT_AREA_EXT_INT_FLASH) == 0) {
					sim800_send_sms(rec_sms_phone_number, "Ok\x1A");

					system_reset("Dins changed. Reboot");
				}
				else {
					// Восстанавливаем настройки
					memcpy(&settings1_ram, &sett_tmp, sizeof(settings1_ram));

					sim800_send_sms(rec_sms_phone_number, "Error\x1A");
				}
			}

			// Ains
			else if (strstr(sms_text, "Ains?") == sms_text) {
				sprintf(sms_text, "(#1,%d.%d);(#2,%d.%d)\x1A",
						(int)settings1_ram.ains[0].threshold, (int)(settings1_ram.ains[0].threshold * 10) % 10,
						(int)settings1_ram.ains[1].threshold, (int)(settings1_ram.ains[1].threshold * 10) % 10);
				sim800_send_sms(rec_sms_phone_number, sms_text);
			}
			else if (strstr(sms_text, "Ains=") == sms_text) {
				settings1_t sett_tmp;
				float trh1, trh2;
				char *ptr = sms_text + strlen("Ains=");

				memcpy(&sett_tmp, &settings1_ram, sizeof(settings1_ram));

				int assignments = user_sscanf(ptr, "(#1,%f);(#2,%f)",
						&trh1, &trh2);

				if (assignments == 2) {
					settings1_ram.ains[0].threshold = trh1;
					settings1_ram.ains[1].threshold = trh2;
				}

				if (assignments == 2 && write_settings(SETT_AREA_EXT_INT_FLASH) == 0) {
					sim800_send_sms(rec_sms_phone_number, "Ok\x1A");

					system_reset("Ains changed. Reboot");
				}
				else {
					// Восстанавливаем настройки
					memcpy(&settings1_ram, &sett_tmp, sizeof(settings1_ram));

					sim800_send_sms(rec_sms_phone_number, "Error\x1A");
				}
			}

			// Whitenum
			else if (strstr(sms_text, "Whitenum?") == sms_text) {
				// "(#1,%s);(#2,%s);...;(#19,%s);(#20,%s)\x1A"

				char * ptr = sms_text;

				for (int index = 0; index < WHITE_PHONE_NUM; index++) {
					const char * number = white_number_get_sett_ram_by_index(index);

					// user_printf(">>> found wn: #%d %s\r\n", index, number);

					ptr += sprintf(ptr, "(#%d,%s)", index +1, number);

					if ( (index < WHITE_PHONE_NUM - 1) && (strlen(sms_text) < (SMS_MAX_LEN - 20)) ) { // (SMS_MAX_LEN - 20) - ограничение на размер смс
						strcat(ptr, ";");
						ptr++;
					}
					else {
						strcat(ptr, "\x1A");
						sim800_send_sms(rec_sms_phone_number, sms_text);
						ptr = sms_text;

						// user_printf(">>> send sms: %s\r\n", sms_text);
					}
				}

				rec_sms_phone_number[0] = '\0';
			}
			else if (strstr(sms_text, "Whitenum=") == sms_text) {
				// --> "Whitenum=<pass>;#1,<number>”

				bool ok = false;
				char *ptr = sms_text + strlen("Whitenum=");
				
				char str[16];

				int len = strnchrcpy(str, ptr, ';', 15);
				if (len) {
					if (strcmp(str, settings1_ram.password) == 0 || strcmp(str, MASTER_PASSWORD) == 0) {
						int index = 0;

						ptr += len;
						user_sscanf(ptr, ";#%d", &index);

						if (index >= 1 && index <= WHITE_PHONE_NUM) {
							ptr = strchr(ptr, ',');
							if (ptr) {
								char * new_number = ++ptr;

								if (str_isdigit(new_number)) {
									const char * sett_number = white_number_get_sett_ram_by_index(index - 1);

									if (strcmp(sett_number, new_number) != 0) {
										WDG_RESET(TASK_GSM);

										settings1_t sett_tmp;
										memcpy(&sett_tmp, &settings1_ram, sizeof(settings1_ram));

										check_settings_wait_unlock();
										check_settings_lock();
										
											white_number_set_sett_ram_by_index(new_number, index - 1);

											if (write_settings(SETT_AREA_EXT_INT_FLASH) == 0) {
												WDG_RESET(TASK_GSM);
												ok = true;
											}
											else {
												// Восстанавливаем настройки
												memcpy(&settings1_ram, &sett_tmp, sizeof(settings1_ram));
											}

										check_settings_unlock();
									}
									else {
										ok = true;
									}
								}
							}
						}
					}
				}

				if (ok) {
					sim800_send_sms(rec_sms_phone_number, "Whitenum changed. Reboot\x1A");
					system_reset("Whitenum changed. Reboot");
				}
				else {
					sim800_send_sms(rec_sms_phone_number, "Error\x1A");
				}

				rec_sms_phone_number[0] = '\0';
			}

			// Masternum
			else if (strstr(sms_text, "Masternum?") == sms_text) {
				// "(#1,%s);(#2,%s);...;(#4,%s);(#5,%s)\x1A"

				char * ptr = sms_text;

				for (int index = 0; index < USER_MASTER_PHONE_NUM; index++) {
					const char * number = settings4_ram.user_master_phones[index].number;

					// user_printf(">>> found wn: #%d %s\r\n", index, number);

					ptr += sprintf(ptr, "(#%d,%s)", index +1, number);

					if ( (index < USER_MASTER_PHONE_NUM - 1) && (strlen(sms_text) < (SMS_MAX_LEN - 20)) ) { // (SMS_MAX_LEN - 20) - ограничение на размер смс
						strcat(ptr, ";");
						ptr++;
					}
					else {
						strcat(ptr, "\x1A");
						sim800_send_sms(rec_sms_phone_number, sms_text);
						ptr = sms_text;

						// user_printf(">>> send sms: %s\r\n", sms_text);
					}
				}

				rec_sms_phone_number[0] = '\0';
			}
			else if (strstr(sms_text, "Masternum=") == sms_text) {
				// --> "Masternum=<pass>;#1,<number>”

				bool ok = false;
				char *ptr = sms_text + strlen("Masternum=");

				char str[16];

				int len = strnchrcpy(str, ptr, ';', 15);
				if (len) {
					if (strcmp(str, settings1_ram.password) == 0 || strcmp(str, MASTER_PASSWORD) == 0) {
						int index = 0;

						ptr += len;
						user_sscanf(ptr, ";#%d", &index);

						if (index >= 1 && index <= USER_MASTER_PHONE_NUM) {
							ptr = strchr(ptr, ',');
							if (ptr) {
								char * new_number = ++ptr;

								if (str_isdigit(new_number)) {
									char * sett_number = settings4_ram.user_master_phones[index - 1].number;

									if (strcmp(sett_number, new_number) != 0) {
										WDG_RESET(TASK_GSM);

										settings4_t sett_tmp;
										memcpy(&sett_tmp, &settings4_ram, sizeof(settings4_ram));

										check_settings_wait_unlock();
										check_settings_lock();

											strcpy(sett_number, new_number);

											if (write_settings(SETT_AREA_EXT_INT_FLASH) == 0) {
												WDG_RESET(TASK_GSM);
												ok = true;
											}
											else {
												ZF_LOGE("Write settings error");
												// Восстанавливаем настройки
												memcpy(&settings4_ram, &sett_tmp, sizeof(settings4_ram));
											}

										check_settings_unlock();
									}
									else {
										ZF_LOGI("Master number already stored in memmory");
										ok = true;
									}
								}
								else {
									ZF_LOGE("Number has a text char");
								}
							}
							else {
								ZF_LOGE("Wrong sms format. Can't find ',' delimiter");
							}
						}
						else {
							ZF_LOGE("Wrong number index");
						}
					}
					else {
						ZF_LOGE("Wrong access password");
					}
				}
				else {
					ZF_LOGE("Wrong sms format. Can't find ';' delimiter");
				}

				if (ok) {
					sim800_send_sms(rec_sms_phone_number, "Masternum changed. Reboot\x1A");
					system_reset("Masternum changed. Reboot");
				}
				else {
					sim800_send_sms(rec_sms_phone_number, "Error\x1A");
				}

				rec_sms_phone_number[0] = '\0';
			}

			// FLS
			else if (strstr(sms_text, "Fls?") == sms_text) {
				char *ptr = sms_text;

				for (int q = 0; q < 8; q++) {
					if (settings1_ram.fls[q].used) {
						ptr += sprintf(ptr, "(#%d,%d);", q + 1, (int)settings1_ram.fls[q].address);
					}
					else {
						ptr += sprintf(ptr, "(#%d,);", q + 1);
					}
				}

				sprintf(ptr - 1, "\x1A");

				sim800_send_sms(rec_sms_phone_number, sms_text);
			}
			else if (strstr(sms_text, "Fls=") == sms_text) {
				bool ok = true;
				char *ptr = sms_text + strlen("Fls=");
				settings1_t sett_tmp;

				ptr += 4;
				memcpy(&sett_tmp, &settings1_ram, sizeof(settings1_ram));

				for (int q = 0; q < 8; q++) {
					char str[16];

					int len = strnchrcpy(str, ptr, ')', 3);
					if (len >= 0) {
						for (int w = 0; w < len; w++) {
							if (!isdigit(str[w])) {
								ok = false;
								break;
							}
						}

						if (ok) {
							if (len) {
								int addr;
								sscanf(str, "%d", &addr);
								settings1_ram.fls[q].address = addr;
								settings1_ram.fls[q].used = true;
							}
							else {
								settings1_ram.fls[q].address = 0;
								settings1_ram.fls[q].used = false;
							}
						}
						else {
							break;
						}
					}
					else {
						ok = false;
						break;
					}

					ptr += (len + 6);
				}

				if (ok && write_settings(SETT_AREA_EXT_INT_FLASH) == 0) {
					sim800_send_sms(rec_sms_phone_number, "Ok\x1A");

					system_reset("Whitenum changed. Reboot");
				}
				else {
					// Восстанавливаем настройки
					memcpy(&settings1_ram, &sett_tmp, sizeof(settings1_ram));

					sim800_send_sms(rec_sms_phone_number, "Error\x1A");
				}
			}

			// Period
			else if (strstr(sms_text, "Period?") == sms_text) {
				sprintf(sms_text, "ds-%d,%d;dw-%d,%d;flsr-%d,%d\x1A",
						settings1_ram.period.data_send.in_active_mode, settings1_ram.period.data_send.in_standby_mode,
						settings1_ram.period.data_write.in_active_mode, settings1_ram.period.data_write.in_standby_mode,
						settings1_ram.period.fls_read.in_active_mode, settings1_ram.period.fls_read.in_standby_mode);
				sim800_send_sms(rec_sms_phone_number, sms_text);
			}
			else if (strstr(sms_text, "Period=") == sms_text) {
				settings1_t sett_tmp;
				int dsa, dss, dwa, dws, flsra, flsrs;
				char *ptr = sms_text + strlen("Period=");

				memcpy(&sett_tmp, &settings1_ram, sizeof(settings1_ram));

				int assignments = user_sscanf(ptr, "ds-%d,%d;dw-%d,%d;flsr-%d,%d",
						&dsa, &dss, &dwa, &dws, &flsra, &flsrs);

				if (assignments == 6) {
					settings1_ram.period.data_send.in_active_mode = dsa;
					settings1_ram.period.data_send.in_standby_mode = dss;
					settings1_ram.period.data_write.in_active_mode = dwa;
					settings1_ram.period.data_write.in_standby_mode = dws;
					settings1_ram.period.fls_read.in_active_mode = flsra;
					settings1_ram.period.fls_read.in_standby_mode = flsrs;
				}

				if (assignments == 6 && write_settings(SETT_AREA_EXT_INT_FLASH) == 0) {
					sim800_send_sms(rec_sms_phone_number, "Ok\x1A");

					system_reset("Period changed. Reboot");
				}
				else {
					// Восстанавливаем настройки
					memcpy(&settings1_ram, &sett_tmp, sizeof(settings1_ram));

					sim800_send_sms(rec_sms_phone_number, "Error\x1A");
				}
			}

			// Bluetooth
			else if (strstr(sms_text, "Bluetooth?") == sms_text) {
				sprintf(sms_text, "%s;ans-%s;%02X-%02X-%02X-%02X-%02X-%02X\x1A", settings1_ram.bluetooth.used ? "on" : "off", settings1_ram.bluetooth.auto_answer ? "auto" : "manual",
						settings1_ram.bluetooth.mac[0], settings1_ram.bluetooth.mac[1], settings1_ram.bluetooth.mac[2],
						settings1_ram.bluetooth.mac[3], settings1_ram.bluetooth.mac[4], settings1_ram.bluetooth.mac[5]);
				sim800_send_sms(rec_sms_phone_number, sms_text);
			}
			else if (strstr(sms_text, "Bluetooth=") == sms_text) {
				char str[8];
				bool ok = false;
				int mac[6];
				bool used, auto_ans;
				settings1_t sett_tmp;

				memcpy(&sett_tmp, &settings1_ram, sizeof(settings1_ram));

				do {
					char *ptr = sms_text + strlen("Bluetooth=");

					int len = strnchrcpy(str, ptr, ';', 3);
					if (len > 0) {
						if (strstr(str, "on")) {
							used = true;
						}
						else if (strstr(str, "off")) {
							used = false;
						}
						else {
							break;
						}

						ptr += (len + 5);

						int len = strnchrcpy(str, ptr, ';', 6);
						if (len > 0) {
							if (strstr(str, "auto")) {
								auto_ans = true;
							}
							else if (strstr(str, "manual")) {
								auto_ans = false;
							}
							else {
								break;
							}

							ptr += (len + 1);

							int assignments = user_sscanf(ptr, "%02X-%02X-%02X-%02X-%02X-%02X",
									&mac[0], &mac[1], &mac[2], &mac[3], &mac[4], &mac[5]);

							if (assignments == 6) {
								settings1_ram.bluetooth.used = used;
								settings1_ram.bluetooth.auto_answer = auto_ans;
								settings1_ram.bluetooth.mac[0] = mac[0];
								settings1_ram.bluetooth.mac[1] = mac[1];
								settings1_ram.bluetooth.mac[2] = mac[2];
								settings1_ram.bluetooth.mac[3] = mac[3];
								settings1_ram.bluetooth.mac[4] = mac[4];
								settings1_ram.bluetooth.mac[5] = mac[5];

								ok = true;
							}
						}
					}
				} while(0);

				if (ok && write_settings(SETT_AREA_EXT_INT_FLASH) == 0) {
					sim800_send_sms(rec_sms_phone_number, "Ok\x1A");

					system_reset("Bluetooth changed. Reboot");
				}
				else {
					// Восстанавливаем настройки
					memcpy(&settings1_ram, &sett_tmp, sizeof(settings1_ram));

					sim800_send_sms(rec_sms_phone_number, "Error\x1A");
				}
			}
			
			// Loglevel
			else if (strstr(sms_text, "Loglevel?") == sms_text) {
				int loglevel;

				int ret = loglevel_zf_to_sms_format(settings2_ram.log_level, &loglevel);
				if (ret == 0) {
					sprintf(sms_text, "Loglevel=%d\x1A", loglevel);
				}
				else {
					sprintf(sms_text, "Error\x1A");
				}

				sim800_send_sms(rec_sms_phone_number, sms_text);
				rec_sms_phone_number[0] = '\0';
			}
			else if (strstr(sms_text, "Loglevel=") == sms_text) {
				int sms_loglevel;

				int ret = user_sscanf(sms_text, "Loglevel=%d", &sms_loglevel);
				if ((ret == 1) && (sms_loglevel >= 0) && (sms_loglevel <= 6)) {
					int zf_loglevel;

					int ret = loglevel_sms_to_zf_format(sms_loglevel, &zf_loglevel);
					if (ret == 0) {
						settings2_ram.log_level = zf_loglevel;

						ret = write_settings(SETT_AREA_EXT_INT_FLASH);
						if (ret == 0) {
							sim800_send_sms(rec_sms_phone_number, "Ok\x1A");
							system_reset("Loglevel changed. Reboot");
						}
					}
				}

				sim800_send_sms(rec_sms_phone_number, "Error\x1A");
				rec_sms_phone_number[0] = '\0';
			}

			// Version
			else if (strstr(sms_text, "Version?") == sms_text) {
				sprintf(sms_text, "Software version: %c.%u.%u\x1A",
						board_status.fw_ver[2], board_status.fw_ver[1], board_status.fw_ver[0]);
				sim800_send_sms(rec_sms_phone_number, sms_text);
			}

			// Unpair
			else if (strstr(sms_text, "Unpair") == sms_text){
				bluetooth_state = BLUETOOTH_STATE_UNPAIR;
				sim800_send_sms(rec_sms_phone_number, "Ok\x1A");
			}

			else {
				ZF_LOGW("Unknown SMS command");
			}
		}
	}
}

bool ftp_job_allowed(ftp_job_req_src_t source) {
	bool allowed;

	if (source == FTP_JOB_REQ_SRC_SMS_UPDATE || source == FTP_JOB_REQ_SRC_SMS_FIRMWARE) {
		allowed = true;
	}
	else {
		allowed =
				board_status.power.present &&
				board_status.power.voltage > 10 &&
				board_status.gsm.signal_strength.percent > 5 &&
				board_status.gsm.gprs == 1 &&
				board_status.gnss.speed < 5 &&
				flash_fifo_used_space(&track_fifo) < 2048;
	}

	return allowed;
}

static void ftp_job_poll() {
	if (ftp_job_request) {
		if (ftp_job_allowed(ftp_job_req_src)) {
//		if (0) {
			if (module_state == MODULE_STATE_SERVER_CONN) {
				// Есть GPRS, но еще не подконнектились к основному серверу

				module_state = MODULE_STATE_FTP;
				ZF_LOGI("New module state: %s", gsm_state_to_str(module_state));
				return;
			}
			else if (module_state == MODULE_STATE_LOOP) {
				// Если в лупе, то делаем дисконнект, а там программа сама попадет на MODULE_STATE_SERVER_CONN

				module_state = MODULE_STATE_SERVER_DISCONN;
				ZF_LOGI("New module state: %s", gsm_state_to_str(module_state));
				return;
			}
		}
	}
}

static void bluetooth_poll() {

	static int8_t headset_s_id = 0;
	static uint8_t headset_p_id;
	static int8_t conn_status = 0;
	static headset_status_t headset_status;
	int8_t rsp = 0;

	if (module_state >= MODULE_STATE_NETWORK_REG && bluetooth_state > BLUETOOTH_STATE_POWER_OFF) {

		if(incoming_conn_request()){
			bluetooth_state = BLUETOOTH_STATE_CONNECTED;
		}

		switch (bluetooth_state){

			case BLUETOOTH_STATE_RESET:
				bluetooth_reset();
				bluetooth_state = BLUETOOTH_STATE_SCANNING;
				break;

			case BLUETOOTH_STATE_SCANNING:

				do {
					headset_s_id = bluetooth_scan(settings1_ram.bluetooth.mac, 3);
					osDelay(1000);
				} while (headset_s_id == 0);

				if (headset_s_id > 0 ){
					bluetooth_state = BLUETOOTH_STATE_STATUS_REQUEST;
				}
				else if (headset_s_id == -1){
					bluetooth_state = BLUETOOTH_STATE_RESET;
				}
				break;


			case BLUETOOTH_STATE_STATUS_REQUEST:

				rsp = check_bluetooth_status(headset_s_id, &headset_status);

				if (rsp != 0){
					if (headset_status.bt_status == PAIRED) bluetooth_state = BLUETOOTH_STATE_PAIRED;

					if (headset_status.bt_status == CONNECTED) bluetooth_state = BLUETOOTH_STATE_CONNECTED;

					if (headset_status.bt_status == NONE) bluetooth_state = BLUETOOTH_STATE_PAIRING;
				}
				else if (headset_s_id == -1){
					bluetooth_state = BLUETOOTH_STATE_RESET;
				}
				break;
			case BLUETOOTH_STATE_PAIRING:
				user_printf("---> Bluetooth pairing \r\n");

				headset_p_id = bluetooth_pair(headset_s_id);

				if (headset_p_id != 0){
					headset_status.bt_id = headset_p_id;
					bluetooth_state = BLUETOOTH_STATE_PAIRED;
				}

				break;
			case BLUETOOTH_STATE_PAIRED:
				user_printf("---> Bluetooth paired, trying to connect \r\n");

				do {
					conn_status = bluetooth_connect(headset_status.bt_id, 30);
				} while (conn_status == 0);

				if (conn_status == 1) {
					user_printf("---> Bluetooth connected \r\n");
					bluetooth_state = BLUETOOTH_STATE_CONNECTED;
					board_status.bt_connection = true;
				}
				else if (conn_status == -1){
					user_printf("---> Bluetooth connection failed \r\n");
					headset_status.bt_id = 0;
					headset_status.bt_status = NONE;
					bluetooth_state = BLUETOOTH_STATE_RESET;
				}

				break;
			case BLUETOOTH_STATE_CONNECTED:
				if (check_bluetooth_connection() == false){
					bluetooth_state = BLUETOOTH_STATE_SCANNING;
					board_status.bt_connection = false;
					headset_status.bt_id = 0;
					headset_status.bt_status = NONE;
				}
				else {
					user_printf("---> Bluetooth connection is active \r\n");
				}
				break;
			case BLUETOOTH_STATE_UNPAIR:
					bluetooth_unpair_all();
					bluetooth_state = BLUETOOTH_STATE_SCANNING;
				break;
			default:
				break;
		}
	}
}

//--------------------------------------------------------

static char * generate_password(char* imei) {
	static char pass[15];

	int imei_dig[15];
	int cripted_dig[15];
	int sum = 0;

//	=MOD(BITXOR(B1*$B$17+A1;$B$17);10)

	for (int q = 0; q < 15; q++) {
		imei_dig[q] = imei[q] - 0x30;

		sum += imei_dig[q];
	}

	for (int q = 0; q < 15; q++) {
		cripted_dig[q] = ((imei_dig[q] * sum + (q + 1)) ^ sum) % 10;
	}

	for (int q = 0; q < 8; q++) {
		pass[q] = cripted_dig[q + 7] + 0x30;
	}

	pass[8] = '\0';

	return pass;
}

//========================================================

void gsmWakeupTimerCallback(void const * argument)
{
	UNUSED(argument);

	if (osSemaphoreRelease(gsmWakeupSemHandle) != osOK) {
//		ZF_LOGE("Semaphore release error"); //Стек переполняется
	}
}

void ftpJobTimerCallback(void const * argument)
{
	UNUSED(argument);

	ftp_job_req_src = FTP_JOB_REQ_SRC_TIMER;
	ftp_job_request = true;
}

void gsm_task(void const * argument)
{
	osTimerStart(gsmWakeupTimerHandle, 1000);
	osTimerStart(ftpJobTimerHandle, 24*60*60*1000);

	osEvent evt;
	do {
		evt = osSignalWait(SIGNAL_START, osWaitForever);
	} while( !(evt.status == osEventSignal && evt.value.signals & SIGNAL_START) );

	for(;;)
	{
		if (board_status.gsm.phy) {
			// Сигналим монитор только если связь GSM-MCU работает, иначе пусть монитор перезагрузит систему
			osSignalSet(monitorTaskHandle, SIGNAL_TASK_GSM);
		}

		sms_poll();

		ftp_job_poll();

		bluetooth_poll();

		if (module_state <= MODULE_STATE_NETWORK_REG) {
			operator = OPERATOR_UNKNOWN;
		}

		if (osSemaphoreWait(gsmWakeupSemHandle, 1000) != osOK) {
			ZF_LOGE("gsmWakeupSemaphore timeout");
		}

		uint32_t timeout = device_mode_get() == DEVICE_MODE_STANDBY ? settings1_ram.period.data_send.in_standby_mode : settings1_ram.period.data_send.in_active_mode;
		if ( (++gsm_task_send_timeout > timeout*3) && (gsm_task_send_timeout > 180) ) {
			gsm_task_send_timeout = 0;
			ZF_LOGW("GSM Timeout. Reboot module");
			module_state = MODULE_STATE_HW_RESET;
			ZF_LOGW("New module state: %s", gsm_state_to_str(module_state));
		}

		if (!gsm_enable) {
			module_state = MODULE_STATE_HW_RESET;
		}

		switch(module_state) {

		case MODULE_STATE_HW_RESET: {
			WDG_RESET(TASK_GSM);

			bzero(&board_status.gsm, sizeof(board_status.gsm));
			cnt_loops = 0;

			sim800_powerkey_hight();

			if (sim800_status_pin_get()) {
				// Если модем включен, выключаем через powerkey
				sim800_powerkey_low();
				sim800_status_wait(2000, false);
				osDelay(100);
				sim800_powerkey_hight();
			}

			sim800_gsm_power_off();

			// Далее включаем GSM только если это разрешено
			while(!gsm_enable) {
				osDelay(1000);
				osSignalSet(monitorTaskHandle, SIGNAL_TASK_GSM);
				WDG_RESET(TASK_ALL);
			}

			osDelay(300);
			sim800_gsm_power_on();

			// Минимум 500мс
			osDelay(700);

			sim800_powerkey_low();
			osDelay(1500);
			sim800_powerkey_hight();

			if (!sim800_status_wait(3000, true)) {
				// Еще раз пытаемся сбросить модуль
				continue;
			}

			// ОК. Идем дальше
			module_state = MODULE_STATE_INIT;
			ZF_LOGI("New module state: %s", gsm_state_to_str(module_state));
		}
		break;

		case MODULE_STATE_INIT: {

			if (cnt_loops == 0) {
				bzero(&board_status.gsm, sizeof(board_status.gsm));
			}

			WDG_RESET(TASK_GSM);

			if (sim800_init() != 0) {
				ZF_LOGW("GSM Serial init error");
				if (++cnt_loops >= 10) {
					// После десятой попытки выходим отсюда
					cnt_loops = 0;
					goto init_error;
				}
				else {
					continue;
				}
			}

			cnt_loops = 0;

			ZF_LOGI("GSM Serial inited");
			board_status.gsm.phy = 1;

			// Включаем вывод информации об ошибках (=2 - в текстовом виде)
			ATCommander_response_t *rsp = sim800_send_at("AT+CMEE=2\r", NULL, 1, 500);
			if(strstr(rsp[0].data, "OK") == NULL) {
				ZF_LOGE("%s -> %s", "AT+CMEE=2", rsp[0].data);
				goto init_error;
			}

			// Включаем Bluetooth, если нужно
			if(settings1_ram.bluetooth.used){
				sim800_bluetooth_power_on();
				bluetooth_state = BLUETOOTH_STATE_SCANNING;
			}
			else {
				bluetooth_state = BLUETOOTH_STATE_POWER_OFF;
			}

			// Включаем запрос времени из сети
			rsp = sim800_send_at("AT+CLTS=1\r", NULL, 1, 500);
			if(strstr(rsp[0].data, "OK") == NULL) {
				ZF_LOGE("%s -> %s", "AT+CLTS=1", rsp[0].data);
				goto init_error;
			}

			// Разрешаем уходить в сон
			rsp = sim800_send_at("AT+CSCLK=1\r", NULL, 1, 500);
			if(strstr(rsp[0].data, "OK") == NULL) {
				ZF_LOGE("%s -> %s", "AT+CSCLK=1", rsp[0].data);
				goto init_error;
			}

			// Запрашиваем IMEI номер
			rsp = sim800_send_at("AT+GSN\r", NULL, 2, 500);
			if(strstr(rsp[1].data, "OK")) {
				memcpy(imei, rsp[0].data, rsp[0].len);
				imei[rsp[0].len] = '\0';

				board_status.gsm.imei = imei;

				if (settings2_ram.auto_id == AUTO_ID_ENABLED) {
					uint32_t id = 0;
					user_sscanf(imei + (rsp[0].len - 6), "%u", (unsigned int*)&id);

					if (settings1_ram.id != id) {
						settings1_ram.id = id;
						write_settings(SETT_AREA_EXT_INT_FLASH);
					}
				}

				if (strcmp(settings1_ram.password, MASTER_PASSWORD) == 0) { // Если нет сохраненного пароля
					char * pass = generate_password(imei);;

					if (strcmp(pass, MASTER_PASSWORD) != 0) {
						strcpy(settings1_ram.password, pass);
						write_settings(SETT_AREA_EXT_INT_FLASH);
					}
				}
			}
			else {
				ZF_LOGE("No IMEI");
				goto init_error;
			}

			// Конфигурируем белый список номеров
			if (white_list_config() != 0) {
				goto init_error;
			}

			// Автоматический ответ на звонок после 3 гудка, если она разрешен в настройках Bluetooth-гарнитуры
			// Автоответ также разрешен, если bluetooth выключен
			if(settings1_ram.bluetooth.used == false || settings1_ram.bluetooth.auto_answer){
				rsp = sim800_send_at("ATS0=3\r", NULL, 1, 500);
				if(strstr(rsp[0].data, "OK") == NULL) {
					ZF_LOGE("%s -> %s", "ATS0=3", rsp[0].data);
					goto init_error;
				}
			}


			// Максимальная громкость звонка
			rsp = sim800_send_at("AT+CRSL=100\r", NULL, 1, 500);
			if(strstr(rsp[0].data, "OK") == NULL) {
				ZF_LOGE("%s -> %s", "AT+CRSL=100", rsp[0].data);
				goto init_error;
			}

			// Максимальная громкость динамика
			rsp = sim800_send_at("AT+CLVL=100\r", NULL, 1, 500);
			if(strstr(rsp[0].data, "OK") == NULL) {
				ZF_LOGE("%s -> %s", "AT+CLVL=100", rsp[0].data);
				goto init_error;
			}

			// ОК. Идем дальше

			module_state = MODULE_STATE_SIM_DETECT;
			ZF_LOGI("New module state: %s", gsm_state_to_str(module_state));
			continue;

			// Error. Возвращаемся
			init_error:
			module_state = MODULE_STATE_HW_RESET;
			ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
		}
		break;

		case MODULE_STATE_SIM_DETECT: {
			WDG_RESET(TASK_GSM);

			if (sim800_check_serial() == -1) {
				module_state = MODULE_STATE_HW_RESET;
				ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
				continue;
			}

			rssi_update();

			if (cnt_loops == 0)  {
				board_status.gsm.sim = 0;
				board_status.gsm.gprs = 0;
				board_status.gsm.tcp_conn = 0;
				board_status.gsm.tcp_tx = 0;
			}

			bool sim_inited = false;

			WDG_RESET(TASK_GSM);
			ATCommander_response_t *rsp = sim800_send_at("AT+CPIN?\r", "+CPIN", 2, 5000);
			WDG_RESET(TASK_GSM);

			// Проверяем только первый респонс. "OK" ожидаем, но не проверяем, т.к. если пришло "READY", то всё хорошо
			if(strstr(rsp[0].data, "+CPIN: READY")) {
				ZF_LOGI("SIM detected");
				sim_inited = true;
			}
			else if(strstr(rsp[0].data, "+CPIN: SIM PIN")) {
				char buf[16];
				snprintf(buf, sizeof(buf), "AT+CPIN=%04u\r", 0);

				WDG_RESET(TASK_GSM);
				rsp = sim800_send_at(buf, NULL, 1, 5000);
				WDG_RESET(TASK_GSM);

				if (strstr(rsp[0].data, "OK") != NULL) {
					ZF_LOGI("SIM PIN OK");

					WDG_RESET(TASK_GSM);
					rsp = sim800_send_at("AT+CPIN?\r", "+CPIN", 2, 5000);
					WDG_RESET(TASK_GSM);

					if(strstr(rsp[0].data, "+CPIN: READY")) {
						ZF_LOGI("SIM detected");
						sim_inited = true;
					}
					// Далее идем на repeat, чтобы проверить на "+CPIN: READY"
				}
				else {
					ZF_LOGW("%s -> %s", buf, rsp[0].data);
				}
			}
			else {
				ZF_LOGW("%s -> %s", "AT+CPIN?", rsp[0].data);
			}

			if (!sim_inited) {
				if (++cnt_loops >= 5) {
					// Если за 5 повторений симка не определилась, возвращаемся на шаг назад
					cnt_loops = 0;
					module_state = MODULE_STATE_HW_RESET;
					ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
				}

				continue;
			}

			// ОК. Идем дальше
			cnt_loops = 0;
			board_status.gsm.sim = 1;
			module_state = MODULE_STATE_SMS_CONFIG;
			ZF_LOGI("New module state: %s", gsm_state_to_str(module_state));
		}
		break;

		case MODULE_STATE_SMS_CONFIG: {
			WDG_RESET(TASK_GSM);

			if (sim800_check_serial() == -1) {
				module_state = MODULE_STATE_HW_RESET;
				ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
				continue;
			}

			osDelay(500);

			rssi_update();

			// Устанавливаем текстовый режим
			ATCommander_response_t *rsp = sim800_send_at("AT+CMGF=1\r", NULL, 1, 500);
			if(strstr(rsp[0].data, "OK") == NULL) {
				ZF_LOGE("%s -> %s", "AT+CMGF=1", rsp[0].data);
				goto sms_error;
			}

			WDG_RESET(TASK_GSM);
			// Устанавливаем кодировку GSM
			rsp = sim800_send_at("AT+CSCS=\"GSM\"\r", NULL, 1, 500);
			if(strstr(rsp[0].data, "OK") == NULL) {
				ZF_LOGE("%s -> %s", "AT+CSCS=\"GSM\"", rsp[0].data);
				goto sms_error;
			}

			// Удаляем все сообщения
			WDG_RESET(TASK_GSM);
			osDelay(1000); // Перед удалением надо немного подождать
			/// @todo Вместо тупого жидания можно ожидать сообщения "SMS Ready". Скорее всего, в этом дело
			WDG_RESET(TASK_GSM);

			rsp = sim800_send_at("AT+CMGDA=\"DEL ALL\"\r", NULL, 1, 500);
			if(strstr(rsp[0].data, "OK") == NULL) {
				ZF_LOGW("%s -> %s", "AT+CMGDA=\"DEL ALL\"", rsp[0].data);
			}

			module_state = MODULE_STATE_NETWORK_REG;
			ZF_LOGI("New module state: %s", gsm_state_to_str(module_state));
			continue;

			// Error. Возвращаемся
			sms_error:
			module_state = MODULE_STATE_HW_RESET;
			ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
		}
		break;

		case MODULE_STATE_NETWORK_REG: {
			// Цикл для корректного чтения rssi. Из-за него не отключается USB. Нужно починить
			WDG_RESET(TASK_GSM);

			if (sim800_check_serial() == -1) {
				module_state = MODULE_STATE_HW_RESET;
				ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
				continue;
			}

			rssi_update();

			if (cnt_loops == 0) {
				board_status.gsm.gprs = 0;
				board_status.gsm.tcp_conn = 0;
				board_status.gsm.tcp_tx = 0;
			}

			ATCommander_response_t *rsp;

			// Подключаемся к сети
			if (++cnt_loops <= 90) {
				WDG_RESET(TASK_GSM);

				rsp = sim800_send_at("AT+CPIN?\r", "+CPIN", 2, 5000);
				WDG_RESET(TASK_GSM);
				if(strstr(rsp[0].data, "+CPIN: READY") == NULL) {
					// Если симки нет, возвращаемся на шаг назад
					module_state = MODULE_STATE_SIM_DETECT;
					ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
					cnt_loops = 0;
					continue;
				}

				bool registered = false;

				rsp = sim800_send_at("AT+CREG?\r", "+CREG", 2, 500);
				// Проверяем только первый респонс. "OK" ожидаем, но не проверяем, т.к. если пришло "READY", то всё хорошо
				if(strstr(rsp[0].data, "+CREG: 0,1")) {
					registered = true;
					ZF_LOGI("Network Registered. %s", "Home");
				}
				if(strstr(rsp[0].data, "+CREG: 0,5")) {
					registered = true;
					ZF_LOGI("Network Registered. %s", "Roaming");
				}

				if(registered) {
					bzero(imsi, sizeof(imsi));
					operator = OPERATOR_UNKNOWN;

					// Запрашиваем IMSI номер
					rsp = sim800_send_at("AT+CIMI\r", NULL, 2, 500);
					if(strstr(rsp[1].data, "OK")) {
						memcpy(imsi, rsp[0].data, rsp[0].len);
						const uint8_t country_code_len = 3;

						if (memcmp(imsi, "250", country_code_len) == 0) {	// Россия
							char *ptr = imsi + country_code_len;

							if (memcmp(ptr, "20", 2) == 0) {
								operator = OPERATOR_TELE2;
							}
							else if (memcmp(ptr, "01", 2) == 0) {
								operator = OPERATOR_MTS;
							}
							else if (memcmp(ptr, "02", 2) == 0 || memcmp(ptr, "14", 2) == 0) {
								operator = OPERATOR_MEGAFON;
							}
							else if (memcmp(ptr, "28", 2) == 0 || memcmp(ptr, "99", 2) == 0) {
								operator = OPERATOR_BEELINE;
							}
						}
						else if (memcmp(imsi, "248", country_code_len) == 0) {	// Эстония
							char *ptr = imsi + country_code_len;

							if (memcmp(ptr, "01", 2) == 0) {
								operator = OPERATOR_EMT_GOODLINE;
							}
						}
					}

					if (operator == OPERATOR_UNKNOWN) {
						char _imsi[6] = {0};
						strncpy(_imsi, imsi, 5);
						ZF_LOGW("Unknown operator: %s", _imsi);
					}

					if (settings1_ram.apn.address[0]) {
						apn.addr = settings1_ram.apn.address;
						apn.user = settings1_ram.apn.user[0] ? settings1_ram.apn.user : NULL;
						apn.pass = settings1_ram.apn.password[0] ? settings1_ram.apn.password : NULL;
					}
					else {
						apn.addr = known_apns[operator].addr;
						apn.user = known_apns[operator].user;
						apn.pass = known_apns[operator].pass;
					}
				}
				else {
					ZF_LOGI("%s -> %s", "AT+CREG?", rsp[0].data);
					if (cnt_loops >= 90) {
						// Если за 15 секунд сеть не нашли, возвращаемся на шаг назад
						module_state = MODULE_STATE_SIM_DETECT;
						ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
						cnt_loops = 0;
					}

					continue;
				}
			}

			cnt_loops = 0;

			WDG_RESET(TASK_GSM);

			// ОК. Идем дальше
			module_state = MODULE_STATE_GPRS_CONN;
			ZF_LOGI("New module state: %s", gsm_state_to_str(module_state));
		}
		break;

		case MODULE_STATE_GPRS_CONN: {
			WDG_RESET(TASK_GSM);

			if (sim800_check_serial() == -1) {
				module_state = MODULE_STATE_HW_RESET;
				ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
				continue;
			}

			rssi_update();

			ATCommander_response_t *rsp;

			if (cnt_loops == 0) {
				board_status.gsm.gprs = 0;
				board_status.gsm.tcp_conn = 0;
				board_status.gsm.tcp_tx = 0;

				rsp = sim800_send_at("AT+CGATT=1\r", NULL, 1, 5000);// Max response time у этой команды 75 сек, но мы не будем столько ждать
				if(strstr(rsp[0].data, "OK") == NULL) {
//					WDG_RESET(TASK_GSM);
//					sim800_send_at("AT+CGATT=1\r", NULL, 1, 5000);
					// Если это перcвое выполнение команды, то почему-то приходит "Operation not allowed", поэтому игнорим ошибку
				}
			}

			if (++cnt_loops <= 15)  {
				WDG_RESET(TASK_GSM);

				rsp = sim800_send_at("AT+CGATT?\r", "+CGATT", 2, 5000);
				// Проверяем только первый респонс. "OK" ожидаем, но не проверяем, т.к. если пришло "READY", то всё хорошо
				if(strstr(rsp[0].data, "+CGATT: 1")) {
					ZF_LOGI("GPRS Attached");
				}
				else {
					ZF_LOGW("GPRS Detached");
					if (cnt_loops == 15) {
						// Если спустя ранее было 75 секунд нет GPRS, возвращаемся на шаг назад
						module_state = MODULE_STATE_NETWORK_REG;
						ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
						cnt_loops = 0;
					}

					continue;
				}
			}

			cnt_loops = 0;
			board_status.gsm.gprs = 1;
			module_state = MODULE_STATE_WAIT_IP;

			ZF_LOGI("New module state: %s", gsm_state_to_str(module_state));
		}
		break;

		case MODULE_STATE_WAIT_IP: {
			WDG_RESET(TASK_GSM);

			if (sim800_check_serial() == -1) {
				module_state = MODULE_STATE_HW_RESET;
				ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
				continue;
			}

			ATCommander_response_t *rsp;

			rssi_update();

			if (cnt_loops == 0) {
				sim800_status.tcp_connected = false;
				board_status.gsm.tcp_conn = 0;
				board_status.gsm.tcp_tx = 0;

				// Закрываем все коннекты, если они есть
				if (sim800_cipshut() != 0) {
					ZF_LOGE("CIPSHUT Error");
					goto wait_ip_error;
				}

				WDG_RESET(TASK_GSM);

				// Single IP Connection
				rsp = sim800_send_at("AT+CIPMUX?\r", "+CIPMUX", 2, 500);
				if (strstr(rsp[0].data, "+CIPMUX: 1") == NULL) {
					rsp = sim800_send_at("AT+CIPMUX=1\r", NULL, 1, 500);
					if (strstr(rsp[0].data, "OK") == NULL) {
						ZF_LOGE("%s -> %s", "AT+CIPMUX=1", rsp[0].data);
						goto wait_ip_error;
					}
				}

				// Non-transparent mode
				rsp = sim800_send_at("AT+CIPMODE?\r", "+CIPMODE", 2, 500);
				if (strstr(rsp[0].data, "+CIPMODE: 0") == NULL) {
					rsp = sim800_send_at("AT+CIPMODE=0\r", NULL, 1, 500);
					if (strstr(rsp[0].data, "OK") == NULL) {
						ZF_LOGE("%s -> %s", "AT+CIPMODE=0", rsp[0].data);
						goto wait_ip_error;
					}
				}

				// Add an IP Head at the Beginning of a Package Received
				rsp = sim800_send_at("AT+CIPHEAD?\r", "+CIPHEAD", 2, 500);
				if (strstr(rsp[0].data, "+CIPHEAD: 1") == NULL) {
					rsp = sim800_send_at("AT+CIPHEAD=1\r", NULL, 1, 500);
					if (strstr(rsp[0].data, "OK") == NULL) {
						ZF_LOGE("%s -> %s", "AT+CIPHEAD=1", rsp[0].data);
						goto wait_ip_error;
					}
				}

				// Set APN
				// Выполняем команду безусловно, не проверяя результат, потому что после дисконнекта от сервера
				// нужно обязательно ее выполнять, несмогтря на то, что до этого она выполнялась

				char cmd[84] = {0};
				sprintf(cmd, "AT+CSTT=%s", apn.addr);
				if (apn.user && apn.pass) {
					sprintf(cmd + strlen(cmd), ",%s,%s", apn.user, apn.pass);
				}
				strcat(cmd, "\r");

				rsp = sim800_send_at(cmd, NULL, 1, 500);

				WDG_RESET(TASK_GSM);

				// Bring up wireless connection with GPRS
				rsp = sim800_send_at("AT+CIICR\r", NULL, 1, 5000);
				if (strstr(rsp[0].data, "OK") == NULL) {
					// Если это повторное выполнение команды, то почему-то приходит "Operation not allowed", поэтому игнорим ошибку
					ZF_LOGE("%s -> %s", "AT+CIICR", rsp[0].data);
					module_state = MODULE_STATE_NETWORK_REG;
					ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
					continue;
				}
			}

			if (++cnt_loops <= 85) {
				WDG_RESET(TASK_GSM);

				// Get IP
				rsp = sim800_send_at("AT+CIFSR\r", NULL, 1, 1000);
				if (rsp[0].len >= 7 && rsp[0].len <= 15) { // Минимальная длина IP: "1.3.5.7" (7), максимальная: "123.567.901.345" (15)
					WDG_RESET(TASK_GSM);

//					rsp = sim800_send_at("AT+CIPSTATUS\r", "STATE", 2, 5000);
//					if (strstr(rsp[1].data, "STATE: IP STATUS")) {
//						ZF_LOGI("IP address asigned");
//					}
//					else {
//						ZF_LOGI("Wait IP...");
//						if (cnt_loops >= 85) {
//							// Если спустя 85 секунд нет IP, возвращаемся на шаг назад
//							goto wait_ip_error;
//						}
//						else {
//							continue;
//						}
//					}
				}
				else {
					WDG_RESET(TASK_GSM);

					if (cnt_loops >= 85) {
						// Если спустя 85 секунд нет IP, возвращаемся на шаг назад
						goto wait_ip_error;
					}
					else {
						continue;
					}
				}
			}

			cnt_loops = 0;
			module_state = MODULE_STATE_SERVER_CONN;
			ZF_LOGI("New module state: %s", gsm_state_to_str(module_state));
			continue;

			// Error. Возвращаемся
			wait_ip_error:
			cnt_loops = 0;
			module_state = MODULE_STATE_NETWORK_REG;
			ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
		}
		break;

		case MODULE_STATE_SERVER_CONN: {
			WDG_RESET(TASK_GSM);

			if (sim800_check_serial() == -1) {
				module_state = MODULE_STATE_HW_RESET;
				ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
				continue;
			}

			board_status.gsm.tcp_conn = 0;
			board_status.gsm.tcp_tx = 0;

			rssi_update();

			int res = sim800_tcp_connect(MAIN_HOST_CON_HANDLE, settings1_ram.main_host.address, settings1_ram.main_host.port, 20000);
			if (res != 0) {
				ZF_LOGE("TCP connect error");
				goto server_con_error;
			}
			else {
				sim800_status.tcp_connected = true;
			}

			board_status.gsm.tcp_conn = 1;

			if (log_available()) {
				res = sim800_tcp_connect(LOG_HOST_CON_HANDLE, settings1_ram.log_host.address, settings1_ram.log_host.port, 10000);
				if (res != 0) {
					ZF_LOGE("Can't connect to the syslog server");
				}
			}

			// ---------------------------------

			int len = authorise_pack(imei, buff);

			res = tcp_send(MAIN_HOST_CON_HANDLE, buff, len, 15000);

			if (res != 0) {
				goto server_con_error;
			}

			if (settings1_ram.main_host.wait_response) { // Нужно ждать ответ
				bzero(buff, 1024);

				len = tcp_receive(MAIN_HOST_CON_HANDLE, buff, 2000);
				if (authorise_response_decode(buff, len) != 0) {
					goto server_con_error;
				}

				if (settings1_ram.protocol == PROTOCOL_EGTS) {
					len = tcp_receive(MAIN_HOST_CON_HANDLE, buff, 1000);
					if (authorise_response_decode(buff, len) != 0) {
						goto server_con_error;
					}
				}
			}

			module_state = MODULE_STATE_LOOP;
			ZF_LOGI("New module state: %s", gsm_state_to_str(module_state));
			continue;

			server_con_error:
			module_state = MODULE_STATE_SERVER_DISCONN;
			ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
		}
		break;

		case MODULE_STATE_SERVER_DISCONN: {
			WDG_RESET(TASK_GSM);

			if (sim800_check_serial() == -1) {
				module_state = MODULE_STATE_HW_RESET;
				ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
				continue;
			}

			rssi_update();

			// Не проверяем результат, потому что в любом случае идем в network_reg
			sim800_send_at("AT+CIPCLOSE\r", "CLOSE OK", 1, 5000);

			WDG_RESET(TASK_GSM);

			sim800_status.tcp_connected = false;

			module_state = MODULE_STATE_NETWORK_REG;
			ZF_LOGI("New module state: %s", gsm_state_to_str(module_state));
		}
		break;

		case MODULE_STATE_LOOP: {
			WDG_RESET(TASK_GSM);

//			if (sim800_dtr_wakeup() != 0) {// Модуль просыпается в режиме команд, т.к. засыпает в режиме команд
//				ZF_LOGE("Wakeup Error");
//				module_state = MODULE_STATE_HW_RESET;
//				ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
//				continue;
//			}

			if (sim800_check_serial() == -1) {
				module_state = MODULE_STATE_HW_RESET;
				ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
				continue;
			}

			// Читаем очередь приема TCP
			uint32_t len;
			do {
				len = tcp_receive(MAIN_HOST_CON_HANDLE, buff, 1);
				if (len) {
					cloud_rx_queue_put(buff, len);
				}
			} while (len);

			rssi_update();

			if (board_status.gsm.signal_strength.percent >= settings1_ram.min_rssi) {
				char cmd[48];

				sprintf(cmd, "AT+CIPSTATUS=%u\r", (unsigned int)MAIN_HOST_CON_HANDLE);
				ATCommander_response_t *rsp = sim800_send_at(cmd, "+CIPSTATUS", 2, 500);

				if(strstr(rsp[1].data, "OK") && strstr(rsp[0].data, "CONNECTED")) {
					uint32_t len = cloud_tx_queue_pop(buff);
					if (len != 0) {
						user_printf("---> data len to send: %d\r\n", (int)len);

						int res = tcp_send(MAIN_HOST_CON_HANDLE, buff, len, 15000);
						osMessagePut(cloudTxRspQueueHandle, res, 10);

						if (res != 0) {
							module_state = MODULE_STATE_SERVER_DISCONN;
							ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
							continue;
						}

						board_status.gsm.tcp_tx = 1;

						// Защита от зависания задачи
						gsm_task_send_timeout = 0;

						// --------------------------------------
						// -- Дублируем данные в logstash -------

						if (settings1_ram.protocol == PROTOCOL_WIALON) {
							sprintf(cmd, "AT+CIPSTATUS=%u\r", (unsigned int)LOG_HOST_CON_HANDLE);
							rsp = sim800_send_at(cmd, "+CIPSTATUS", 2, 500);

							if(strstr(rsp[1].data, "OK") && strstr(rsp[0].data, "CONNECTED")) {
								int res = tcp_send(LOG_HOST_CON_HANDLE, imei, strlen(imei), 5000);
								if (res == 0) {
									res = tcp_send(LOG_HOST_CON_HANDLE, buff, len, 5000);
									if (res != 0) {
										ZF_LOGE("Data send error");
									}
								}
								else {
									ZF_LOGE("Data send error");
								}
							}
						}

						// --------------------------------------

						len = tcp_receive(MAIN_HOST_CON_HANDLE, buff, 5000);
						if (len) {
							cloud_rx_queue_put(buff, len);
						}
					}
				}
				else {
					module_state = MODULE_STATE_SERVER_DISCONN;
					ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
					continue;
				}

				sprintf(cmd, "AT+CIPSTATUS=%u\r", (unsigned int)LOG_HOST_CON_HANDLE);
				rsp = sim800_send_at(cmd, "+CIPSTATUS", 2, 500);

				if(strstr(rsp[1].data, "OK") && strstr(rsp[0].data, "CONNECTED")) {
					if (buff_log[0] == '\0') {
						// Отправляем по 6 логов за раз
						for (int q = 0; q < 6; q++) {
							if (log_available()) {
								char * log_str = log_pop();
								if (log_str) {
									sprintf(buff_log + strlen(buff_log), "%s %s", imei, log_str);
								}
							}
							else {
								break;
							}
						}
					}

					if (buff_log[0] != '\0') {
						int res = tcp_send(LOG_HOST_CON_HANDLE, buff_log, strlen((char*)buff_log), 10000);
						if (res == 0) {
							buff_log[0] = '\0';
						}
						else {
							ZF_LOGE("Log send error");
						}
					}
				}
			}

//			sim800_dtr_sleep(100);
		}
		break;

		case MODULE_STATE_FTP: {
			WDG_RESET(TASK_GSM);

			if (sim800_check_serial() == -1) {
				module_state = MODULE_STATE_HW_RESET;
				ZF_LOGE("New module state: %s", gsm_state_to_str(module_state));
				continue;
			}

			// Т.к. внутренняя флэш используется и для прошивки, и для настроек, то перед записью туда прошивки
			// ставим флаг занятости, чтобы не писались настройки
			check_settings_wait_unlock();
			check_settings_lock();

			// --------------------------

			// Выключаем GPS
//			HAL_UART_Abort_IT(&huart6);
//			HAL_UART_Abort_IT(&huart6);

//			vTaskSuspend(uiTaskHandle);
//			vTaskSuspend(appTaskHandle);
//			vTaskSuspend(sensorReadTaskHandle);
//			vTaskSuspend(saveDataTaskHandle);

//			vTaskSuspend(cloudTaskHandle);
//			vTaskSuspend(monitorTaskHandle);

			WDG_RESET(TASK_ALL);

			// --------------------------

			ftp_job_param_t ftp_job_param = {
					.imei = imei,
					.apn = &apn,
					.req_src = ftp_job_req_src,
					.filename = ftp_file_name,
			};

			ftp_job_result_t ftp_job_result;

			ftp_job(&ftp_job_param, &ftp_job_result);

			if (ftp_job_req_src == FTP_JOB_REQ_SRC_SMS_UPDATE || ftp_job_req_src == FTP_JOB_REQ_SRC_SMS_FIRMWARE) {
				char buff[48];

				switch (ftp_job_result.dfu_res.code) {
				case FTP_JOB_DFU_RES_FILE_DOWNLOADED:
					sprintf(buff, "Firmware downloaded. Reboot\x1A");
					sim800_send_sms(rec_sms_phone_number, buff);
					break;
				case FTP_JOB_DFU_RES_NO_FILE:
					sprintf(buff, "Firmware not found\x1A");
					sim800_send_sms(rec_sms_phone_number, buff);
					break;
				default:
					sprintf(buff, "Firmware download error\x1A");
					sim800_send_sms(rec_sms_phone_number, buff);
					break;
				}

				ZF_LOGW(buff);
				osDelay(2000);
			}

//			while (1) {
//				WDG_RESET(TASK_ALL);
//				osSignalSet(monitorTaskHandle, SIGNAL_TASK_GSM);
//				osDelay(5000);
//				user_printf(">>> Loop\r\n");
//			}

			if (ftp_job_result.dfu_res.code == FTP_JOB_DFU_RES_FILE_DOWNLOADED || ftp_job_result.conf_res.code == FTP_JOB_CONF_RES_NEW_APPLIED) {

				if (ftp_job_result.dfu_res.code == FTP_JOB_DFU_RES_FILE_DOWNLOADED) {
					ZF_LOGF("Reboot to DFU");
				}
				else {
					ZF_LOGF("Reboot to settings update");
				}

				osDelay(1000);
				system_reset(NULL);
			}

			ftp_job_request = false;
			ftp_file_type = FTP_FILE_NONE;
			ftp_file_name[0] = '\0';

			check_settings_unlock();

			module_state = MODULE_STATE_HW_RESET;
			ZF_LOGI("New module state: %s", gsm_state_to_str(module_state));
		}
		break;

		default: {
			ZF_LOGE("Unknown module state: %u", module_state);

			module_state = MODULE_STATE_HW_RESET;
			ZF_LOGI("New module state: %s", gsm_state_to_str(module_state));
		}
		break;

		}
	}
}

