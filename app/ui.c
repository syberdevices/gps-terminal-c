/*
 * ui.c
 *
 *  Created on: 25 сент. 2018 г.
 *      Author: Denis Shreiber
 */

// C includes
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
// HAL includes
#include "rtos.h"
#include "usbd_cdc_if.h"
#include "usb_device.h"
#include "main.h"
// User includes
#include "common.h"
#include "settings/settings.h"
#include "log/flash_fifo/flash_fifo.h"
#include "sim800/sim800.h"

//======================================================================================

#ifndef DEBUG
#define MAIN_MENU				"\r\n"												\
								"============================================\r\n"	\
								"============= GPS Terminal =================\r\n"	\
								"============= Service Mode =================\r\n"	\
								"\r\n"												\
								"\tb   - Board info\r\n"					        \
								"\ts   - Show settings\r\n"							\
								"\tl   - Load settings file\r\n"					\
								"\tet  - Erase track data\r\n"						\
								"\tel  - Erase system log data\r\n"					\
								"\te   - Erase all flash data (with settings)\r\n"	\
								"\tr   - Reboot\r\n"								\
								"\r\n"												\
								"============================================\r\n"	\
								"\r\n"												\
								"Enter a command...\r\n"
#else
#define MAIN_MENU				"\r\n"												\
								"============================================\r\n"	\
								"============= GPS Terminal =================\r\n"	\
								"============= Service Mode =================\r\n"	\
								"\r\n"												\
								"\tb   - Board info\r\n"					        \
								"\ts   - Show settings\r\n"							\
								"\tl   - Load settings file\r\n"					\
								"\tet  - Erase track data\r\n"						\
								"\tel  - Erase system log data\r\n"					\
								"\te   - Erase all flash data (with settings)\r\n"	\
								"\tat  - Enter AT Command mode\r\n"					\
								"\tr   - Reboot\r\n"								\
								"\r\n"												\
								"============================================\r\n"	\
								"\r\n"												\
								"Enter a command...\r\n"
#endif

#define UI_BUFF_LEN				CONF_JSON_MAX_LEN

#define CHECK_SNPRINTF_RET(snprintf_ret)			do {							\
														int ret = (snprintf_ret);	\
														written += ret;				\
														max_len -= ret;				\
														if (max_len < 1) {			\
															return -1;				\
														}							\
													} while(0);

typedef enum {
	UI_STATE_LOG_MODE,
	UI_STATE_PASSWORD_MODE,
	UI_STATE_SERVICE_MODE,
	UI_STATE_AT_MODE
}ui_state_t;

typedef enum {
	PM_NONE,
	PM_LOAD_SETTINGS_FILE,
}program_mode_t;

//======================================================================================

ui_state_t ui_state;
program_mode_t program_mode_type;
char ui_buff[UI_BUFF_LEN];

extern bool enableShutdown;
extern bool enableSleepMode;

//======================================================================================

/**
 * Вывести данные в UI
 * @param ptr
 * @param len
 * @return Количество записанных байт
 */
int ui_write(char *ptr, int len) {
	uint32_t q = 0;

	for (; q < len; q++) {
		osMessagePut(usboutQueueHandle, ptr[q], 0);
	}

	return q;
}

/**
 * Прочитать данные из USB
 * @param data
 * @param len
 * @return
 */
int ui_read(uint8_t* data, uint32_t len) {
	uint32_t q = 0;

	if (len > 2048) {
		len = 2048;
	}

	for (; q < len; q++) {
		osMessagePut(usbinQueueHandle, data[q], 0);
	}

	return USBD_OK;
}

int user_printf(const char* format, ...) {
	int ret = -1;

	if (ui_state == UI_STATE_LOG_MODE) {
		va_list arg;
		va_start(arg, format);
		ret = vprintf(format, arg);
		va_end(arg);
	}

	return ret;
}

int user_vprintf(const char* format, va_list arg) {
	if (ui_state == UI_STATE_LOG_MODE) {
		return vprintf(format, arg);
	}

	return -1;
}

//======================================================================================

static void usb_print_data(uint8_t *data, uint32_t len) {
	int timeout_ms = 50;
	while(CDC_Transmit_FS(data, len) != USBD_OK && timeout_ms-- > 0) {
		osDelay(1);
	}
}

static void usb_print_queue() {
	while(1) {
		uint8_t buff[512];
		uint16_t q = 0;
		for (; q < 512; ) {
			osEvent evt = osMessageGet(usboutQueueHandle, 10);
			if (evt.status == osEventMessage) {
				buff[q++] = (uint8_t)evt.value.v;
			}
			else {
				break;
			}
		}

		if (q != 0) {
			usb_print_data(buff, q);
		}
		else {
			break;
		}
	}

	return;
}

/**
 * Получить файл из USB
 * @param timeout_before Таймаут ожидания начала приема
 * @param timeout_after Таймат, после которого файл считается принятым
 * @return указатель на данные или NULL
 */
static char *ui_get_file(uint32_t timeout_before, uint32_t timeout_after) {
	uint32_t cnt = 0;

	osEvent evt = osMessageGet(usbinQueueHandle, timeout_before);
	if (evt.status != osEventMessage) {
		return NULL;
	}

	/// \todo \warning Нет защиты от перполнения буфера

	ui_buff[cnt++] = (char)evt.value.v;

	while(1) {
		evt = osMessageGet(usbinQueueHandle, timeout_after);
		if (evt.status == osEventMessage) {
			ui_buff[cnt++] = (char)evt.value.v;
		}
		else {
			break;
		}
	}

	ui_buff[cnt] = '\0';

	return ui_buff;
}

/**
 * Получить сообщение-строку из USB
 * @param timeout
 * @return Указатель на пришедшую строку-сообщение
 */
static char *ui_get_message_str(uint32_t timeout) {
#define UI_IN_BUFF_SIZE		64

	static char inbuff[UI_IN_BUFF_SIZE] = {0};
	static char outbuff[UI_IN_BUFF_SIZE] = {0};
	static uint32_t cnt = 0;
	osEvent evt;

	///todo Обработать бэкспейс и esc
	evt = osMessageGet(usbinQueueHandle, timeout);
	if (evt.status == osEventMessage && (char)evt.value.v != '\r') {
		char byte = (char)evt.value.v;

		usb_print_data((uint8_t*)&byte, 1);

		if (byte == 0x08) {	// Backspase
			cnt--;
		}

		if (cnt >= UI_IN_BUFF_SIZE) {
			cnt = 0;
		}

		if (byte != '\n') {
			inbuff[cnt++] = byte;
		}
	}

	if ((char)evt.value.v == '\r') {
		usb_print_data((uint8_t*)"\r\n", 2);

		inbuff[cnt] = '\0';
		cnt = 0;
		strcpy(outbuff, inbuff);

		return outbuff;
	}

	return NULL;
}

static bool is_command(char *str, const char *cmd) {
	if ( strlen(str) == strlen(cmd) && strstr(str, cmd) ) {
		return true;
	}
	else {
		return false;
	}
}

//static bool all_digits(char *str) {
//	size_t len = strlen(str);
//
//	for (int q = 0; q < len; q ++) {
//		if (str[q] < 0x30 || str[q] > 0x39) {
//			return false;
//		}
//	}
//
//	return true;
//}

static void print_board_info() {
	float tmp;

	printf("-------======== Board Info: ========--------\r\n");

	printf("{\"unixtime\":\"%u\",", (unsigned int)time(NULL));
	printf("\"fw_version\":\"%c.%u.%u\",", board_status.fw_ver[2], board_status.fw_ver[1], board_status.fw_ver[0]);
	printf("\"hw_version\":\"%c.%u.%u\",", board_status.hw_ver[2], board_status.hw_ver[1], board_status.hw_ver[0]);
	printf("\"id\":\"%u\",", (unsigned int)settings1_ram.id);
	printf("\"gsm_phy\":\"%s\",", board_status.gsm.phy ? "Ok" : "Err");
	printf("\"gsm_sim\":\"%s\",", board_status.gsm.sim ? "Ok" : "Err");
	printf("\"gsm_gprs\":\"%s\",", board_status.gsm.gprs ? "Ok" : "Err");
	printf("\"gsm_tcp_conn\":\"%s\",", board_status.gsm.tcp_conn ? "Ok" : "Err");
	printf("\"gsm_tcp_tx\":\"%s\",", board_status.gsm.tcp_tx ? "Ok" : "Err");
	printf("\"imei\":\"%s\",", board_status.gsm.imei ? board_status.gsm.imei : "");
	printf("\"gsm_rssi\":\"%u%% (%d dBm)\",", (unsigned int)board_status.gsm.signal_strength.percent, (int)board_status.gsm.signal_strength.rssi);
	printf("\"gnss_phy\":\"%s\",", board_status.gnss.phy ? "Ok" : "Err");
	printf("\"gnss_fix\":\"%s\",", board_status.gnss.fix == GNSS_FIX_3D ? "3D" : board_status.gnss.fix == GNSS_FIX_2D ? "2D" : "");
	printf("\"gnss_hdop\":\"%d,%02d\",", (int)board_status.gnss.hdop, (int)fabsf(modff(board_status.gnss.hdop, &tmp)* 100));
	printf("\"bat_voltage\":\"%d,%03d\",", (int)board_status.batt.voltage, (int)fabsf(modff(board_status.batt.voltage, &tmp)* 1000));
	printf("\"bat_charging\":\"%s\",", board_status.batt.charging ? "Yes" : "No");
	printf("\"power_voltage\":\"%d,%03d\",", (int)board_status.power.voltage, (int)fabsf(modff(board_status.power.voltage, &tmp)* 1000));
	printf("\"accel_phy\":\"%s\",", board_status.accel.phy ? "Ok" : "Err");
	printf("\"accel_motion\":\"%s\",", board_status.motion.active ? "Yes" : "No");
	printf("\"spi_flash\":\"%s\",", board_status.spi_flash.phy ? "Ok" : "Err");
	printf("\"din1\":\"%d\",", (int)((board_status.io.din_mask & 0x01) == 0x01));
	printf("\"din2\":\"%d\",", (int)((board_status.io.din_mask & 0x02) == 0x02));
	printf("\"din3\":\"%d\",", (int)((board_status.io.din_mask & 0x04) == 0x04));
	printf("\"din4\":\"%d\",", (int)((board_status.io.din_mask & 0x08) == 0x08));
	printf("\"count1\":\"%u\",", (unsigned int)(board_status.io.din_counter[0]));
	printf("\"count2\":\"%u\",", (unsigned int)(board_status.io.din_counter[1]));
	printf("\"count3\":\"%u\",", (unsigned int)(board_status.io.din_counter[2]));
	printf("\"count4\":\"%u\",", (unsigned int)(board_status.io.din_counter[3]));
	printf("\"dout1\":\"%d\",", (int)((board_status.io.dout_mask & 0x01) == 0x01));
	printf("\"dout2\":\"%d\",", (int)((board_status.io.dout_mask & 0x02) == 0x02));
	printf("\"ain1\":\"%d,%03d\",", (int)board_status.io.ain[0], (int)fabsf(modff(board_status.io.ain[0], &tmp)* 1000));
	printf("\"ain2\":\"%d,%03d\",", (int)board_status.io.ain[1], (int)fabsf(modff(board_status.io.ain[1], &tmp)* 1000));
	printf("\"lls\":[");
	for (int q = 0; q < 8; q++) {
		if (settings1_ram.fls[q].used && fuel_data[q].ok) {
			printf("{\"address\":\"%u\",\"level\":\"%u\", \"temp\":\"%d\"}", (unsigned int)settings1_ram.fls[q].address, (unsigned int)fuel_data[q].level, (int)fuel_data[q].temp);
		}
		else {
			printf("{\"address\":\"\",\"level\":\"\", \"temp\":\"\"}");
		}

		if (q < 7) {
			printf(",");
		}
	}
	printf("],");
	printf("\"ds18b20\":[");
	for (int q = 0; q < 8; q++) {
		if (q < ow_temp.count) {
			uint8_t *code = ow_temp.dev[q].code;
			printf("{\"id\":\"%02X-%02X-%02X-%02X-%02X-%02X-%02X-%02X\",\"temp\":\"%d\"}", code[7], code[6], code[5], code[4], code[3], code[2], code[1], code[0], (int)ow_temp.dev[q].val);
		}
		else {
			printf("{\"id\":\"\",\"temp\":\"\"}");
		}

		if (q < 7) {
			printf(",");
		}
	}
	printf("],");
	printf("\"ibutton\":[");
	for (int q = 0; q < 3; q++) {
		if (q < ow_ibutton.count) {
			uint8_t *code = ow_ibutton.dev[q].code;
			printf("{\"id\":\"%02X-%02X-%02X-%02X-%02X-%02X-%02X-%02X\"}", code[7], code[6], code[5], code[4], code[3], code[2], code[1], code[0]);
		}
		else {
			printf("{\"id\":\"\"}");
		}

		if (q < 2) {
			printf(",");
		}
	}
	printf("],");
	printf("\"ignition\":\"%s\",", board_status.ignition == IGNITION_DISABLED ? "disabled" : board_status.ignition == IGNITION_ON ? "on" : "off");
	printf("\"mode\":\"%s\",", cache.device_mode == DEVICE_MODE_STANDBY ? "standby" : "active");
	printf("\"dev_case\":\"%s\",", board_status.casing.open ? "open" : "closed");
	printf("\"temp_mcu\":\"%d\",", board_status.mcu.temp);
	printf("\"can_rpm\":\"%d\",", (int)roundf(canlog_data.rpm));
	printf("\"can_fuel_l\":\"%d\",", (int)roundf(canlog_data.fuel_liter));
	printf("\"can_fuel_p\":\"%d\",", (int)roundf(canlog_data.fuel_percent));
	printf("\"can_odo\":\"%d\",", (int)roundf(canlog_data.odometer));
	printf("\"can_temp\":\"%d\",", (int)canlog_data.temp);
	printf("\"can_ign\":\"%d\",", (int)canlog_data.security_flags.ignition == CANLOG_SEC_FLAG_IGN_ON);
	printf("\"can_check_oil\":\"%d\",", (int)canlog_data.indication_flags.oil_level == CANLOG_IND_FLAG_OIL_LEVEL_ON);
	printf("\"alert_temp_eng\":\"%s\",", board_status.alert.high_engine_temp ? "Yes" : "No");
	printf("\"alert_rpm\":\"%s\",", board_status.alert.high_rpm ? "Yes" : "No");
	printf("\"alert_check_oil\":\"%s\"}", board_status.alert.check_oil ? "Yes" : "No");

	printf("\r\n");
	fflush(stdout);
}

static void print_settings() {
	printf("--------========= Settings: ========---------\r\n");

	(void)pack_settings(ui_buff, UI_BUFF_LEN, &settings1_ram, &settings2_ram, &settings3_ram, &settings4_ram);

	printf(ui_buff);

	fflush(stdout);
}

int pack_settings(char * str, int max_len, settings1_t *s1, settings2_t *s2, settings3_t *s3, settings4_t *s4) {
	int written = 0;

	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "{\"password\":\"%s\",", s1->password));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"id\":\"%u\",",(unsigned int)s1->id));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"protocol\":\"%s\",", s1->protocol == PROTOCOL_EGTS ? "EGTS" : "Wialon"));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"period\":{\"data_send\":{\"in_active_mode\":\"%u\",", (unsigned int)s1->period.data_send.in_active_mode));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"in_standby_mode\":\"%u\"},", (unsigned int)s1->period.data_send.in_standby_mode));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"data_write\":{\"in_active_mode\":\"%u\",", (unsigned int)s1->period.data_write.in_active_mode));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"in_standby_mode\":\"%u\"},", (unsigned int)s1->period.data_write.in_standby_mode));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"fls_read\":{\"in_active_mode\":\"%u\",", (unsigned int)s1->period.fls_read.in_active_mode));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"in_standby_mode\":\"%u\"}},", (unsigned int)s1->period.fls_read.in_standby_mode));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"fls_sensor\":["));
	for (int q = 0; q < 8; q++) {
		if (s1->fls[q].used) {
			CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "{\"address\":\"%u\"}", (unsigned int)s1->fls[q].address));
		}
		else {
			CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "{\"address\":\"\"}"));
		}

		if (q < 7) {
			CHECK_SNPRINTF_RET(snprintf(str + written, max_len, ","));
		}
	}
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "],"));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"ds18b20\":["));
	for (int q = 0; q < 8; q++) {
		if (s1->ow_temp[q].used) {
			uint8_t *code = s1->ow_temp[q].id;
			CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "{\"id\":\"%02X-%02X-%02X-%02X-%02X-%02X-%02X-%02X\"}",
													code[7], code[6], code[5], code[4], code[3], code[2], code[1], code[0]));
		}
		else {
			CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "{\"id\":\"\"}"));
		}

		if (q < 7) {
			CHECK_SNPRINTF_RET(snprintf(str + written, max_len, ","));
		}
	}
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "],"));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"gnss\":{\"filter\":\"%s\",", s1->gnss.filter_on ? "on" : "off"));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"freeze_speed\":\"%u\",", (unsigned int)s1->gnss.freeze_speed));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"freeze_time\":\"%u\",", (unsigned int)s1->gnss.freeze_time));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"hdop_max\":\"%u.%u\",", (unsigned int)s1->gnss.hdop_max, (unsigned int)(s1->gnss.hdop_max * 10) % 10));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"satellites_min\":\"%u\",", (unsigned int)s1->gnss.satellites_min));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"max_speed\":\"%u\",", (unsigned int)s1->gnss.max_speed));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"speed_avg\":\"%u\"},", (unsigned int)s1->gnss.speed_avg));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"min_rssi\":\"%u\",", (unsigned int)s1->min_rssi));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"untracked_course\":\"%u\",", (unsigned int)s1->untracked_course));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"untracked_distance\":\"%u\",", (unsigned int)s1->untracked_distance));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"acceleration_threshold\":\"%u\",", (unsigned int)s1->acceleration_threshold));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"apn\":{\"address\":\"%s\",", s1->apn.address));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"user\":\"%s\",", s1->apn.user));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"password\":\"%s\"},", s1->apn.password));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"main_host\":{\"address\":\"%s\",", s1->main_host.address));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"port\":\"%u\",", (unsigned int)s1->main_host.port));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"wait_response\":\"%s\"},", s1->main_host.wait_response ? "true" : "false"));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"log_host\":{\"address\":\"%s\",", s1->log_host.address));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"port\":\"%u\"},", (unsigned int)s1->log_host.port));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"dins\":["));
	for (int q = 0; q < 4; q++) {
		CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "{\"type\":\"%s\",", s1->dins[q].type == NORMALLY_OPEN ? "NO" : "NC"));
		CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"mode\":\"%u\"}", (unsigned int)s1->dins[q].mode));

		if (q < 3) {
			CHECK_SNPRINTF_RET(snprintf(str + written, max_len, ","));
		}
	}
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "],"));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"douts\":[{\"type\":\"%s\"},{", s1->douts[0].type == NORMALLY_OPEN ? "NO" : "NC"));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"type\":\"%s\"}],", s1->douts[1].type == NORMALLY_OPEN ? "NO" : "NC"));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"ains\":[{\"threshold\":\"%u.%u\"},{", (unsigned int)s1->ains[0].threshold, (unsigned int)(s1->ains[0].threshold * 10) % 10));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"threshold\":\"%u.%u\"}],", (unsigned int)s1->ains[1].threshold, (unsigned int)(s1->ains[1].threshold * 10) % 10));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"white_phone_numbers\":["));
	for (int q = 0; q < WHITE_PHONE_NUM_1; q++) {
		CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"%s\",", s1->white_phones_1[q].number));
	}
	for (int q = 0; q < WHITE_PHONE_NUM_2; q++) {
		CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"%s\"", s4->white_phones_2[q].number));

		if (q < (WHITE_PHONE_NUM_2 - 1)) {
			CHECK_SNPRINTF_RET(snprintf(str + written, max_len, ","));
		}
	}
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "],"));

	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"master_phone_numbers\":["));
	for (int q = 0; q < USER_MASTER_PHONE_NUM; q++) {
		CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"%s\"", s4->user_master_phones[q].number));

		if (q < (USER_MASTER_PHONE_NUM - 1)) {
			CHECK_SNPRINTF_RET(snprintf(str + written, max_len, ","));
		}
	}
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "],"));

	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"bluetooth\":{\"used\":\"%s\",", s1->bluetooth.used ? "true" : "false"));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"auto_answer\":\"%s\",", s1->bluetooth.auto_answer ? "true" : "false"));
	if (s1->bluetooth.used) {
		uint8_t *code = s1->bluetooth.mac;
		CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"mac\":\"%02X-%02X-%02X-%02X-%02X-%02X\"},", code[0], code[1], code[2], code[3], code[4], code[5]));
	}
	else {
		CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"mac\":\"\"},"));
	}
	// Settings2
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"auto_id\":\"%s\",", s2->auto_id == AUTO_ID_ENABLED ? "true" : "false"));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"log_level\":\"%s\",", s2->log_level == ZF_LOG_VERBOSE ? "Verbose" :
									s2->log_level == ZF_LOG_DEBUG ? "Debug" :
									s2->log_level == ZF_LOG_INFO ? "Info" :
									s2->log_level == ZF_LOG_WARN ? "Warning" :
									s2->log_level == ZF_LOG_ERROR ? "Error" :
									s2->log_level == ZF_LOG_FATAL ? "Fatal" : "None"));

	// Setting3
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"can\":{\"used\":\"%s\",", s3->can.used ? "true" : "false"));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"used_param\":\"%u\",", (unsigned int)s3->can.used_param.all));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"period\":{\"in_active_mode\":\"%u\",", (unsigned int)s3->can.send_period.in_active_mode));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"in_standby_mode\":\"%u\"},", (unsigned int)s3->can.send_period.in_standby_mode));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"alert\":{\"temp_trh\":{\"max\":\"%u\",", (unsigned int)s3->can.alert.on_temp_trh.temp));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"duration\":\"%u\"},", (unsigned int)s3->can.alert.on_temp_trh.duration));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"rpm_trh\":{\"max\":\"%u\",", (unsigned int)s3->can.alert.on_rpm_trh.max));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"duration\":\"%u\"},", (unsigned int)s3->can.alert.on_rpm_trh.duration));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"on_check_oil\":\"%s\"},", (unsigned int)s3->can.alert.on_check_oil ? "true" : "false"));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"gen_pack_on_ign\":\"%s\"},", (unsigned int)s3->can.gen_packet_on_ignition ? "true" : "false"));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"auto_update\":\"%s\",", (unsigned int)s4->auto_update ? "true" : "false"));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "\"settings_version\":\"%u\"", (unsigned int)s4->settings_version));
	CHECK_SNPRINTF_RET(snprintf(str + written, max_len, "}\r\n"));

	return 0;
}

typedef struct
{
    bool is_empty;
    union
    {
        int d;
        float f;
    };
	char *str;
} json_val_t;

typedef enum
{
    JSON_VAL_TYPE_D,
    JSON_VAL_TYPE_F,
    JSON_VAL_TYPE_STR,
} json_val_type_t;

static int json_parse_field(const char *json_str, const char *field_str, json_val_type_t val_type, json_val_t *json_val) {
    char *ptr = strstr(json_str, field_str);
    if (ptr == NULL) {
        return -1;
    }
    
    ptr += strlen(field_str);
    
    if (strstr(ptr, "\":\"") != ptr) {
        return -1;
    }
    
    ptr += 3;
    
    bool is_empty;
    
    if (ptr[0] == '\"') {
        is_empty = true;
    }
    else {
        bool success = false;
        bool is_digit = isdigit(ptr[0]) || ((ptr[0] == '-') && isdigit(ptr[1]));
        
        switch (val_type) {
            case JSON_VAL_TYPE_D:
                if (is_digit) {
                    json_val->d = atoi(ptr);
                    is_empty = false;
                    success = true;
                }
                break;
            
            case JSON_VAL_TYPE_F:
                if (is_digit) {
                    json_val->f = atof(ptr);
                    is_empty = false;
                    success = true;
                }
                break;
            
            case JSON_VAL_TYPE_STR: {
                char *end_ptr = strchr(ptr, '\"');
                if (end_ptr) {
                    if (json_val->str) {
                        int len = end_ptr - ptr;
                        memcpy(json_val->str, ptr, len);
                        json_val->str[len] = '\0';
                        
                        is_empty = false;
                        success = true;
                    }
                }
                break;
            }
        }
        
        if (success == false) {
            return -1;
        }
    }
    
    json_val->is_empty = is_empty;
    
    return 0;
}

static char *json_next_field (const char *json_str) {
    char *ptr = strchr(json_str, ',');
    if (ptr) {
        ptr = strchr(ptr, '\"');
    }
    
    return ptr;
}

int parse_settings(char *str, settings1_t *s1, settings2_t *s2, settings3_t *s3, settings4_t *s4) {
	char *ptr = str;

	// Копируем текущие настройки, т.к. если json-поле будет пустым, то нужно оставить настройку неизмененой
	// Также при этом остаются неизмененными настройки FTP сервера и данные одометра

	memcpy(s1, &settings1_ram, sizeof(settings1_t));
	memcpy(s2, &settings2_ram, sizeof(settings2_t));
	memcpy(s3, &settings3_ram, sizeof(settings3_t));
	memcpy(s4, &settings4_ram, sizeof(settings4_t));

	char buff[48];
	json_val_t json_val;
	json_val.str = buff;

	// Settings 1

	// password
	if (json_parse_field(ptr, "password", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strcmp(json_val.str, "x") == 0) {
			// Пароль "x" запрещен, т.к. это команда выхода из меню
			return -1;
		}
		strcpy(s1->password, json_val.str);
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// id
	if (json_parse_field(ptr, "id", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->id = json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// protocol
	if (json_parse_field(ptr, "protocol", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strcmp(json_val.str, "EGTS") == 0) {
			s1->protocol = PROTOCOL_EGTS;
		}
		else if (strcmp(json_val.str, "Wialon") == 0) {
			s1->protocol = PROTOCOL_WIALON;
		}
		else {
			return -1;
		}
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// period->data_send->in_active_mode
	if (json_parse_field(ptr, "in_active_mode", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->period.data_send.in_active_mode = (uint16_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// period->data_send->in_standby_mode
	if (json_parse_field(ptr, "in_standby_mode", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->period.data_send.in_standby_mode = (uint16_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// period->data_write->in_active_mode
	if (json_parse_field(ptr, "in_active_mode", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->period.data_write.in_active_mode = (uint16_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// period->data_write->in_standby_mode
	if (json_parse_field(ptr, "in_standby_mode", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->period.data_write.in_standby_mode = (uint16_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// period->fls_read->in_active_mode
	if (json_parse_field(ptr, "in_active_mode", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->period.fls_read.in_active_mode = (uint16_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// period->fls_read->in_standby_mode
	if (json_parse_field(ptr, "in_standby_mode", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->period.fls_read.in_standby_mode = (uint16_t)json_val.d;
	}

	// fls_sensor
	for (int q = 0; q < 8; q++) {
		if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

		// fls_sensor->address[]
		if (json_parse_field(ptr, "address", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
		if (!json_val.is_empty) {
			if (json_val.d > 255) {
				return -1;
			}
			s1->fls[q].used = true;
			s1->fls[q].address = (uint8_t)json_val.d;
		}
		else {
			// Номер датчика может отсутствовать. Это значит - датчик не используется
			s1->fls[q].used = false;
		}
	}

	// ds18b20
	for (int q = 0; q < 8; q++) {
		if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

		// ds18b20->id[]
		if (json_parse_field(ptr, "id", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
		if (!json_val.is_empty) {
			unsigned int id[8];
			int ret = user_sscanf(json_val.str, "%02X-%02X-%02X-%02X-%02X-%02X-%02X-%02X", &id[7], &id[6], &id[5], &id[4], &id[3], &id[2], &id[1], &id[0]);
			if (ret == 8) {
				for (int w = 0; w < 8; w++) {
					s1->ow_temp[q].id[w] = (uint8_t)id[w];
				}
				s1->ow_temp[q].used = true;
			}
			else {
				return -1;
			}
		}
		else {
			// Датчик не используется
			s1->ow_temp[q].used = false;
		}
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// gnss->filter
	if (json_parse_field(ptr, "filter", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strcmp(json_val.str, "on") == 0) {
			s1->gnss.filter_on = true;
		}
		else if (strcmp(json_val.str, "off") == 0) {
			s1->gnss.filter_on = false;
		}
		else {
			return -1;
		}
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// gnss->freeze_speed
	if (json_parse_field(ptr, "freeze_speed", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->gnss.freeze_speed = (uint8_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// gnss->freeze_time
	if (json_parse_field(ptr, "freeze_time", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->gnss.freeze_time = (uint16_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// gnss->hdop_max
	if (json_parse_field(ptr, "hdop_max", JSON_VAL_TYPE_F, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->gnss.hdop_max = json_val.f;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// gnss->satellites_min
	if (json_parse_field(ptr, "satellites_min", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->gnss.satellites_min = (uint8_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// gnss->max_speed
	if (json_parse_field(ptr, "max_speed", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->gnss.max_speed = (uint8_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// gnss->speed_avg
	if (json_parse_field(ptr, "speed_avg", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (json_val.d > SPEED_AVG_BUFF_LEN) {
			return -1;
		}
		s1->gnss.speed_avg = (uint8_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// min_rssi
	if (json_parse_field(ptr, "min_rssi", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->min_rssi = (uint8_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// untracked_course
	if (json_parse_field(ptr, "untracked_course", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->untracked_course = (uint8_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// untracked_distance
	if (json_parse_field(ptr, "untracked_distance", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->untracked_distance = (uint16_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// acceleration_threshold
	if (json_parse_field(ptr, "acceleration_threshold", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->acceleration_threshold = (uint16_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// apn->address
	if (json_parse_field(ptr, "address", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strlen(json_val.str) > sizeof(s1->apn.address) - 1) { return -1; }
		strcpy(s1->apn.address, json_val.str);
	}
	else {
		bzero(s1->apn.address, sizeof(s1->apn.address));
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// apn->user
	if (json_parse_field(ptr, "user", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strlen(json_val.str) > sizeof(s1->apn.user) - 1) { return -1; }
		strcpy(s1->apn.user, json_val.str);
	}
	else {
		bzero(s1->apn.user, sizeof(s1->apn.user));
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// apn->password
	if (json_parse_field(ptr, "password", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strlen(json_val.str) > sizeof(s1->apn.password) - 1) { return -1; }
		strcpy(s1->apn.password, json_val.str);
	}
	else {
		bzero(s1->apn.password, sizeof(s1->apn.password));
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// main_host->address
	if (json_parse_field(ptr, "address", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strlen(json_val.str) > sizeof(s1->main_host.address) - 1) { return -1; }
		strcpy(s1->main_host.address, json_val.str);
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// main_host->port
	if (json_parse_field(ptr, "port", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->main_host.port = (uint32_t)json_val.d;
	}
	
	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// main_host->wait_response
	if (json_parse_field(ptr, "wait_response", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strcmp(json_val.str, "true") == 0) {
			s1->main_host.wait_response = true;
		}
		else if (strcmp(json_val.str, "false") == 0) {
			s1->main_host.wait_response = false;
		}
		else {
			return -1;
		}
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// log_host->address
	if (json_parse_field(ptr, "address", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strlen(json_val.str) > sizeof(s1->log_host.address) - 1) { return -1; }
		strcpy(s1->log_host.address, json_val.str);
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// log_host->port
	if (json_parse_field(ptr, "port", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s1->log_host.port = (uint32_t)json_val.d;
	}

	// dins
	for (int q = 0; q < sizeof(s1->dins)/sizeof(s1->dins[0]); q++) {
		if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

		// dins->type
		if (json_parse_field(ptr, "type", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
		if (!json_val.is_empty) {
			if (strcmp(json_val.str, "NO") == 0) {
				s1->dins[q].type = NORMALLY_OPEN;
			}
			else if (strcmp(json_val.str, "NC") == 0) {
				s1->dins[q].type = NORMALLY_CLOSED;
			}
			else {
				return -1;
			}
		}

		if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

		// dins->mode
		if (json_parse_field(ptr, "mode", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
		if (!json_val.is_empty) {
			if (json_val.d > DI_MODE_WRITE_POINT_TRIGGER) { return -1; }
			s1->dins[q].mode = (di_mode_t)json_val.d;
		}
	}

	// douts
	for (int q = 0; q < sizeof(s1->douts)/sizeof(s1->douts[0]); q++) {
		if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

		// douts->type
		if (json_parse_field(ptr, "type", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
		if (!json_val.is_empty) {
			if (strcmp(json_val.str, "NO") == 0) {
				s1->douts[q].type = NORMALLY_OPEN;
			}
			else if (strcmp(json_val.str, "NC") == 0) {
				s1->douts[q].type = NORMALLY_CLOSED;
			}
			else {
				return -1;
			}
		}
	}

	// ains
	for (int q = 0; q < sizeof(s1->ains)/sizeof(s1->ains[0]); q++) {
		if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

		// ains->threshold
		if (json_parse_field(ptr, "threshold", JSON_VAL_TYPE_F, &json_val) != 0) { return -1; }
		if (!json_val.is_empty) {
			s1->ains[q].threshold = json_val.f;
		}
	}

	// white_phone_numbers
	ptr = strstr(ptr, "white_phone_numbers");
	if (!ptr) { return -1; }

	ptr += strlen("white_phone_numbers\":[");

	for (int q = 0; q < WHITE_PHONE_NUM; q++) {
		ptr = strstr(ptr, "\"");
		if (!ptr) { return -1; }

		ptr++;
		int number_length = strchr(ptr, '"') - ptr;
		if (number_length > sizeof(s1->white_phones_1[0].number) - 1) { return -1; }

		char * p_number;
		if (q < WHITE_PHONE_NUM_1) {
			p_number = s1->white_phones_1[q].number;
		}
		else {
			p_number = s4->white_phones_2[q - WHITE_PHONE_NUM_1].number;
		}

		memcpy(p_number, ptr, number_length);
		p_number[number_length] = '\0';

		ptr = strstr(ptr, "\"") + 1;
	}

	// master_phone_numbers
	ptr = strstr(ptr, "master_phone_numbers");
	if (!ptr) { return -1; }

	ptr += strlen("master_phone_numbers\":[");

	for (int q = 0; q < USER_MASTER_PHONE_NUM; q++) {
		ptr = strstr(ptr, "\"");
		if (!ptr) { return -1; }

		ptr++;
		int number_length = strchr(ptr, '"') - ptr;
		if (number_length > sizeof(s4->user_master_phones[0].number) - 1) { return -1; }

		char * p_number = s4->user_master_phones[q].number;
		memcpy(p_number, ptr, number_length);
		p_number[number_length] = '\0';

		ptr = strstr(ptr, "\"") + 1;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// bluetooth->used
	if (json_parse_field(ptr, "used", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strcmp(json_val.str, "true") == 0) {
			s1->bluetooth.used = true;
		}
		else if (strcmp(json_val.str, "false") == 0) {
			s1->bluetooth.used = false;
		}
		else {
			return -1;
		}
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// bluetooth->auto_answer
	if (json_parse_field(ptr, "auto_answer", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strcmp(json_val.str, "true") == 0) {
			s1->bluetooth.auto_answer = true;
		}
		else if (strcmp(json_val.str, "false") == 0) {
			s1->bluetooth.auto_answer = false;
		}
		else {
			return -1;
		}
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// bluetooth->mac
	if (json_parse_field(ptr, "mac", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		unsigned int mac[6];
		int ret = user_sscanf(json_val.str, "%02X-%02X-%02X-%02X-%02X-%02X", &mac[0], &mac[1], &mac[2], &mac[3], &mac[4], &mac[5]);
		if (ret == 6) {
			for (int q = 0; q < 6; q++) {
				s1->bluetooth.mac[q] = (uint8_t)mac[q];
			}
		}
		else {
			return -1;
		}
	}
	else {
		if (!s1->bluetooth.used) {
			// Bluetooth не используется
			bzero(s1->bluetooth.mac, sizeof(s1->bluetooth.mac));
		}
		else {
			return -1;
		}
	}

	// Settings 2

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// auto_id
	if (json_parse_field(ptr, "auto_id", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strcmp(json_val.str, "true") == 0) {
			s2->auto_id = AUTO_ID_ENABLED;
		}
		else if (strcmp(json_val.str, "false") == 0) {
			s2->auto_id = AUTO_ID_DISABLED;
		}
		else {
			return -1;
		}
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// log_level
	if (json_parse_field(ptr, "log_level", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strcmp(json_val.str, "Verbose") == 0) {
			s2->log_level = ZF_LOG_VERBOSE;
		}
		else if (strcmp(json_val.str, "Debug") == 0) {
			s2->log_level = ZF_LOG_DEBUG;
		}
		else if (strcmp(json_val.str, "Info") == 0) {
			s2->log_level = ZF_LOG_INFO;
		}
		else if (strcmp(json_val.str, "Warning") == 0) {
			s2->log_level = ZF_LOG_WARN;
		}
		else if (strcmp(json_val.str, "Error") == 0) {
			s2->log_level = ZF_LOG_ERROR;
		}
		else if (strcmp(json_val.str, "Fatal") == 0) {
			s2->log_level = ZF_LOG_FATAL;
		}
		else if (strcmp(json_val.str, "None") == 0) {
			s2->log_level = ZF_LOG_NONE;
		}
		else {
			return -1;
		}
	}

	// Settings3

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// can->used
	if (json_parse_field(ptr, "used", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strcmp(json_val.str, "true") == 0) {
			s3->can.used = true;
		}
		else if (strcmp(json_val.str, "false") == 0) {
			s3->can.used = false;
		}
		else {
			return -1;
		}
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// can->used_param
	if (json_parse_field(ptr, "used_param", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s3->can.used_param.all = (uint32_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// can->period->in_active_mode
	if (json_parse_field(ptr, "in_active_mode", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s3->can.send_period.in_active_mode = (uint16_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// can->period->in_standby_mode
	if (json_parse_field(ptr, "in_standby_mode", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s3->can.send_period.in_standby_mode = (uint16_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// can->alert->temp_trh->max
	if (json_parse_field(ptr, "max", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s3->can.alert.on_temp_trh.temp = (int16_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// can->alert->temp_trh->duration
	if (json_parse_field(ptr, "duration", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s3->can.alert.on_temp_trh.duration = (uint16_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// can->alert->rpm_trh->max
	if (json_parse_field(ptr, "max", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s3->can.alert.on_rpm_trh.max = (uint16_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// can->alert->rpm_trh->duration
	if (json_parse_field(ptr, "duration", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		s3->can.alert.on_rpm_trh.duration = (uint16_t)json_val.d;
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// can->alert->on_check_oil
	if (json_parse_field(ptr, "on_check_oil", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strcmp(json_val.str, "true") == 0) {
			s3->can.alert.on_check_oil = true;
		}
		else if (strcmp(json_val.str, "false") == 0) {
			s3->can.alert.on_check_oil = false;
		}
		else {
			return -1;
		}
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// can->gen_pack_on_ign
	if (json_parse_field(ptr, "gen_pack_on_ign", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strcmp(json_val.str, "true") == 0) {
			s3->can.gen_packet_on_ignition = true;
		}
		else if (strcmp(json_val.str, "false") == 0) {
			s3->can.gen_packet_on_ignition = false;
		}
		else {
			return -1;
		}
	}

	// Settings 4

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// auto_update
	if (json_parse_field(ptr, "auto_update", JSON_VAL_TYPE_STR, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		if (strcmp(json_val.str, "true") == 0) {
			s4->auto_update = true;
		}
		else if (strcmp(json_val.str, "false") == 0) {
			s4->auto_update = false;
		}
		else {
			return -1;
		}
	}

	if ((ptr = json_next_field(ptr)) == NULL) { return -1; }

	// settings_version
	if (json_parse_field(ptr, "settings_version", JSON_VAL_TYPE_D, &json_val) != 0) { return -1; }
	if (!json_val.is_empty) {
		/// \note Не применяем settings_version, потмоу что этот параметр только для чтения
	}

	return 0;
}

static ui_state_t ui_log_mode_case(char *str) {
	ui_state_t ui_state = UI_STATE_LOG_MODE;

	if (is_command(str, "x")) {		// Try enter to Service Mode
		ui_state = UI_STATE_PASSWORD_MODE;
		printf("\r\nPlease enter the password\r\n");
	}

	return ui_state;
}

static ui_state_t ui_password_mode_case(char *str) {
	if (is_command(str, "x")) {
		printf("[INF] Go back to Log Mode...\r\n");

		return UI_STATE_LOG_MODE;
	}
	else if (is_command(str, settings1_ram.password) || is_command(str, MASTER_PASSWORD)) {		// Enter to Service Mode
		enableShutdown = false;
		enableSleepMode = false;
		printf(MAIN_MENU);

		return UI_STATE_SERVICE_MODE;
	}
	else {
		printf("Incorrect password\r\n");

		return UI_STATE_PASSWORD_MODE;
	}
}

static ui_state_t ui_program_mode_first_level_case(char *str) {
	if (is_command(str, "x")) {		// Exit from Service Mode
		enableShutdown = true;
		enableSleepMode = true;
		printf("[INF] Go back to Log Mode...\r\n");

		return UI_STATE_LOG_MODE;
	}
	else if (is_command(str, "b")) {
		print_board_info();
	}
	else if (is_command(str, "s")) {
		print_settings();
	}
	else if (is_command(str, "l")) {
		printf("[INF] Please send settings file...\r\n");
		usb_print_queue();

		WDG_RESET(TASK_ALL);

		char *str = ui_get_file(2*60*1000, 200);
		if (str) {
			settings1_t sett1 = {0};
			settings2_t sett2 = {0};
			settings3_t sett3 = {0};
			settings4_t sett4 = {0};

			int res = parse_settings(str, &sett1, &sett2, &sett3, &sett4);
			if (res == 0) {
				if (!check_settings_locked()) {
					check_settings_lock();

						taskENTER_CRITICAL();

						sett1.rom.odometer = settings1_ram.rom.odometer;
						memcpy(&settings1_ram, &sett1, sizeof(settings1_t));
						memcpy(&settings2_ram, &sett2, sizeof(settings2_t));
						memcpy(&settings3_ram, &sett3, sizeof(settings3_t));
						memcpy(&settings4_ram, &sett4, sizeof(settings4_t));

						taskEXIT_CRITICAL();

						res = write_settings(SETT_AREA_EXT_INT_FLASH);

					check_settings_unlock();

					WDG_RESET(TASK_ALL);

					if (res == 0) {
						printf("[INF] Settings saved successfully!\r\n");
						usb_print_queue();
						print_settings();
						usb_print_queue();
						printf("[INF] Reboot...\r\n");
						usb_print_queue();

						osDelay(500);
						system_reset("Settings saved. Reboot");
					}
					else {
						printf("[ERR] Could not save settings\r\n");
					}
				}
				else {
					printf("[ERR] Busy. Repeat please\r\n");
				}
			}
			else {
				printf("[ERR] Incorrect settings file:\r\n%s\r\n", str);
			}
		}
		else {
			printf("[ERR] Settings file is not received\r\n");
		}

		usb_print_queue();
	}
	else if (is_command(str, "et")) {
		printf("[INF] %s now erasing. PLease wait about %d minutes...\r\n", "Tracks", 2);
		usb_print_queue();

		flash_fifo_erase_full_memory(&track_fifo);

		printf("[INF] Done!\r\n");
		usb_print_queue();
	}
	else if (is_command(str, "el")) {
		printf("[INF] %s now erasing. PLease wait about %d minutes...\r\n", "Logs", 2);
		usb_print_queue();

		flash_fifo_erase_full_memory(&log_fifo);

		printf("[INF] Done!\r\n");
		usb_print_queue();
	}
	else if (is_command(str, "e")) {
		printf("[INF] %s now erasing. PLease wait about %d minutes...\r\n", "Chip", 2);
		usb_print_queue();

		erase_user_external_flash();
		flash_fifo_erase_full_memory(&log_fifo);
		flash_fifo_erase_full_memory(&track_fifo);

		printf("[INF] Done!\r\n");
		usb_print_queue();
	}
	else if (is_command(str, "at")) {
		printf("[INF] Entering AT Command mode\r\n");
		usb_print_queue();

		return UI_STATE_AT_MODE;
	}
	else if (is_command(str, "r")) {
		printf("[INF] Reboot on command\r\n");
		usb_print_queue();

		system_reset("Reboot on command");
	}
	else {
		printf("[ERR] Unknown command\r\n");
	}

	if (program_mode_type == PM_NONE) {
		usb_print_queue();
		printf(MAIN_MENU);
	}

	return UI_STATE_SERVICE_MODE;
}

static void ui_program_mode_second_level_case(char *str) {
	size_t len = strlen(str);
	if (len == 0) {
		printf("Error. %s\r\n", "No input data");
		program_mode_type = PM_NONE;
		return;
	}

	switch (program_mode_type) {
	case PM_LOAD_SETTINGS_FILE:
		break;

	default:
		break;
	}

	program_mode_type = PM_NONE;
}

static ui_state_t ui_at_mode_case(char *str) {
	if (str) {
		if (is_command(str, "x")) {		// Exit from AT Mode
			usb_print_queue();
			printf(MAIN_MENU);

			return UI_STATE_SERVICE_MODE;
		}
		else {
			char cmd[UI_IN_BUFF_SIZE];
			strcpy(cmd, str);
			strcat(cmd, "\r");

			sim800_direct_uart_tx_str(cmd);
		}
	}

	char byte = sim800_direct_uart_rx_char(0);
	if (byte != (char)-1) {
		usb_print_data((uint8_t*)&byte, 1);
	}

	return UI_STATE_AT_MODE;
}

static ui_state_t ui_program_mode_case(char *str) {
	if (program_mode_type == PM_NONE) {
		return ui_program_mode_first_level_case(str);
	}
	else {
		ui_program_mode_second_level_case(str);

		if (program_mode_type == PM_NONE) {
			printf(MAIN_MENU);
			usb_print_queue();
		}
	}

	return UI_STATE_SERVICE_MODE;;
}

//======================================================================================

void ui_task(void const * argument)
{
	UNUSED(argument);

	MX_USB_DEVICE_Init();

	osDelay(1000);

	while (1) {
		char *str = ui_get_message_str(0);
		if (str) {
			switch (ui_state) {
			case UI_STATE_LOG_MODE:
				ui_state = ui_log_mode_case(str);
				break;

			case UI_STATE_PASSWORD_MODE:
				ui_state = ui_password_mode_case(str);
				break;

			case UI_STATE_SERVICE_MODE:
				ui_state = ui_program_mode_case(str);
				break;

			default:
				break;
			}
		}

		if (ui_state == UI_STATE_AT_MODE) {
			// В режиме AT_MODE выполняем функцию, даже при str == NULL
			ui_state = ui_at_mode_case(str);
		}

		usb_print_queue();

		osSignalSet(monitorTaskHandle, SIGNAL_TASK_UI);
	}
}


