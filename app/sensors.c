/*
 * sensor_read.c
 *
 *  Created on: 19 дек. 2018 г.
 *      Author: Denis Shreiber
 */

// C includes
#include <string.h>
// HAL includes
#include "rtos.h"
#include "main.h"
#include "adc.h"
#include "usart.h"
// User includes
#include "common.h"
#include "log/log.h"
#include "settings/settings.h"
#include "tm_stm32f4_onewire.h"
#include "tm_stm32f4_ds18b20.h"
#include "din/din.h"
#include "lls/lls.h"
#include "rs485/rs485.h"
#include <math.h>

// ===========================================================================

#define TASK_PERIOD					( 500 )

#define IBUTTON_SEARCH_PERIOD		( 500 )
#define TEMP_READ_PERIOD			( 15*1000 )
#define GNSS_READ_PERIOD			( 1*1000 )
#define IO_READ_PERIOD				( 1*1000 )
//#define FLS_READ_PERIOD				( 1*1000 )

#if (TASK_PERIOD > IBUTTON_SEARCH_PERIOD) || (TASK_PERIOD > TEMP_READ_PERIOD) || (TASK_PERIOD > GNSS_READ_PERIOD)
#error "Error"
#endif

#define IBUTTON_SEARCH_CNT			( IBUTTON_SEARCH_PERIOD / TASK_PERIOD )
#define TEMP_READ_CNT				( TEMP_READ_PERIOD / TASK_PERIOD )
#define GNSS_READ_CNT				( GNSS_READ_PERIOD / TASK_PERIOD )
#define IO_READ_CNT					( IO_READ_PERIOD / TASK_PERIOD )

#define TEMP_SENSORS_NUMBER			8
#define IBUTTON_NUMBER				3

#define MOTION_STOP_TIMEOUT			10	//In seconds

#define ADC_VALUES_BUFFER_SIZE		6

#define VREFINT_VDDA_CAL			(3300.0f)
#define VREFINT_CAL					(*((uint16_t*) ((uint32_t) 0x1FFF7A2A)))
#define TEMP30_CAL					(*((uint16_t*) ((uint32_t) 0x1FFF7A2C)))
#define TEMP110_CAL					(*((uint16_t*) ((uint32_t) 0x1FFF7A2E)))

#define POWER_PRESENT_THRESHOLD_VOLTAGE						6.0f

// ===========================================================================



// ===========================================================================



// ---------------------------------------------------------------------------



// ===========================================================================

static __IO uint16_t adc_values[ADC_VALUES_BUFFER_SIZE];
static TM_OneWire_t OWStruct;
// Счетчики сбора данных сразу проинициализированы так, чтобы с первого захода в задачу прочитать данные
static uint32_t ibutton_read_cnt = IBUTTON_SEARCH_CNT;
static uint32_t temp_read_cnt = TEMP_READ_CNT;
static uint32_t gnss_read_cnt = GNSS_READ_CNT;
static uint32_t io_read_cnt = GNSS_READ_CNT;
static bool motion_int = true;
static uint32_t motion_timeout = 0;

// ---------------------------------------------------------------------------

static void check_motion_flag();

// ===========================================================================

static void dout1_write(uint8_t value) {
	HAL_GPIO_WritePin(DOUT1_GPIO_Port, DOUT1_Pin, (settings1_ram.douts[0].type == NORMALLY_OPEN) ? value : !value);

	if (value == 0) {
		board_status.io.dout_mask &= ~0x01;
	}
	else {
		board_status.io.dout_mask |= 0x01;
	}
}

static void dout2_write(uint8_t value) {
	HAL_GPIO_WritePin(DOUT2_GPIO_Port, DOUT2_Pin, (settings1_ram.douts[1].type == NORMALLY_OPEN) ? value : !value);

	if (value == 0) {
		board_status.io.dout_mask &= ~0x02;
	}
	else {
		board_status.io.dout_mask |= 0x02;
	}
}

// ===========================================================================

void adcTimerCallback(void const * argument) {
	HAL_ADC_Start_DMA(&hadc1, (uint32_t *)adc_values, ADC_VALUES_BUFFER_SIZE);
}

void fuelTimerCallback(void const * argument) {
	osSemaphoreRelease(fuelSemHandle);
}

void checkMotionTimerCallback (void const * argument) {
	check_motion_flag();
}

void canlogTimerCallback(void const * argument) {
	osSemaphoreRelease(canlogSemHandle);
}

// ---------------------------------------------------------------------------

void update_convert_adc() {
	float vdda = VREFINT_VDDA_CAL * VREFINT_CAL / adc_values[5]; // опорное

	board_status.mcu.temp = (int)rint((vdda * adc_values[4] / VREFINT_VDDA_CAL - TEMP30_CAL) * (110 - 30) / (TEMP110_CAL - TEMP30_CAL) + 30);
	board_status.io.ain[0] = (vdda/1000 * adc_values[0] * 15.706f / 4095.0f);
	board_status.io.ain[1] = (vdda/1000 * adc_values[1] * 15.706f / 4095.0f);
	board_status.power.voltage = (vdda/1000 * adc_values[2] * 15.706f / 4095.0f) + 0.2;	// 0.2V - поправка на входной диод
	board_status.batt.voltage = (vdda/1000 * adc_values[3] * 1.5f / 4095.0f);


	dout1_write(board_status.io.ain[0] > settings1_ram.ains[0].threshold ? 1 : 0);
	dout2_write(board_status.io.ain[1] > settings1_ram.ains[1].threshold ? 1 : 0);


	static uint32_t time_ms = 0;

	if (board_status.power.voltage >= POWER_PRESENT_THRESHOLD_VOLTAGE) {
		board_status.power.present = true;
		board_status.power.present_time_ms += (HAL_GetTick() - time_ms);
	}
	else {
		board_status.power.present = false;
		board_status.power.present_time_ms = 0;
	}

	time_ms = HAL_GetTick();
}

void read_din_status(uint16_t GPIO_Pin) {
	// Проверяем маску, откуда пришло прерывание

	if ((GPIO_Pin & DIN1_NO_Pin) == DIN1_NO_Pin) {
		apply_din_state(DIN_1);
	}

	if ((GPIO_Pin & DIN2_NO_Pin) == DIN2_NO_Pin) {
		apply_din_state(DIN_2);
	}

	if ((GPIO_Pin & DIN3_NC_Pin) == DIN3_NC_Pin) {
		apply_din_state(DIN_3);
	}

	if ((GPIO_Pin & DIN4_NC_Pin) == DIN4_NC_Pin) {
		apply_din_state(DIN_4);
	}

	if ((GPIO_Pin & DIN_PHOTO_Pin) == DIN_PHOTO_Pin) {
		board_status.casing.open = HAL_GPIO_ReadPin(DIN_PHOTO_GPIO_Port, DIN_PHOTO_Pin) == GPIO_PIN_RESET;
	}

	if ((GPIO_Pin & CHARGE_STATUS_Pin) == CHARGE_STATUS_Pin) {
		board_status.batt.charging = HAL_GPIO_ReadPin(CHARGE_STATUS_GPIO_Port, CHARGE_STATUS_Pin) == GPIO_PIN_RESET;
	}

	if ((GPIO_Pin & ACCEL_INT1_Pin) == ACCEL_INT1_Pin) {
		if (HAL_GPIO_ReadPin(ACCEL_INT1_GPIO_Port, ACCEL_INT1_Pin) == GPIO_PIN_RESET) {
			// Falling edge (старт INT1)
			motion_int = true;
		}
		else {
			// Rising edge (стоп INT1)
			motion_int = false;
		}

		motion_timeout = MOTION_STOP_TIMEOUT;
		board_status.motion.active = true;
	}

	if ((GPIO_Pin & GSM_RING_Pin) == GSM_RING_Pin) {

	}
}

void record_io_update() {
	if (osMutexWait(recordMutexHandle, 1000) == osOK) {
		io_record_t *rec_io = &teledata_record_global.data.io;
		io_status_t *board_io = &board_status.io;

		rec_io->dout = (uint8_t)(board_io->dout_mask & 0xFF);

		rec_io->ain[0] = board_io->ain[0];
		rec_io->ain[1] = board_io->ain[1];

		// din5 - dout1
		// din6 - dout2
		// din7 - флаг кз на gps антенне
		// din8 - флаг motion (статус движения), для теста
		// din9 - флаг валидности координат (только в wialon)
		// din10 - Резерв
		// din11 - Флаг алерта. Обороты двигателя
		// din12 - Флаг алерта. Температура ОЖ
		// din13 - Флаг алерта. Давление/Уровень масла

		rec_io->din._16bit = (uint16_t)(board_io->din_mask & 0xFF);
		rec_io->din._16bit |= (uint16_t)(rec_io->dout & 0x03) << 4;
		if (hw_version[0] == 1) {
			// Для железа версии 2.1 в din пишем статус кз на gps антенне
			if (HAL_GPIO_ReadPin(GPS_SHORT_GPIO_Port, GPS_SHORT_Pin) == GPIO_PIN_RESET) {
				rec_io->din._16bit |= 0x0040;
			}
			else {
				rec_io->din._16bit &= ~0x0040;
			}
		}
		if (board_status.motion.active == true) {
			rec_io->din._16bit |= 0x0080;
		}
		else {
			rec_io->din._16bit &= ~0x0080;
		}

		rec_io->count[0] = board_io->din_counter[0];
		rec_io->count[1] = board_io->din_counter[1];
		rec_io->count[2] = board_io->din_counter[2];
		rec_io->count[3] = board_io->din_counter[3];

		osMutexRelease(recordMutexHandle);
	}
}

static void update_fuel_level() {
	static const int tries = 3;
	int try_more_cnt = tries;

	for (int q = 0; q < sizeof(settings1_ram.fls)/sizeof(settings1_ram.fls[0]); q++) {
		if (settings1_ram.fls[q].used) {
			lls_data_t lls_data;

			int ret = lls_read(settings1_ram.fls[q].address, &lls_data);
			if (ret == 0) {
				fuel_data[q].temp = lls_data.temp;
				fuel_data[q].level = (uint32_t)lls_data.fuel_level;
				fuel_data[q].ok = true;

				try_more_cnt = tries;

				continue;
			}

			if (--try_more_cnt > 0) {
				// Пробуем еще раз обратиться к этому же датчику
				osDelay(100);
				q--;
				continue;
			}
		}

		fuel_data[q].ok = false;

		try_more_cnt = tries;
	}

	if (board_status.power.present && board_status.power.present_time_ms > 5000) {
		memcpy(&lls_cache, fuel_data, sizeof(fuel_data_t) * 8);
		lls_cache_crc_update();
	}
}

static void check_motion_flag() {
	if (motion_int) {
		motion_timeout = MOTION_STOP_TIMEOUT;
	}
	else {
		if(motion_timeout) {
			if (--motion_timeout == 0) {
				board_status.motion.active = false;
			}
		}
	}
}

static void ow_search(ow_ibutton_t *ibutton, ow_temp_t *temp) {
	TM_OneWire_ResetSearch(&OWStruct);

	uint8_t devices;
	uint8_t temp_sensor_count = 0;
	uint8_t ibutton_count = 0;

	do {
		taskENTER_CRITICAL();
		devices = TM_OneWire_Search(&OWStruct, ONEWIRE_CMD_SEARCHROM);
		taskEXIT_CRITICAL();

		if (devices > 0) {
			if (TM_DS18B20_Is(OWStruct.ROM_NO)) {
				if (temp_sensor_count >= TEMP_SENSORS_NUMBER) {
					continue;
				}

				memcpy(temp->dev[temp_sensor_count++].code, OWStruct.ROM_NO, 8);
			}
			else if (OWStruct.ROM_NO[0] == 0x01) {
				if (ibutton_count >= IBUTTON_NUMBER) {
					continue;
				}

				memcpy(ibutton->dev[ibutton_count++].code, OWStruct.ROM_NO, 8);
			}
		}
	} while(devices);

	temp->count = temp_sensor_count;
	ibutton->count = ibutton_count;
}

static void ow_read_temp(ow_temp_t *temp) {
	if (temp->count) {
		taskENTER_CRITICAL();
		TM_DS18B20_StartAll(&OWStruct);
		taskEXIT_CRITICAL();

		uint8_t cnt = 80;	// Таймаут 800 мс
		bool done = false;
		do {
			osDelay(10);

			taskENTER_CRITICAL();
			done = TM_DS18B20_AllDone(&OWStruct);
			taskEXIT_CRITICAL();

		} while ( !done && (--cnt != 0) );

		if (cnt == 0) {
			ZF_LOGW("Conversion timeout");
		}

		/* Read temperature from each device 0,separatelly */
		for (uint8_t i = 0; i < TEMP_SENSORS_NUMBER; i++) {
			if (i < temp->count) {
				/* Read temperature from ROM address and store it to temps variable */
				taskENTER_CRITICAL();
				uint8_t res = TM_DS18B20_Read(&OWStruct, temp->dev[i].code, &temp->dev[i].val);
				taskEXIT_CRITICAL();

				if (res) {
//					user_printf("Temp %d: %d\r\n", i, (int)temp->dev[i].val);
				} else {
					temp->dev[i].val = OW_TEMP_UNKNOWN_VALUE;
					ZF_LOGE("Temp sensor %d read error", i);
				}
			}
			else {
				temp->dev[i].val = OW_TEMP_UNKNOWN_VALUE;
//				ZF_LOGW("Temp sensor %d unavailable", i);
			}
		}
	}
}

static void canlog_used_or_lls_used_check() {
	static bool canlog_used_pre = false;
	bool canlog_used_new = canlog_used();

	if (canlog_used_pre != canlog_used_new) {
		if (canlog_used_new) {
			canlog_init_serial();
		}
		else {
			lls_init_serial();
		}

		canlog_used_pre = canlog_used_new;
	}
}
// ===========================================================================

void sensor_read_task(void const * argument) {
	UNUSED(argument);

	TM_OneWire_Init(&OWStruct, GPIOB, GPIO_PIN_10);

	adcTimerCallback(NULL);
	osTimerStart(adcTimerHandle, 10);
	osTimerStart(fuelTimerHandle, settings1_ram.period.fls_read.in_standby_mode * 1000);
	osTimerStart(canlogTimerHandle, settings3_ram.can.send_period.in_standby_mode * 1000);
	osTimerStart(checkMotionTimerHandle, 1000);

	osEvent evt;
	do {
		evt = osSignalWait(SIGNAL_START, osWaitForever);
	} while( !(evt.status == osEventSignal && evt.value.signals & SIGNAL_START) );

	// Генерируем обновление данных топлива сразу после старта
	osSemaphoreRelease(fuelSemHandle);

	while(1) {
		osDelay(TASK_PERIOD);

		osSignalSet(monitorTaskHandle, SIGNAL_TASK_SENSORREAD);

		canlog_used_or_lls_used_check();

		// --------------------------------
		// Motion
		// --------------------------------

		motion_status_set(board_status.motion.active);

		// --------------------------------
		// io
		// --------------------------------

		if (++io_read_cnt >= IO_READ_CNT) {
			io_read_cnt = 0;

			record_io_update();
		}

		// --------------------------------
		// ibutton & driver tag
		// --------------------------------

		if (++ibutton_read_cnt >= IBUTTON_SEARCH_CNT) {
			ibutton_read_cnt = 0;

			ow_search(&ow_ibutton, &ow_temp);

			if (osMutexWait(recordMutexHandle, 1000) == osOK) {
				memcpy(&teledata_record_global.data.ow.ibutton, &ow_ibutton, sizeof(ow_ibutton_t));

				osMutexRelease(recordMutexHandle);
			}
		}

		// --------------------------------
		// 1-wire temperature
		// --------------------------------

		if (++temp_read_cnt >= TEMP_READ_CNT) {
			temp_read_cnt = 0;

			ow_read_temp(&ow_temp);

			if (osMutexWait(recordMutexHandle, 1000) == osOK) {
				for (int q = 0; q < TEMP_SENSORS_NUMBER; q++) {
					ow_temp_record_t * temp_rec = &teledata_record_global.data.ow.temp[q];

					temp_rec->used = settings1_ram.ow_temp[q].used;

					if (temp_rec->used) {
						bool found = false;

						for (int w = 0; w < ow_temp.count && q < TEMP_SENSORS_NUMBER; w++) {
							ow_temp_data_t * temp = &ow_temp.dev[w];

							if (memcmp(settings1_ram.ow_temp[q].id, temp->code, sizeof(temp->code)) == 0) {
								temp_rec->val = (int8_t)temp->val;
								found = true;
								break;
							}
						}

						if (!found) {
							temp_rec->val = OW_TEMP_UNKNOWN_VALUE;
						}
					}
				}

				osMutexRelease(recordMutexHandle);
			}
		}

		// --------------------------------
		// GNSS data
		// --------------------------------

		if (++gnss_read_cnt >= GNSS_READ_CNT) {
			gnss_read_cnt = 0;
		}

		// --------------------------------
		// fuel sensor data
		// --------------------------------

		if (osSemaphoreWait(fuelSemHandle, 0) == osOK) {

			// Опрашиваем ДУТ только если не настроен Canlog
			if (!canlog_used()) {
				update_fuel_level();

				if (osMutexWait(recordMutexHandle, 1000) == osOK) {
					bool save_to_flash = false;

					for (uint8_t q = 0; q < 8; q++) {
						if (lls_cache.unit[q].ok) {
							fuel_record_global.data[q].num = q;
							fuel_record_global.data[q].level = lls_cache.unit[q].level;
							fuel_record_global.data[q].temp = lls_cache.unit[q].temp;

							save_to_flash = true;
						}
						else {
							// Данных с датчика нет
							fuel_record_global.data[q].num = 0xFF;
						}
					}

					osMutexRelease(recordMutexHandle);

					// Пишем топливо на флеш, если данные есть хотя бы с одного датчика
					if (save_to_flash) {
						save_fuel_data_to_flash();
					}
				}
			}
		}

		// --------------------------------
		// canlog data
		// --------------------------------

		if (osSemaphoreWait(canlogSemHandle, 0) == osOK) {

			// Берем данные с Canlog, только если он настроен на это
			if (canlog_used()) {
				if (osMutexWait(recordMutexHandle, 1000) == osOK) {

					canlog_data_get(&canlog_record_global.data.param, &canlog_record_global.data.active_param);

					canlog_record_global.data.used_param.all = settings3_ram.can.used_param.all;

					osMutexRelease(recordMutexHandle);

					save_canlog_data_to_flash();
				}
			}
		}
	}
}

