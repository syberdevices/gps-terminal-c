/*
 * monitor.c
 *
 *  Created on: 25 мар. 2019 г.
 *      Author: Denis Shreiber
 */

#include "common.h"
#include "rtos.h"

// ===================================================================================

// Т.к. во всех задачах есть лог и, следовательно, стирание флеш, то таймаут нельзя ставить меньше времени стирания флеш (~2 мин для Winbond)

#define UI_TASK_TIMEOUT				2*60*1000
#define GSM_TASK_TIMEOUT			2*60*1000
#define CLOUD_TASK_TIMEOUT			2*60*1000
#define APP_TASK_TIMEOUT			2*60*1000
#define SAVEDATA_TASK_TIMEOUT		2*60*1000
#define SENSORREAD_TIMEOUT			2*60*1000

// ===================================================================================

static uint32_t ui_task_ticks = 0;
static uint32_t gsm_task_ticks = 0;
static uint32_t cloud_task_ticks = 0;
static uint32_t app_task_ticks = 0;
static uint32_t savedata_task_ticks = 0;
static uint32_t sensorread_task_ticks = 0;

static osEvent evt;
static uint32_t curr_ticks;

// ===================================================================================

void monitor_task(void const * argument)
{
	for(;;)
	{
		curr_ticks = HAL_GetTick();

		evt = osSignalWait(SIGNAL_TASK_UI | SIGNAL_TASK_GSM | SIGNAL_TASK_CLOUD | SIGNAL_TASK_APP | SIGNAL_TASK_SAVEDATA | SIGNAL_TASK_SENSORREAD, 0);
		if (evt.status == osEventSignal) {
			if (evt.value.signals & SIGNAL_TASK_UI) {
				ui_task_ticks = curr_ticks;
			}
			if (evt.value.signals & SIGNAL_TASK_GSM) {
				gsm_task_ticks = curr_ticks;
			}
			if (evt.value.signals & SIGNAL_TASK_CLOUD) {
				cloud_task_ticks = curr_ticks;
			}
			if (evt.value.signals & SIGNAL_TASK_APP) {
				app_task_ticks = curr_ticks;
			}
			if (evt.value.signals & SIGNAL_TASK_SAVEDATA) {
				savedata_task_ticks = curr_ticks;
			}
			if (evt.value.signals & SIGNAL_TASK_SENSORREAD) {
				sensorread_task_ticks = curr_ticks;
			}
		}

		// На самом деле у нескольких задач может быть таймаут одновременно, но пока будем писать в лог только какую-то одну
		char * fault_task = NULL;
		if (curr_ticks - ui_task_ticks > UI_TASK_TIMEOUT) {
			fault_task = "UI";
		}
		else if (curr_ticks - gsm_task_ticks > GSM_TASK_TIMEOUT) {
			fault_task = "GSM";
		}
		else if (curr_ticks - cloud_task_ticks > CLOUD_TASK_TIMEOUT) {
			fault_task = "CLOUD";
		}
		else if (curr_ticks - app_task_ticks > APP_TASK_TIMEOUT) {
			fault_task = "APP";
		}
		else if (curr_ticks - savedata_task_ticks > SAVEDATA_TASK_TIMEOUT) {
			fault_task = "SAVEDATA";
		}
		else if (curr_ticks - sensorread_task_ticks > SENSORREAD_TIMEOUT) {
			fault_task = "SENSORREAD";
		}

		if (fault_task) {
			if (osRecursiveMutexWait(fifoRecursiveMutexHandle, 0) == osOK && osRecursiveMutexWait(spiflashRecursiveMutexHandle, 0) == osOK) {
				// Пишем лог, только если fifo и spiflash не заняты другой задачей, иначе заблочимся тут, т.к текущая задача высокоприоритетная
				ZF_LOGF("%s task timeout. Reboot", fault_task);

				odometer_flash_update_direct(cache.odometer.val);
			}

			NVIC_SystemReset();
		}

		WDG_RESET(TASK_ALL);

		osDelay(1000);
	}
}
