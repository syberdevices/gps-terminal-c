/*
 * common.c
 *
 *  Created on: 7 дек. 2018 г.
 *      Author: Denis Shreiber
 */

// C includes
#include <string.h>
// HAL includes
#include "rtos.h"
// User includes
#include "common.h"
#include "crc32/crc32.h"
#include "settings/settings.h"
#include "spi_flash/spi_flash.h"
#include "dout/dout.h"

// ===========================================================================

extern float last_point_course;
extern float current_course;
extern uint32_t save_track_data_timer_sec_cnt;

// ===========================================================================

cloud_cache_t cloud_cache __section(".cache");
speed_cache_t gnss_cache __section(".cache");
lls_cache_t lls_cache __section(".cache");
cache_t cache __section(".cache");
board_status_t board_status = {0};
ow_ibutton_t ow_ibutton;
ow_temp_t ow_temp;
fuel_data_t fuel_data[8] = {0};
teledata_record_t teledata_record_global = {0};
fuel_record_t fuel_record_global = {0};
canlog_record_t canlog_record_global = {0};
volatile uint32_t gsm_task_send_timeout = 0;
volatile static time_t unix_time = DEFAULT_TIME;

// ===========================================================================

static volatile bool _check_settings_locked = false;

// ===========================================================================

/**
 * Вызывается при записи точки для того, чтобы сбросить счетчики других объектов.
 * Например, если записали точку по курсу, то одометр начинаем считать сначала и таймер начинаем сначала. И т.д.
 */
static void synchronize_odo_cource_and_timer_points() {
	taskENTER_CRITICAL();
	last_point_course = current_course;
	save_track_data_timer_sec_cnt = 0;
	odometer_last_point_updadte();
	taskEXIT_CRITICAL();
}

// ===========================================================================

void detect_hw_version() {
	/*
	 * pull | ext up 10K | ext down 0R | floating
	 * ------------------------------------------
	 * no   |     1      |      0      |    ?
	 * up   |     1      |      0      |    1
	 * down |     1      |      0      |    0
	 */

	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = HW_VERSION_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;

	// Подтяжка отключена при инициализации
	GPIO_PinState hw_pin_nopull = HAL_GPIO_ReadPin(HW_VERSION_GPIO_Port, HW_VERSION_Pin);

	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(HW_VERSION_GPIO_Port, &GPIO_InitStruct);
	GPIO_PinState hw_pin_pullup = HAL_GPIO_ReadPin(HW_VERSION_GPIO_Port, HW_VERSION_Pin);

	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(HW_VERSION_GPIO_Port, &GPIO_InitStruct);
	GPIO_PinState hw_pin_pulldown = HAL_GPIO_ReadPin(HW_VERSION_GPIO_Port, HW_VERSION_Pin);

	if ( (hw_pin_nopull == GPIO_PIN_SET) && (hw_pin_pullup == GPIO_PIN_SET) && (hw_pin_pulldown == GPIO_PIN_SET) ) {
		hw_version[0] = 0;
	}
	else if ( (hw_pin_nopull == GPIO_PIN_RESET) && (hw_pin_pullup == GPIO_PIN_RESET) && (hw_pin_pulldown == GPIO_PIN_RESET) ) {
		hw_version[0] = 1;
		dout_Objects[LED_GPS_GREEN].inversion = true;
		dout_Objects[LED_GPS_RED].inversion = true;
		dout_Objects[LED_GSM_GREEN].inversion = true;
		dout_Objects[LED_GSM_RED].inversion = true;
		HAL_GPIO_WritePin(LED_GSM_GREEN_GPIO_Port, LED_GSM_GREEN_Pin, 1);
		HAL_GPIO_WritePin(LED_GSM_RED_GPIO_Port, LED_GSM_RED_Pin, 1);
		HAL_GPIO_WritePin(LED_GPS_GREEN_GPIO_Port, LED_GPS_GREEN_Pin, 1);
		HAL_GPIO_WritePin(LED_GPS_RED_GPIO_Port, LED_GPS_RED_Pin, 1);
	}
	else {
		hw_version[0] = 2;
	}
}

// ---------------------------------------------------------------------------

uint32_t cloud_cache_crc_get() {
	return crc32_get(&cloud_cache, sizeof(cloud_cache) - sizeof(cloud_cache.crc), 0);
}

void cloud_cache_crc_update() {
	taskENTER_CRITICAL();
	cloud_cache.crc = cloud_cache_crc_get();
	taskEXIT_CRITICAL();
}

uint32_t gnss_cache_crc_get() {
	return crc32_get(&gnss_cache, sizeof(gnss_cache) - sizeof(gnss_cache.crc), 0);
}

void gnss_cache_crc_update() {
	gnss_cache.crc = gnss_cache_crc_get();
}

uint32_t cache_crc_get() {
	return crc32_get(&cache, sizeof(cache) - sizeof(cache.crc), 0);
}

void cache_crc_update() {
	DISABLE_IRQ_USER();
	cache.crc = cache_crc_get();
	ENABLE_IRQ_USER();
}

uint32_t lls_cache_crc_get() {
	return crc32_get(&lls_cache, sizeof(lls_cache) - sizeof(lls_cache.crc), 0);
}

void lls_cache_crc_update() {
	lls_cache.crc = lls_cache_crc_get();
}

// ---------------------------------------------------------------------------

void unix_time_set(time_t _time) {
	DISABLE_IRQ_USER();
	unix_time = _time;
	ENABLE_IRQ_USER();
}

time_t unix_time_get() {
	time_t _time;
	DISABLE_IRQ_USER();
	_time = unix_time;
	ENABLE_IRQ_USER();
	return _time;
}

// ---------------------------------------------------------------------------

bool canlog_used() {
	return (settings3_ram.can.used && (settings3_ram.can.used_param.all != 0));
}

// ---------------------------------------------------------------------------

int erase_user_external_flash() {
	return SPI_FLASH_EraseBlock64(EXT_FLASH_USER_REGION_ADDR);
}

int erase_user_internal_flash() {
    uint32_t error = 0;
    FLASH_EraseInitTypeDef flash = {
        .TypeErase = FLASH_TYPEERASE_SECTORS,
        .Banks = FLASH_BANK_1,
        .Sector = FLASH_SECTOR_6,
        .NbSectors = 2,
        .VoltageRange = FLASH_VOLTAGE_RANGE_3
    };

    HAL_FLASH_Unlock();
    HAL_StatusTypeDef res = HAL_FLASHEx_Erase(&flash, &error);
    HAL_FLASH_Lock();

    if (res != HAL_OK || error != 0xFFFFFFFFU) {
        return -1;
    }

    return 0;
}

int user_internal_flash_program(uint32_t addr, const void * data, uint32_t len) {
	if (addr < USER_INT_FLASH_ADDR || addr + len > INT_FLASH_ADDR + INT_FLASH_SIZE) {
		return -1;
	}

    HAL_StatusTypeDef res = HAL_ERROR;

    HAL_FLASH_Unlock();

    while (len--) {
        res = HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, addr++, *(uint8_t*)data++);
        if (res != HAL_OK) {
            break;
        }
    }

    HAL_FLASH_Lock();

    if (res == HAL_OK) {
        return 0;
    }
    else {
        return -1;
    }
}

// ---------------------------------------------------------------------------

/**
 * Активировать запись данных на флэш
 */
void save_track_point_to_flash(source_record_t src) {
	synchronize_odo_cource_and_timer_points();

	if (osMessagePut(saveDataSourceQueueHandle, src, 100) != osOK) {
		ZF_LOGE("Can't put data to queue");
	}
}

void save_fuel_data_to_flash() {
	if (osMessagePut(saveDataSourceQueueHandle, SRC_REC_TYPE_FUEL, 100) != osOK) {
		ZF_LOGE("Can't put data to queue");
	}
}

void save_canlog_data_to_flash() {
	if (osMessagePut(saveDataSourceQueueHandle, SRC_REC_TYPE_CANLOG, 100) != osOK) {
		ZF_LOGE("Can't put data to queue");
	}
}

ignition_t ignition_get() {
	return cache.ignition;
}

void ignition_set(ignition_t ign_status) {
	board_status.ignition = ign_status;

	if (cache.ignition != ign_status) {
		cache.ignition = ign_status;
		cache_crc_update();
	}
}

bool motion_status_get() {
	return cache.motion_status;
}

void motion_status_set(bool motion_status) {
	if (cache.motion_status != motion_status) {
		cache.motion_status = motion_status;
		cache_crc_update();
	}
}

void device_mode_set(device_mode_t mode) {
	if (mode == DEVICE_MODE_ACTIVE) {
		osTimerStart(cloudSendTimerHandle,      settings1_ram.period.data_send. in_active_mode * 1000);
		osTimerStart(fuelTimerHandle,           settings1_ram.period.fls_read.  in_active_mode * 1000);
		osTimerStart(canlogTimerHandle,         settings3_ram.can.send_period.  in_active_mode * 1000);
	}
	else {
		osTimerStart(cloudSendTimerHandle,      settings1_ram.period.data_send. in_standby_mode * 1000);
		osTimerStart(fuelTimerHandle,           settings1_ram.period.fls_read.  in_standby_mode * 1000);
		osTimerStart(canlogTimerHandle,         settings3_ram.can.send_period.  in_standby_mode * 1000);
	}

	// При смене режима сбрасываем счетчики таймаута обмена с сервером,
	// т.к. новый период может немедленно привести к перезагрузке по таймауту
	gsm_task_send_timeout = 0;

	save_track_data_timer_sec_cnt = 0;

	if (cache.device_mode != mode) {
		cache.device_mode = mode;
		cache_crc_update();
	}

	ZF_LOGW("New device mode: %s", mode == DEVICE_MODE_ACTIVE ? "ACTIVE" : "STANDBY");
}

device_mode_t device_mode_get() {
	return cache.device_mode;
}

void system_reset(char *msg) {
	taskENTER_CRITICAL();

	odometer_flash_update_direct(cache.odometer.val);

	if (msg) {
		ZF_LOGF(msg);
	}

	NVIC_SystemReset();
}

int user_sscanf(const char * str, const char * format, ... ) {
	int ret;

	DISABLE_IRQ_USER();
	{
		va_list args;
		va_start (args, format);
		ret = vsscanf (str, format, args);
		va_end (args);
	}
	ENABLE_IRQ_USER();

	return ret;
}

const char * white_number_get_sett_ram_by_index(int index) {
	if (index < WHITE_PHONE_NUM_1) {
		return (const char *)settings1_ram.white_phones_1[index].number;
	}
	else {
		return (const char *)settings4_ram.white_phones_2[index - WHITE_PHONE_NUM_1].number;
	}
}

void white_number_set_sett_ram_by_index(const char *number, int index) {
	if (index < WHITE_PHONE_NUM_1) {
		strcpy(settings1_ram.white_phones_1[index].number, number);
	}
	else {
		strcpy(settings4_ram.white_phones_2[index - WHITE_PHONE_NUM_1].number, number);
	}
}

void check_settings_lock() {
	_check_settings_locked = true;
}

void check_settings_unlock() {
	_check_settings_locked = false;
}

bool check_settings_locked() {
	return _check_settings_locked;
}

void check_settings_wait_unlock() {
	while(_check_settings_locked) {
		osDelay(2);
	}
}
