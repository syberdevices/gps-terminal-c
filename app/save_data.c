/*
 * save_data.c
 *
 *  Created on: 19 дек. 2018 г.
 *      Author: Denis Shreiber
 */

// C includes
#include <string.h>
#include <math.h>
// HAL includes
#include "rtos.h"
#include "main.h"
// User includes
#include "common.h"
#include "log/flash_fifo/flash_fifo.h"
#include "log/log.h"
#include "settings/settings.h"

// ============================================================================



// ============================================================================

static uint8_t rec_buff[1024];

// ============================================================================



// ============================================================================

static int build_record(source_record_t src) {
	int ret_len = 0;

	if (osMutexWait(recordMutexHandle, 1000) == osOK) {
		head_record_t *head = (head_record_t *)rec_buff;

		head->time = time(NULL);
		head->len = sizeof(head_record_t);

		if (src == SRC_REC_TYPE_FUEL) {
			head->type = RECORD_TYPE_FUEL;

			fuel_subrecord_t *subrec = (fuel_subrecord_t *)(rec_buff + sizeof(head_record_t));
			uint8_t written = 0;

			for (int q = 0; q < 8; q++) {
				if (fuel_record_global.data[q].num < 8) {
					subrec[written].num = fuel_record_global.data[q].num;
					subrec[written].level = fuel_record_global.data[q].level;
					subrec[written].temp = fuel_record_global.data[q].temp;

					written++;
					head->len += sizeof(fuel_subrecord_t);
				}
			}

			ret_len = written ? head->len : 0;

			user_printf(">>> Put record (type %d, time %d)\r\n", (int)head->type, head->time);
		}
		else if (src == SRC_REC_TYPE_CANLOG) {
			head->type = RECORD_TYPE_CANLOG;

			canlog_subrecord_t *subrec = (canlog_subrecord_t *)(rec_buff + sizeof(head_record_t));
			memcpy(subrec, &canlog_record_global.data, sizeof(canlog_subrecord_t));

			head->len += sizeof(canlog_subrecord_t);
			ret_len = head->len;

			user_printf(">>> Put record (type %d, time %d)\r\n", (int)head->type, head->time);
		}
		else {
			head->type = RECORD_TYPE_TELEDATA;

			teledata_subrecord_t *subrec = (teledata_subrecord_t *)(rec_buff + sizeof(head_record_t));
			memcpy(subrec, &teledata_record_global.data, sizeof(teledata_subrecord_t));

			subrec->source = src;
			subrec->mode = device_mode_get() == DEVICE_MODE_ACTIVE ? MODE_ACTIVE : MODE_PASSIVE;
			subrec->power.main_voltage = board_status.power.voltage;
			subrec->power.batt_voltage = board_status.batt.voltage;
			subrec->power.batt_used = subrec->power.main_voltage < subrec->power.batt_voltage;
			if (board_status.alert.mask) {
				// din11 - Флаг алерта. Обороты двигателя
				// din12 - Флаг алерта. Температура ОЖ
				// din13 - Флаг алерта. Давление/Уровень масла

				if (board_status.alert.high_rpm) {
					subrec->io.din._16bit |= 0x0400;	// DIN11
				}

				if (board_status.alert.high_engine_temp) {
					subrec->io.din._16bit |= 0x0800;	// DIN12
				}

				if (board_status.alert.check_oil) {
					subrec->io.din._16bit |= 0x1000;	// DIN13
				}
			}

			head->len += sizeof(teledata_subrecord_t);

			ret_len = head->len;

			user_printf(">>> Put record (type %d, source %d, time %d)\r\n", (int)head->type, (int)subrec->source, head->time);
		}

		osMutexRelease(recordMutexHandle);
	}

	return ret_len;
}

// ============================================================================

void save_data_task(void const * argument) {
	UNUSED(argument);

	osEvent evt;
	do {
		evt = osSignalWait(SIGNAL_START, osWaitForever);
	} while( !(evt.status == osEventSignal && evt.value.signals & SIGNAL_START) );

	while (1) {
		while (1) {
			evt = osMessageGet(saveDataSourceQueueHandle, 1000);
			if (evt.status == osEventMessage) {
				break;
			}

			osSignalSet(monitorTaskHandle, SIGNAL_TASK_SAVEDATA);
		}

		int rec_size = build_record((source_record_t)evt.value.v);
		if (rec_size > 0) {
			if (flash_fifo_put(&track_fifo, &rec_buff, rec_size) != 0) {
				system_reset("Can't put track data to flash");
			}
		}
		else {
			ZF_LOGW("No data to write to flash");
		}

		osSignalSet(monitorTaskHandle, SIGNAL_TASK_SAVEDATA);
	}
}

